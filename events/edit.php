<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');
connect();
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$event_id = $_REQUEST['evid'];


$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {

    $event_name = $_POST['event_name'];
    $event_description = $_POST['event_desc'];
    $event_edate = $_POST['date_created'] . " " . $_POST['time_created'];
    $query = "UPDATE events SET event_name = '" . mysql_real_escape_string($event_name) . "',  event_modifiedby = $userid,  event_modifiedon = now(), event_description = '" . mysql_real_escape_string($event_description) . "', event_edate = '" . mysql_real_escape_string($event_edate) . "' WHERE event_id = $event_id";
    $result = mysql_query($query, connect());

//    log history

    require_once("../models/tblog.php");
    $tblog = new TB_LOG();

    $event_name_orig = $_POST['event_name_orig'];
    $event_description_orig = $_POST['event_desc_orig'];
    $event_edate_orig = $_POST['date_created_orig'];

    if ($event_name_orig != $event_name) {
        $changes .= 'event name to "' . $event_name . '"';
    }
    if ($event_description_orig != $event_description) {
        $changes .= 'event desc to "' . $event_description . '"';
    }
    if ($event_edate_orig != $_POST['date_created']) {
        $changes .= 'event end date to "' . $_POST['date_created'] . '"';
    }

    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' updated event ' . $event_id . ' and changed[' . $changes . ']',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);


//    log history


    header('Location: /88dbphcrm/events/');
    exit;
}

$query = "SELECT 
			ev.*, 
			concat(u.user_firstname,' ',u.user_lastname) as createdby,
			concat(u2.user_firstname,' ',u2.user_lastname) as modifiedby
			from events ev
			LEFT JOIN users u ON ev.event_createdby = u.user_id
			LEFT JOIN users u2 ON ev.event_modifiedby = u2.user_id
			WHERE event_id = $event_id";
$result = mysql_query($query, connect());
$num = mysql_numrows($result);
if ($num > 0) {
    $event_name = mysql_result($result, 0, 'event_name');
    $event_edate = substr(mysql_result($result, 0, 'event_edate'), 0, 10);
    $event_description = mysql_result($result, 0, 'event_description');
    $createdon = mysql_result($result, 0, 'event_createdon');
    $createdby = mysql_result($result, 0, 'createdby');
    $modifiedby = mysql_result($result, 0, 'modifiedby');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Edit Events</title>

        <?php include '../header.php'; ?>

    <div class="main-section">

        <form name="form1" id="form1" action="edit.php?evid=<?php echo $_GET['evid']; ?>" method="post" >

            <div class="commands">

                <div class="head-label">
                    <h2>Edit Events</h2>
                </div><!-- end of add new account -->

                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/events/'>Cancel</a></li>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">


                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Event Name:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="event_name"  id="event_name" class="required" value="<?php echo $event_name; ?>"/>
                            <input type="hidden" name="event_name_orig"  id="event_name_orig"  value="<?php echo $event_name; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Event End Date:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="date_created" maxlength="10" id="date_created" class="required" value="<?php echo $event_edate; ?>"/>
                            <input type="hidden" name="date_created_orig" maxlength="10" id="date_created_orig" value="<?php echo $event_edate; ?>"/>
                            <input type="hidden" name="time_created" id="time_created" />
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Description:</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea cols="25" rows="3" name="event_desc" id="event_desc" class="required" >
                                <?php echo $event_description; ?>
                            </textarea>
                            <input type="hidden" name="event_desc_orig"  id="event_desc_orig"  value="<?php echo $event_description; ?>"/>
                        </td>
                    </tr>


                    <tr class="edit-account-footer">
                        <td colspan="4">

                        </td>
                    </tr>


                    <tr class="edit-account-footer">
                        <td colspan="4">
                            <?php echo "Created by: " . $createdby . " On: " . $createdon . " | Modified by: " . $modifiedby; ?>
                        </td>
                    </tr>
                </table>



            </div><!-- gen-section --> 
        </form>

    </div><!-- main-section --> 




</div>

<script type="text/javascript">
	
    $(document).ready(function() {
	
        $("#form1").validate();
			
    });
</script>
<script type="text/javascript">
    function GetCurrentTime()
    {
                                            		
        var currentTime = new Date()
        var hours = currentTime.getHours()
        var minutes = currentTime.getMinutes()
        var seconds = currentTime.getSeconds()
        if (minutes < 10){
            minutes = "0" + minutes
        }
        if (seconds < 10){
            seconds = "0" + seconds
        }
                                            			
        $("#time_created").val(hours + ":" + minutes + ":" + seconds);
                                            		
    }
    $(function() {
                                            		
        $('#date_created').datepicker({dateFormat: 'yy-mm-dd'});
        setInterval('GetCurrentTime()', 100 );

    });

</script>

</body>
</html>