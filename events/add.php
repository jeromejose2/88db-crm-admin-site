<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');


session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {


    $event_name = $_POST['event_name'];
    $event_description = $_POST['event_desc'];
    $event_edate = $_POST['date_created'] . " " . $_POST['time_created'];

    mysql_connect('localhost', 'root', '');
    mysql_select_db("88dbphcrm");


    $query = "INSERT INTO events (event_name, event_description,event_edate,  event_createdby,  event_createdon) 
			VALUES ('" . mysql_real_escape_string($event_name) . "', '" . mysql_real_escape_string($event_description) . "', '" . mysql_real_escape_string($event_edate) . "', '$userid', now())";
    $result = mysql_query($query, connect());

//    history log

    require_once("../models/tblog.php");
    $tblog = new TB_LOG();
    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' create new event[name:' . $event_name . ']',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);

//    history log

    header('Location: /88dbphcrm/events/');
    exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Add Event</title>

        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="form1" id="form1" action="add.php" method="post">
            <div class="commands">
                <div class="head-label">
                    <h2>Add New Event</h2>
                </div><!-- end of add new account -->

                <ul>


                    <li><a class="link-button gray" href='/88dbphcrm/events/'>Cancel</a></li>

                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Event Name:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="event_name"  id="event_name" class="required"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Event End Date:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="date_created" maxlength="10" id="date_created" class="required" value=""/>
                            <input type="hidden" name="time_created" id="time_created" />
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Description:</td>
                        <td>&nbsp;</td>
                        <td><textarea cols="25" rows="3" name="event_desc" id="event_desc" class="required" ></textarea>
                        </td>
                    </tr>



                    <tr class="edit-account-footer">
                        <td colspan="4">

                        </td>
                    </tr>
                </table>


            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main-section -->

    <script type="text/javascript">
        $(document).ready(function(){

            $("#payment_amount").numeric();
		
            $("#contract_number").numeric();

            $("#form1").validate();
	
		
        });	
    </script>
    <script type="text/javascript">
        function GetCurrentTime()
        {
                                            		
            var currentTime = new Date()
            var hours = currentTime.getHours()
            var minutes = currentTime.getMinutes()
            var seconds = currentTime.getSeconds()
            if (minutes < 10){
                minutes = "0" + minutes
            }
            if (seconds < 10){
                seconds = "0" + seconds
            }
                                            			
            $("#time_created").val(hours + ":" + minutes + ":" + seconds);
                                            		
        }
        $(function() {
                                            		
            $('#date_created').datepicker({dateFormat: 'yy-mm-dd'});
            setInterval('GetCurrentTime()', 100 );
                                            			
        });
                                            	
    </script>
</body>
</html>