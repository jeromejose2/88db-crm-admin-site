<?php

session_start();

require_once("../models/tbaging.php");
$tbaging = new TB_AGING();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$eventid = $_GET['event'];
if ($eventid) {
    $leads = $tbaging->Selectaccountsleads($eventid);
    $event = $tbaging->Selectevent($eventid);
}
?>		