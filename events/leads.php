<?php
session_start();
require_once ("../ctrl/ctrl_leads.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Event [Leads]</title>






        <?php include '../header.php'; ?>

        <script type="text/javascript">
            $(function() {
                $("#createexcel").click(function()
                {
                    //                    $event_id = <?php echo $event['event_id']; ?>;
                    //                    alert($event_id);
                    window.open ("create_excel.php?event_id="+ $("#event_id").val() +"&event_edate="+$("#event_edate").val()+"&event_name="+$("#event_name").val() );
                    	 
                });
            });
        </script>

    <div class="main-section">

        <div class="commands">
            <div class="head-label">
                <h2>Event [ <?php echo $event['event_name'] ?> ]</h2> 

            </div><!-- end of add new account -->
            <ul>
                <li><a class="link-button gray" href='/88dbphcrm/events/'>Cancel</a></li>
                <li><input type="submit" value="Create Excel" id="createexcel"></li>
            </ul>

        </div><!-- end of grid-commands -->


        <div class="gen-section">

            <table cellpadding="5" cellspacing="0">
                <tr>
                    <td class="grid-head">
                        Event info : <?php echo $event['event_description'] ?>    
                        <form name="createexcel" id="createexcel"  method="post">
                            <input type="hidden" value="<?php echo $event['event_edate'] ?>" id="event_edate">
                            <input type="hidden" value="<?php echo $event['event_name'] ?>" id="event_name">
                            <input type="hidden" value="<?php echo $event['event_description'] ?>" id="event_description">
                            <input type="hidden" value="<?php echo $event['event_id'] ?>" id="event_id">
                        </form>
                    </td>
                </tr>
                <tr>
                    <td class="grid-head">No. of leads : 
                        <?php
                        $c = 0;
                        if ($leads) {

                            foreach ($leads as $val) {
                                $c++;
                            }
                        }
                        echo $c;
                        ?>    </td>
                </tr>
                <!--                <tr>
        <td class="grid-head">Date : <?php
                        $today = date('Y-m-j');
                        echo $today;
                        ?>    </td>
                </tr>-->

                <tr>
                    <td class="grid-head">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>


            </table>
            <table class="main-grid">
                <tr class="grid-head">
                    <td>ID</td>
                    <td>Account</td>
                    <td>AE</td>
                    <td>Status</td> 


                </tr>
                <?php
                if ($leads)
                    foreach ($leads as $val):
                        ?>
                        <tr class="grid-content">
                            <td><?php echo $val['account_id'] ?></td>
                            <?php
                            $date1 = strtotime(substr($event['event_edate'], 0, 10));
                            $date2 = strtotime($today);
                            $seconds_diff = $date2 - $date1;
                            $daysdaw = floor($seconds_diff / 3600 / 24);
                            ?>
                            <td> 
                                <a href="/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&account_id=<?php echo $val['account_id'] ?>" target="_blank"
                                <?php if ($date1)
                                    if ($daysdaw >= 30): ?>
                                           style="color: red;"
                                   <?php  endif; ?>
                                   >
                                       <?php echo $val['account_name'] ?>
                                </a>
                                (<?php
                               $date1 = strtotime(substr($event['event_edate'], 0, 10));
                               $date2 = strtotime($today);
                               $seconds_diff = $date2 - $date1;
                               if ($date1) {
                                   echo floor($seconds_diff / 3600 / 24) . " day/s";
                               } else {
                                   echo'No Event end date';
                               }
                                       ?>)
                            </td>
                            <td> <?php echo $val['AE'] ?></td>
                            <td> <?php echo $val['status_name'] ?></td>


                        </tr>
                    <?php endforeach; ?>
            </table>

        </div><!-- end of gen-section -->

    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>