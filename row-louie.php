<?php
session_start();

connect();

$date_condition = "DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday last week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "')";

//echo('test ni lawrence');
?>
<?php if ($department_id == 2 || $department_id == 4) { ?>
    <div class="row">
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE DATE(a.account_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "')" . $condition;
//        echo $query;
        $result = mysql_query($query);
        ?>

        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Accounts Today</span>
            <span class="units" id="w1"><?php
    if ($result) {
        $row = mysql_fetch_array($result, MYSQL_NUM);
        echo $row[0];
        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
    } else {
        echo "0";
        echo "<input type='hidden' id='h1' value='0' />";
    }
        ?></span>

        </div><!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " WHERE r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition;

        $result = mysql_query($query);
        ?>
        <div id="total_accounts" class="dashboard-chart-2 col-5">
            <div id="main_total_accounts">
                <span  class="chart-label">Total Accounts to Date</span>
                <span class="units" id="w2">
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        $total_accounts_to_date = number_format($row[0]);
                        echo $total_accounts_to_date;
                        echo "<input type='hidden' id='h2' value='" . $total_accounts_to_date . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h2' value='0' />";
                    }
                    ?>
                </span>
            </div>
            <?php
            $condition = "";


            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            $condition_au = "AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " WHERE r.user_id = " . $userid . " ";
                            $condition_au = "AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            $condition_au = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    $condition_au = "AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "SELECT (
						SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition . ") - (SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition_au . " GROUP BY a.account_id
						) x) - (SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 )" . $condition_au . ")";

            $result = mysql_query($query);
            ?>	
            <div id="top_leads">
                <span  class="chart-label">Leads: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h2' value='0' />";
                    }
                    ?>
                </span>

            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "
						SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x
					 ";

            $result = mysql_query($query);
            ?>	

            <div id="bottom_signed">
                <span  class="chart-label">Signed:
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h2' value='0' />";
                    }
                    ?></span>

            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "
						 SELECT count(a.account_id) as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 ) " . $condition . "

					 ";

            $result = mysql_query($query);
            ?>	

            <div id="bottom_signed">
                <span  class="chart-label">Declined: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);
            echo number_format($row[0]);
            echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h2' value='0' />";
        }
            ?></span>

            </div>


        </div><!-- end of col-2 -->
        <?php
        $condition = "";

        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') " . $condition . " GROUP BY au.account_id
						) x";
//        echo $query;
        $result = mysql_query($query);
        ?>
        <div  class="signed_column dashboard-chart-2 col-5">
            <div class="main_signed_accounts">
                <span class="chart-label">Signed Shops This Week</span>
                <span class="units"  id="w5">
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h5' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
            $result = mysql_query($query);
            ?>


            <div class="top_signed_accounts">
                <span class="chart-label">Signed Only:
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h5' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
            $result = mysql_query($query);
            ?>     
            <div  class="middle_signed_accounts">
                <span class="chart-label">Paid-Partial: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);
            echo number_format($row[0]);
            echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h5' value='0' />";
        }
            ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "  ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";
//            echo $query;
            $result = mysql_query($query);
            ?>     
            <div  class="bottom_signed_accounts">
                <span class="chart-label">Paid-Full: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h5' value='0' />";
                    }
                    ?></span>

            </div>
        </div><!-- end of col-3 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') " . $condition . " GROUP BY au.account_id
						) x";




        $result = mysql_query($query);
        ?>
        <div class="signed_column dashboard-chart-2 col-5">
            <div class="main_signed_accounts">
                <span class="chart-label">Signed Shops This Month</span>
                <span class="units"  id="w3">
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h3' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')  and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
            $result = mysql_query($query);
            ?>
            <div class="top_signed_accounts">
                <span class="chart-label">Signed Only: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h3' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
            $result = mysql_query($query);
            ?>     

            <div class="middle_signed_accounts">
                <span class="chart-label">Paid-Partial: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h3' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . "  AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";



            $result = mysql_query($query);
            ?>
            <div class="bottom_signed_accounts">
                <span class="chart-label">Paid-Full:
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h3' value='0' />";
                    }
                    ?></span>

            </div>

        </div><!-- end of col-4 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x";



        $result = mysql_query($query);
        ?>
        <div class="signed_column dashboard-chart-2 col-5">
            <div class="main_signed_accounts">
                <span class="chart-label">Signed Shops to Date</span>
                <span class="units" id="w2">
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h2' value='0' />";
                    }
                    ?></span>
            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            /*
              $query = "select count(x.total) from
              (
              select au.account_id as total from audit au
              LEFT JOIN
              (

              SELECT a.account_id from accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  where a.account_paid = 1 ".$condition."

              ) a ON au.account_id = a.account_id

              where au.after_status_id = 13 and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)

              ) x";
             */
            $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
            $result = mysql_query($query);
            ?>

            <div class="top_signed_accounts">
                <span class="chart-label">Signed Only: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);
                        echo number_format($row[0]);
                        echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h2' value='0' />";
                    }
                    ?></span>
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";

//                echo $query;

                $result = mysql_query($query);
                ?>  
            </div>
            <div class="middle_signed_accounts">
                <span class="chart-label">Paid-Partial: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);

                        if ($row[0] != 0) {
                            echo "<a href='17' class='dashboard-link'>" . $row[0] . "</a>";
                        } else {
                            echo $row[0];
                        }
                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h1' value='0' />";
                    }
                    ?></span>

            </div>
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";

//             echo $query;

            $result = mysql_query($query);
            ?>
            <div class="bottom_signed_accounts">
                <span class="chart-label">Paid-Full: 
                    <?php
                    if ($result) {
                        $row = mysql_fetch_array($result, MYSQL_NUM);

                        if ($row[0] != 0) {
                            echo "<a href='16' class='dashboard-link'>" . $row[0] . "</a>";
                        } else {
                            echo $row[0];
                        }
                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    } else {
                        echo "0";
                        echo "<input type='hidden' id='h1' value='0' />";
                    }
                    ?>
                </span>

            </div>
        </div><!-- end of col-5 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a  LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 22 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">For Prospecting</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='22' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 10 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Set Appointment</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='10' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 11 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Send Proposal</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='11' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 12 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">For Callback</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='12' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 15 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">For Follow Up</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='15' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 19 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Interested</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='19' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 14 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Declined</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='14' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 20 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">For Signing</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='20' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 13 " . $condition . " GROUP by a.account_id
						) x";
        $query1 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 49 " . $condition . " GROUP by a.account_id
						) x";

//        echo $query;
        $result = mysql_query($query);
        $result1 = mysql_query($query1);
        ?>

        <?php //if($role_id !=3){     ?>
        <div class="dashboard-chart-2 col-5">
            <span class="chart-label">Signed (for Payment) :
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='13' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br><br>
            <span class="chart-label">Reject - No Payment :
                <?php
                if ($result1) {
                    $row1 = mysql_fetch_array($result1, MYSQL_NUM);

                    if ($row1[0] != 0) {
                        echo "<a href='49' class='dashboard-link'>" . number_format($row1[0]) . "</a>";
                    } else {
                        echo $row1[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row1[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="units" id="w1">

            </span>

        </div>
        <!-- end of col-1 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                    case 5:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }


        $query = "select count(x.total) from
						(
                                                 
                                                SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";


        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 60 " . $condition . " GROUP by a.account_id
						
						) x";



        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";
        $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 39 " . $condition . " GROUP by a.account_id
						
						) x";
//
//        echo $query;
        //        echo $query2;
//        echo $query3;
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        $result4 = mysql_query($query4);

//        echo($query2);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: -10px;padding-top: 18px;">
            <span class="chart-label">Rejected by Finance </span><br>
            <span class="chart-label" style="font-size: 11px;">Unclear WCF: 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='59' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Incomplete Mats:
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='60' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span></br>
            <span class="chart-label" style="font-size: 11px;">Incomplete WCF:
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='61' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
             <span class="chart-label" style="font-size: 11px;">No Contract:
                <?php
                if ($result4) {
                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                    if ($row4[0] != 0) {
                        echo "<a href='39' class='dashboard-link'>" . $row4[0] . "</a>";
                    } else {
                        echo $row4[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1" style="font-size: 12px;">
                <?php
                echo $row[0] + $row2[0] + $row3[0] + $row4[0];
                ?>
            </span>

        </div>
    </div><!-- end of row -->





<?php } ?>
<?php if ($department_id == 1 || $department_id == 4) { ?>

    <div class="row">


        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id
          WHERE a.account_paid=1 AND a.status_id = 1 AND a.shop_editor <> 0" . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 1 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>

        <?php //if($role_id !=3){       ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 15px;">
            <span class="chart-label">Lined up for Production</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='1' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>
        <!-- end of col-1 -->
        <?php //}       ?>
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 7 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 55 " . $condition . " GROUP by a.account_id
						) x";


        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 56 " . $condition . " GROUP by a.account_id
						) x";



        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 57 " . $condition . " GROUP by a.account_id
						) x";
        $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 58 " . $condition . " GROUP by a.account_id
						) x";

        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        $result4 = mysql_query($query4);

//        echo($query2);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: -15px;padding-top: 18px;">
            <span class="chart-label">Rejected by Prod </span><br>
            <span class="chart-label" style="font-size: 11px;">Unclear WCF: 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='55' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Incomplete Mats:
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='56' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span></br>
            <span class="chart-label" style="font-size: 11px;">Incomplete WCF:
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='57' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            </br>
            <span class="chart-label" style="font-size: 11px;">Ins. Not Followed:
                <?php
                if ($result4) {
                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                    if ($row4[0] != 0) {
                        echo "<a href='57' class='dashboard-link'>" . $row4[0] . "</a>";
                    } else {
                        echo $row4[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1" style="font-size: 12px;">
                <?php
                echo $row[0] + $row2[0] + $row3[0] + $row4[0];
                ?>
            </span>

        </div>
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 2 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 2 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);

//        echo($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 15px;">
            <span  class="chart-label">For Proofreading</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='2' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div><!-- end of col-2 -->

        <!--        singit ko lang For Design Instructions-->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 2 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 18 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 15px;">
            <span  class="chart-label">For Design Instructions</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='18' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div><!-- end of col-2 -->
        <!--        singit ko lang For Design Instructions-->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 21 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 21 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 15px;">
            <span  class="chart-label">For Development</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='21' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div><!-- end of col-2 -->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 3 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 3 " . $condition . " GROUP by a.account_id
						) x";

//        echo($query);

        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 34px;">
            <span class="chart-label">Under Construction</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='3' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
        </div><!-- end of col-3 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 4 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 4 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 34px;">
            <span class="chart-label">Editor QA</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='4' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div><!-- end of col-4 -->


        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 7 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 7 " . $condition . " GROUP by a.account_id
						) x";


        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 28 " . $condition . " GROUP by a.account_id
						) x";



        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 29 " . $condition . " GROUP by a.account_id
						) x";

        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);

//        echo($query2);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 4px;padding-top: 18px;">
            <span class="chart-label">Internal Revisions-Editor</span>
            <span class="chart-label" style="font-size: 11px;">Revisions 1: 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='7' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Revisions 2:
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='28' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span></br>
            <span class="chart-label" style="font-size: 11px;">Revisions 3:
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='29' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">
                <?php
//                if ($result) {
//                    $row = mysql_fetch_array($result, MYSQL_NUM);
//
//                    if ($row[0] != 0) {
//                        echo "<a href='7' class='dashboard-link'>" . $row[0] . "</a>";
//                    } else {
//                        echo $row[0];
//                    }
//                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
//                } else {
//                    echo "0";
//                    echo "<input type='hidden' id='h1' value='0' />";
//                }
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div>

        <!-- end of col-6 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 5 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 5 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 34px;">
            <span class="chart-label">For Sr. Designer QA</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='5' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div><!-- end of col-5 -->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 7 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 32 " . $condition . " GROUP by a.account_id
						) x";


        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 33 " . $condition . " GROUP by a.account_id
						) x";



        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 34 " . $condition . " GROUP by a.account_id
						) x";

        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);

//        echo($query2);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 4px;padding-top: 18px;">
            <span class="chart-label">Internal Revisions-QA&nbsp;&nbsp;&nbsp;</span>
            <span class="chart-label" style="font-size: 11px;">Revisions 1: 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='32' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Revisions 2:
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='33' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span></br>
            <span class="chart-label" style="font-size: 11px;">Revisions 3:
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='34' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">
                <?php
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div>

        <!-- end of col-6 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 5 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 38 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 34px;">
            <span class="chart-label">Final QA</span>
            <span class="units" id="w1">

                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='38' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

        </div>


        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 6 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 6 " . $condition . " GROUP by a.account_id
						) x";
        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 26 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 25 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 4px;">
            <span  class="chart-label">Client Approval : 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='6' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Client (Feedback) : 
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='26' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Client (Consolidation) : 
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='25' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">

                <?php
//                if ($result) {
//                    $row = mysql_fetch_array($result, MYSQL_NUM);
//
//                    if ($row[0] != 0) {
//                        echo "<a href='6' class='dashboard-link'>" . $row[0] . "</a>";
//                    } else {
//                        echo $row[0];
//                    }
//                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
//                } else {
//                    echo "0";
//                    echo "<input type='hidden' id='h1' value='0' />";
//                }
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div><!-- end of col-8 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id

          WHERE a.account_paid=1 and a.status_id = 8 AND a.shop_editor <> 0 " . $condition;
         */
        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 8 " . $condition . " GROUP by a.account_id
						) x";

        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 30 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 31 " . $condition . " GROUP by a.account_id
						) x";
        $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 41 " . $condition . " GROUP by a.account_id
						) x";
        $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 42 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        $result4 = mysql_query($query4);
        $result5 = mysql_query($query5);

//        echo ($query);
        ?>

        <div class="dashboard-chart-2 col-5" style="margin-top: -11px;">
            <span class="chart-label" style="font-size: 11px;">Client Revisions 1:
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='8' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label" style="font-size: 11px;">Client Revisions 2:
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='30' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label"  style="font-size: 11px;">Client Revisions 3:
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='31' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label"  style="font-size: 11px;">Client Revisions 4:
                <?php
                if ($result4) {
                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                    if ($row4[0] != 0) {
                        echo "<a href='41' class='dashboard-link'>" . $row4[0] . "</a>";
                    } else {
                        echo $row4[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label" style="font-size: 11px;">Client Revisions 5:
                <?php
                if ($result5) {
                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                    if ($row5[0] != 0) {
                        echo "<a href='41' class='dashboard-link'>" . $row5[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>

            <span class="units" id="w1" style="font-size: 12px;">

                <?php
//                if ($result) {
//                    $row = mysql_fetch_array($result, MYSQL_NUM);
//
//                    if ($row[0] != 0) {
//                        echo "<a href='8' class='dashboard-link'>" . $row[0] . "</a>";
//                    } else {
//                        echo $row[0];
//                    }
//                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
//                } else {
//                    echo "0";
//                    echo "<input type='hidden' id='h1' value='0' />";
//                }
                echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0];
                ?>
            </span>

        </div>
        <!-- end of col-7 -->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }



        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 37 " . $condition . " GROUP by a.account_id
						) x";
        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 41 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 42 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 4px;">
            <span  class="chart-label">Redesign 1 : 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='37' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Redesign 2 : 
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='41' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Redesign 3 : 
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='42' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">

                <?php
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div><!-- end of col-8 -->

        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }



        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 53 " . $condition . " GROUP by a.account_id
						) x";
        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 54 " . $condition . " GROUP by a.account_id
						) x";

        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: 4px;">
            <span  class="chart-label">Rejected by CSR</span><br>
            <span  class="chart-label" style="font-size: 11px;">Ins. Not Followed: 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='37' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label" style="font-size: 11px;">Design not Followed : 
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='41' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span> 
            <span class="units" id="w1">

                <?php
                echo $row[0] + $row2[0];
                ?>
            </span>

        </div><!-- end of col-8 -->


        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }



        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 50 " . $condition . " GROUP by a.account_id
						) x";
        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 51 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 52 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: -11px;">
            <span  class="chart-label">Rejected by CSR</span><br>
            <span  class="chart-label" style="font-size: 11px;">Unclear WCF : 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='50' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label" style="font-size: 11px;">Incomplete Mats : 
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='51' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label" style="font-size: 11px;">Incomplete WCF : 
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='52' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">

                <?php
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div><!-- end of col-8 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {
            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }
        /*
          $query = "SELECT
          count(a.account_id) as ct1
          FROM accounts a
          LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id

          WHERE a.account_paid=1 and a.status_id = 9 " . $condition ." GROUP BY a.account_id";
         */

        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 27 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 35 " . $condition . " GROUP by a.account_id
						) x";
        $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 36 " . $condition . " GROUP by a.account_id
						) x";
        $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 45 " . $condition . " GROUP by a.account_id
						) x";
        $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 46 " . $condition . " GROUP by a.account_id
						) x";

        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        $result4 = mysql_query($query4);
        $result5 = mysql_query($query5);
        $result6 = mysql_query($query6);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: -25px;">
            <span class="chart-label" style="font-size: 11px;">Live :
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='9' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label" style="font-size: 11px;">Live (Client Updates) :
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='27' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label" style="font-size: 11px;">Live (Temporary) :
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='35' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span class="chart-label" style="font-size: 11px;">Temp(Bal. Collection) :
                <?php
                if ($result4) {
                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                    if ($row4[0] != 0) {
                        echo "<a href='36' class='dashboard-link'>" . $row4[0] . "</a>";
                    } else {
                        echo $row4[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Temp(Awaiting Mats) :
                <?php
                if ($result5) {
                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                    if ($row5[0] != 0) {
                        echo "<a href='45' class='dashboard-link'>" . $row5[0] . "</a>";
                    } else {
                        echo $row5[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <br>
            <span class="chart-label" style="font-size: 11px;">Temp(Awaiting Final) :
                <?php
                if ($result6) {
                    $row6 = mysql_fetch_array($result6, MYSQL_NUM);

                    if ($row6[0] != 0) {
                        echo "<a href='46' class='dashboard-link'>" . $row6[0] . "</a>";
                    } else {
                        echo $row6[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row6[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1" style="font-size: 11px;">

                <?php
                echo number_format($row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0] + $row6[0]);
                ?>
            </span>
        </div><!-- end of col-9 -->
        <?php
        $condition = "";
        if ($_SESSION['dashboard_shoptype'] == 0) {

            if ($_SESSION['dashboard_user'] == 0) {
                switch ($role_id) {
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
            } else {
                $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
            }
        } else {
            $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
        }



        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 24 " . $condition . " GROUP by a.account_id
						) x";
        $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 47 " . $condition . " GROUP by a.account_id
						) x";
        $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 48 " . $condition . " GROUP by a.account_id
						) x";
        $result = mysql_query($query);
        $result2 = mysql_query($query2);
        $result3 = mysql_query($query3);
        ?>
        <div class="dashboard-chart-2 col-5" style="margin-top: -11px;">
            <span  class="chart-label">Temporary Closed : 
                <?php
                if ($result) {
                    $row = mysql_fetch_array($result, MYSQL_NUM);

                    if ($row[0] != 0) {
                        echo "<a href='24' class='dashboard-link'>" . $row[0] . "</a>";
                    } else {
                        echo $row[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Temp Closed(Delinquent): 
                <?php
                if ($result2) {
                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                    if ($row2[0] != 0) {
                        echo "<a href='47' class='dashboard-link'>" . $row2[0] . "</a>";
                    } else {
                        echo $row2[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span><br>
            <span  class="chart-label">Temp Closed(Request from Client) : 
                <?php
                if ($result3) {
                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                    if ($row3[0] != 0) {
                        echo "<a href='48' class='dashboard-link'>" . $row3[0] . "</a>";
                    } else {
                        echo $row3[0];
                    }
                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                } else {
                    echo "0";
                    echo "<input type='hidden' id='h1' value='0' />";
                }
                ?>
            </span>
            <span class="units" id="w1">

                <?php
                echo $row[0] + $row2[0] + $row3[0];
                ?>
            </span>

        </div><!-- end of col-8 -->



    </div><!-- end of row -->



<?php } ?>




<script>
    function slotmachine(id, changeto) {
        var thisid = '#' + id;
        var $obj = $(thisid);
        $obj.css('opacity', '.3');
        var original = $obj.text();

        var spin = function() {
            return Math.floor(Math.random() * 10);
        };

        var spinning = setInterval(function() {
            $obj.text(function() {
                var result = '';
                for (var i = 0; i < original.length; i++) {
                    result += spin().toString();
                }
                return result;
            });
        }, 50);

        var done = setTimeout(function() {
            clearInterval(spinning);
            $obj.text(changeto).css('opacity', '1');
        }, 1000);
    }           
           
</script>


