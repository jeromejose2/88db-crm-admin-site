<?php
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 1) {
        header('Location: /88dbphcrm/');
        exit;
    } else {
//        header('Location: /88dbphcrm/login/');
//        exit;
    }
} else {
//    header('Location: /88dbphcrm/login/');
//    exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

    <head>

        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">


        <title>Login Page</title>

        <!-- STYLESHEETS -->
        <link rel="stylesheet" type="text/css" href="/88dbphcrm/css/style.css" />

        <!--[if lt IE 9]>
            <link rel="stylesheet" href="/88dbphcrm/css/ie.css" />
        <![endif]-->

    </head>


    <body>

        <div class="log-main-container">

            <div class="log-section">

                <div id="logo"><!-- Logo Container --></div>

                <form action="login.php" method="POST">
                    Username: <br /><input type="text" name="username" /><br />
                    Password: <br /><input type="password" name="password" /><br>

                    <div class="log-commands">
                        <input type="submit" value="Submit" />
                        <a class="link-button" href="signup.php">Sign up</a>
                    </div><!-- end of log-commands -->
                </form>

            </div><!-- end of login-section -->

        </div><!-- end of main-login-container -->

    </body>
</html>
