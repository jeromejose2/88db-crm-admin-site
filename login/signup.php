<?php
session_start();


include_once ('functions/connection.php');
include_once ('functions/signup-alert.php');
$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {
    $user_firstname = $_POST['user_firstname'];
    $user_lastname = $_POST['user_lastname'];
    $user_name = $_POST['user_name'];
    $user_password = md5($_POST['user_password']);
    $user_email = $_POST['user_email'];
    $team_id = $_POST['team_id'];
    $department_id = $_POST['department_id'];
    $query = "SELECT user_email FROM users WHERE user_email = '$user_email'";
    $result = mysql_query($query, connect());
    if (mysql_num_rows($result) > 0) {
        header('Location: /88dbphcrm/error.php?err=3');
        exit;
    }
    $query = "SELECT user_name FROM users WHERE user_name = '$user_name'";
    $result = mysql_query($query);
    if (mysql_num_rows($result) > 0) {
        header('Location: /88dbphcrm/error.php?err=4');
        exit;
    }

    $query = "INSERT INTO users (user_firstname, user_lastname, user_name, user_password, user_email, department_id, role_id, team_id, user_createdby) VALUES ('$user_firstname', '$user_lastname', '$user_name', '$user_password', '$user_email', $department_id, 3, $team_id , 1)";
    SendEmailtoAdmin($user_firstname . " " . $user_lastname, $user_email, $department_id);
    $result = mysql_query($query);
    $user_id = mysql_insert_id();

    //header('Location: /88dbphcrm/login/');
    header('Location: /88dbphcrm/error.php?err=6');
    exit;
}

$query = "SELECT department_id, department_name FROM departments";
$result = mysql_query($query, connect());
$departmentoptions = "";
while ($row = mysql_fetch_array($result)) {
    $depid = $row["department_id"];
    $depname = $row["department_name"];
    $departmentoptions .= '<option value="' . $depid . '">' . $depname . "</option>";
}

$queryteam = "SELECT team_id, team_name FROM teams order by team_name asc";
$resultteam = mysql_query($queryteam, connect());
$teamoptions = "";
while ($row = mysql_fetch_array($resultteam)) {
    $teamid = $row["team_id"];
    $teamname = $row["team_name"];
    $teamoptions .= '<option value="' . $teamid . '">' . $teamname . "</option>";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>

        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">

        <title>Sign up</title>
        <script language="javascript">
        
            function validate()
            {
                if(document.form1.user_firstname.value=='')
                {
                    alert('Please enter First Name.');
                    document.form1.user_firstname.focus();
                    return false;
                }
                if(document.form1.user_lastname.value=='')
                {
                    alert('Please enter Last Name.');
                    document.form1.user_lastname.focus();
                    return false;
                }
                if(document.form1.user_name.value=='')
                {
                    alert('Please enter Username.');
                    document.form1.user_name.focus();
                    return false;
                }
                if(document.form1.user_password.value=='')
                {
                    alert('Please enter Password.');
                    document.form1.user_password.focus();
                    return false;
                }
                if(document.form1.user_email.value==""){
                    alert("Please enter Email Address");
                    document.form1.user_email.focus();
                    return false;
                }
                var email = document.form1.user_email.value;
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(email)) {
                    alert('Please provide a valid email address');
                    document.form1.user_email.focus();
                    return false;
                }
                
                if(document.form1.department_id.value==0){
                    alert("Please select a Department");
                    document.form1.department_id.focus();
                    return false;
                }
                return true;
            }
        </script>

        <!-- STYLESHEETS -->
        <link rel="stylesheet" type="text/css" href="/88dbphcrm/css/style.css" />

        <!--[if lt IE 9]>
            <link rel="stylesheet" href="/88dbphcrm/css/ie.css" />
        <![endif]-->

    </head>

    <body>


        <div class="signup">

            <form name="form1" action="signup.php" method="post" onsubmit="return validate()">

                <div class="log-main-container" style="margin-top: 120px;">

                    <div class="log-section">

                        <div class="section-label">
                            <h2>Sign up</h2>
                        </div><!-- end of section-label -->

                        <div class="inner-box">
                            <input type="text" name="user_firstname" id="user_firstname" value='First Name' onfocus="if (this.value=='First Name')this.value=''" onblur="if (this.value=='')this.value='First Name'"/><br />

                            <input type="text" name="user_lastname" id="user_lastname" value='Last Name' onfocus="if (this.value=='Last Name')this.value=''" onblur="if (this.value=='')this.value='Last Name'"/><br />

                            <input type="text" name="user_name" id="user_name" value='Username' onfocus="if (this.value=='Username')this.value=''" onblur="if (this.value=='')this.value='Username'"/><br />

                            <input type="password" name="user_password" id="user_password" value='Password' onfocus="if (this.value=='Password')this.value=''" onblur="if (this.value=='')this.value='Password'"/><br />

                            <input type="text" name="user_email" id="user_email" value='Email' onfocus="if (this.value=='Email')this.value=''" onblur="if (this.value=='')this.value='Email'"/><br />
                            <select name='team_id' id='team_id'>
                                <option value='0'>Select Team</option>
                                <?php echo $teamoptions; ?>
                            </select>
                            <select name='department_id' id='department_id'>
                                <option value='0'>Select Department</option>
                                <?php echo $departmentoptions; ?>
                            </select>


                            <div class="log-commands">
                                <ul>
                                    <li><a class="link-button gray" href="/88dbphcrm/login/">Cancel</a></li>
                                    <li><input type="submit" value="Submit" /></li>
                                </ul>
                            </div><!-- end of log-commands -->

                        </div><!-- end of inner-box -->

                    </div><!-- end of login-section -->

                </div><!-- end of main-login-container -->

            </form>

        </div><!-- end of signup -->


    </body>
</html>