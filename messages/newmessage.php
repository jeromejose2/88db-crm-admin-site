<?php
session_start();
include_once("../functions/connection.php");
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {
    $to = $_POST['msg_recipient'];
    $msg_content = $_POST['msg_content'];
	
	$date_message = date('Y-m-d',strtotime('today'))." ".$_POST['current_time'];
    $query = "INSERT INTO messages (message_content, message_to, message_from,message_createdon) VALUES ('$msg_content', $to, '".$_SESSION['user_id']."','$date_message')";    
    $result = mysql_query($query,connect());
   
   header("Location: /88dbphcrm/messages");
}
	$query = "SELECT 
						user_id, 
						CONCAT(user_firstname,' ',user_lastname) as user_fullname 
						FROM users WHERE enabled = 1 ORDER BY user_fullname ASC";

	$result = mysql_query($query,connect());
	$useroptions = "";
	while ($row = mysql_fetch_array($result)) {
			$userid = $row["user_id"];
			$userfullname = $row["user_fullname"];
			if ($_SESSION['user_id']!=$userid) 
			{
				
					$useroptions .= '<option value="'.$userid.'">' . $userfullname . "</option>";
				
				
			}
		}

					
					
					
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Add Remarks</title>
    
    
    <!-- STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="/88dbphcrm/css/style.css" />

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="/88dbphcrm/css/ie.css"/>
    <![endif]-->

    <link rel="stylesheet" href="/88dbphcrm/js/tip-yellow/tip-yellow.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-violet/tip-violet.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-darkgray/tip-darkgray.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-skyblue/tip-skyblue.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-twitter/tip-twitter.css" type="text/css" />
    <link rel="stylesheet" href="/88dbphcrm/js/tip-green/tip-green.css" type="text/css" />    
    <link rel="stylesheet" href="/88dbphcrm/css/colorbox.css" type="text/css" />
    
    <!-- SCRIPTS -->
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <!--<script type="text/javascript" src="/88dbphcrm/js/jquery-1.4.4.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="/88dbphcrm/js/jquery.colorbox.js"></script>
    <script type="text/javascript" src="/88dbphcrm/js/jquery.poshytip.js"></script>
	<script type="text/javascript" src="/88dbphcrm/js/jquery.validate.pack.js"></script>
	<script type="text/javascript">
    $(document).ready(function(){ 
		$('#send_msg_btn').click(function () {
			
			var recipient =  jQuery.trim($("#msg_recipient").val());
			var msg =  jQuery.trim($("#msg_content").val());
			
			if((recipient =="") || (msg ==""))
			{
			
			}
			else
			{
				$("#new_msg_frm").submit();
			}
				
		});
	});
	</script>
</head>

<body>
    
    <div class="main-container" style="width: 560px;">

    <div class="main-section" style="border:0">
    <form name="new_msg_frm" id="new_msg_frm" action="newmessage.php" method="post">
        <div class="commands">
            <div class="head-label">
                <h2>New Message</h2>
            </div><!-- end of add new account -->
            
           
        </div><!-- end of grid-commands -->
        
        <div class="gen-section">

                <table cellpadding="5" cellspacing="0" style="width:500px;">
					<tr>
                        <td class="grid-head">To:</td>
                        <td>&nbsp;</td>
                        <td>
						<select name="msg_recipient" id="msg_recipient" class="required error">
						<option value="">Select Here</option>
						<?php echo $useroptions; ?>
						</select>
						</td>
                    </tr>
                    <tr>
                        <td class="grid-head">Message:</td>
                        <td>&nbsp;</td>
                        <td><textarea cols="25" rows="3" name="msg_content" id="msg_content" style="resize:none"></textarea></td>
                    </tr>
					 <tr>
                        <td class="grid-head">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><a class="link-button" id="send_msg_btn" style="background-color: #F68520; border: 1px solid #F68520; color: #9C4114;float:right;right:38px;position:relative;" href="#">Send Message</a>
						<input type="hidden" name="current_time" id="current_time" />
						</td>
						
                    </tr>

                </table>
            

        </div><!-- end of gen-section -->
       
		
</form>
    </div><!-- end of main-section -->
	<script type="text/javascript">
		function GetCurrentTime()
		{
		
			var currentTime = new Date()
			var hours = currentTime.getHours()
			var minutes = currentTime.getMinutes()
			var seconds = currentTime.getSeconds()
			if (minutes < 10){
			minutes = "0" + minutes
			}
			if (seconds < 10){
			seconds = "0" + seconds
			}
			
			$("#current_time").val(hours + ":" + minutes + ":" + seconds);
		
		}
		$(function() {
		
		
			setInterval('GetCurrentTime()', 100 );
			
		});
	
	</script>
</body>
</html>