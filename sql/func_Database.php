<?php

	function connect_DB ($server = DB_SERVER,	$username = DB_SERVER_USERNAME, 
			$password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') 
	{
		global $$link;

		if (USE_PCONNECT == 'true') {
			$$link = mysql_pconnect($server, $username, $password);
		} else {
			$$link = mysql_connect($server, $username, $password);
		}
		if ($$link) mysql_select_db($database);
		return $$link;
	}

	function error_DB ( $query,	$errno,	$error ) 
	{ 
		die(
				'<div align=center><font color="#000000"><b>' . $errno . ' - ' . $error . 
				'<br><br>' . $query . '<br><br><small><font color="#ff0000">
				[ S T O P ]</font></small><br><br></b></font></div>');
	}
	

	function close_DB($link = 'db_link') {
		global $$link;
		return mysql_close($$link);
	}

	function database_Query($query, $link='db_link') 
	{
		global $$link;	
		$result = mysql_query($query, $$link) or error_DB($query, mysql_errno(), mysql_error());
		return $result;
	}

	function record_Fetch($query_db)
	{
		return mysql_fetch_array($query_db, MYSQL_ASSOC);
	}

	function rows_Count($query_db) 
	{
		return mysql_num_rows($query_db);
	}

	function data_Seek($db_query, $row_number) {
		return mysql_data_seek($db_query, $row_number);
	}

	function generate_RecordID($table = '', $primaryKey = 'pk_id')
	{
		if ( $table != '') {
			$get_ID = database_Query('SELECT MAX('.$primaryKey.') as LastID FROM ' . $table);
			if (rows_Count ($get_ID)>0) {
				if ($res = record_Fetch ($get_ID)) { return $res['LastID']+1; }
				else { return 1; }
			} else { return 1; }
		} else { return false; }
	}

	function db_insert_id() {
		return mysql_insert_id();
	}


	function Add2Database ($table_name, $data)
	{
		$query = 'INSERT INTO '. $table_name .' (';
		while (list($columns, ) = each($data)) { $query .= $columns . ', '; }
		$query = substr($query, 0, -2) . ') VALUES (';
		reset($data);
		while (list(, $values) = each($data)) { $query .= '\''.$values . '\', '; }
		$query = substr($query, 0, -2) . ');';
		$runquery = database_Query($query);
		if (mysql_affected_rows() > 0) return true;
		else return false;
	}

	function Edit2Database ($table_name, $data, $parameters)
	{
		
		$query = 'UPDATE '. $table_name .' SET ';
		while (list($columns, $values) = each($data)) 
		{ $query .= $columns . '= \''.$values . '\', '; }
		$query = substr($query, 0, -2) . ' WHERE '. $parameters .' LIMIT 1;';
		
		$runquery = database_Query($query);
		if (mysql_affected_rows() > 0) return true;
		else return false;
	}


	function Delete2Database ($table_name, $parameters = '')
	{
		$query = 'DELETE FROM '. $table_name .' ';
		$query = $query . ' WHERE '. $parameters .';';
		
		$runquery = database_Query($query);
		if (mysql_affected_rows() > 0) return true;
		else return false;
	}
?>