<?php
session_start();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];

mysql_connect('localhost', 'root', '');
mysql_select_db('88dbphcrm') or die("Unable to select database");

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {
    $user_firstname = $_POST['user_firstname'];
    $user_lastname = $_POST['user_lastname'];
    $user_name = $_POST['user_name'];
    $user_password = $_POST['user_password'];
    $user_email = $_POST['user_email'];
    $department_id = $_POST['department_id'];
    $query = "SELECT user_id FROM users WHERE user_name = '$user_name'";
    $result = mysql_query($query);
    if (mysql_num_rows($result) > 0) {
        header('Location: /88dbphcrm/error.php?err=3');
        exit;
    }

    $query = "INSERT INTO users (user_firstname, user_lastname, user_name, user_password, user_email, department_id, role_id, user_createdby) VALUES ('$user_firstname', '$user_lastname', '$user_name', '$user_password', '$user_email', $department_id, 3, 1)";
    $result = mysql_query($query);
    $user_id = mysql_insert_id();
    header('Location: /88dbphcrm/users/');
    exit;
}

$query = "SELECT department_id, department_name FROM departments";
$result = mysql_query($query);
$departmentoptions = "";
while ($row = mysql_fetch_array($result)) {
    $depid = $row["department_id"];
    $depname = $row["department_name"];
    $departmentoptions .= '<option value="' . $depid . '">' . $depname . "</option>";
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Add User</title>

        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="form1" id="form1" action="add.php" method="post">
            <div class="commands">
                <div class="head-label">
                    <h2>Add New User</h2>
                </div><!-- end of add new account -->

                <ul>
                    <?php if (($role_id == "1") || ($role_id == "2") || ($role_id == "5")) { ?>
                        <li><a class="link-button gray" href='/88dbphcrm/users'>Cancel</a></li>
                        <?php
                    } else if ($role_id == "3") {
                        ?>
                        <li><a class="link-button gray" href='/88dbphcrm/'>Cancel</a></li>
                    <?php } ?>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->


            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*First Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_firstname" id="user_firstname" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Last Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_lastname" id="user_lastname" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Username:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_name" id="user_name" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Password:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_password" id="user_password" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Email:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_email" id="user_email" class="required email"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Account Name:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='department_id' id='department_id'>
                                <option value='0'>Select Department</option>
                                <?php echo $departmentoptions; ?>
                            </select>                            
                        </td>
                    </tr>

                </table>

            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>