<?php
session_start();
if ($_SESSION['role_id'] == 3) {
    $_SESSION['title_page'] = "MyAccounts";
}

require_once ("../ctrl/ctrl_edituser.php");


$query = "SELECT department_id, department_name FROM departments";
$result = mysql_query($query);
$departmentoptions = "";

while ($row = mysql_fetch_array($result)) {

    $depid = $row["department_id"];
    $depname = $row["department_name"];
    if ($currentuser[department_id] == $depid) {
        $departmentoptions .= '<option value="' . $depid . '" selected="selected">' . $depname . "</option>";
    } else {
        $departmentoptions .= '<option value="' . $depid . '">' . $depname . "</option>";
    }
}

$query = "SELECT role_id, role_name FROM roles";
$result = mysql_query($query);
$roleoptions = "";

while ($row = mysql_fetch_array($result)) {

    $roleid = $row["role_id"];
    $rolename = $row["role_name"];
    if ($currentuser[role_id] == $roleid) {
        $roleoptions .= '<option value="' . $roleid . '" selected="selected">' . $rolename . "</option>";
    } else {
        $roleoptions .= '<option value="' . $roleid . '">' . $rolename . "</option>";
    }
}

$query = "SELECT job_desc_id, job_desc FROM job_desc";
$result = mysql_query($query);
$job_descoptions = "";

while ($row = mysql_fetch_array($result)) {

    $job_desc_id = $row["job_desc_id"];
    $job_desc = $row["job_desc"];
    if ($currentuser[job_desc] == $job_desc_id) {
        $job_descoptions .= '<option value="' . $job_desc_id . '" selected="selected">' . $job_desc . "</option>";
    } else {
        $job_descoptions .= '<option value="' . $job_desc_id . '">' . $job_desc . "</option>";
    }
}


$query = "SELECT team_id, team_name FROM teams";
$result = mysql_query($query);
$teamoptions = "";

while ($row = mysql_fetch_array($result)) {

    $teamid = $row["team_id"];
    $teamname = $row["team_name"];
    if ($currentuser[team_id] == $teamid) {
        $teamoptions .= '<option value="' . $teamid . '" selected="selected">' . $teamname . "</option>";
    } else {
        $teamoptions .= '<option value="' . $teamid . '">' . $teamname . "</option>";
    }
}

$query = "SELECT user_id,concat(user_firstname, ' ', user_lastname) as user_completename FROM users Where role_id = 2 AND enabled = 1";
$result = mysql_query($query);
$manageroptions = "";

while ($row = mysql_fetch_array($result)) {

    $managerid = $row["user_id"];
    $managername = $row["user_completename"];
    if ($currentuser[manager_id] == $managerid) {
        $manageroptions .= '<option value="' . $managerid . '" selected="selected">' . $managername . "</option>";
    } else {
        $manageroptions .= '<option value="' . $managerid . '">' . $managername . "</option>";
    }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Edit User</title>

        <script language="javascript">
        
            function validate()
            {
                if(document.edituser.user_firstname.value=='')
                {
                    alert('Please enter First Name.');
                    document.edituser.user_firstname.focus();
                    return false;
                }
                if(document.edituser.user_lastname.value=='')
                {
                    alert('Please enter Last Name.');
                    document.edituser.user_lastname.focus();
                    return false;
                }
                if(document.edituser.user_name.value=='')
                {
                    alert('Please enter Username.');
                    document.edituser.user_name.focus();
                    return false;
                }
                if(document.edituser.user_email.value==""){
                    alert("Please enter Email Address");
                    document.edituser.user_email.focus();
                    return false;
                }
                var email = document.edituser.user_email.value;
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test(email)) {
                    alert('Please provide a valid email address');
                    document.edituser.user_email.focus();
                    return false;
                }
                if(document.edituser.department_id.value==0){
                    alert("Please select a Department");
                    document.edituser.department_id.focus();
                    return false;
                }
                return true;
            }
        </script>


        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="edituser" id="edituser" action="/88dbphcrm/users/edit.php?act=update&user_id=<?php echo $_GET[user_id] ?>" method="post" onsubmit="return validate()">
            <div class="commands">
                <div class="head-label">
                    <h2>Edit User : <?php echo "$currentuser[user_firstname]" . " " . "$currentuser[user_lastname]" ?></h2>
                </div><!-- end of add new account -->

                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/users/changepassword.php?user_id=<?php echo $_GET[user_id] ?>'>Change Password</a></li>

                    <?php if (($role_id == "1") || ($role_id == "2") || ($role_id == "5")) { ?>
                        <li><a class="link-button gray" href='/88dbphcrm/users'>Cancel</a></li>
                        <?php
                    } else if ($role_id == "3") {
                        ?>
                        <li><a class="link-button gray" href='/88dbphcrm/'>Cancel</a></li>
                    <?php } ?>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->


            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="3">Created On: <?php echo $currentuser[user_createdon]; ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">Modified On: <?php echo $currentuser[user_modifiedon]; ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">Modified by: <?php echo $usermodified[user_completename]; ?> (<?php echo $usermodified[role]; ?> - <?php echo $usermodified[dept_name]; ?>) </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*First Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_firstname" id="user_firstname" class="required" value="<?php echo $currentuser[user_firstname]; ?>"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Last Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_lastname" id="user_lastname" class="required" value="<?php echo $currentuser[user_lastname] ?>"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Username:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_name" id="user_name" class="required" value="<?php echo $currentuser[user_name] ?>"/></td>
                    </tr>
<!--                    <tr>
                        <td class="grid-head">*Password:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_password" id="user_password" class="required"/></td>
                    </tr>-->
                    <tr>
                        <td class="grid-head">*Email:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="user_email" id="user_email" class="required email" value="<?php echo $currentuser[user_email]; ?>"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">Team:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='team_id' id='team_id'>
                                <option value='0'>-Select Team-</option>
                                <?php echo $teamoptions; ?>
                            </select>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Manager:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='manager_id' id='manager_id'>
                                <option value='0'>-Select Manager-</option>
                                <?php echo $manageroptions; ?>
                            </select>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">User Type:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='type_id' id='type_id'>
                                <option value='0'>-Select Type-</option>
                                <option value='1' <?php
                                if ($currentuser[user_type] == 1) {
                                    echo 'selected';
                                }
                                ?>>88DB</option>
                                <option value='2' <?php
                                        if ($currentuser[user_type] == 2) {
                                            echo 'selected';
                                        }
                                ?>>OpenRice</option>

                            </select>                            
                        </td>
                    </tr>

                    <tr>
                        <td class="grid-head">Job Desc:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='job_descoptions' id='job_descoptions'>
                                <option value='0'>Select Job desc</option>
                                <?php echo $job_descoptions; ?>
                            </select>                            
                        </td>
                    </tr>

                    <?php if ($_SESSION['department_id'] == 4) { ?>




                        <tr>
                            <td class="grid-head">*Department:</td>
                            <td>&nbsp;</td>
                            <td>
                                <select name='department_id' id='department_id'>
                                    <option value='0'>Select Department</option>
                                    <?php echo $departmentoptions; ?>
                                </select>                            
                            </td>
                        </tr>


                        <tr>
                            <td class="grid-head">*Role:</td>
                            <td>&nbsp;</td>
                            <td>
                                <select name='role_id' id='role_id'>
                                    <option value='0'>Select Role</option>
                                    <?php echo $roleoptions; ?>
                                </select>                            
                            </td>
                        </tr>
                         
                        <tr>
                            <td class="grid-head">*Enable:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="checkbox" name="chkbox_enabled" id="chkbox_enabled"  id="user_enable" />
                                <input type="hidden" name="user_enable" id="user_enable" value="<?php echo $currentuser[enabled]; ?>" />
                            </td>
                        </tr>
                    <?php } ?>

<!--                    <tr>
<td colspan="3" align="right">
<span>Created On: <?php echo $currentuser[user_createdon]; ?> Modified : <?php echo $currentuser[user_modifiedon]; ?> by: <?php echo $usermodified[user_completename]; ?></span>                                
</td>
</tr>-->
                </table>

            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>

<script type="text/javascript">

    $('#chkbox_enabled').click(function()
    {
        if($("#chkbox_enabled").is(':checked'))
        {
            $("#user_enable").val("1");
        }
        else
        {
            $("#user_enable").val("0");
        }
    }
);
    $(function()
    {
		
        if ($("#user_enable").val()==="1")
        {
		 
            $("#chkbox_enabled").attr('checked',true);
			
        }
        else
        {
            $("#chkbox_enabled").attr('checked',false);
        }
    });

</script>
</html>