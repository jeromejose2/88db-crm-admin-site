<?php
session_start();
$_SESSION['title_page'] = "Users";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
    if ($_SESSION['role_id'] == 3) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
require_once ("../ctrl/ctrl_user.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Users</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
        <style>
            body {
                font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
                font-size: 62.5%;
            }
        </style>

        <script language="javascript">
            $(function() {
                $( "#accordion" ).accordion();
            });
            function Delete(id)
            {
                if(confirm('Are you sure you want to \nremove the selected user?'))
                {	
                    document.user.action="delete.php?act="+id;
                    document.user.submit();
                }
            }
        </script>
        <?php include '../header.php'; ?>

    <div class="main-section">

        <div class="commands">

            <div class="head-label">
                <h2>Users</h2>
            </div><!-- end of add new account -->
            <?php if ($_SESSION['role_id'] == 1 && $_SESSION['department_id'] == 4) : ?>
                <ul>
                    <li><a class="link-button" href='/88dbphcrm/users/add.php'>Add New</a></li>
                </ul>
            <?php endif; ?>
        </div>
        <!-- end of grid-commands -->
        <form name="user" method="post">
            <?php if ($_SESSION['role_id'] == 1 && $_SESSION['department_id'] == 4) : ?>
                <?php if ($usersnot): ?>    
                    <div class="commands">

                        <div class="head-label">
                            <h3 style="font-size: 13px;">Inactive</h3>
                        </div><!-- end of add new account -->
                    </div>


                    <div class="gen-section" style="padding-bottom: 10px;">

                        <table class="main-grid">

                            <tr class="grid-head">
                                <td>ID</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Team</td>
                                <td></td>

                            </tr>
                            <?php if ($usersnot)
                                foreach ($usersnot as $val): ?>
                                    <tr class='grid-content'>

                                        <td><?php echo $val[user_id] ?></td>
                                        <td>
                                            <a href="/88dbphcrm/users/edit.php?user_id=<?php echo $val[user_id] ?>" >
                                                <?php echo "$val[user_firstname]" . " " . "$val[user_lastname]" ?>
                                            </a>

                                        </td>
                                        <td><?php echo $val[user_email] ?></td>
                                        <td>
                                            <?php if (!$val[team_name]): ?>
                                                <?php
                                                switch ($val[team_id]) {
                                                    case 1:
                                                        echo "Team Nemesis";
                                                        break;
                                                    case 2:
                                                        echo "Team Athena";
                                                        break;
                                                    case 3:
                                                        echo "Team Wild Artemis";
                                                        break;
                                                    case 4:
                                                        echo "Team Jupiter Jedi";
                                                        break;
                                                    case 5:
                                                        echo "Team Lion A";
                                                        break;
                                                    case 6:
                                                        echo "Team Tiger";
                                                        break;
                                                    case 7:
                                                        echo "Team Lion B";
                                                        break;
                                                    case 8:
                                                        echo "Team Cebu";
                                                        break;
                                                }
                                                ?>
                                            <?php else: ?>

                                                <?php
                                                echo $val[team_name];
                                                ?>

                                            <?php endif; ?>

                                        </td>

                                        <td><a href="#" onclick="return Delete(<?php echo $val[user_id] ?>)" id="delete">Delete</a></td>
                                    </tr>
                                <?php endforeach; ?>

                        </table>

                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <div class="commands">

                <div class="head-label">
                    <h3 style="font-size: 13px;">Active</h3>
                </div><!-- end of add new account -->


            </div>

            <div id="accordion">
                <h3>
                    <div style="position: relative;left: 17px;">
                        Section 1
                    </div>
                </h3>
                <div>
                    <p>
                        test 1
                    </p>
                </div>
                <h3> 
                    <div style="position: relative;left: 17px;">
                        Section 2
                    </div>
                </h3>
                <div>
                    <p>
                        Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                        purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                        velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                        suscipit faucibus urna.
                    </p>
                </div>
                <h3>
                    <div style="position: relative;left: 17px;">
                        Section 3
                    </div>
                </h3>
                <div>
                    <p>
                        Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                        Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                        ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                        lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                    </p>
                    <ul>
                        <li>List item one</li>
                        <li>List item two</li>
                        <li>List item three</li>
                    </ul>
                </div>
                <h3>
                    <div style="position: relative;left: 17px;">
                        Section 4
                    </div>
                </h3>
                <div>
                    <p>
                        Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                        et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                        faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                        mauris vel est.
                    </p>
                    <p>
                        Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
                        Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                        inceptos himenaeos.
                    </p>
                </div>
            </div>


            <div class="gen-section" style="overflow-x: hidden;overflow-y: scroll;height: 545px;width: 935px;">

                <table class="main-grid">

                    <tr class="grid-head">
                        <td>ID</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Team</td>
                        <td>User Log</td>
                        <?php if ($role_id == 1 || $role_id == 2)
                            echo "<td></td>"; ?>
                    </tr>
                    <?php
                    $c = 0;
                    $a = 0;
                    if ($users)
                        foreach ($users as $val):
                            ?>
                            <tr class='grid-content'>

                                <td><?php echo $val[user_id] ?></td>
                                <td>
                                    <a href="/88dbphcrm/users/edit.php?user_id=<?php echo $val[user_id] ?>" >
                                        <?php echo "$val[user_firstname]" . " " . "$val[user_lastname]" ?>
                                    </a>
                                    <?php if ($val[log] == 1): ?>
                                        <span style="font-size: 10px;color: red;"> - logged in</span>
                                        <?php $c++;
                                    endif; ?>
                                </td>
                                <td><?php echo $val[user_email] ?></td>
                                <td>
                                    <?php if (!$val[team_name]): ?>
                                        <?php
                                        switch ($val[team_id]) {
                                            case 1:
                                                echo "Team Nemesis";
                                                break;
                                            case 2:
                                                echo "Team Athena";
                                                break;
                                            case 3:
                                                echo "Team Wild Artemis";
                                                break;
                                            case 4:
                                                echo "Team Jupiter Jedi";
                                                break;
                                            case 5:
                                                echo "Team Lion A";
                                                break;
                                            case 6:
                                                echo "Team Tiger";
                                                break;
                                            case 7:
                                                echo "Team Lion B";
                                                break;
                                            case 8:
                                                echo "Team Cebu";
                                                break;
                                        }
                                        ?>
                                    <?php else: ?>

                                        <?php
                                        echo $val[team_name];
                                        ?>

                                    <?php endif; ?>

                                </td>
                                <td><a href="userlog.php?user_id=<?php echo $val[user_id] ?>" id="view">View</a></td>
                                <?php if ($_SESSION['role_id'] == 1 && $_SESSION['department_id'] == 4): ?>
                                    <td><a href="#" onclick="return Delete(<?php echo $val[user_id] ?>)" id="delete">Delete</a></td>
                                <?php endif; ?>


                            </tr>
                            <?php $a++;
                        endforeach; ?>

                </table>

            </div>
            <div class="commands" style="margin-top: 15px;">

                <div class="head-label">
                    <h3 style="font-size: 12px;">
                        Total active login: <?php echo $c; ?>&nbsp;&nbsp;&nbsp;&nbsp;
                    </h3>
                </div><!-- end of add new account -->
                <div class="head-label">
                    <h3 style="font-size: 12px;">
                        Total users: <?php echo $a; ?>
                    </h3>
                </div><!-- end of add new account -->


            </div>
        </form>

        <!-- end of gen-section -->


    </div><!-- end of main-section -->

</div><!-- end of main-container -->


</body>
</html>