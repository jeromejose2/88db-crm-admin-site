<?php
session_start();
if ($_SESSION['role_id'] == 3) {
    $_SESSION['title_page'] = "MyAccounts";
}

require_once ("../ctrl/ctrl_edituser.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>User Log Trail</title>




        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="edituser" id="edituser" action="/88dbphcrm/users/edit.php?act=update&user_id=<?php echo $_GET[user_id] ?>" method="post" onsubmit="return validate()">
            <div class="commands">
                <div class="head-label">
                    <h2>Edit User : <?php echo "$currentuser[user_firstname]" . " " . "$currentuser[user_lastname]" ?></h2>
                </div><!-- end of add new account -->

                <ul>

                    <?php if (($role_id == "1") || ($role_id == "2") || ($role_id == "5") ) { ?>
                        <li><a class="link-button gray" href='/88dbphcrm/users'>Cancel</a></li>
                        <?php
                    } else if ($role_id == "3") {
                        ?>
                        <li><a class="link-button gray" href='/88dbphcrm/'>Cancel</a></li>
                    <?php } ?>

                </ul>
            </div><!-- end of grid-commands -->


            <div class="gen-section">

                <table cellpadding="0" class="main-grid" cellspacing="0">
                    <tr class="grid-head">
                        <td width="100">Log</td>
                        <td>Date and Time</td>
                    </tr>
                    <?php if ($currentuserlog)
                        foreach ($currentuserlog as $val): ?>
                            <tr class="grid-content">
                                <td><?php echo $val[audit_act] ?></td>
                                <td><?php echo $val[audit_log_time] ?></td>
                            </tr>
                        <?php endforeach; ?>
                </table>
            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>

<script type="text/javascript">

    $('#chkbox_enabled').click(function()
    {
        if($("#chkbox_enabled").is(':checked'))
        {
            $("#user_enable").val("1");
        }
        else
        {
            $("#user_enable").val("0");
        }
    }
);
    $(function()
    {
		
        if ($("#user_enable").val()==="1")
        {
		 
            $("#chkbox_enabled").attr('checked',true);
			
        }
        else
        {
            $("#chkbox_enabled").attr('checked',false);
        }
    });

</script>
</html>