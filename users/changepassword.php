<?php
session_start();
require_once ("../ctrl/ctrl_edituser.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Edit User [Change Password]</title>

        <script language="javascript">
         
            function validate()
            {
                if(document.edituser.user_passworda.value=='')
                {
                    alert('Please input old password.');
                    document.edituser.user_passworda.focus();
                    return false;
                }
             
                if(document.edituser.new_password.value=='')
                {
                    alert('Please input new passowrd.');
                    document.edituser.new_password.focus();
                    return false;
                }
                if(document.edituser.repnew_password.value=='')
                {
                    alert('Please repeat new password.');
                    document.edituser.repnew_password.focus();
                    return false;
                }
                if(document.edituser.repnew_password.value != document.edituser.new_password.value)
                {
                    alert('New Password Did not match');
                    document.edituser.repnew_password.focus();
                    return false;
                }
                    
                return true;
            }
        </script>


        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="edituser" id="edituser" action="/88dbphcrm/users/changepassword.php?act=changepass&user_id=<?php echo $_GET[user_id] ?>" method="post" onsubmit="return validate()">
            <div class="commands">
                <div class="head-label">
                    <h2>Edit [Change Password] : <?php echo "$currentuser[user_firstname]" . " " . "$currentuser[user_lastname]" ?></h2>
                </div><!-- end of add new account -->

                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/users/edit.php?user_id=<?php echo $currentuser['user_id']; ?>'>Cancel</a></li>

                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->


            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                   
                   

                    <tr>
                        <td class="grid-head">*New Password:</td>
                        <td>&nbsp;</td>
                        <td><input type="password" name="new_password" id="new_password" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Repeat New Password:</td>
                        <td>&nbsp;</td>
                        <td><input type="password" name="repnew_password" id="repnew_password" class="required"/></td>
                    </tr>


                </table>

            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>