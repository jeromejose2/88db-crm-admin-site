<?php

session_start();

require_once("../models/tbaging.php");
$tbaging = new TB_AGING();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$accountid = $_GET['account_id'];
if ($accountid) {
    $account = $tbaging->Selectaccount($accountid);
    $accounteditor = $tbaging->Selectaccounteditor($accountid);
    $accountdesigner = $tbaging->Selectaccountdesigner($accountid);
    $accountaging = $tbaging->Selectaccountaging($accountid);
    $accounthistory = $tbaging->Selectaccounthistory($accountid);
}
?>		