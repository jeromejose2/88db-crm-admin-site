<?php

session_start();

require_once("../models/tbaging.php");
$tbaging = new TB_AGING();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$eventid = $_GET['event'];
if ($eventid) {
    $leads = $tbaging->Selectaccountsleads($eventid);
    $event = $tbaging->Selectevent($eventid);
}

$accountbackupv1 = $_GET['accountbackupv1'];
if ($accountbackupv1 == 'true') {
    $accounts = $tbaging->Selectaccountsbackup1();
}
if ($accountbackupv1 == 'false') {
    $accounts = $tbaging->Selectaccountsbackup1not();
}

$accountbackupv2 = $_GET['accountbackupv2'];
if ($accountbackupv2 == 'true') {
    $accounts = $tbaging->Selectaccountsbackup2();
}
if ($accountbackupv2 == 'false') {
    $accounts = $tbaging->Selectaccountsbackup2not();
}



$accountseo = $_GET['accountseo'];
if ($accountseo == 'true') {
    $accounts = $tbaging->Selectaccountsseo();
}
if ($accountseo == 'false') {
    $accounts = $tbaging->Selectaccountsseonot();
}


$accountads = $_GET['accountads'];
if ($accountads == 'true') {
    $accounts = $tbaging->Selectaccountsads();
}
if ($accountads == 'false') {
    $accounts = $tbaging->Selectaccountsadsnot();
}

$accountpackage = $_GET['package'];
$packages = $tbaging->Selectpackages();
$shoptype = $_GET['shoptype'];
if ($accountpackage) {
    $accounts = $tbaging->Selectaccountspackage($accountpackage, $shoptype);
}

$classification = $_GET['class'];
if ($classification) {
    $accounts = $tbaging->Selectaccountsclass($classification, $shoptype);
}
?>		