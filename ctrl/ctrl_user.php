<?php

session_start();

require_once("../models/tbuser.php");
$tbuser = new TB_USER();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$team_id = $_SESSION['team_id'];

if ($role_id == 2) {
    $users = $tbuser->Selectusers2($_SESSION[department_id], $userid);
} elseif ($role_id == 3) {
    $users = $tbuser->Selectusers3($userid);
} else if ($role_id == 5 ) {
    $users = $tbuser->SelectusersTL($_SESSION[department_id], $team_id);
    
} else {
    $users = $tbuser->Selectusers();
    $usersnot = $tbuser->Selectusersnot();
}


if ($_GET[act] == 'delete') {

    $user_id = $_GET[id];

    $tbuser->Deleteusers($user_id);
    header("Location: /88dbphcrm/users/index.php");
    exit();
}
?>		