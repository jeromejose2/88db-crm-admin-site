<?php

session_start();

require_once("../models/tbaging.php");
$tbaging = new TB_AGING();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$dash = $_GET['dash'];
$id = $_GET['designer_id']?$_GET['designer_id'] : $_GET['user_id'];
if ($id) {
    switch ($dash) {
        case 1:
            $accounts = $tbaging->Selectaccountsfordev($id);
            break;
        case 2:
            $accounts = $tbaging->Selectaccountsforundercons($id);
            break;
        case 3:
            $accounts = $tbaging->Selectaccountsforclientrev($id);
            break;
        case 4:
            $accounts = $tbaging->Selectaccountsforinternalrev($id);
            break;
        case 5:
            $accounts = $tbaging->Selectaccountsforredesign($id);
            break;
        case 6:
            $accounts = $tbaging->Selectaccountsforclientupdates($id);
            break;
        case 7:
            $accounts = $tbaging->Selectaccountslinedup($id);
            break;
        case 8:
            $accounts = $tbaging->Selectaccountsrejected($id);
            break;
        case 9:
            $accounts = $tbaging->Selectaccountscsrqa($id);
            break;
        case 10:
            $accounts = $tbaging->Selectaccountsclientapp($id);
            break;
    }
}
?>		