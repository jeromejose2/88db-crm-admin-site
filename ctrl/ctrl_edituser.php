<?php

session_start();

require_once("../models/tbuser.php");
include_once("../functions/connection.php");
$tbuser = new TB_USER();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];



$currentuser = $tbuser->Selectuser($_GET[user_id]);
$currentuserlog = $tbuser->Selectuserlog($_GET[user_id]);
$usermodified = $tbuser->Selectusermodified($_GET[user_id]);


if ($_GET[act] == 'update') {

    $user_id = $_GET[user_id];
    $user_firstname = $_POST[user_firstname];
    $user_lastname = $_POST[user_lastname];
    $user_username = $_POST[user_name];
    $user_email = $_POST[user_email];
    $type_id = $_POST[type_id];
    $team_id = $_POST[team_id];
    $manager_id = $_POST[manager_id];
    $job_descoptions = $_POST[job_descoptions];
    $today = date("Y-m-d H:i:s");

    if ($_SESSION[department_id] == 4) {
        //FOR Admin department
        $user_enabled = $_POST[user_enable];
    } else { //For non-Admin department
        $user_enabled = 1;
    }



    if (!isset($_POST[department_id]) || !isset($_POST[role_id])) {
        $dept_id = $dep_id;

        $rolet_id = $_SESSION['role_id'];
    } else {
        $dept_id = $_POST[department_id];

        $rolet_id = $_POST[role_id];
    }

    $query = "SELECT enabled FROM users where user_id='" . $user_id . "'";
    $result = mysql_query($query, connect());


    //FOR USER'S EMAIL CONFIRMATION
    if (mysql_result($result, 0, 'enabled') != 1) {


        //change this to your email. 
        $to = $user_email;
        $from = '88DBPH CRM Admin';
        $subject = "88DBPH CRM | Account Activation!";

        //begin of HTML message 
        $message = '
		<html> 
		  <body bgcolor="#DCEEFC"> 
		  <p>
		  Hi ' . $user_firstname . ' ' . $user_lastname . ', <br/><br/>
		  
		  Your account in 88DBPH CRM is now activated. Kindly click the link below to login.<br/>
		  <a href="http://172.21.0.51/88dbphcrm/login/">http://172.21.0.51/88dbphcrm/login/</a>
		  </p>
			  
		  </body> 
		</html> 
		';
        //end of message 
        $headers = "From: $from\r\n";
        $headers .= "Content-type: text/html\r\n";

        // now lets send the email. 
        mail($to, $subject, $message, $headers);
        //mail('charles.c@88db.com.ph', $subject, $message, $headers); 
    }



    $data = array(
        'user_firstname' => $user_firstname,
        'user_lastname' => $user_lastname,
        'user_name' => $user_username,
        'user_email' => $user_email,
        'department_id' => $dept_id,
        'user_type' => $type_id,
        'role_id' => $rolet_id,
        'team_id' => $team_id,
        'manager_id' => $manager_id,
        'job_desc' => $job_descoptions,
        'user_modifiedby' => $_SESSION['user_id'],
        'user_modifiedon' => $today,
        'enabled' => $user_enabled
    );

    $tbuser->Updateusers($data, "user_id=$user_id");
    if ($_SESSION['role_id'] == 3) {
        header("Location: /88dbphcrm/");
        exit();
    } else {
        header("Location: /88dbphcrm/users/index.php");
        exit();
    }
}
if ($_GET[act] == 'changepass') {

    $user_id = $_GET[user_id];


    $user_newpass = $_POST[new_password];
    //echo "$user_password" . " " . "$user_oldpass";

    $data = array(
        'user_password' => md5($user_newpass),
    );

    $query = "SELECT * FROM users where user_id='" . $user_id . "'";
    $result = mysql_query($query, connect());
    $row = mysql_fetch_array($result);
    $user_firstname = $row['user_firstname'];
    $user_lastname = $row['user_lastname'];
    $user_email = $row['user_email'];

    $to = $user_email;
    $from = '88DBPH CRM Admin';
    $subject = "88DBPH CRM | Change Password!";

    //begin of HTML message 
    $message = "
		<html> 
		  <body bgcolor='#DCEEFC'> 
		  <p>
		  Hi " . $user_firstname . " " . $user_lastname . ", <br/><br/>
		  
		  Your Account's new password in 88DBPH CRM is <br/><br/>" . $user_newpass . ". <br/><br/>Kindly click the link below to login.<br/>
		  <a href='http://172.21.0.51/88dbphcrm/login/'>http://172.21.0.51/88dbphcrm/login/</a>
		  </p>
			  
		  </body> 
		</html> 
		";
    //end of message 
    $headers = "From: $from\r\n";
    $headers .= "Content-type: text/html\r\n";


    mail($to, $subject, $message, $headers);



    $tbuser->Updateusers($data, "user_id=$user_id");
    header("Location: /88dbphcrm/users/index.php");
    exit();
}
?>		