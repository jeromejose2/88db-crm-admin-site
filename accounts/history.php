<?php
session_start();
require_once ("../ctrl/ctrl_aging.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Account [history - <?php echo $account['account_name'] ?></title>




        <?php include '../header.php'; ?>

    <div class="main-section">

        <div class="commands">
            <div class="head-label">
                <h2>Account [ Aging - <?php echo $account['account_name'] ?> ]</h2> 

            </div><!-- end of add new account -->
            <ul>
                <li><a class="link-button gray" href='/88dbphcrm/accounts/edit.php?account_id=<?php echo $_GET['account_id'] ?>'>Cancel</a></li>
            </ul>

        </div><!-- end of grid-commands -->


        <div class="gen-section">

            <table cellpadding="5" cellspacing="0">
                <tr>
                    <td class="grid-head">Editor : <?php echo $accounteditor['editor'] ?> </td>
                </tr>
                <tr>
                    <td class="grid-head">Designer : <?php echo $accountdesigner['designer'] ?></td>
                </tr>
                <tr>
                    <td class="grid-head">Account Created On : <?php echo $account['account_createdon'] ?></td>
                </tr>
                <tr>
                    <td class="grid-head">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>


            </table>
            <table class="main-grid">
                <tr class="grid-head">
                    <td width="80%">Account history log</td>
                    <td align="right" width="20%">date and time</td>

                </tr>
                <?php if ($accounthistory)
                    foreach ($accounthistory as $val): ?>
                        <tr class="grid-content">
                            <td> <?php echo $val['audit_act'] ?></td>
                            <td align="right"> <?php echo $val['audit_log_time'] ?></td>

                        </tr>
                    <?php endforeach; ?>
            </table>

        </div><!-- end of gen-section -->

    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>