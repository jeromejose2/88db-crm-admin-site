<?php
session_start();
require_once ("../ctrl/ctrl_dashboard.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Accounts</title>






        <?php include '../header.php'; ?>

        <script type="text/javascript">
            $(function() {
                $("#createexcel").click(function()
                {
                    //                    $event_id = <?php echo $event['event_id']; ?>;
                    //                    alert($event_id);
                    window.open ("create_excel.php?event_id="+ $("#event_id").val() +"&event_edate="+$("#event_edate").val()+"&event_name="+$("#event_name").val() );
                    	 
                });
            });
        </script>
        <style>
            .gen-section table tr td {
                width: 110px;
            }
            .gen-section table.main-grid tr.grid-content td:nth-child(2) {
                width: 30px;
            }
            table{
                font-size: 11px!important;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            }
        </style>
    <div class="main-section">

        <div class="commands">
            <div class="head-label">


            </div><!-- end of add new account -->


        </div><!-- end of grid-commands -->


        <div class="gen-section">

            <table cellpadding="5" cellspacing="0">

                <tr>
                    <td class="grid-head">No. of Accounts : 
                        <?php
                        $c = 0;
                        if ($accounts) {

                            foreach ($accounts as $val) {
                                $c++;
                            }
                        }
                        echo $c;
                        ?>    </td>
                </tr>
                <tr>
                    <td class="grid-head">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>


            </table>
            <table class="main-grid">
                <tr class="grid-head">
                    <td>Account</td>
                    <td>Shop Url</td>
                    <td>CMS</td>
                    <td>SEO</td>
                    <td>AE entry</td>
                    <td>Prod entry</td>
                    <td>Last modified on</td>

                    <td>Prev Status</td>
                    <td>Current Status</td>
                    <td>CSR</td>
                    <td>AE</td>
                    <td>Designer</td>
                    <td>Editor</td>
                    <td>
                        Current Status date
                    </td>




                </tr>
                <?php
                if ($accounts)
                    foreach ($accounts as $val):
                        ?>
                        <tr class="grid-content">
        <!--                            <td><?php echo $val['account_id'] ?></td>-->

                            <td> 
                                <a href="/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&account_id=<?php echo $val['account_id'] ?>" target="_blank"

                                   >
                                       <?php echo $val['account_name'] ?>
                                </a>

                            </td>
                            <td> 
                                <?php
                                $shop_url = $val['shop_url'];

                                if ($shop_url) {
                                    if (substr($shop_url, 0, 4) != "http") {
                                        $shop_url_new = "http://" . $shop_url;
                                    } else {
                                        $shop_url_new = $shop_url;
                                    }
                                } else {
                                    $shop_url_new = "";
                                }
                                ?>
                                <a href="<?php echo $shop_url_new ?>" target="_blank"><?php echo(strlen($shop_url_new) > 25 ? "http..." : "") . substr($shop_url_new, -25) ?></a>
                            </td>
                            <td> V.<?php echo $val['account_cmstype'] ?></td>
                            <?php
                            if ($val['account_seo'] == 1) {
                                $seo = '<img src="green.gif">';
                            } else {
                                $seo = '<img src="red.gif">';
                            }
                            if ($val['account_ads'] == 1) {
                                $ads = '<img src="green.gif">';
                            } else {
                                $ads = '<img src="red.gif">';
                            }
                            ?>
                            <td>
                                seo : <?php echo $seo ?> <br>ads : <?php echo $ads ?>
                            </td>
                            <td> <?php echo $val['account_createdon'] ?></td>
                            <td> <?php echo $val['prodentry'] ?></td>
                            <td> <?php echo $val['account_modifiedon'] ?></td>
                            <td> <?php echo $val['prev_status'] ?></td>
                            <td> <?php echo $val['current_status'] ?></td>
                            <td> <?php echo $val['CSR'] ?></td>
                            <td> <?php echo $val['AE'] ?></td>
                            <td> <?php echo $val['DE'] ?></td>
                            <td> <?php echo $val['editor'] ?></td>
                            <td> <?php echo $val['enter_status'] ?></td>
                        </tr>
                    <?php endforeach; ?>
            </table>

        </div><!-- end of gen-section -->

    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>