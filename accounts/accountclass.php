<?php
session_start();
require_once ("../ctrl/ctrl_leads.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Accounts</title>






        <?php include '../header.php'; ?>

        <script type="text/javascript">
            $(function() {
                $("#createexcel").click(function()
                {
                     
                    window.open ("create_excel_class.php?class_id="+ $("#class_id").val() + "&shoptype=" + $("#shop_type").val());
                    	 
                });
                $("#class_id").change(function()
                {
                    var val = $("#class_id").val();
                    var shoptype = <?php
        if ($_GET['shoptype']) {
            echo $_GET['shoptype'];
        } else {
            echo '0';
        }
        ?>;
                    if(val == ''){
                        alert('Please Select Classification!');
                        $("#class_id").val(<?php echo $_GET['class'] ?>)
                        $("#class_id").focus();
                    }
                    else{
                        window.location='/88dbphcrm/accounts/accountclass.php?class=' + $("#class_id").val() + '&shoptype=' + shoptype;
                    }
                });
                
                $("#shop_type").change(function()
                {
                    var shoptype = $("#shop_type").val();
                    var val = <?php echo $_GET['class']; ?>;
                    
                    window.location='/88dbphcrm/accounts/accountclass.php?class=' + val + '&shoptype=' + shoptype;
                    
                });
            });
        </script>
        <style>
            .gen-section table tr td {
                width: 10px;
            }
            .gen-section table.main-grid tr.grid-content td:nth-child(2) {
                width: 30px;
            }
            table{
                font-size: 11px!important;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            }
        </style>
    <div class="main-section">

        <div class="commands">
            <div class="head-label">


            </div><!-- end of grid-commands -->


            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">

                    <tr>
                        <td class="grid-head">No. of Accounts : 
                            <?php
                            $c = 0;
                            $c1 = 0;
                            $c2 = 0;
                            $c3 = 0;
                            if ($accounts) {

                                foreach ($accounts as $val) {
                                    if ($val[shop_type] == 1) {
                                        $c1++;
                                    }
                                    if ($val[shop_type] == 2) {
                                        $c2++;
                                    }
                                    if ($val[shop_type] == 0) {
                                        $c3++;
                                    }
                                    $c++;
                                }
                            }
                            echo $c;
                            ?><br/>
                            88DB: <?php echo $c1; ?><br/>
                            OpenRice: <?php echo $c2; ?><br/>
                            No tag: <?php echo $c3; ?>
                        </td>
                        <td class="grid-head" style="width: 250px;">
                            Select Class.: 
                            <select name="class_id" id="class_id">
                                <option value="">Select Classification</option>

                                <option value="1" <?php
                            if ($_GET['class'] == 1) {
                                echo 'selected';
                            }
                            ?>>
                                    VIP
                                </option>
                                <option value="2" <?php
                                        if ($_GET['class'] == 2) {
                                            echo 'selected';
                                        }
                            ?>>
                                    Irate
                                </option>
                                <option value="3" <?php
                                        if ($_GET['class'] == 3) {
                                            echo 'selected';
                                        }
                            ?>>
                                    Cooperative
                                </option>
                                <option value="4" <?php
                                        if ($_GET['class'] == 4) {
                                            echo 'selected';
                                        }
                            ?>>
                                    Aging
                                </option>
                                <option value="5" <?php
                                        if ($_GET['class'] == 5) {
                                            echo 'selected';
                                        }
                            ?>>
                                    Normal
                                </option>
                                <option value="6" <?php
                                        if ($_GET['class'] == 6) {
                                            echo 'selected';
                                        }
                            ?>>
                                    Non-cooperative
                                </option>

                            </select>
                            Shop Type: 
                            <select name="shop_type" id="shop_type">
                                <option value="0">Select Shop Type</option>
                                <option value="1" 
                                <?php
                                if ($_GET['shoptype'] == 1) {
                                    echo 'selected';
                                }
                                ?>
                                        >
                                    88DB
                                </option>
                                <option value="2"
                                <?php
                                if ($_GET['shoptype'] == 2) {
                                    echo 'selected';
                                }
                                ?>
                                        >
                                    OpenRice
                                </option>
                            </select>
                        </td>
                        <td align="right">
                            <input type="submit" value="Create Excel" id="createexcel">

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>


                </table>
                <table class="main-grid">
                    <tr class="grid-head">
                        <td>ID</td>
                        <td>Account</td>
                        <td>AE</td>
                        <td>CSR</td>
                        <td>Editor</td>
                        <td>Designer</td>
                        <td>Status</td>
                        <td align="center">Classification</td>
                        <td>Created On</td>
                        <td>Shop Type</td>



                    </tr>
                    <?php
                    if ($accounts)
                        foreach ($accounts as $val):
                            ?>
                            <tr class="grid-content">
                                <td><?php echo $val['account_id'] ?></td>

                                <td> 
                                    <a href="/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&account_id=<?php echo $val['account_id'] ?>" target="_blank">
                                        <?php echo $val['account_name'] ?>
                                    </a>

                                </td>
                                <td> <?php echo $val['AE'] ?></td>
                                <td> <?php echo $val['CSR'] ?></td>
                                <td> <?php echo $val['editor'] ?></td>
                                <td> <?php echo $val['DE'] ?></td>
                                <td> <?php echo $val['status_name'] ?></td>
                                <td align="center"> 
                                    <?php
                                    switch ($val['account_classification']) {
                                        case 1:
                                            echo 'VIP';
                                            break;
                                        case 2:
                                            echo 'Irate';
                                            break;
                                        case 3:
                                            echo 'Cooperative';
                                            break;
                                        case 4:
                                            echo 'Aging';
                                            break;
                                        case 5:
                                            echo 'Normal';
                                            break;
                                        case 6:
                                            echo 'Non-cooperative';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td> <?php echo $val['account_createdon'] ?></td>
                                <td> 
                                    <?php
                                    if ($val['shop_type'] == 1) {
                                        echo '88DB';
                                    } elseif ($val['shop_type'] == 2) {
                                        echo 'OpenRice';
                                    } else {
                                        echo '';
                                    }
                                    ?>

                                </td>




                            </tr>
                        <?php endforeach; ?>
                </table>

            </div><!-- end of gen-section -->

        </div><!-- end of main section -->
    </div><!-- end of main container -->

</body>
</html>