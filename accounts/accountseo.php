<?php
session_start();
require_once ("../ctrl/ctrl_leads.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Accounts</title>






        <?php include '../header.php'; ?>

        <script type="text/javascript">
            $(function() {
                $("#createexcel").click(function()
                {
                    //                    $event_id = <?php echo $event['event_id']; ?>;
                    //                    alert($event_id);
                    window.open ("create_excel.php?event_id="+ $("#event_id").val() +"&event_edate="+$("#event_edate").val()+"&event_name="+$("#event_name").val() );
                    	 
                });
            });
        </script>

    <div class="main-section">

        <div class="commands">
            <div class="head-label">
<!--                <h2>Event [ <?php echo $event['event_name'] ?> ]</h2> -->

            </div><!-- end of add new account -->
            <!--            <ul>
                            <li><a class="link-button gray" href='/88dbphcrm/events/'>Cancel</a></li>
                            <li><input type="submit" value="Create Excel" id="createexcel"></li>
                        </ul>-->

        </div><!-- end of grid-commands -->


        <div class="gen-section">

            <table cellpadding="5" cellspacing="0">

                <tr>
                    <td class="grid-head">No. of Accounts : 
                        <?php
                        $c = 0;
                        if ($accounts) {

                            foreach ($accounts as $val) {
                                $c++;
                            }
                        }
                        echo $c;
                        ?>    </td>
                </tr>
                <tr>
                    <td class="grid-head">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>


            </table>
            <table class="main-grid">
                <tr class="grid-head">
                    <td>ID</td>
                    <td>Account</td>
                    <td>AE</td>
                    <td>Status</td>
                    <td>SEO</td>



                </tr>
                <?php
                if ($accounts)
                    foreach ($accounts as $val):
                        ?>
                        <tr class="grid-content">
                            <td><?php echo $val['account_id'] ?></td>

                            <td> 
                                <a href="/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&account_id=<?php echo $val['account_id'] ?>" target="_blank"

                                   >
                                       <?php echo $val['account_name'] ?>
                                </a>

                            </td>
                            <td> <?php echo $val['AE'] ?></td>
                            <td> <?php echo $val['status_name'] ?></td>
                            <?php
                            if ($val['account_seo'] == 1) {
                                $seo = '<img src="green.gif">';
                            } else {
                                $seo = '<img src="red.gif">';
                            }
                            if ($val['account_ads'] == 1) {
                                $ads = '<img src="green.gif">';
                            } else {
                                $ads = '<img src="red.gif">';
                            }
                            ?>
                            <td>
                                seo : <?php echo $seo ?> <br>ads : <?php echo $ads ?>
                            </td>



                        </tr>
                    <?php endforeach; ?>
            </table>

        </div><!-- end of gen-section -->

    </div><!-- end of main section -->
</div><!-- end of main container -->

</body>
</html>