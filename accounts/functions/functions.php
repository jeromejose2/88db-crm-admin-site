<?php
error_reporting(null);
session_start();

function StatusBox($sid, $role_id, $department_id) {

    switch ($role_id) {
        case 1:
            $query = "SELECT status_id, status_name, department_id FROM statuses ORDER BY status_name ASC";
            break;

        case 2:
            $query = "SELECT 
							status_id, 
							status_name,
							department_id						
							FROM statuses 
							WHERE department_id =  '" . $department_id . "' ORDER BY status_name ASC
							";
            break;

        case 3:
            $query = "SELECT 
							status_id, 
							status_name, 
							department_id
							FROM statuses
							WHERE department_id =  '" . $department_id . "' ORDER BY status_name ASC
							";
            break;
        case 5:
            $query = "SELECT 
							status_id, 
							status_name,
							department_id						
							FROM statuses 
							WHERE department_id =  '" . $department_id . "' ORDER BY status_name ASC
							";
            break;
    }

    //$query = "SELECT status_id, status_name, department_id FROM statuses ORDER BY status_name ASC";

    $result = mysql_query($query, connect());
    $statusoptions = "";
    while ($row = mysql_fetch_array($result)) {
        $statusid = $row["status_id"];
        $statusname = $row["status_name"];
        if ($sid) {
            if ($statusid == $sid) {
                $statusoptions .= '<option value="' . $statusid . '" selected="selected">' . $statusname . "</option>";
            } else {
                $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
            }
        } else {
            $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
        }
    }
    return $statusoptions;
}

function UserBox($department_id, $role_id, $user_id, $selectd_user) {
    switch ($role_id) {
        case 1:
            $query = "SELECT 
					user_id, 
					CONCAT(user_firstname,' ',user_lastname) as user_fullname 
					FROM users WHERE enabled = 1 ORDER BY user_fullname ASC";
            break;
        case 2:
            $query = "SELECT 
					user_id, 
					CONCAT(user_firstname,' ',user_lastname) as user_fullname 
					FROM users 
					WHERE department_id =  '" . $department_id . "' AND enabled = 1 ORDER BY user_fullname ASC
					";
            break;
        case 3:
            $query = "SELECT 
					user_id, 
					CONCAT(user_firstname,' ',user_lastname) as user_fullname 
					FROM users 
					WHERE user_id =  '" . $user_id . "' AND enabled = 1 ORDER BY user_fullname ASC
					";
            break;
        case 5:
            $query = "SELECT 
					user_id, 
					CONCAT(user_firstname,' ',user_lastname) as user_fullname 
					FROM users 
					WHERE department_id =  '" . $department_id . "' AND team_id =  '" . $_SESSION['team_id'] . "' AND enabled = 1 ORDER BY user_fullname ASC
					";
            break;
    }
    /*
      $query = "SELECT
      user_id,
      CONCAT(user_firstname,' ',user_lastname) as user_fullname
      FROM users WHERE enabled = 1 ORDER BY user_fullname ASC";
     */
    $result = mysql_query($query, connect());
    $useroptions = "";
    while ($row = mysql_fetch_array($result)) {
        $userid = $row["user_id"];
        $userfullname = $row["user_fullname"];
        if ($selectd_user) {
            if ($userid == $selectd_user) {
                $useroptions .= '<option value="' . $userid . '" selected="selected">' . $userfullname . "</option>";
            } else {
                $useroptions .= '<option value="' . $userid . '">' . $userfullname . "</option>";
            }
        } else {
            $useroptions .= '<option value="' . $userid . '">' . $userfullname . "</option>";
        }
    }

    return $useroptions;
}

function NextPage($current_url, $next, $current_page, $search, $sid) {

    $string = $current_url;
    $find_search = 'search=';
    $find_page = 'page=';


    //$next_page_url = str_replace(find,replace,$string,count)


    if ((strpos($string, $find_search) === false) || (strpos($string, $find_page) === false)) {
        $new_url = "<a href='" . $current_url . "?page=$next'> Next-></a>";
    }
    //else if (strpos($string, $find_page)===true)
    //{
    //	$new_url = "<a href='#'> Next-></a>";
    //}
    else {
        //$new_url = "<a href='".$current_url."&page=$next'> Next-></a>";
        $new_url = "<a href='#'> Next-></a>";
    }


    return $new_url;
}

function CategoryBox($cat) {



    $query = "SELECT category_id, category_name FROM categories ORDER BY category_name ASC";
    $result = mysql_query($query, connect());
    $categoryoptions = "";
    while ($row = mysql_fetch_array($result)) {
        $categoryid = $row["category_id"];
        $categoryname = $row["category_name"];
        if ($cat) {
            if ($categoryid == $cat) {
                $categoryoptions .= '<option value="' . $categoryid . '" selected="selected">' . $categoryname . "</option>";
            } else {
                $categoryoptions .= '<option value="' . $categoryid . '">' . $categoryname . "</option>";
            }
        } else {
            $categoryoptions .= '<option value="' . $categoryid . '">' . $categoryname . "</option>";
        }
    }

    return $categoryoptions;
}

function SourceBox($source) {

    $query = "SELECT source_id, source_name FROM sources ORDER BY source_name ASC";
    $result = mysql_query($query, connect());
    $sourceoptions = "";
    while ($row = mysql_fetch_array($result)) {
        $sourceid = $row["source_id"];
        $sourcename = $row["source_name"];
        if ($source) {
            if ($sourceid == $source) {
                $sourceoptions .= '<option value="' . $sourceid . '" selected="selected">' . $sourcename . "</option>";
            } else {
                $sourceoptions .= '<option value="' . $sourceid . '">' . $sourcename . "</option>";
            }
        } else {
            $sourceoptions .= '<option value="' . $sourceid . '">' . $sourcename . "</option>";
        }
    }

    return $sourceoptions;
}

function GetDesignersName($e) {



    $query = "SELECT user_firstname, user_lastname FROM users where user_id='$e' order by concat(user_firstname,' ',user_lastname)";
    $result = mysql_query($query, connect());

    $row = mysql_fetch_array($result);
    $designername = $row['user_firstname'] . " " . $row['user_lastname'];

    return $designername;
}

function GetNumOfData($condition, $department_id) {
    if ($department_id == 1) {
        /* previous query of production department
          $fquery = "SELECT
          Count(*) as num,
          a.*,
          s.status_name,
          concat(ed.user_firstname,' ',ed.user_lastname) as editor,
          concat(ae.user_firstname,' ',ae.user_lastname) as AE,
          sr.source_name
          FROM accounts a
          LEFT JOIN statuses s ON a.status_id = s.status_id
          LEFT JOIN users ed ON a.shop_editor = ed.user_id
          LEFT JOIN users ae ON a.account_createdby = ae.user_id
          LEFT JOIN sources sr ON a.source_id = sr.source_id

          ".$condition." AND a.account_paid = 1 ORDER BY a.account_createdon DESC";
         */
        $fquery = "SELECT 
						Count(*) as num, 
						a.*, 
						s.status_name, 
						concat(ed.user_firstname,' ',ed.user_lastname) as editor,
						concat(ae.user_firstname,' ',ae.user_lastname) as AE,
						sr.source_name 
						FROM accounts a 
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN users ed ON a.shop_editor = ed.user_id
						LEFT JOIN users ae ON a.account_createdby = ae.user_id
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
						LEFT JOIN audit au ON a.account_id = au.account_id 
						" . $condition . " AND au.after_status_id = 13 ORDER BY a.account_createdon DESC";
    }
    if ($department_id == 2) {
        $fquery = "SELECT 
						Count(*) as num, 
						a.*, 
						s.status_name, 
						c.category_name, 
						sr.source_name 
						FROM accounts a 
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN categories c ON a.category_id = c.category_id 
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						" . $condition . "  ORDER BY a.account_createdon DESC";
//                echo $fquery;
    }
    if ($department_id == 3) {

        $fquery = "SELECT 
						count(*) as num,
						a.account_name
						FROM `audit` au
						LEFT JOIN accounts a ON au.account_id = a.account_id
						LEFT JOIN categories c ON a.category_id = c.category_id
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
						" . $condition . " AND au.after_status_id = 13 ORDER BY a.account_createdon DESC";
    }
    if ($department_id == 4) {
        $fquery = "SELECT 
						Count(*) as num, 
						a.*, 
						s.status_name, 
						c.category_name, 
						sr.source_name 
						FROM accounts a 
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN categories c ON a.category_id = c.category_id 
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						" . $condition . " ORDER BY a.account_createdon DESC";
    }
//echo $fquery;
    
    $total_pages = mysql_fetch_array(mysql_query($fquery, connect()));
    $total_pages = $total_pages[num];

    return $total_pages;
}

function GetAllOfData($condition, $department_id) {
    if ($department_id != 1) {
        $query = "SELECT 
								a.*, 
								s.status_name, 
								c.category_name, 
								sr.source_name 
								FROM accounts a 
								LEFT JOIN statuses s ON a.status_id = s.status_id 
								LEFT JOIN categories c ON a.category_id = c.category_id 
								LEFT JOIN sources sr ON a.source_id = sr.source_id 
								LEFT JOIN users r ON a.account_createdby = r.user_id 
								" . $condition . " ORDER BY a.account_createdon DESC LIMIT $start, $limit";
    } else {
        $query = "SELECT 
								a.*, s.status_name, 
								concat(ed.user_firstname,' ',ed.user_lastname) as editor,
								concat(ae.user_firstname,' ',ae.user_lastname) as AE,
                                                                concat(de.user_firstname,' ',de.user_lastname) as DE,
								sr.source_name 
								FROM accounts a 
								LEFT JOIN statuses s ON a.status_id = s.status_id 
								LEFT JOIN users ed ON a.shop_editor = ed.user_id
								LEFT JOIN users ae ON a.account_createdby = ae.user_id
                                                                LEFT JOIN users de ON a.shop_designer = de.user_id
								LEFT JOIN sources sr ON a.source_id = sr.source_id 
								
								" . $condition . " AND a.account_paid = 1 ORDER BY a.account_createdon DESC LIMIT $start, $limit";
    }


    echo($query);
    $dresult = mysql_query($query, connect());
    //$i = 0;
    //$j = 1;
    //$num = mysql_num_rows($dresult);

    while ($idrow = mysql_fetch_row($dresult)) {
        $dataresult [] = $idrow;
    }

    if ($department_id != 1) {

        for ($i = 0; $i < count($dataresult); $i++) {
            $table .= "<tr>";
            for ($j = 0; $j < count($dataresult[$i]); $j++) {
                $table .= "<td >{$dataresult[$i][$j]}</td>";
            }


            $table .= "</tr>";
        }
    } else {
        for ($i = 0; $i < count($dataresult); $i++) {
            $table .= "<tr>";
            for ($j = 0; $j < count($dataresult[$i]); $j++) {
                $table .= "<td >{$dataresult[$i][$j]}</td>";
            }


            $table .= "</tr>";
        }
    }

    return $table;
}

function GetNumOfAccountsForDeletion($condition) {
    $fquery = "SELECT 
							count(*) as num,
							a.*, 
							concat(ae.user_firstname,' ',ae.user_lastname) as AE
							FROM accounts a 
							LEFT JOIN users ae ON a.account_createdby = ae.user_id
							LEFT JOIN sources sr ON a.source_id = sr.source_id 
							" . $condition . " AND a.status_id = 23 ORDER BY a.account_createdon DESC";

    $total_pages = mysql_fetch_array(mysql_query($fquery, connect()));
    $total_pages = $total_pages[num];

    return $total_pages;
}

function TrashAccounts($user, $account_id) {
    $N = count($account_id);
    for ($i = 0; $i < $N; $i++) {


        $query = "INSERT INTO trash_accounts
						(
						account_id, account_name, account_contactperson, account_telephone, account_mobile, account_fax, account_email, account_address, category_id, source_id, event_id, status_id, declined_status_id, declined_status_reason, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, shop_domainstatus, shop_cmsstatus, shop_designer, shop_editor, account_contractnumber, account_contractamount, account_paid, account_createdby, account_createdon, account_modifiedby, account_modifiedon,account_deletedby
						) 
						
						SELECT 
								account_id, account_name, account_contactperson, account_telephone, account_mobile, account_fax, account_email, account_address, category_id, source_id, event_id, status_id, declined_status_id, declined_status_reason, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, shop_domainstatus, shop_cmsstatus, shop_designer, shop_editor, account_contractnumber, account_contractamount, account_paid, account_createdby, account_createdon, account_modifiedby, account_modifiedon, '" . $user . "'
								
						FROM `accounts`
						WHERE `account_id` = '" . $account_id[$i] . "'";
        $result = mysql_query($query, connect());

        $query = "INSERT INTO trash_addon
						(
						addon_id, account_id, addon_description, addon_amount, addon_createdby, addon_createdon, addon_modifiedby, addon_modifiedon,addon_deletedby
						) 
						
						SELECT 
								addon_id, account_id, addon_description, addon_amount, addon_createdby, addon_createdon, addon_modifiedby, addon_modifiedon, '" . $user . "'
								
						FROM `addon`
						WHERE `account_id` = '" . $account_id[$i] . "'";
        $result = mysql_query($query, connect());

        $query = "INSERT INTO trash_audit
						(
						audit_id, account_id, before_status_id, after_status_id, audit_createdby, audit_createdon, audit_deletedby
						) 
						
						SELECT 
								audit_id, account_id, before_status_id, after_status_id, audit_createdby, audit_createdon, '" . $user . "'
								
						FROM `audit`
						WHERE `account_id` = '" . $account_id[$i] . "'";
        $result = mysql_query($query, connect());

        $query = "INSERT INTO trash_payments
						(
						payment_id, account_id, payment_amount, payment_createdby, payment_createdon, payment_modifiedon, payment_deletedby
						) 
						
						SELECT 
								payment_id, account_id, payment_amount, payment_createdby, payment_createdon, payment_modifiedon, '" . $user . "'
								
						FROM `payments`
						WHERE `account_id` = '" . $account_id[$i] . "'";
        $result = mysql_query($query, connect());

        $query = "INSERT INTO trash_remarks
						(
						remark_id, remark_remarks, remark_type, remark_uid, remark_createdby, remark_createdon, remark_modifiedby, remark_modifiedon, remark_deletedby
						) 
						
						SELECT 
								remark_id, remark_remarks, remark_type, remark_uid, remark_createdby, remark_createdon, remark_modifiedby, remark_modifiedon, '" . $user . "'
								
						FROM `remarks`
						WHERE `account_id` = '" . $account_id[$i] . "'";
        $result = mysql_query($query, connect());
    }
}

function SelectedUser($sales) {

    $query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as sales FROM users WHERE (department_id = 2 or department_id = 4) AND enabled = 1 ";
    $result = mysql_query($query, connect());
    $select = "";
    while ($row = mysql_fetch_array($result)) {
        $salesid = $row["user_id"];
        $salesname = $row["sales"];
        if ($salesid == $sales) {

            $select .= '<option value="' . $salesid . '" selected="selected" >' . $salesname . "</option>";
        } else {
            $select .= '<option value="' . $salesid . '">' . $salesname . "</option>";
        }
    }
    return $select;
}

function CountTransferAccount($userfrom) {
    $fquery = "SELECT 
					COUNT(account_id) as num
					FROM `accounts`
					WHERE `account_createdby` = '" . $userfrom . "'				";

    $total_pages = mysql_fetch_array(mysql_query($fquery, connect()));
    $total_pages = $total_pages[num];

    return $total_pages;
}

function GetSalesName($user) {
    $query = "SELECT concat(user_firstname,' ',user_lastname) as name FROM users WHERE department_id = 2 AND enabled = 1 AND user_id = '" . $user . "'";
    $result = mysql_query($query, connect());
    $createdby = mysql_result($result, 0, 'name');

    return $createdby;
}

function TransferAccount($userfrom, $userto, $transferedby) {
    $query = "INSERT INTO accounts_transfer_logs
						(
						account_id, account_name, account_contactperson, account_telephone, account_mobile, account_fax, account_email, account_address, category_id, source_id, event_id, status_id, declined_status_id, declined_status_reason, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, shop_domainstatus, shop_cmsstatus, shop_designer, shop_editor, account_contractnumber, account_contractamount, account_paid, account_createdby, account_createdon, account_modifiedby, account_modifiedon,account_transferedto,account_transferedby
						) 
						
						SELECT 
								account_id, account_name, account_contactperson, account_telephone, account_mobile, account_fax, account_email, account_address, category_id, source_id, event_id, status_id, declined_status_id, declined_status_reason, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, shop_domainstatus, shop_cmsstatus, shop_designer, shop_editor, account_contractnumber, account_contractamount, account_paid, account_createdby, account_createdon, account_modifiedby, account_modifiedon, '" . $userto . "','" . $transferedby . "'
								
						FROM `accounts`
						WHERE `account_createdby` = '" . $userfrom . "'";
    $result = mysql_query($query, connect());

    $query = "UPDATE accounts SET account_createdby='" . $userto . "' where account_createdby ='" . $userfrom . "'";
    $result = mysql_query($query, connect());
}

?>