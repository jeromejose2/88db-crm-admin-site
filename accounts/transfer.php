<?php 
include_once ('functions/functions.php');
include_once ('functions/connection.php');



session_start();
$_SESSION['title_page']  = "Accounts";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$department_id = $_SESSION['department_id'];

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

$user1 = $_POST['user_from'];
$user2 = $_POST['user_to'];

if ($isPostBack) 
{
	$name1 = GetSalesName($user1);
	$name2 = GetSalesName($user2);
	$number_of_accounts = CountTransferAccount($user1);
	TransferAccount($user1,$user2,$userid);
}




	
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Transfer Accounts</title>

<?php include '../header.php'; ?>

    <div class="main-section">
	
        <div class="commands">
            
            <div class="head-label">
                <h2>Transfer Accounts</h2>
            </div><!-- end of add new account -->

        </div><!-- end of grid-commands -->

        <div style="height: 100%; width: 100%; border: 1px solid #DDD; margin-bottom: 15px; padding-top: 15px;">
	
			
			
			<div style="margin-left:15px;padding-top:5px;padding-bottom:15px;">
				<form name="form1" id="form1" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="post">
							Transfer all accounts of 
							
								<select name='user_from' id='user_from' class="required" >
									<option value="">Select User</option>
									<?php echo SelectedUser($_POST['user_from']); ?>	
									
								</select> 
								
							to  
								<select name='user_to' id='user_to' class="required" >
									<option value="">Select User</option>
									<?php echo SelectedUser($_POST['user_to']); ?>	
								</select>
								
								<input type="submit" value="Transfer" id="view_report">
				</form>
				<div>
				<?php 
				if(isset($_POST['user_to']) || isset($_POST['user_from']))
				{
					echo "You have successfully transferred ".$number_of_accounts." account(s) of ".$name1." to ".$name2;
				}
				?>
				
				</div>
			</div>
        </div>
    </div><!-- end of main-section -->

</div><!-- end of main-container -->
</body>

<script type="text/javascript">
     $(document).ready(function() {
	 
		$("#form1").validate();
		
		$("#form1").submit(function() {
			if(($("label.error").is(":visible")))
				{
					return false;
				}
				else
				{	
					if($("#user_from").val() === $("#user_to").val())
					{
						alert("You cannot select the same user account");
						return false;
					}
					else
					{
					var r = confirm ("Are you sure you want to transfer all of the account(s)?");
				
						if(r)
						{
							return true;
						}
						else
						{
						return false;
						}
					
					
					}
						
				}
		});
    });
 
</script>
</html>