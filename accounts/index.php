<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');



session_start();
$_SESSION['title_page'] = "Accounts";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$department_id = $_SESSION['department_id'];

if (isset($_POST['user'])) {

    $_SESSION['user_search'] = $_POST['user'];
}

if (isset($_POST['sid'])) {

    $_SESSION['sid'] = $_POST['sid'];
}

if (isset($_POST['search'])) {

    $_SESSION['search'] = $_POST['search'];
}
if (isset($_GET['page'])) {

    $_SESSION['page'] = $_GET['page'];
}
connect();
$condition = "";
//if (!empty($_REQUEST['cat'])) $catquery = "and a.category_id='". $_REQUEST['cat'] ."'";
if (!empty($_SESSION['source']))
    $sourcequery = "and a.source_id='" . $_SESSION['source'] . "'";


if ($department_id == 1) {
    if (!empty($_SESSION['user_search']))
        $userquery = ' and (a.shop_designer = "' . $_SESSION['user_search'] . '" or a.shop_editor="' . $_SESSION['user_search'] . '") ';
}
else if ($department_id == 2) {
    if (!empty($_SESSION['user_search']))
        $userquery = " and a.account_createdby='" . $_SESSION['user_search'] . "'";
}
else {
    if (!empty($_SESSION['user_search']))
        $userquery = ' and a.account_createdby="' . $_SESSION['user_search'] . '" or a.shop_designer = "' . $_SESSION['user_search'] . '" or a.shop_editor="' . $_SESSION['user_search'] . '" ';
}



switch ($role_id) {
    case 1:
        if (!empty($_SESSION['sid'])) {
            $condition .= ' WHERE a.status_id = ' . $_SESSION['sid'] . ' and a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } elseif ($department_id == 3) {
            $condition = ' WHERE a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"';
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } else {
            $condition .= ' WHERE  a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        }


        break;
    case 2:
        if ($department_id == 1) {
            $condition = ' WHERE a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' and au.after_status_id = ' . $_SESSION['sid'] . ' AND a.status_id = ' . $_SESSION['sid'] . ' ';
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } elseif ($department_id == 3) {
            $condition = ' WHERE a.account_name like "%' . $_SESSION['search'] . '%"';
            if (!empty($_SESSION['sid'])) {
                $condition .= ' and au.after_status_id = ' . $_SESSION['sid'] . ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } else {

            $condition = ' WHERE r.department_id = ' . $_SESSION['department_id'] . ' and r.manager_id = ' . $_SESSION['user_id'] . ' and a.account_name like "%' . $_SESSION['search'] . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' and au.after_status_id = ' . $_SESSION['sid'] . ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        }
        break;
    case 3:
        if ($department_id == 2) {
            $condition = ' WHERE a.account_createdby = ' . $userid . ' and a.account_name like "%' . $_SESSION['search'] . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'] . ' ';
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } elseif ($department_id == 3) {
            $condition = ' WHERE a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"';
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } else {
            $condition = ' WHERE a.shop_designer = ' . $userid . ' and a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'] . ' ';
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        }
        break;
    case 5:
        if ($department_id == 1) {
            $condition = ' WHERE a.account_name like "%' . mysql_real_escape_string($_SESSION['search']) . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'] . ' ';
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } elseif ($department_id == 3) {
            $condition = ' WHERE a.account_name like "%' . $_SESSION['search'] . '%"';
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        } else {

            $condition = ' WHERE r.department_id = ' . $_SESSION['department_id'] . ' and r.team_id = ' . $_SESSION['team_id'] . ' and a.account_name like "%' . $_SESSION['search'] . '%"' . $catquery . $sourcequery . $userquery;
            if (!empty($_SESSION['sid'])) {
                $condition .= ' AND a.status_id = ' . $_SESSION['sid'];
            }
            if (!empty($_SESSION['dashboard_shoptype'])) {
                $condition .= ' AND a.shop_type = ' . $_SESSION['dashboard_shoptype'];
            }
            if (!empty($_SESSION['dashboard_shopcsr'])) {
                $condition .= ' AND a.shop_csr = ' . $_SESSION['dashboard_shopcsr'];
            }
        }
        break;
}


// How many adjacent pages should be shown on each side?
$adjacents = 3;

$total_pages = GetNumOfData($condition, $department_id);




/* Setup vars for query. */
$req_limit = $_REQUEST['limit'];
$page = $_SESSION['page'];
if ($limit)
    $limit = $req_limit;    //how many items to show per page
else
    $limit = 25;

if ($page)
    $start = ($page - 1) * $limit;    //first item to display on this page
else
    $start = 0;



if ($department_id == 1) { /*
  $query = "SELECT
  a.*, s.status_name,
  concat(ed.user_firstname,' ',ed.user_lastname) as editor,
  concat(ae.user_firstname,' ',ae.user_lastname) as AE,
  sr.source_name
  FROM accounts a
  LEFT JOIN statuses s ON a.status_id = s.status_id
  LEFT JOIN users ed ON a.shop_editor = ed.user_id
  LEFT JOIN users ae ON a.account_createdby = ae.user_id
  LEFT JOIN sources sr ON a.source_id = sr.source_id
  ".$condition." AND a.account_paid = 1 ORDER BY a.account_createdon DESC LIMIT $start, $limit";
  inalis ko ung AND  a.account_paid = 1
 */
    $query = "SELECT 
						a.*, 
                                                s.status_id,
						s.status_name as current_status, 
                                                                                     prev.status_name AS prev_status,
                                                                                     au.audit_createdon AS enter_status ,
						concat(ed.user_firstname,' ',ed.user_lastname) as editor,
						concat(ae.user_firstname,' ',ae.user_lastname) as AE,
                                                                                    concat(de.user_firstname,' ',de.user_lastname) as DE,
                                                                                    concat(csr.user_firstname,' ',csr.user_lastname) as CSR,
						sr.source_name , au.before_status_id, au.after_status_id ,max(au.audit_createdon) AS enter_status
						FROM accounts a 
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN users ed ON a.shop_editor = ed.user_id
						LEFT JOIN users ae ON a.account_createdby = ae.user_id
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
                                                                                    LEFT JOIN users de ON a.shop_designer = de.user_id
                                                                                    LEFT JOIN users csr ON a.shop_csr = csr.user_id
						LEFT JOIN audit au ON a.account_id = au.account_id 
                                                

                                                                                    LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
                                                                                    LEFT OUTER JOIN audit au2 ON 
                                                                                    (a.account_id = au2.account_id 
                                                                                    AND 
                                                                                    (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
                                                                                    AND au.account_id < au.account_id))

						" . $condition . " AND au2.account_id IS NULL    AND a.status_id =au.after_status_id  AND  a.account_paid = 1  GROUP BY a.account_id ORDER BY a.account_classification,enter_status ASC  LIMIT $start, $limit";
}
if ($department_id == 2) {
    $query = "SELECT 
							a.*, 
							s.status_name, 
							c.category_name, 
							sr.source_name,
                                                        concat(r.user_firstname,' ',r.user_lastname) as AE
							FROM accounts a 
							LEFT JOIN statuses s ON a.status_id = s.status_id 
							LEFT JOIN categories c ON a.category_id = c.category_id 
							LEFT JOIN sources sr ON a.source_id = sr.source_id 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							" . $condition . " 
                                                        GROUP BY a.account_id ORDER BY a.account_createdon DESC LIMIT $start, $limit";
//    echo($query);
}
if ($department_id == 3) {
    $query = "
						SELECT 
						a.account_id,
						a.account_name,
						s.status_name, 
						c.category_name, 
						sr.source_name 
						FROM audit au
						LEFT JOIN accounts a ON au.account_id = a.account_id
						LEFT JOIN categories c ON a.category_id = c.category_id
						LEFT JOIN statuses s ON a.status_id = s.status_id 
						LEFT JOIN sources sr ON a.source_id = sr.source_id 
						" . $condition . " AND au.after_status_id = 13 GROUP BY a.account_id ORDER BY a.account_createdon DESC LIMIT $start, $limit";
}
if ($department_id == 4) {
    $query = "SELECT 
							a.*, 
							s.status_name, 
							c.category_name, 
                                                        concat(ed.user_firstname,' ',ed.user_lastname) as editor,
                                                        concat(ae.user_firstname,' ',ae.user_lastname) as AE,
                                                        concat(de.user_firstname,' ',de.user_lastname) as DE,
                                                        concat(csr.user_firstname,' ',csr.user_lastname) as CSR,
                                                         
							sr.source_name 
                                                        
							FROM accounts a 
							LEFT JOIN statuses s ON a.status_id = s.status_id 
							LEFT JOIN categories c ON a.category_id = c.category_id 
							LEFT JOIN sources sr ON a.source_id = sr.source_id 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
                                                        LEFT JOIN users ae ON a.account_createdby = ae.user_id
                                                        LEFT JOIN users de ON a.shop_designer = de.user_id
                                                        LEFT JOIN users csr ON a.shop_csr = csr.user_id
                                                        LEFT JOIN users ed ON a.shop_editor = ed.user_id
							" . $condition . " ORDER BY a.account_classification,a.account_createdon DESC LIMIT $start, $limit";
}


//echo($query);
$result = mysql_query($query);
$i = 0;
$j = 1;
$num = mysql_num_rows($result);

//$data = GetAllOfData($condition,$department_id);

/* Setup page vars for display. */
if ($page == 0)
    $page = 1;
//if no page var is given, default to 1.
//next page is page + 1
$lastpage = ceil($total_pages / $limit);  //lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;      //last page minus 1
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
            <title>Accounts</title>

            <?php include '../header.php'; ?>
            <?php if ($department_id == 1): ?>
                <style>
                    .gen-section table.main-grid tr.grid-content td:nth-child(2) {
                        width: 202px;
                    }

                    .gen-section table.main-grid tr.grid-head td:nth-child(1) {
                        width: 150px!important;
                    }
                </style>
            <?php endif; ?>
            <div class="main-section">

                <div class="commands">

                    <div class="head-label">
                        <h2>Accounts</h2>
                    </div><!-- end of add new account -->
                    <?php if ($department_id == 2 || $department_id == 4) { ?>
                        <ul>
                            <li><a class="link-button" href='/88dbphcrm/accounts/add.php'>Add New</a></li>
                        </ul>
                    <?php } ?>
                </div><!-- end of grid-commands -->

                <div style="height: 100%; width: 100%; border: 1px solid #DDD; margin-bottom: 15px; padding-top: 15px;">



                    <div style="margin-left:15px;padding-top:5px;padding-bottom:15px;">
                        <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" id="searchfrm" name="searchfrm" >
                            <input type="text" value="<?php if ($_SESSION['search'])
                        echo $_SESSION['search']; ?>" style="color:#000;width:150px;padding-top:2px;padding-bottom:2px;" placeholder="Enter Account Name" id="search" name="search" />
                                   <?php if ($department_id != 3) { ?>	
                                <select id="user" name="user" style="margin-left: 15px;" >
                                    <option value="0">Select User</option>


                                    <?php echo UserBox($department_id, $role_id, $userid, $_SESSION['user_search']); ?>
                                </select>
                            <?php } ?>
                            <?php
                            /*

                              <select id="cat" name="cat" style="margin-left: 15px;" >
                              <option value>Select Category</option>
                              <?php echo CategoryBox($_REQUEST['cat']); ?>
                              </select>

                              <select id="source" name="source" style="margin-left: 15px;" >
                              <option value>Select Source</option>
                              <?php echo SourceBox($_REQUEST['source']); ?>
                              </select>

                             */
                            ?>
                            <select id="status_id" name="sid" style="margin-left: 15px;" >
                                <option value="0">Select Status</option>
                                <?php echo StatusBox($_SESSION['sid'], $role_id, $department_id); ?>
                            </select>

                            <a class="link-button" href="javascript:void(0)" onclick="document.searchfrm.submit();" >Search</a>
                            <a class="link-button" href='/88dbphcrm/accounts/reset.php' style="background-color: red; border: 1px solid red; color: #fff;margin-bottom:0px;">Reset</a>
                        </form>

                    </div>
                </div>


                <div class="gen-section">
                    <?php if ($department_id == 1) { ?>
                        <style>
                            .gen-section table tr td {
                                width: 110px;
                            }
                            .gen-section table.main-grid tr.grid-content td:nth-child(2) {
                                width: 30px;
                            }
                            table{
                                font-size: 11px!important;
                                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                            }
                        </style>
                        <table class="main-grid">
                            <tr class="grid-head">

                                <td style="width: 225px;">Account</td>
                                <td style="width: 100px;">Shop Url</td> 
                                <td>CMS</td>
                                <td>SEO</td>
<!--                                <td>AE entry</td>
                                <td>Prod entry</td>-->
                                <td>Last modified on</td>

                                <td>Prev Status</td>
                                <td>CSR</td>
                                <td>AE</td>
                                <td>Designer</td>
                                <td>Editor</td>
                                <td>Current Status date</td>

                            </tr>

                            <?php
//                           error_reporting(E_ALL);
//                            echo $num;

                            while ($i < $num) {
//                                $create = new DateTime(substr(mysql_result($result, $i, 'account_createdon'), 0, 10));
//                                $mod = new DateTime(substr(mysql_result($result, $i, 'account_modifiedon'), 0, 10));
//                                $diff = $mod->diff($create);
//                                $resultdiff = $diff->m . " months, " . $diff->d . " days ";
//                                "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";

                                $class = mysql_result($result, $i, 'account_classification');
                                if ($class == 1) {
                                    $color = '#ffe1e1';
                                } elseif ($class == 2) {
                                    $color = '#ffdfcc';
                                } elseif ($class == 3) {
                                    $color = '#ffebcc';
                                } elseif ($class == 4) {
                                    $color = '#ffffcc';
                                } elseif ($class == 5) {
                                    $color = '#ffffff';
                                } elseif ($class == 6) {
                                    $color = '#f3f3f3';
                                } else {
                                    $color = '#F9F9F9';
                                }

                                echo "<tr class='grid-content' style='background:" . $color . "'>";
                                echo "<td><a target='_blank' href='/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&user=" . $_SESSION['user_id'] . "&account_id=" . mysql_result($result, $i, 'account_id') . "'>" . substr(mysql_result($result, $i, 'account_name'), 0, 25) . (strlen(mysql_result($result, $i, 'account_name')) < 25 ? "" : "...") . "</a></td>";

                                $shop_url = mysql_result($result, $i, 'shop_url');

                                if ($shop_url) {
                                    if (substr($shop_url, 0, 4) != "http") {
                                        $shop_url_new = "http://" . $shop_url;
                                    } else {
                                        $shop_url_new = $shop_url;
                                    }
                                } else {
                                    $shop_url_new = "";
                                }

                                echo "<td><a href='" . $shop_url_new . "' target='_blank' >" . (strlen($shop_url_new) > 25 ? "http..." : "") . substr($shop_url_new, -25) . "</a></td>";
                                echo "<td> v." . mysql_result($result, $i, 'account_cmstype') . " </td>";
                                if (mysql_result($result, $i, 'account_seo') == 1) {
                                    $seo = '<img src="green.gif">';
                                } else {
                                    $seo = '<img src="red.gif">';
                                }
                                if (mysql_result($result, $i, 'account_ads') == 1) {
                                    $ads = '<img src="green.gif">';
                                } else {
                                    $ads = '<img src="red.gif">';
                                }
                                echo "<td> seo :" . $seo . "<br>ads :" . $ads . "</td>";
//                                echo "<td>" . mysql_result($result, $i, 'account_createdon') . "</td>";
//                                $account_id_el = mysql_result($result, $i, 'account_id');
//                                to get the date(For Proofreading) 
//                                $queryprof = " Select max(a.audit_createdon) as date , a.account_id as id";
//                                $queryprof.=" From audit a , accounts b ";
//                                $queryprof.=" Where a.after_status_id = 2";
//                                $queryprof.=" And a.account_id = b.account_id";
//                                $queryprof.=" And a.account_id =" . $account_id_el . " ";
//                                $queryprof.=" Group by b.account_id";
//                                echo $queryprof;
//                                echo $i;
//                                $resultprof = mysql_query($queryprof);
//                                $il = 0;
//                                $numprof = mysql_num_rows($resultprof);
//                                to get the date(For Proofreading) 
//                                $profdate = mysql_result($resultprof, 0, 'date');
//                                if ($profdate) {
//                                    $datedaw = $profdate;
//                                } else {
//                                    $datedaw = '0000-00-00 00:00:00';
//                                }

//                                echo "<td>" . $datedaw . "</td>";

                                echo "<td>" . mysql_result($result, $i, 'account_modifiedon') . "</td>";

//                                $date1 = strtotime(substr($datedaw, 0, 10));
//                                $date2 = strtotime(substr(mysql_result($result, $i, 'account_modifiedon'), 0, 10));
//                                $seconds_diff = $date2 - $date1;
//                                echo "<td>" . floor($seconds_diff / 3600 / 24) . " day/s" . "</td>";
//                                $status_id_el = mysql_result($result, $i, 'status_id');
//
//                                $querycurrentstat = " Select max(a.audit_createdon) as date , a.account_id as id,c.status_name";
//                                $querycurrentstat.=" From audit a , accounts b ,statuses c";
//                                $querycurrentstat.=" Where a.after_status_id = " . $status_id_el . " ";
//                                $querycurrentstat.=" And a.before_status_id = c.status_id";
//                                $querycurrentstat.=" And a.account_id = b.account_id";
//                                $querycurrentstat.=" And a.account_id =" . $account_id_el . " ";
//                                $querycurrentstat.=" Group by b.account_id";
                                //echo $querycurrentstat;
//                                $resultcurrentstat = mysql_query($querycurrentstat);
//                                $currentstatdate = mysql_result($resultcurrentstat, 0, 'date');
//                                $prevstat = mysql_result($resultcurrentstat, 0, 'status_name');

                                echo "<td>" . mysql_result($result, $i, 'prev_status') . "</td>";
                                echo "<td>" . mysql_result($result, $i, 'CSR') . "</td>";
                                echo "<td>" . mysql_result($result, $i, 'AE') . "</td>";
                                echo "<td>" . mysql_result($result, $i, 'DE') . "</td>";
                                echo "<td>" . mysql_result($result, $i, 'editor') . "</td>";
                                //                                to get the date(current status) 

//                                if ($currentstatdate) {
//                                    $datedawcurrent = $currentstatdate;
//                                } else {
//                                    $datedawcurrent = '0000-00-00 00:00:00';
//                                }

                                echo "<td>" . mysql_result($result, $i, 'enter_status') . "</td>";
//                                echo $queryprof;
                                if ($role_id == 1) {
//                                    echo "<td><a href='/88dbphcrm/accounts/delete.php?account_id=" . mysql_result($result, $i, 'account_id') . "' onclick=\"return confirm('Are you sure you want to delete this account?');\">Delete</a></td>";
                                }
                                echo "</tr>";
                                $i++;
                                $j++;
                            }
                            ?>
                        </table>     
                            <?php
                        }
                        if ($department_id == 2) {
                            ?>
                        <table class="main-grid">
                            <tr class="grid-head">
                                <td>ID</td>
                                <td>Account</td>
                                <td>Category</td><!--
                                <td>Contact</td>
                                <td>Telephone</td>
                                <td>Mobile</td>
                                <td>Fax</td>
                                <td>Email</td>-->
                                <td>Source</td>
                                <td>Status</td>
    <?php if ($role_id == 5)
        echo "<td>AE</td>"; ?>
                                <?php if ($role_id == 1)
                                    echo "<td></td>"; ?>

                            </tr>
                                <?php
                                while ($i < $num) {
                                    $class = mysql_result($result, $i, 'account_classification');
                                    if ($class == 1) {
                                        $color = '#ffe1e1';
                                    } elseif ($class == 2) {
                                        $color = '#ffdfcc';
                                    } elseif ($class == 3) {
                                        $color = '#ffebcc';
                                    } elseif ($class == 4) {
                                        $color = '#ffffcc';
                                    } elseif ($class == 5) {
                                        $color = '#ffffff';
                                    } elseif ($class == 6) {
                                        $color = '#f3f3f3';
                                    } else {
                                        $color = '#F9F9F9';
                                    }

                                    echo "<tr class='grid-content' style='background:" . $color . "'>";
                                    echo "<td>" . mysql_result($result, $i, 'account_id') . "</td>";
                                    echo "<td><a href='/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&user=" . $_SESSION['user_id'] . "&account_id=" . mysql_result($result, $i, 'account_id') . "'>" . mysql_result($result, $i, 'account_name') . "</a></td>";
                                    echo "<td>" . mysql_result($result, $i, 'category_name') . "</td>";
                                    /* echo "<td>".mysql_result($result, $i, 'account_contactperson')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_telephone')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_mobile')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_fax')."</td>";
                                      echo "<td><a href='mailto:".mysql_result($result, $i, 'account_email')."'>".mysql_result($result, $i, 'account_email')."</a></td>";
                                     */echo "<td>" . mysql_result($result, $i, 'source_name') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'status_name') . "</td>";
                                    if ($role_id == 5) {
                                        echo "<td>" . mysql_result($result, $i, 'AE') . "</td>";
                                    }
                                    if ($role_id == 1) {
                                        echo "<td><a href='/88dbphcrm/accounts/delete.php?account_id=" . mysql_result($result, $i, 'account_id') . "' onclick=\"return confirm('Are you sure you want to delete this account?');\">Delete</a></td>";
                                    }
                                    echo "</tr>";
                                    $i++;
                                    $j++;
                                }
                                ?>
                        </table>
                            <?php
                        }
                        if ($department_id == 3) {
                            ?>

                        <table class="main-grid">
                            <tr class="grid-head">
                                <td>ID</td>
                                <td>Account</td>
                                <td>Category</td><!--
                                <td>Contact</td>
                                <td>Telephone</td>
                                <td>Mobile</td>
                                <td>Fax</td>
                                <td>Email</td>-->
                                <td>Source</td>
                                <td>Status</td>
    <?php if ($role_id == 1)
        echo "<td></td>"; ?>
                            </tr>

                                <?php
                                while ($i < $num) {
                                    $class = mysql_result($result, $i, 'account_classification');
                                    if ($class == 1) {
                                        $color = '#ffe1e1';
                                    } elseif ($class == 2) {
                                        $color = '#ffdfcc';
                                    } elseif ($class == 3) {
                                        $color = '#ffebcc';
                                    } elseif ($class == 4) {
                                        $color = '#ffffcc';
                                    } elseif ($class == 5) {
                                        $color = '#ffffff';
                                    } elseif ($class == 6) {
                                        $color = '#f3f3f3';
                                    } else {
                                        $color = '#F9F9F9';
                                    }

                                    echo "<tr class='grid-content' style='background:" . $color . "'>";
                                    echo "<td>" . mysql_result($result, $i, 'account_id') . "</td>";
                                    echo "<td><a target='_blank' href='/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&user=" . $_SESSION['user_id'] . "&account_id=" . mysql_result($result, $i, 'account_id') . "'>" . mysql_result($result, $i, 'account_name') . "</a></td>";
                                    echo "<td>" . mysql_result($result, $i, 'category_name') . "</td>";
                                    /* echo "<td>".mysql_result($result, $i, 'account_contactperson')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_telephone')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_mobile')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_fax')."</td>";
                                      echo "<td><a href='mailto:".mysql_result($result, $i, 'account_email')."'>".mysql_result($result, $i, 'account_email')."</a></td>";
                                     */echo "<td>" . mysql_result($result, $i, 'source_name') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'status_name') . "</td>";
                                    if ($role_id == 1) {
                                        echo "<td><a href='/88dbphcrm/accounts/delete.php?account_id=" . mysql_result($result, $i, 'account_id') . "' onclick=\"return confirm('Are you sure you want to delete this account?');\">Delete</a></td>";
                                    }
                                    echo "</tr>";
                                    $i++;
                                    $j++;
                                }
                                ?>
                        </table> 
                            <?php
                        }
                        if ($department_id == 4) {
                            ?>
                        <style>

                            table{
                                font-size: 12px!important;
                                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                            }
                        </style>
                        <table class="main-grid">
                            <tr class="grid-head">
                                <td>ID</td>
                                <td>Account</td>
                                <td>Shop Type</td>
                                <td>Category</td>
                                <!--
                                <td>Contact</td>
                                <td>Telephone</td>
                                <td>Mobile</td>
                                <td>Fax</td>
                                <td>Email</td>-->
                                <td>Source</td>
                                <td>AE</td>
                                <td>Designer</td>
                                <td>Editor</td>
                                <td>CSR</td>
                                <td>Status</td>
    <?php if ($role_id == 1)
        echo "<td></td>"; ?>
                            </tr>

                                <?php
                                $shoptype = '';
                                while ($i < $num) {

                                    $class = mysql_result($result, $i, 'account_classification');
                                    if ($class == 1) {
                                        $color = '#ffe1e1';
                                    } elseif ($class == 2) {
                                        $color = '#ffdfcc';
                                    } elseif ($class == 3) {
                                        $color = '#ffebcc';
                                    } elseif ($class == 4) {
                                        $color = '#ffffcc';
                                    } elseif ($class == 5) {
                                        $color = '#ffffff';
                                    } elseif ($class == 6) {
                                        $color = '#f3f3f3';
                                    } else {
                                        $color = '#F9F9F9';
                                    }

                                    echo "<tr class='grid-content' style='background:" . $color . "'>";
                                    echo "<td>" . mysql_result($result, $i, 'account_id') . "</td>";
                                    echo "<td><a target='_blank' href='/88dbphcrm/accounts/ctrl_audit_account_log.php?act=log&user=" . $_SESSION['user_id'] . "&account_id=" . mysql_result($result, $i, 'account_id') . "'>" . mysql_result($result, $i, 'account_name') . "</a></td>";
                                    if (mysql_result($result, $i, 'shop_type') == 1) {
                                        $shoptype = '88DB';
                                    } elseif (mysql_result($result, $i, 'shop_type') == 2) {
                                        $shoptype = 'Openrice';
                                    } else {
                                        $shoptype = '';
                                    }
                                    echo "<td>" . $shoptype . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'category_name') . "</td>";
                                    /* echo "<td>".mysql_result($result, $i, 'account_contactperson')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_telephone')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_mobile')."</td>";
                                      echo "<td>".mysql_result($result, $i, 'account_fax')."</td>";
                                      echo "<td><a href='mailto:".mysql_result($result, $i, 'account_email')."'>".mysql_result($result, $i, 'account_email')."</a></td>";
                                     */echo "<td>" . mysql_result($result, $i, 'source_name') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'AE') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'DE') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'editor') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'CSR') . "</td>";
                                    echo "<td>" . mysql_result($result, $i, 'status_name') . "</td>";
                                    if ($role_id == 1) {
                                        echo "<td><a href='/88dbphcrm/accounts/delete.php?account_id=" . mysql_result($result, $i, 'account_id') . "' onclick=\"return confirm('Are you sure you want to delete this account?');\">Delete</a></td>";
                                    }
                                    echo "</tr>";
                                    $i++;
                                    $j++;
                                }
                                ?>
                        </table>
                        <?php } ?>
                    <div style="margin:20px auto;width:200px;">

                    <?php
                    if ($page == 1) {
                        $status_id = $_REQUEST['sid'];
                        if ($status_id)
                            $sid = "&sid=" . $status_id;

                        echo "<a href='#'> <-Previous</a>";
                    }
                    else {
                        $previous = $page - 1;


                        if ((strpos($_SERVER["REQUEST_URI"], 'search=') === false) || (strpos($_SERVER["REQUEST_URI"], 'sid=') === false)) {
                            $prev_new_url = "<a href='" . $current_url . "?page=$previous'> Previous-></a>";
                        } else {

                            //$prev_new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&sid=". $_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=". $previous ."'> Previous-></a>";



                            $prev_new_url = "<a href='" . basename($_SERVER["SCRIPT_NAME"]) . "?search=" . $_REQUEST['search'] . "&user=" . $_REQUEST['user'] . "&sid=" . $_REQUEST['sid'] . "&page=" . $previous . "'> Previous-></a>";
                        }

                        echo $prev_new_url;
                    }
                    ?>
                        <select id="selectfield" name="selectfield" style="margin: 0 15px;" onchange="document.location.href=this.options[this.selectedIndex].value">
                        <?php
                        //$status_id = $_REQUEST['sid'];
                        //if ($status_id) $sid = "&sid=".$status_id;	
                        $ct = 1;



                        while ($ct <= $lastpage) {

                            if ((strpos($_SERVER["REQUEST_URI"], 'search=') === false)) {
                                $url_filter = "?page=";
                            } else {

                                //$url_filter ="?search=".$_REQUEST['search']."&sid=".$_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page
                                $url_filter = "?search=" . $_REQUEST['search'] . "&user=" . $_REQUEST['user'] . "&sid=" . $_REQUEST['sid'] . "&page=";
                            }


                            if ($page == $ct) {
                                echo "<option value='" . basename($_SERVER["SCRIPT_NAME"]) . $url_filter . $ct . "' selected>" . $ct . "</option>";
                            } else {
                                echo "<option value='" . basename($_SERVER["SCRIPT_NAME"]) . $url_filter . $ct . "'>" . $ct . "</option>";
                            }

                            $ct++;
                        }
                        ?>
                        </select>
                            <?php
                            if ($page) {
                                $next = $page + 1;
                                $status_id = $_REQUEST['sid'];


                                if ($next > $lastpage) {
                                    echo "<a href='#'> Next-></a>";
                                } else {

                                    if ((strpos($_SERVER["REQUEST_URI"], 'search=') === false) || (strpos($_SERVER["REQUEST_URI"], 'sid=') === false)) {
                                        $new_url = "<a href='" . $current_url . "?page=$next'> Next-></a>";
                                    } else {

                                        //$new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&sid=". $_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=". $next ."'> Next-></a>";
                                        $new_url = "<a href='" . basename($_SERVER["SCRIPT_NAME"]) . "?search=" . $_REQUEST['search'] . "&user=" . $_REQUEST['user'] . "&sid=" . $_REQUEST['sid'] . "&page=" . $next . "'> Next-></a>";
                                    }





                                    echo $new_url;
                                }
                            }
                            ?>
                    </div>
                </div><!-- end of gen-section -->

            </div><!-- end of main-section -->

            </div><!-- end of main-container -->
            <script type="text/javascript">
                function filter() 
                {
                    var e = document.getElementById("status_id");
                    var g = e.options[e.selectedIndex].value;
		
                    if(g=="")
                    {
                        window.location.href='/88dbphcrm/accounts/';
                    }
                    else
                    {
                        window.location.href='/88dbphcrm/accounts/?sid='+g;
                    }
                }
            </script>
            </body>
            </html>