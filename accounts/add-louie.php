<?php
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];

mysql_connect('localhost', 'root', '');
mysql_select_db('88dbphcrm') or die("Unable to select database");

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {
    $account_name = $_POST['account_name'];
    $account_contactperson = $_POST['account_contactperson'];
    $account_contactperson_gender = $_POST['account_contactperson_gender'];
    $account_contactperson_age = $_POST['account_contactperson_age'];
    $account_telephone = $_POST['account_telephone'];
    $account_mobile = $_POST['account_mobile'];
    $account_fax = $_POST['account_fax'];
    $account_email = $_POST['account_email'];
    $account_address = $_POST['account_address'];
    $category_id = $_POST['category_id'];
    $source_id = $_POST['source_id'];
    $status_id = $_POST['status_id'];
    $package_id = $_POST['package_id'];
    $account_memberid = $_POST['account_memberid'];
    $account_brno = $_POST['account_brno'];
    $account_adid = $_POST['account_adid'];
    $shop_type = $_POST['shop_type'];
    $shop_url = $_POST['shop_url'];
    $shop_domain = $_POST['shop_domain'];
    $account_paid = 0;
    $account_createdon = $_POST['date_created'] . " " . $_POST['time_created'];

    if (!empty($_POST['event_box'])) {
        $event_id = $_POST['event_box'];
    } else {
        $event_id = 0;
    }

    if ($_POST['decline_reason']) {

        $decline_id = $_POST['decline_reason'];
    } else {
        $decline_id = 0;
    }
    $decline_reason = $_POST['other_reason'];

    $account_contractnumber = $_POST['account_contractnumber'];

    if ($role_id == 4 || $dep_id == 4) {

        $query = "INSERT INTO accounts (account_name, account_contactperson, account_contactperson_gender, account_contactperson_age,  account_telephone,  account_mobile, account_fax, account_email, account_address, category_id, source_id, status_id, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, account_contractnumber, account_createdby,event_id, declined_status_id, declined_status_reason,account_paid,account_createdon) VALUES 
		('" . mysql_real_escape_string($account_name) . "', '" . mysql_real_escape_string($account_contactperson) . "', '$account_contactperson_gender', '$account_contactperson_age' , '$account_telephone', '$account_mobile', '$account_fax', '$account_email', '" . mysql_real_escape_string($account_address) . "', $category_id, $source_id, $status_id, $package_id, '$account_memberid', '$account_brno', '$account_adid', $shop_type, '$shop_url', '$shop_domain', '$account_contractnumber', $userid,$event_id,$decline_id, '" . mysql_real_escape_string($decline_reason) . "',$account_paid, '$account_createdon')";
    } else {

        $query = "INSERT INTO accounts (account_name, account_contactperson, account_contactperson_gender, account_contactperson_age, account_telephone,  account_mobile, account_fax, account_email, account_address, category_id, source_id, status_id, package_id, account_memberid, account_brno, account_adid, shop_type, shop_url, shop_domain, account_contractnumber, account_createdby,event_id, declined_status_id, declined_status_reason,account_paid) VALUES 
		('" . mysql_real_escape_string($account_name) . "', '" . mysql_real_escape_string($account_contactperson) . "', '$account_contactperson_gender', '$account_contactperson_age' , '$account_telephone', '$account_mobile', '$account_fax', '$account_email', '" . mysql_real_escape_string($account_address) . "', $category_id, $source_id, $status_id, $package_id, '$account_memberid', '$account_brno', '$account_adid', $shop_type, '$shop_url', '$shop_domain', '$account_contractnumber', $userid,$event_id,$decline_id, '" . mysql_real_escape_string($decline_reason) . "',$account_paid)";
    }



    $result = mysql_query($query);
    $account_id = mysql_insert_id();
    //echo $query;
//    if ($status_id == 13) {
    $query = "INSERT INTO audit (account_id, before_status_id, after_status_id, audit_createdby, audit_createdon) VALUES ('$account_id', 0, $status_id, $userid,now())";
    $result = mysql_query($query);
//    }


    header('Location: /88dbphcrm/accounts/');
    exit;
}

$query = "SELECT category_id, category_name FROM categories order by category_name ASC";
$result = mysql_query($query);
$categoryoptions = "";
while ($row = mysql_fetch_array($result)) {
    $catid = $row["category_id"];
    $catname = $row["category_name"];
    $categoryoptions .= '<option value="' . $catid . '">' . $catname . "</option>";
}

$query = "SELECT source_id, source_name FROM sources order by source_name ASC";
$result = mysql_query($query);
$sourceoptions = "";
while ($row = mysql_fetch_array($result)) {
    $sourceid = $row["source_id"];
    $sourcename = $row["source_name"];
    if ($sourceid == $source_id) {

        $sourceoptions .= '<option value="' . $sourceid . '" selected="selected">' . $sourcename . "</option>";
    } else {
        $sourceoptions .= '<option value="' . $sourceid . '">' . $sourcename . "</option>";
    }
}


$query = "SELECT event_id, event_name FROM events order by event_name ASC";
$result = mysql_query($query);
$eventoptions = "";
while ($row = mysql_fetch_array($result)) {
    $eventid = $row["event_id"];
    $eventname = $row["event_name"];

    $eventoptions .= '<option value="' . $eventid . '">' . $eventname . "</option>";
}

$query = "SELECT status_id, status_name, department_id FROM statuses ORDER BY status_name ASC";
$result = mysql_query($query);
$statusoptions = "";
while ($row = mysql_fetch_array($result)) {

    $statusid = $row["status_id"];
    $statusname = $row["status_name"];
    if ($statusid == $status_id) {
        $statusoptions .= '<option value="' . $statusid . '" selected="selected">' . $statusname . "</option>";
    } else {
        $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
    }
}

$query = "SELECT package_id, package_name FROM packages";
$result = mysql_query($query);
$packageoptions = "";
while ($row = mysql_fetch_array($result)) {
    $packageid = $row["package_id"];
    $packagename = $row["package_name"];
    if ($packageid == $package_id) {
        $packageoptions .= '<option value="' . $packageid . '" selected="selected">' . $packagename . "</option>";
    } else {
        $packageoptions .= '<option value="' . $packageid . '">' . $packagename . "</option>";
    }
}

$shoptypesoptions = "";
if ($shop_type == 1) {
    $shoptypesoptions = '<option value="1" selected="selected">88DB</option><option value="2">OpenRice</option>';
} elseif ($shop_type == 2) {
    $shoptypesoptions = '<option value="1">88DB</option><option value="2" selected="selected">OpenRice</option>';
} else {
    $shoptypesoptions = '<option value="1">88DB</option><option value="2">OpenRice</option>';
}

$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 ORDER BY designer ASC";
$result = mysql_query($query);
$designers = "";
while ($row = mysql_fetch_array($result)) {
    $designerid = $row["user_id"];
    $designername = $row["designer"];
    if ($designerid == $shop_designer) {
        $designers .= '<option value="' . $designerid . '" selected="selected">' . $designername . "</option>";
    } else {
        $designers .= '<option value="' . $designerid . '">' . $designername . "</option>";
    }
}

$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as editorname FROM users WHERE department_id = 1 AND enabled = 1 ORDER BY editorname ASC";
$result = mysql_query($query);
$editors = "";
while ($row = mysql_fetch_array($result)) {
    $editorid = $row["user_id"];
    $editorname = $row["editorname"];
    if ($editorid == $shop_editor) {
        $editors .= '<option value="' . $editorid . '" selected="selected">' . $editorname . "</option>";
    } else {
        $editors .= '<option value="' . $editorid . '">' . $editorname . "</option>";
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Add Account</title>

        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="form1" id="form1" action="add.php" method="post">
            <div class="commands">
                <div class="head-label">
                    <h2>Add New Account</h2>
                </div><!-- end of add new account -->

                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/accounts/'>Cancel</a></li>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Account Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_name" id="account_name" class="required"/>
                            <input type="hidden" name="verify_accountname" id="verify_accountname" />
                        </td>
                    </tr>

                    <tr>
                        <td class="grid-head">*Contact Person:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_contactperson" id="account_contactperson" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Contact Person gender:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="account_contactperson_gender" id="account_contactperson_gender" class="required">
                                <option value="0">Select Gender</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Contact Person Age:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_contactperson_age" id="account_contactperson_age"  /></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Telephone:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_telephone" id="account_telephone" class="required"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">Mobile:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_mobile" id="account_mobile" /></td>
                    </tr>
                    <tr>
                        <td class="grid-head">Fax:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_fax" id="account_fax" /></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Email:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_email" id="account_email" class="account_email"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Address:</td>
                        <td>&nbsp;</td>
                        <td><textarea cols="25" rows="3" name="account_address" id="account_address" class="required"></textarea></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Category:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='category_id' id='category_id' class="required">
                                <option value="">Select Category</option>
                                <?php echo $categoryoptions; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Source:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='source_id' id='source_id' class="required">
                                <option value="">Select Source</option>
                                <?php echo $sourceoptions; ?>
                            </select>   

                        </td>
                    </tr>
                    <tr id="events_source">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>


                            <select name="event_box" id="event_box">
                                <option value="">Select Events</option>
                                <?php echo $eventoptions; ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Status:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='status_id' id='status_id' class="required">
                                <option value="">Select Status</option>
                                <?php //echo $statusoptions; ?>
                                <option value="14">Declined</option>
                                <option value="12">For Callback</option>
                                <option value="23">For Deletion</option>
                                <option value="15">For Follow Up</option>
                                <option value="22">For Prospecting</option>
                                <option value="20">For Signing</option>
                                <option value="19">Interested</option>
                                <option value="11">Sent Proposal</option>
                                <option value="10">Set Appointment</option>
                                <option value="13">Signed (for Payment)</option>
                            </select> 
                        </td>
                    </tr>
                    <tr>
                    <tr id="decline_container">
                        <td class="grid-head">*Reason:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='decline_reason' id='decline_reason' >
                                <option value="">Select Reason</option>
                                <option value="1">Already Has a Website</option>
                                <option value="2">Website Not a Priority</option>
                                <option value="3">No Budget / Limited Resources</option>
                                <option value="4">Not Keen on Online Marketing</option>
                                <option value="5">Package is Expensive</option>
                                <option value="6">Limited Features</option>
                                <option value="7">Other</option>

                            </select> 
                        </td>
                    </tr>
                    <tr id="reason_container">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <input type="text" id="other_reason" name="other_reason" />
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Package:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='package_id' id='package_id' >
                                <option value='0'>Select Package</option>
                                <?php echo $packageoptions; ?>
                            </select> 
                        </td>
                    </tr>
                    <?php if ($role_id == 4 || $dep_id == 4) { ?>
                        <tr>
                            <td class="grid-head">*Account Created On:</td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="date_created" maxlength="10" id="date_created" class="required"/>
                                <input type="hidden" name="time_created" id="time_created" />
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="grid-head">Member ID:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_memberid" id="account_memberid" value="<?php echo $account_memberid; ?>"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">B.R. No.:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_brno" id="account_brno" value="<?php echo $account_brno; ?>"/></td>
                    </tr>
                    <tr>
                        <td class="grid-head">Ad ID:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_adid" id="account_adid" value="<?php echo $account_adid; ?>"/></td>
                    </tr>      
                    <tr>
                        <td class="grid-head">Contract Number:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" <?php
                    if ($dep_id == 2) {
                        echo "readonly='readonly'";
                    }
                    ?> name="account_contractnumber" id="account_contractnumber" value="<?php echo $account_contractnumber; ?>"/></td>
                    </tr>                    
                    <tr>
                        <td class="grid-head">Shop Type:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_type' id='shop_type'>
                                <option value='0'>Select Shop Type</option>
                                <?php echo $shoptypesoptions; ?>
                            </select>
                        </td>
                    </tr>              
                    <tr>
                        <td class="grid-head">Shop URL:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="shop_url" id="shop_url" value="<?php echo $shop_url; ?>"/></td>
                    </tr>   
                    <tr>
                        <td class="grid-head">Shop Domain:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="shop_domain" id="shop_domain" value="<?php echo $shop_domain; ?>"/></td>
                    </tr>   
                    <tr>
                        <td class="grid-head">Assigned CSR:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_csr' id='shop_csr'>
                                <option value='0'>Select CSR</option>
                                <?php echo $designers; ?>
                            </select>
                        </td>
                    </tr> 
                    <tr>
                        <td class="grid-head">Assigned Designer:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_designer' id='shop_designer'>
                                <option value='0'>Select Designer</option>
                                <?php echo $designers; ?>
                            </select>
                        </td>
                    </tr> 
                    <tr>
                        <td class="grid-head">Assigned Editor:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_editor' id='shop_editor'>
                                <option value='0'>Select Editor</option>
                                <?php echo $editors; ?>
                            </select>
                        </td>
                    </tr>                     
                    <tr class="edit-account-footer">
                        <td colspan="4">
                            <?php echo "Created by: " . $createdby . " | Modified by: " . $modifiedby; ?>
                        </td>
                    </tr>
                </table>


            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main-section -->

    <script type="text/javascript">
        jQuery.validator.addMethod(
        "multiemails",
        function(value, element) {
            if (this.optional(element)) // return true on optional element
                return true;
            var account_email = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in account_email) {
                value = account_email[i];
                valid = valid &&
                    jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

        jQuery.validator.messages.multiemails
    );
        $(document).ready(function(){
	
            $("#account_name").blur(function(){
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: 'functions/ajax.check.accountname.php',
                    timeout: 20000,
                    data: { value: $("#account_name").val() },
                    success: function(html){
                        $("#verify_accountname").val(html);
                    },
					
                });
            });
			
            $("#account_name").keyup(function(){
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: 'functions/ajax.check.accountname.php',
                    timeout: 20000,
                    data: { value: $("#account_name").val() },
                    success: function(html){
                        $("#verify_accountname").val(html);
                    },
					
                });
            });
            $("#form1").submit(function() 
            {
		
                if($("#verify_accountname").val().length > 0)
                {
                    alert("Account name is already taken");
                }
                else if ($("#status_id").val() == 16 || $("#status_id").val() ==17 )
                {
                    alert("Account not yet signed");
			
                }
                else
                {
                    return true;
                }
									
											
									
									
									 
                return false;
            });
		
            jQuery.validator.addMethod("multiemail", function(value, element) {
                if (this.optional(element)) {
                    return true;
                }
                var emails = value.split( new RegExp( "\\s*;\\s*", "gi" ) );
                valid = true;
                for(var i in emails) {
                    value = emails[i];
                    valid=valid && jQuery.validator.methods.email.call(this, value,element);
                }
                return valid;}, "Invalid email format");
		
        
            $("#form1").validate({
                rules: {
                    account_email: { required: true, multiemails: true }
                },
                messages: {
                    account_email: {
								
                        required: "This field is required",
                        multiemails: "Please enter a valid email address. Multiple emails are separated by comma or semicolon."
                    }
                }

            });
            $("#account_telephone").numeric();
            $("#account_mobile").numeric();
            $("#account_fax").numeric();
		
            $("#account_contractnumber").numeric();
            $("#account_contactperson_age").numeric();
	
            $("#source_id").change(function()
            {
                if ($("#source_id").val()==="5")
                {
			
                    $("#events_source").show();
                    $("#event_box").addClass("required");
                }
                else
                {	$("#events_source").hide();
                    $("#event_box").removeClass("required");
				
			
                }
		 
            });
            $("#status_id").change(function()
            {
                if ($("#status_id").val()==="14")
                {
			 
                    $("#decline_container").show();
                    $("#decline_reason").addClass("required");
                }
                else
                {
                    $("#decline_container").hide();
                    $("#decline_reason").removeClass("required");
                }
		 
            });
		 
            $("#decline_reason").change(function()
            {
                if ($("#decline_reason").val()==="5" || $("#decline_reason").val()==="6" || $("#decline_reason").val()==="7")
                {
			 
                    $("#reason_container").show();
                    $("#other_reason").addClass("required");
                }
                else
                {
                    $("#reason_container").hide();
                    $("#other_reason").removeClass("required");
                }
		 
            });
        });
        $(function() {
            if ($("#source_id").val()==="5")
            {
	 
                $("#events_source").show();
                $("#event_box").addClass("required");
            }
            else
            {
                $("#events_source").hide();
                $("#events_box").removeClass("required");
            }
	 
	 
            if ($("#status_id").val()==="14")
            {
	 
                $("#decline_container").show();
                $("#decline_reason").addClass("required");
            }
            else
            {
                $("#decline_container").hide();
                $("#decline_reason").removeClass("required");
            }
	 
            if ($("#decline_reason").val()==="5" || $("#decline_reason").val()==="6" || $("#decline_reason").val()==="7")
            {
			 
                $("#reason_container").show();
                $("#other_reason").addClass("required");
            }
            else
            {
                $("#reason_container").hide();
                $("#other_reason").removeClass("required");
            }	
		 
		
        });
    </script>
    <?php if ($role_id == 4 || $dep_id == 4) { ?>
        <script type="text/javascript">
            function GetCurrentTime()
            {
                                                                                                    		
                var currentTime = new Date()
                var hours = currentTime.getHours()
                var minutes = currentTime.getMinutes()
                var seconds = currentTime.getSeconds()
                if (minutes < 10){
                    minutes = "0" + minutes
                }
                if (seconds < 10){
                    seconds = "0" + seconds
                }
                                                                                                    			
                $("#time_created").val(hours + ":" + minutes + ":" + seconds);
                                                                                                    		
            }
            $(function() {
                                                                                                    		
                $('#date_created').datepicker({dateFormat: 'yy-mm-dd'});
                setInterval('GetCurrentTime()', 100 );
                                                                                                    			
            });
                                                                                                    	
        </script>
    <?php } ?>
</body>
</html>