<?php

session_start();
require_once("../models/tblog.php");
require_once("../models/tbaging.php");

$tblog = new TB_LOG();
$tbaging = new TB_AGING();

include_once("functions/connection.php");
include_once("functions/functions.php");
include_once("classes/class.export_excel.php");



date_default_timezone_set('Asia/Manila');

$package_id = $_GET['package_id'];
$shop_type = $_GET['shoptype'];

$packages = $tbaging->Selectpackagesbyid($package_id);

//log history


$data = array(
    'user_id' => $_SESSION['user_id'],
    'audit_act' => 'User ' . $_SESSION['user_id'] . ' create excel file from Package : ' . $packages['package_name'] . '',
    'ip_add' => $_SESSION['ipaddniya']
);
$tblog->Insertaudit_log($data);

//log history

$sql = " SELECT a.account_id, a.account_name , b.status_name  , concat(ed.user_firstname,' ',ed.user_lastname) as editor, concat(c.user_firstname,' ',c.user_lastname) as AE,";
$sql .= " concat(de.user_firstname,' ',de.user_lastname) as DE,";
$sql .= " concat(csr.user_firstname, ' ', csr.user_lastname) as CSR ,p.package_name,a.account_createdon , a.account_memberid ";
$sql .= " ,case a.shop_type when '0' then '' when '1' then '88DB' when '2' then 'Open Rice' end";
$sql .= " FROM accounts a";
$sql .= " LEFT JOIN statuses b ON a.status_id = b.status_id";
$sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
$sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
$sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id";
$sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id";
$sql .= " LEFT JOIN packages p ON a.package_id = p.package_id";
if ($package_id == 10) {
    $sql .= " WHERE (a.package_id is null or a.package_id = 0)";
} else {
    $sql .= " WHERE a.package_id = $package_id";
}

if ($shop_type == 0) {
    
} else {
    $sql .= " AND a.shop_type = $shop_type";
}
$sql .= " AND a.account_paid = 1";

//echo $sql;
$result = mysql_query($sql, connect());




$heads = array("Account id", "Account name", "Current Status", "Editor", "AE", "Designer", "CSR", "Package", "Createdon", "Member id", "Shop Type");

while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}
if ($package_id == 10) {
    $fn = "NO_PACKAGE.xls";
} else {
    $fn = "" . $packages['package_name'] . ".xls";
}
//$fn = "Excel_" . substr($event_edate,0,10) . "" . date('Y-m-d-G-i') . ".xls";
//create the instance of the exportexcel format
$excel_obj = new ExportExcel("$fn");
//setting the values of the headers and data of the excel file 
//and these values comes from the other file which file shows the data
$excel_obj->setHeadersAndValues($heads, $account_table);
//now generate the excel file with the data and headers set
$excel_obj->GenerateExcelFile();
?>
