<?php

session_start();

require_once("../models/tblog.php");
$tblog = new TB_LOG();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}

$accountid = $_GET['account_id'];
$userid = $_GET['user'];
if (!$userid) {
    $userid = $_SESSION['user_id'];
}

if ($_GET['act'] == 'log') {
    $datalog = array(
        'account_id' => $accountid,
        'user_id' => $userid ? $userid : $_SESSION['user_id'],
        'flag' => 0
    );

    $tblog->Insertaudit_account_log($datalog);


    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' access account ' . $accountid . '',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);


    header('Location: /88dbphcrm/accounts/edit.php?account_id=' . $accountid . '');
    exit;
}

if ($_GET['act'] == 'log_event') {

    $evid = $_GET['evid'];
    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' access Event ' . $evid . '',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);


    header('Location: /88dbphcrm/events/edit.php?evid=' . $evid . '');
    exit;
}
if ($_GET['act'] == 'log_event_leads') {

    $evid = $_GET['evid'];
    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' access Event Leads ' . $evid . '',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);


    header('Location: /88dbphcrm/events/leads.php?event=' . $evid . '');
    exit;
}
?>		