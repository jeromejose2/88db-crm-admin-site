<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];

mysql_connect('localhost', 'root', '');
mysql_select_db('88dbphcrm') or die("Unable to select database");

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

$paid_amount = 0;

if ($isPostBack) {
    $account_id = $_POST['paccount_id'];
    $account_name = $_POST['account_name'];
    $account_contactperson = $_POST['account_contactperson'];
    $account_contactperson_gender = $_POST['account_contactperson_gender'];
    $account_contactperson_age = $_POST['account_contactperson_age'];
    $account_telephone = $_POST['account_telephone'];
    $account_mobile = $_POST['account_mobile'];
    $account_fax = $_POST['account_fax'];
    $account_email = $_POST['account_email'];
    $account_address = $_POST['account_address'];
    $category_id = $_POST['category_id'];
    $source_id = $_POST['source_id'];

    $package_id = $_POST['package_id'];
    $account_memberid = $_POST['account_memberid'];
    $account_brno = $_POST['account_brno'];
    $account_adid = $_POST['account_adid'];
    $shop_type = $_POST['shop_type'];
    $shop_url = $_POST['shop_url'];
    $shop_domain = $_POST['shop_domain'];
    $shop_domaintype = $_POST['shop_domaintype'];
    $shop_domainstatus = $_POST['shop_domainstatus'];
    $shop_cmsstatus = $_POST['shop_cmsstatus'];
    $shop_csr = $_POST['shop_csr'];
    $shop_designer = $_POST['shop_designer'];
    $shop_editor = $_POST['shop_editor'];
    $account_contractnumber = $_POST['account_contractnumber'];
    $account_contractamount = $_POST['account_contractamount'];
    $account_paid = $_POST['account_paid'];
    $account_createdon = $_POST['date_created'] . " " . $_POST['time_created'];
    $account_classification = $_POST['acc_classification'];
    $account_seo = $_POST['account_seo'];
    $account_ads = $_POST['account_ads'];

    if ($department_id == 2) {
        if (!isset($_POST['status_id'])) {
            //$account_paid = 1;
            $status_id = 13;
        } else {
            $status_id = $_POST['status_id'];
            //$account_paid = 0;
        }
    } else {
        if (!isset($_POST['status_id'])) {
            //$account_paid = 1;
            $status_id = 13;
        } else {
            $status_id = $_POST['status_id'];
            //$account_paid = 1;
        }
    }


    if ($_POST['status_id'] == 14) {
        $decline_id = $_POST['decline_reason'];
    } else {
        $decline_id = 0;
    }

    if ($_POST['decline_reason'] == 5 || $_POST['decline_reason'] == 6 || $_POST['decline_reason'] == 7) {
        $decline_reason = $_POST['other_reason'];
    } else {
        $decline_reason = 0;
    }

    if ($source_id == 5) {
        if (!empty($_POST['event_box'])) {
            $event_id = $_POST['event_box'];
        } else {
            $event_id = 0;
        }
    } else {
        $event_id = 0;
    }

    if ($role_id == 1 && $dep_id == 4) {
        $query = "UPDATE accounts SET account_seo = '" . mysql_real_escape_string($account_seo) . "',account_ads = '" . mysql_real_escape_string($account_ads) . "',account_classification = '" . mysql_real_escape_string($account_classification) . "',account_name = '" . mysql_real_escape_string($account_name) . "', account_contactperson = '" . mysql_real_escape_string($account_contactperson) . "', account_contactperson_gender = '$account_contactperson_gender' ,account_contactperson_age = '$account_contactperson_age', account_paid = $account_paid, account_telephone = '$account_telephone', account_mobile = '$account_mobile', account_fax = '$account_fax', account_email = '$account_email', account_address = '" . mysql_real_escape_string($account_address) . "', category_id = $category_id, source_id = $source_id, status_id = $status_id, package_id = $package_id, account_memberid = '$account_memberid', account_brno = '$account_brno', account_adid = '$account_adid', shop_type = $shop_type, shop_url = '$shop_url', shop_domain = '$shop_domain', shop_domaintype = $shop_domaintype, shop_domainstatus = $shop_domainstatus, shop_cmsstatus = $shop_cmsstatus, shop_csr = $shop_csr, shop_designer = $shop_designer, shop_editor = $shop_editor, account_contractnumber = '$account_contractnumber', account_contractamount = $account_contractamount, event_id = $event_id, account_modifiedby = $userid, declined_status_id = $decline_id,  declined_status_reason = '" . mysql_real_escape_string($decline_reason) . "', account_createdon ='$account_createdon', account_modifiedon = now() WHERE account_id = $account_id";
    } elseif ($role_id == 1 && $dep_id == 1) {
        if ($status_id == 21) {
            $query = "UPDATE accounts SET account_seo = '" . mysql_real_escape_string($account_seo) . "',account_ads = '" . mysql_real_escape_string($account_ads) . "',account_classification = '" . mysql_real_escape_string($account_classification) . "',account_name = '" . mysql_real_escape_string($account_name) . "', account_contactperson = '" . mysql_real_escape_string($account_contactperson) . "', account_contactperson_gender = '$account_contactperson_gender' ,account_contactperson_age = '$account_contactperson_age', account_paid = $account_paid, account_telephone = '$account_telephone', account_mobile = '$account_mobile', account_fax = '$account_fax', account_email = '$account_email', account_address = '" . mysql_real_escape_string($account_address) . "', category_id = $category_id, source_id = $source_id, status_id = $status_id, package_id = $package_id, account_memberid = '$account_memberid', account_brno = '$account_brno', account_adid = '$account_adid', shop_type = $shop_type, shop_url = '$shop_url', shop_domain = '$shop_domain', shop_domaintype = $shop_domaintype, shop_domainstatus = $shop_domainstatus, shop_cmsstatus = $shop_cmsstatus, shop_csr = $shop_csr, shop_designer = $shop_designer, shop_editor = $shop_editor, account_contractnumber = '$account_contractnumber', account_contractamount = $account_contractamount, event_id = $event_id, account_modifiedby = $userid, declined_status_id = $decline_id,  declined_status_reason = '" . mysql_real_escape_string($decline_reason) . "',  account_cmstype = 2,  account_modifiedon = now() WHERE account_id = $account_id";
        } else {

            $query = "UPDATE accounts SET account_seo = '" . mysql_real_escape_string($account_seo) . "',account_ads = '" . mysql_real_escape_string($account_ads) . "',account_classification = '" . mysql_real_escape_string($account_classification) . "',account_name = '" . mysql_real_escape_string($account_name) . "', account_contactperson = '" . mysql_real_escape_string($account_contactperson) . "', account_contactperson_gender = '$account_contactperson_gender' ,account_contactperson_age = '$account_contactperson_age', account_paid = $account_paid, account_telephone = '$account_telephone', account_mobile = '$account_mobile', account_fax = '$account_fax', account_email = '$account_email', account_address = '" . mysql_real_escape_string($account_address) . "', category_id = $category_id, source_id = $source_id, status_id = $status_id, package_id = $package_id, account_memberid = '$account_memberid', account_brno = '$account_brno', account_adid = '$account_adid', shop_type = $shop_type, shop_url = '$shop_url', shop_domain = '$shop_domain', shop_domaintype = $shop_domaintype, shop_domainstatus = $shop_domainstatus, shop_cmsstatus = $shop_cmsstatus, shop_csr = $shop_csr, shop_designer = $shop_designer, shop_editor = $shop_editor, account_contractnumber = '$account_contractnumber', account_contractamount = $account_contractamount, event_id = $event_id, account_modifiedby = $userid, declined_status_id = $decline_id,  declined_status_reason = '" . mysql_real_escape_string($decline_reason) . "',  account_modifiedon = now() WHERE account_id = $account_id";
        }
    } else {
//        $query = "UPDATE accounts SET account_name = '" . mysql_real_escape_string($account_name) . "', account_contactperson = '" . mysql_real_escape_string($account_contactperson) . "', account_contactperson_gender = '$account_contactperson_gender' ,account_contactperson_age = '$account_contactperson_age',account_paid = $account_paid, account_telephone = '$account_telephone', account_mobile = '$account_mobile', account_fax = '$account_fax', account_email = '$account_email', account_address = '" . mysql_real_escape_string($account_address) . "', category_id = $category_id, source_id = $source_id, status_id = $status_id, package_id = $package_id, account_memberid = '$account_memberid', account_brno = '$account_brno', account_adid = '$account_adid', shop_type = $shop_type, shop_url = '$shop_url', shop_domain = '$shop_domain', shop_domaintype = $shop_domaintype, shop_domainstatus = $shop_domainstatus, shop_cmsstatus = $shop_cmsstatus, shop_csr = $shop_csr, shop_designer = $shop_designer, shop_editor = $shop_editor, account_contractnumber = '$account_contractnumber', account_contractamount = $account_contractamount, event_id = $event_id, account_modifiedby = $userid, declined_status_id = $decline_id,  declined_status_reason = '" . mysql_real_escape_string($decline_reason) . "', account_modifiedon = now() WHERE account_id = $account_id";
        $query = "UPDATE accounts SET account_name = '" . mysql_real_escape_string($account_name) . "', account_contactperson = '" . mysql_real_escape_string($account_contactperson) . "', account_contactperson_gender = '$account_contactperson_gender' ,account_contactperson_age = '$account_contactperson_age',account_paid = $account_paid, account_telephone = '$account_telephone', account_mobile = '$account_mobile', account_fax = '$account_fax', account_email = '$account_email', account_address = '" . mysql_real_escape_string($account_address) . "', category_id = $category_id, source_id = $source_id, status_id = $status_id, package_id = $package_id, account_memberid = '$account_memberid', account_brno = '$account_brno', account_adid = '$account_adid', shop_type = $shop_type, shop_url = '$shop_url', shop_domain = '$shop_domain',  account_contractnumber = '$account_contractnumber', account_contractamount = $account_contractamount, event_id = $event_id, account_modifiedby = $userid, declined_status_id = $decline_id,  declined_status_reason = '" . mysql_real_escape_string($decline_reason) . "', account_modifiedon = now() WHERE account_id = $account_id";
    }



    $result = mysql_query($query);
    if ($_POST['oldstatusid'] <> $_POST['newstatusid']) {
        $oldstat = $_POST['oldstatusid'];
        $newstat = $_POST['newstatusid'];
        $query = "INSERT INTO audit (account_id, before_status_id, after_status_id, audit_createdby, audit_createdon) VALUES ('$account_id', $oldstat, $newstat, $userid,now())";
        $result = mysql_query($query);
        if ($newstat == 38) {
            include_once("emailcsr.php");
            SendEmailtoCsr($_POST['account_name'], $_POST['shop_csr_orig']);
        } else if ($newstat == 9) {
            include_once("emailmarkenal.php");
            SendEmailtoFinance($_POST['account_name']);
        }
    }

    if (($role_id == 1 && $dep_id == 4) || ($role_id == 1 && $dep_id == 1)) {

        if ($_POST['oldshopdesigner'] <> $_POST['newshopdesigner']) {
            $oldshopdesigner = GetDesignersName($_POST['oldshopdesigner']);
            $newshopdesigner = GetDesignersName($_POST['newshopdesigner']);
            $shop_designer_remark = "Designer changed from " . $oldshopdesigner . " to " . $newshopdesigner;
            $query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$shop_designer_remark','1', $account_id, $userid)";
            $result = mysql_query($query);
        }
    }
    if (($role_id == 1 && $dep_id == 4) || ($role_id == 1 && $dep_id == 1)) {

        if ($_POST['shop_editor_orig'] <> $_POST['shop_editor']) {
            $oldshopeditor = GetDesignersName($_POST['shop_editor_orig']);
            $newshopeditor = GetDesignersName($_POST['shop_editor']);
            $shop_editor_remark = "Editor changed from " . $oldshopeditor . " to " . $newshopeditor;
            $query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$shop_editor_remark','1', $account_id, $userid)";
            $result = mysql_query($query);
        }
    }
    if (($role_id == 1 && $dep_id == 4) || ($role_id == 1 && $dep_id == 1)) {
        if ($_POST['shop_domainstatus'] <> $_POST['shop_domainstatus_orig']) {
            $oldshopdomainstat = $_POST['shop_domainstatus_orig'];
            $newshopdomainstat = $_POST['shop_domainstatus'];
            if ($oldshopdomainstat == 1) {
                $oldshopdomainstats = 'For Request';
            } elseif ($oldshopdomainstat == 2) {
                $oldshopdomainstats = 'Pending';
            } elseif ($oldshopdomainstat == 3) {
                $oldshopdomainstats = 'Activated';
            } elseif ($oldshopdomainstat == 4) {
                $oldshopdomainstats = 'Domain Unavailable';
            } else {
                $oldshopdomainstats = ' ';
            }
            if ($newshopdomainstat == 1) {
                $newshopdomainstats = 'For Request';
            } elseif ($newshopdomainstat == 2) {
                $newshopdomainstats = 'Pending';
            } elseif ($newshopdomainstat == 3) {
                $newshopdomainstats = 'Activated';
            } elseif ($newshopdomainstat == 4) {
                $newshopdomainstats = 'Domain Unavailable';
            } else {
                $newshopdomainstats = ' ';
            }

            $shop_domain_remark = "Shop Domain Status changed from " . $oldshopdomainstats . " to " . $newshopdomainstats;
            $query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$shop_domain_remark','1', $account_id, $userid)";
            $result = mysql_query($query);
        }
        if ($_POST['shop_cmsstatus_orig'] <> $_POST['shop_cmsstatus']) {
            $oldshopcmsstat = $_POST['shop_cmsstatus_orig'];
            $newshopcmsstat = $_POST['shop_cmsstatus'];
            if ($oldshopcmsstat == 1) {
                $oldshopcmsstats = 'For Backup';
            } elseif ($oldshopcmsstat == 3) {
                $oldshopcmsstats = 'Activated';
            } elseif ($oldshopcmsstat == 4) {
                $oldshopcmsstats = 'Deactivated';
            } else {
                $oldshopcmsstats = ' ';
            }
            if ($newshopcmsstat == 1) {
                $newshopcmsstats = 'For Backup';
            } elseif ($newshopcmsstat == 3) {
                $newshopcmsstats = 'Activated';
            } elseif ($newshopcmsstat == 4) {
                $newshopcmsstats = 'Deactivated';
            } else {
                $newshopcmsstats = ' ';
            }

            $shop_cms_remark = "Shop CMS Status changed from " . $oldshopcmsstats . " to " . $newshopcmsstats;
            $query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$shop_cms_remark','1', $account_id, $userid)";
            $result = mysql_query($query);
        }
    }

    require_once("../models/tblog.php");
    $tblog = new TB_LOG();
    $datalog = array(
        'account_id' => $account_id,
        'user_id' => $userid ? $userid : $_SESSION['user_id'],
        'flag' => 1
    );

    $tblog->Insertaudit_account_log($datalog);

    if ($_POST['account_name_orig'] != $_POST['account_name']) {
        $changes .= 'acct name to "' . $_POST['account_name'] . '"';
    }
    if ($_POST['account_contactperson_orig'] != $_POST['account_contactperson']) {
        $changes .= 'contact person to "' . $_POST['account_contactperson'] . '"';
    }
    if ($_POST['account_contactperson_gender_orig'] != $_POST['account_contactperson_gender']) {
        $changes .= 'gender to "' . $_POST['account_contactperson_gender'] . '"';
    }
    if ($_POST['account_contactperson_age_orig'] != $_POST['account_contactperson_age']) {
        $changes .= 'age to "' . $_POST['account_contactperson_age'] . '"';
    }
    if ($_POST['account_telephone_orig'] != $_POST['account_telephone']) {
        $changes .= 'tel no. to "' . $_POST['account_telephone'] . '"';
    }
    if ($_POST['account_mobile_orig'] != $_POST['account_mobile']) {
        $changes .= 'mobile to "' . $_POST['account_mobile'] . '"';
    }
    if ($_POST['account_fax_orig'] != $_POST['account_fax']) {
        $changes .= 'fax to "' . $_POST['account_fax'] . '"';
    }
    if ($_POST['account_email_orig'] != $_POST['account_email']) {
        $changes .= 'email to "' . $_POST['account_email'] . '"';
    }
    if ($_POST['account_address_orig'] != $_POST['account_address']) {
        $changes .= 'address to "' . $_POST['account_address'] . '"';
    }
    if ($_POST['category_id_orig'] != $_POST['category_id']) {
        $changes .= 'category  to "' . $_POST['category_id'] . '"';
    }
    if ($_POST['source_id_orig'] != $_POST['source_id']) {
        $changes .= 'source  to "' . $_POST['source_id'] . '"';
    }
    if ($_POST['status_id_orig'] != $_POST['status_id']) {
        $changes .= 'status to "' . $_POST['status_id'] . '"';
    }
    if ($_POST['decline_reason_orig'] != $_POST['decline_reason']) {
        $changes .= 'decline reason to "' . $_POST['decline_reason'] . '"';
    }
    if ($_POST['other_reason_orig'] != $_POST['other_reason']) {
        $changes .= 'other reason to "' . $_POST['other_reason'] . '"';
    }
    if ($_POST['date_created_orig'] != $_POST['date_created']) {
        $changes .= 'date created to "' . $_POST['date_created'] . '"';
    }
    if ($_POST['account_memberid_orig'] != $_POST['account_memberid']) {
        $changes .= 'member id to "' . $_POST['account_memberid'] . '"';
    }
    if ($_POST['account_brno_orig'] != $_POST['account_brno']) {
        $changes .= 'B.R No. to "' . $_POST['account_brno'] . '"';
    }
    if ($_POST['account_adid_orig'] != $_POST['account_adid']) {
        $changes .= 'Ad ID to "' . $_POST['account_adid'] . '"';
    }
    if ($_POST['account_contractnumber_orig'] != $_POST['account_contractnumber']) {
        $changes .= 'contract no. to "' . $_POST['account_contractnumber'] . '"';
    }
    if ($_POST['account_contractamount_orig'] != $_POST['account_contractamount']) {
        $changes .= 'contract amount to "' . $_POST['account_contractamount'] . '"';
    }
    if ($_POST['shop_type_orig'] != $_POST['shop_type']) {
        $changes .= 'shoptype to "' . $_POST['shop_type'] . '"';
    }
    if ($_POST['shop_url_orig'] != $_POST['shop_url']) {
        $changes .= 'URL to "' . $_POST['shop_url'] . '"';
    }
    if ($_POST['shop_domain_orig'] != $_POST['shop_domain']) {
        $changes .= 'shop domain to "' . $_POST['shop_domain'] . '"';
    }
    if ($_POST['shop_domaintype_orig'] != $_POST['shop_domaintype']) {
        $changes .= 'domain type to "' . $_POST['shop_domaintype'] . '"';
    }
    if ($_POST['shop_domainstatus_orig'] != $_POST['shop_domainstatus']) {
        $changes .= 'domain status to "' . $_POST['shop_domainstatus'] . '"';
    }
    if ($_POST['shop_cmsstatus_orig'] != $_POST['shop_cmsstatus']) {
        $changes .= 'CMS status to "' . $_POST['shop_cmsstatus'] . '"';
    }
    if ($_POST['shop_csr_orig'] != $_POST['shop_csr']) {
        $changes .= 'CSR to "' . $_POST['shop_csr'] . '"';
    }
    if ($_POST['shop_designer_orig'] != $_POST['shop_designer']) {
        $changes .= 'Designer to "' . $_POST['shop_designer'] . '"';
    }
    if ($_POST['shop_editor_orig'] != $_POST['shop_editor']) {
        $changes .= 'Editor to "' . $_POST['shop_editor'] . '"';
    }

    if ($_POST['acc_classification'] != $_POST['acc_classification_data']) {
        $changes .= 'classification to "' . $_POST['acc_classification'] . '"';
    }
    $data = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'User ' . $_SESSION['user_id'] . ' updated account ' . $account_id . ' and changed[' . $changes . ']',
        'ip_add' => $_SESSION['ipaddniya']
    );
    $tblog->Insertaudit_log($data);
} else {
    $account_id = $_REQUEST['account_id'];
    $account_name = '';
    $account_contactperson = '';
    $account_telephone = '';
    $account_mobile = '';
    $account_fax = '';
    $account_email = '';
    $account_address = '';
    $category_id = 0;
    $source_id = 0;
    $status_id = 0;
    $package_id = 0;
    $account_memberid = '';
    $account_brno = '';
    $account_adid = '';
    $shop_type = 0;
    $shop_url = '';
    $shop_domain = '';
    $shop_domainstatus = 0;
    $shop_domaintype = 0;
    $shop_cmsstatus = 0;
    $shop_csr = 0;
    $shop_designer = 0;
    $shop_editor = 0;
    $account_contractnumber = '';
    $account_contractamount = 0;
}

$query = "SELECT a.*,DATE(a.account_createdon) as datecreated, concat(u1.user_firstname,' ',u1.user_lastname) AS createdby, concat(u2.user_firstname,' ',u2.user_lastname) AS modifiedby FROM accounts a LEFT JOIN users u1 ON a.account_createdby = u1.user_id LEFT JOIN users u2 ON a.account_modifiedby = u2.user_id WHERE a.account_id = $account_id";
$result = mysql_query($query);
$num = mysql_numrows($result);
if ($num > 0) {
    $account_name = mysql_result($result, 0, 'account_name');
    $account_contactperson = mysql_result($result, 0, 'account_contactperson');
    $account_contactperson_gender = mysql_result($result, 0, 'account_contactperson_gender');
    $account_contactperson_age = mysql_result($result, 0, 'account_contactperson_age');
    $account_telephone = mysql_result($result, 0, 'account_telephone');
    $account_mobile = mysql_result($result, 0, 'account_mobile');
    $account_fax = mysql_result($result, 0, 'account_fax');
    $account_email = mysql_result($result, 0, 'account_email');
    $account_address = mysql_result($result, 0, 'account_address');
    $category_id = mysql_result($result, 0, 'category_id');
    $source_id = mysql_result($result, 0, 'source_id');
    $status_id = mysql_result($result, 0, 'status_id');
    $package_id = mysql_result($result, 0, 'package_id');
    $account_memberid = mysql_result($result, 0, 'account_memberid');
    $account_brno = mysql_result($result, 0, 'account_brno');
    $account_adid = mysql_result($result, 0, 'account_adid');
    $shop_type = mysql_result($result, 0, 'shop_type');
    $shop_url = mysql_result($result, 0, 'shop_url');
    $shop_domain = mysql_result($result, 0, 'shop_domain');
    $shop_domainstatus = mysql_result($result, 0, 'shop_domainstatus');
    $shop_domaintype = mysql_result($result, 0, 'shop_domaintype');
    $shop_cmsstatus = mysql_result($result, 0, 'shop_cmsstatus');
    $event_id = mysql_result($result, 0, 'event_id');
    $account_contractnumber = mysql_result($result, 0, 'account_contractnumber');
    $account_contractamount = mysql_result($result, 0, 'account_contractamount') == "" ? 0 : mysql_result($result, 0, 'account_contractamount');
    $shop_csr = mysql_result($result, 0, 'shop_csr') == "" ? 0 : mysql_result($result, 0, 'shop_csr');
    $shop_designer = mysql_result($result, 0, 'shop_designer') == "" ? 0 : mysql_result($result, 0, 'shop_designer');
    $shop_editor = mysql_result($result, 0, 'shop_editor');
    $createdby = mysql_result($result, 0, 'createdby');
    $createdon = mysql_result($result, 0, 'account_createdon');
    $modifiedby = mysql_result($result, 0, 'modifiedby');
    $account_paid = mysql_result($result, 0, 'account_paid');
    $decline_id = mysql_result($result, 0, 'declined_status_id');
    $account_createdon = mysql_result($result, 0, 'datecreated');
    $account_classification = mysql_result($result, 0, 'account_classification');
    $decline_reason = mysql_result($result, 0, 'declined_status_reason');
    $account_seo = mysql_result($result, 0, 'account_seo');
    $account_ads = mysql_result($result, 0, 'account_ads');
    $account_backupv1 = mysql_result($result, 0, 'account_backupv1');
    $account_backupv2 = mysql_result($result, 0, 'account_backupv2');
    if ($decline_reason == "0") {
        $decline_reason = "";
    }
}

if ($status_id == 13) {

    if (($role_id == 3) AND ($dep_id == 2)) {
        $status_box = "disabled='disabled'";
    } else {
        $status_box = " ";
    }
}

$query = "SELECT category_id, category_name FROM categories order by category_name ASC";
$result = mysql_query($query);
$categoryoptions = "";
while ($row = mysql_fetch_array($result)) {
    $catid = $row["category_id"];
    $catname = $row["category_name"];
    if ($catid == $category_id) {
        $categoryoptions .= '<option value="' . $catid . '" selected="selected">' . $catname . "</option>";
    } else {
        $categoryoptions .= '<option value="' . $catid . '">' . $catname . "</option>";
    }
}

$query = "SELECT source_id, source_name FROM sources order by source_name ASC";
$result = mysql_query($query);
$sourceoptions = "";
while ($row = mysql_fetch_array($result)) {
    $sourceid = $row["source_id"];
    $sourcename = $row["source_name"];
    if ($sourceid == $source_id) {
        $sourceoptions .= '<option value="' . $sourceid . '" selected="selected">' . $sourcename . "</option>";
    } else {
        $sourceoptions .= '<option value="' . $sourceid . '">' . $sourcename . "</option>";
    }
}

$query = "SELECT event_id, event_name FROM events order by event_name ASC";
$result = mysql_query($query);
$eventoptions = "";
while ($row = mysql_fetch_array($result)) {
    $eventid = $row["event_id"];
    $eventname = $row["event_name"];
    if ($eventid == $event_id) {
        $eventoptions .= '<option value="' . $eventid . '" selected="selected" >' . $eventname . "</option>";
    } else {
        $eventoptions .= '<option value="' . $eventid . '">' . $eventname . "</option>";
    }
}


$query = "SELECT status_id, status_name, department_id FROM statuses ORDER BY status_name ASC";
$result = mysql_query($query);
$statusoptions = "";
while ($row = mysql_fetch_array($result)) {

    if ($dep_id == 4) {
        $statusid = $row["status_id"];
        $statusname = $row["status_name"];


        if ($statusid == $status_id) {
            $statusoptions .= '<option value="' . $statusid . '" selected="selected">' . $statusname . "</option>";
        } else {
            $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
        }
    } else {
        if ($row['department_id'] == $dep_id || $row['status_id'] == $status_id) {

            $statusid = $row["status_id"];
            $statusname = $row["status_name"];


            if ($statusid == $status_id) {
                $statusoptions .= '<option value="' . $statusid . '" selected="selected">' . $statusname . "</option>";
            } else {
                $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
            }
        }
    }
}

$query = "SELECT package_id, package_name FROM packages order by package_name ASC";
$result = mysql_query($query);
$packageoptions = "";
while ($row = mysql_fetch_array($result)) {
    $packageid = $row["package_id"];
    $packagename = $row["package_name"];
    if ($packageid == $package_id) {
        $packageoptions .= '<option value="' . $packageid . '" selected="selected">' . $packagename . "</option>";
    } else {
        $packageoptions .= '<option value="' . $packageid . '">' . $packagename . "</option>";
    }
}

$shoptypesoptions = "";
if ($shop_type == 1) {
    $shoptypesoptions = '<option value="1" selected="selected">88DB</option><option value="2">OpenRice</option>';
} elseif ($shop_type == 2) {
    $shoptypesoptions = '<option value="1">88DB</option><option value="2" selected="selected">OpenRice</option>';
} else {
    $shoptypesoptions = '<option value="1">88DB</option><option value="2">OpenRice</option>';
}

//$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 6  ORDER BY designer ASC ";
$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE    enabled = 1 AND job_desc = 6  ORDER BY designer ASC ";

$result = mysql_query($query);
$csr = "";
while ($row = mysql_fetch_array($result)) {
    $csrid = $row["user_id"];
    $csrname = $row["designer"];
    if ($csrid == $shop_csr) {
        $csr .= '<option value="' . $csrid . '" selected="selected">' . $csrname . "</option>";
    } else {
        $csr .= '<option value="' . $csrid . '">' . $csrname . "</option>";
    }
}


$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 2 ORDER BY designer ASC ";
$result = mysql_query($query);
$designers = "";
while ($row = mysql_fetch_array($result)) {
    $designerid = $row["user_id"];
    $designername = $row["designer"];
    if ($designerid == $shop_designer) {
        $designers .= '<option value="' . $designerid . '" selected="selected">' . $designername . "</option>";
    } else {
        $designers .= '<option value="' . $designerid . '">' . $designername . "</option>";
    }
}

//$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as editorname FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 3 ORDER BY editorname ASC ";
$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as editorname FROM users WHERE   enabled = 1 AND job_desc = 3 ORDER BY editorname ASC ";

$result = mysql_query($query);
$editors = "";
while ($row = mysql_fetch_array($result)) {
    $editorid = $row["user_id"];
    $editorname = $row["editorname"];
    if ($editorid == $shop_editor) {
        $editors .= '<option value="' . $editorid . '" selected="selected">' . $editorname . "</option>";
    } else {
        $editors .= '<option value="' . $editorid . '">' . $editorname . "</option>";
    }
}

$query = "SELECT SUM(payment_amount) as paid_amount FROM payments WHERE account_id = $account_id";
$result = mysql_query($query);
$num = mysql_numrows($result);
if ($num > 0) {
    $paid_amount = mysql_result($result, 0, 'paid_amount');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Edit Account</title>

        <?php include '../header.php'; ?>

    <div class="main-section">

        <form name="form1" id="form1" action="edit.php?account_id=<?php echo $_GET['account_id']; ?>" method="post" onsubmit="return statusCheck()" >

            <div class="commands">

                <div class="head-label">
                    <h2>Edit Account</h2>
                </div><!-- end of add new account -->

                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/accounts/'>Cancel</a></li>
                    <?php if ($_SESSION['department_id'] == 1 || $_SESSION['department_id'] == 4): ?>
                        <li><a class="link-button gray" href='/88dbphcrm/accounts/aging.php?account_id=<?php echo $_GET['account_id']; ?>'>Aging</a></li>

                    <?php endif; ?>
                    <?php if ($_SESSION['role_id'] == 1 && $_SESSION['department_id'] == 4): ?>
                        <li><a class="link-button gray" href='/88dbphcrm/accounts/history.php?account_id=<?php echo $_GET['account_id']; ?>'>Log</a></li>
                    <?php endif; ?>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

                <table>
                    <tr>
                        <td class="grid-head">Account Id:</td>
                        <td>&nbsp;</td>
                        <td><?php echo $account_id; ?><input type="hidden" name="paccount_id" id="paccount_id" value="<?php echo $account_id; ?>"/></td>
                        <td rowspan="40" style="width: 35%; vertical-align: top;">
                            <?php
                            $uid = $account_id;
                            $type = $_SESSION['department_id'];

//echo "<a class='link-button' style='background-color: #e3e3e3; border: 1px solid #ddd; color: #666;' href='/88dbphcrm/remarks/add.php?uid=".$uid."&type=1'>Add New Remark</a><br><br>";

                            echo "<a class='iframe link-button' style='background-color: #F68520; border: 1px solid #F68520; color: #9C4114;' href='/88dbphcrm/remarks/add.php?uid=" . $uid . "&type=1&account_id=" . $account_id . "'>Add New Remark</a><br><br>";

                            mysql_connect('localhost', 'root', '');
                            mysql_select_db('88dbphcrm') or die("Unable to select database");
                            $query = "SELECT r.*, concat(u.user_firstname, ' ', u.user_lastname) as r_createdby FROM remarks r LEFT JOIN users u ON r.remark_createdby = u.user_id WHERE r.remark_uid = $uid AND r.remark_type = 1 ORDER BY r.remark_createdon DESC";
                            $result = mysql_query($query);
                            if (mysql_num_rows($result) > 0) {
                                echo "<div class='remarks-head'><h3>Remarks:</h3></div>";
                            }
                            while ($row = mysql_fetch_array($result)) {
                                echo "<div class='remarks'>";
                                echo "<span style='color: black;'>" . $row['remark_remarks'] . "</span><br>";
                                echo "By: " . $row['r_createdby'] . " on: " . $row['remark_createdon'] . "<br>";
                                echo "</div>";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Account Name:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_name" id="account_name" value="<?php echo $account_name; ?>" class="required"/>
                            <input type="hidden" name="account_name_orig" id="account_name_orig" value="<?php echo $account_name; ?>"/>
                            <input type="hidden" name="verify_accountname" id="verify_accountname" />

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Contact Person:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_contactperson" id="account_contactperson" value="<?php echo $account_contactperson; ?>" class="required"/>
                            <input type="hidden" name="account_contactperson_orig" id="account_contactperson_orig" value="<?php echo $account_contactperson; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Contact Person gender:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="account_contactperson_gender" id="account_contactperson_gender" class="required">
                                <option value="">Select Gender</option>
                                <option value="1" <?php If ($account_contactperson_gender == 1): ?> selected="selected" <?php endif; ?>>Male</option>
                                <option value="2" <?php If ($account_contactperson_gender == 2): ?> selected="selected" <?php endif; ?>>Female</option>
                            </select>
                            <input type="hidden" name="account_contactperson_gender_orig" id="account_contactperson_gender_orig" value="<?php echo $account_contactperson_gender; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Contact Person Age:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_contactperson_age" id="account_contactperson_age"   value="<?php echo $account_contactperson_age; ?>"/>
                            <input type="hidden" name="account_contactperson_age_orig" id="account_contactperson_age_orig"   value="<?php echo $account_contactperson_age; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Telephone:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_telephone" id="account_telephone" value="<?php echo $account_telephone; ?>" class="required"/>
                            <input type="hidden" name="account_telephone_orig" id="account_telephone_orig" value="<?php echo $account_telephone; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Mobile:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_mobile" id="account_mobile" value="<?php echo $account_mobile; ?>"/>
                            <input type="hidden" name="account_mobile_orig" id="account_mobile_orig" value="<?php echo $account_mobile; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Fax:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_fax" id="account_fax" value="<?php echo $account_fax; ?>"/>
                            <input type="hidden" name="account_fax_orig" id="account_fax_orig" value="<?php echo $account_fax; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Email:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_email" id="account_email" value="<?php echo $account_email; ?>" class="account_email"/>
                            <input type="hidden" name="account_email_orig" id="account_email_orig" value="<?php echo $account_email; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Address:</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea cols="25" rows="3" name="account_address" id="account_address" class="required"><?php echo $account_address; ?></textarea>
                            <input type="hidden" name="account_address_orig" id="account_address_orig" value="<?php echo $account_address; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Category:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='category_id' id='category_id' class="required">
                                <option value="">Select Category</option>
                                <?php echo $categoryoptions; ?>
                            </select>
                            <input type="hidden" name="category_id_orig" id="category_id_orig" value="<?php echo $category_id; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Source:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='source_id' id='source_id' class="required">
                                <option value="">Select Source</option>
                                <?php echo $sourceoptions; ?>
                            </select> 
                            <input type="hidden" name="source_id_orig" id="source_id_orig" value="<?php echo $source_id; ?>"/>


                        </td>
                    </tr>
                    <tr id="events_source">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>

                            <select name="event_box" id="event_box">
                                <option value="">Select Events</option>
                                <?php echo $eventoptions; ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Status:</td>
                        <td style="background-color: blue;">&nbsp;</td>
                        <td >
                            <?php if ($dep_id == 4 && $role_id == 1): ?>
                                <select name='status_id' id='status_id' class="required" <?php echo $status_box; ?> >
                                    <option value="">Select Status</option>
                                    <?php
                                    $query = "SELECT status_id, status_name, department_id FROM statuses ORDER BY status_name ASC";
                                    $result = mysql_query($query);
                                    $statusoptions = "";
                                    while ($row = mysql_fetch_array($result)) {
                                        $statusid = $row["status_id"];
                                        $statusname = $row["status_name"];


                                        if ($statusid == $status_id) {
                                            $statusoptions .= '<option value="' . $statusid . '" selected="selected">' . $statusname . "</option>";
                                        } else {
                                            $statusoptions .= '<option value="' . $statusid . '">' . $statusname . "</option>";
                                        }
                                    }

                                    echo $statusoptions;
                                    ?>
                                </select> 
                            <?php elseif ($status_id == 22): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15">For Follow Up</option>
                                    <option value="22" selected="selected">For Prospecting</option>
                                    <option value="19">Interested</option>
                                    <option value="11">Sent Proposal</option>
                                    <option value="10">Set Appointment</option>
                                    <option value="20">For Signing</option>

                                </select> 
                            <?php elseif ($status_id == 10): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15">For Follow Up</option>

                                    <option value="19">Interested</option>
                                    <option value="11">Sent Proposal</option>
                                    <option value="10" selected="selected">Set Appointment</option>
                                    <option value="20">For Signing</option>

                                </select> 
                            <?php elseif ($status_id == 11): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15">For Follow Up</option>

                                    <option value="19">Interested</option>
                                    <option value="11" selected="selected">Sent Proposal</option>
                                    <option value="10">Set Appointment</option>
                                    <option value="20">For Signing</option>

                                </select> 
                            <?php elseif ($status_id == 12): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12" selected="selected">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15">For Follow Up</option>
                                    <option value="19">Interested</option>                            
                                    <option value="10">Set Appointment</option>
                                    <option value="20">For Signing</option>

                                </select> 
                            <?php elseif ($status_id == 15): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15" selected="selected">For Follow Up</option>
                                    <option value="19">Interested</option>                            
                                    <option value="10">Set Appointment</option>
                                    <option value="20">For Signing</option>
                                </select> 
                            <?php elseif ($status_id == 19): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="14">Declined</option>
                                    <option value="12">For Callback</option>
                                    <option value="23">For Deletion</option>
                                    <option value="15">For Follow Up</option>
                                    <option value="19" selected="selected">Interested</option>                            
                                    <option value="10">Set Appointment</option>
                                    <option value="20">For Signing</option>
                                </select> 
                            <?php elseif ($status_id == 14): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="14"  selected="selected">Declined</option>                                 
                                    <option value="23">For Deletion</option>


                                </select> 
                            <?php elseif ($status_id == 20): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="20" selected="selected">For Signing</option>
                                    <option value="13">Signed (for Payment)</option>
                                </select> 
                            <?php elseif ($status_id == 23): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="23" selected="selected">For Deletion</option>
                                </select> 
                            <?php elseif ($status_id == 13): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="13"  selected="selected">Signed (for Payment)</option>
                                    <option value="16">Paid (Full)</option>
                                    <option value="17">Paid (Partial)</option>
                                </select> 
                            <?php elseif ($status_id == 17): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="17"  selected="selected">Paid (Partial)</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="49">REJECTED BY FINANCE - No Payment</option>
                                    <option value="60">REJECTED BY FINANCE - Incomplete Mats</option>
                                    <option value="61">REJECTED BY FINANCE - Incomplete WCF</option>
                                    <option value="59">REJECTED BY FINANCE- Unclear WCF</option>
                                </select> 
                            <?php elseif ($status_id == 16): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="16" selected="selected">Paid (Full)</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="49">REJECTED BY FINANCE - No Payment</option>
                                    <option value="60">REJECTED BY FINANCE - Incomplete Mats</option>
                                    <option value="61">REJECTED BY FINANCE - Incomplete WCF</option>
                                    <option value="59">REJECTED BY FINANCE- Unclear WCF</option>
                                    <option value="9">Live</option>
                                </select> 
                            <?php elseif ($status_id == 1): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="1" selected="selected">Lined Up for Production</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="2">For Proofreading</option>
                                    <option value="51">REJECTED BY CSR - Incomplete Mats</option>
                                    <option value="52">REJECTED BY CSR - Incomplete WCF</option>
                                    <option value="50">REJECTED BY CSR - Unclear WCF</option>
                                </select> 
                            <?php elseif ($status_id == 2): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="21">For Development</option>
                                    <option value="2" selected="selected">For Proofreading</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="55">REJECTED BY PROD- Unclear WCF</option>                                    
                                    <option value="57">REJECTED BY PROD - Incomplete WCF</option>

                                </select> 
                            <?php elseif ($status_id == 21): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="21" selected="selected">For Development</option>
                                    <option value="3">Under Construction</option>

                                </select> 
                            <?php elseif ($status_id == 3): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="58">REJECTED BY PROD - Instructions Not Followed</option>
                                    <option value="3" selected="selected">Under Construction</option>

                                </select> 
                            <?php elseif ($status_id == 4): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="4" selected="selected">For Editor QA</option>
                                    <option value="38">CSR QA</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>

                                    <option value="7">Internal Revisions 1</option>
                                    <option value="28">Internal Revisions 2</option>
                                    <option value="29">Internal Revisions 3</option>
                                    <option value="5">Design QA</option>
                                    <option value="63">REJECTED by Editor - Ins.not followed 1</option>
                                    <option value="64">REJECTED by Editor - Ins.not followed 2</option>
                                    <option value="65">REJECTED by Editor - Ins.not followed 3</option>
                                </select> 
                            <?php elseif ($status_id == 7): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="7" selected="selected">Internal Revisions 1</option>
                                    <option value="28">Internal Revisions 2</option>
                                    <option value="29">Internal Revisions 3</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="5">Design QA</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 28): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>


                                    <option value="28" selected="selected">Internal Revisions 2</option>
                                    <option value="29">Internal Revisions 3</option>
                                    <option value="5">Design QA</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 29): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="5">Design QA</option>
                                    <option value="29" selected="selected">Internal Revisions 3</option>
                                    <option value="58">REJECTED BY PROD - Instructions Not Followed</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 5): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="5"  selected="selected">Design QA</option>
                                    <option value="7">Internal Revisions  - Editor 1</option>
                                    <option value="28">Internal Revisions - Editor 2</option>
                                    <option value="29">Internal Revisions - Editor 3</option>
                                    <option value="32">Internal Revisions QA - 1</option>
                                    <option value="33">Internal Revisions QA - 2</option>
                                    <option value="34">Internal Revisions QA - 3</option>
                                    <option value="38">CSR QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                </select>
                            <?php elseif ($status_id == 32): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="32" selected="selected">Internal Revisions QA - 1</option>
                                    <option value="33">Internal Revisions QA - 2</option>
                                    <option value="34">Internal Revisions QA - 3</option>
                                    <option value="38">CSR QA</option>
                                    <option value="5">Design QA</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 33): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>


                                    <option value="33"  selected="selected">Internal Revisions QA - 2</option>
                                    <option value="34">Internal Revisions QA - 3</option>
                                    <option value="38">CSR QA</option>
                                    <option value="5">Design QA</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 34): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="38">CSR QA</option>
                                    <option value="34" selected="selected">Internal Revisions QA - 3</option>
                                    <option value="3">Under Construction</option>
                                    <option value="5">Design QA</option>
                                </select>
                            <?php elseif ($status_id == 38): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="38" selected="selected">CSR QA</option>


                                    <option value="6">For Client Approval</option>
                                    <option value="53">REJECTED BY CSR - Instructions Not Followed</option>

                                </select>
                            <?php elseif ($status_id == 6): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="6" selected="selected">For Client Approval</option>
                                    <option value="53">REJECTED BY CSR - Instructions Not Followed</option>
                                    <option value="54">REJECTED BY CSR - Design not Followed</option>
                                    <option value="9">Live</option>
                                    <option value="35">Temporary Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="25">For Client Approval - Consolidation</option>
                                    <option value="26">For Client Approval - Feedback</option


                                </select>
                            <?php elseif ($status_id == 25): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="25" selected="selected">For Client Approval - Consolidation</option>
                                    <option value="26">For Client Approval - Feedback</option>
                                    <option value="27">Client Updates</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="35">Temporary Live</option>

                                </select>
                            <?php elseif ($status_id == 26): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="25">For Client Approval - Consolidation</option>
                                    <option value="26" selected="selected">For Client Approval - Feedback</option>
                                    <option value="27">Client Updates</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="35">Temporary Live</option>


                                </select>
                            <?php elseif ($status_id == 27): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="27" selected="selected">Client Updates</option>
                                    <option value="5">Design QA</option>
                                    <option value="3">Under Construction</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>


                                </select>
                            <?php elseif ($status_id == 8): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="8" selected="selected">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="5">Design QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>

                                </select>
                            <?php elseif ($status_id == 30): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="30" selected="selected">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>

                                </select>
                            <?php elseif ($status_id == 31): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>


                                    <option value="31" selected="selected">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 41): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="41" selected="selected">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>

                                </select>
                            <?php elseif ($status_id == 42): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>  
                                    <option value="42" selected="selected">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>
                                </select>

                            <?php elseif ($status_id == 37): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="5">Design QA</option>
                                    <option value="37"  selected="selected">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="3">Under Construction</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 43): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="5">Design QA</option>

                                    <option value="43"  selected="selected">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="3">Under Construction</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 44): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="5">Design QA</option>                                  
                                    <option value="44" selected="selected">Redesign 3</option>
                                    <option value="3">Under Construction</option>
                                    <option value="62">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="56">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="3">Under Construction</option>
                                </select>
                            <?php elseif ($status_id == 35): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="35" selected="selected">Temporary Live</option>
                                    <option value="25">For Client Approval - Consolidation</option>
                                    <option value="26">For Client Approval - Feedback</option>5
                                    <option value="36">Temporary Live - Awaiting Balance Collection</option>
                                    <option value="46">Temporary Live - Awaiting Final Approval from Client</option>
                                    <option value="45">Temporary Live - Awaiting Mats from Client</option>
                                    <option value="9">Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="24">Temporary Closed</option>
                                </select>
                            <?php elseif ($status_id == 45): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="45" selected="selected">Temporary Live - Awaiting Mats from Client</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="35">Temporary Live</option>
                                    <option value="24">Temporary Closed</option>
                                </select>
                            <?php elseif ($status_id == 46): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="46" selected="selected">Temporary Live - Awaiting Final Approval from Client</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="5">Design QA</option>
                                    <option value="9">Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="35">Temporary Live</option>
                                    <option value="24">Temporary Closed</option>
                                </select>
                            <?php elseif ($status_id == 36): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="36" selected="selected">Temporary Live - Awaiting Balance Collection</option>
                                    <option value="9">Live</option>
                                    <option value="16">Paid (Full)</option>
                                </select>
                            <?php elseif ($status_id == 9): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="9"  selected="selected">Live</option>
                                    <option value="27">Client Updates</option>
                                    <option value="24">Temporary Closed</option>
                                    <option value="47">Temporary Closed - Delinquent</option>
                                    <option value="48">Temporary Closed - Request from Client</option>
                                </select>
                            <?php elseif ($status_id == 24): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="9" >Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="6">For Client Approval</option>
                                    <option value="24" selected="selected">Temporary Closed</option>

                                </select>
                            <?php elseif ($status_id == 47): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="9" >Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>

                                    <option value="47" selected="selected">Temporary Closed - Delinquent</option>
                                </select>
                            <?php elseif ($status_id == 48): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="9" >Live</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                    <option value="48" selected="selected">Temporary Closed - Request from Client</option>

                                </select>
                            <?php elseif ($status_id == 49): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="49"  selected="selected">REJECTED BY FINANCE - No Payment</option>
                                    <option value="13">Signed (for Payment)</option>
                                </select>

                            <?php elseif ($status_id == 50): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="50" selected="selected">REJECTED BY CSR - Unclear WCF</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="2">For Proofreading</option>
                                </select>
                            <?php elseif ($status_id == 51): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="51" selected="selected">REJECTED BY CSR - Incomplete Mats</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="2">For Proofreading</option>
                                </select>
                            <?php elseif ($status_id == 52): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="52" selected="selected">REJECTED BY CSR - Incomplete WCF</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="2">For Proofreading</option>
                                </select>
                            <?php elseif ($status_id == 53): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="53" selected="selected">REJECTED BY CSR - Instructions Not Followed</option>
                                    <option value="5">Design QA</option>
                                    <option value="7">Internal Revisions 1</option>
                                    <option value="28">Internal Revisions 2</option>
                                    <option value="29">Internal Revisions 3</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                </select>
                            <?php elseif ($status_id == 54): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="54" selected="selected">REJECTED BY CSR - Design not Followed</option>
                                    <option value="5">Design QA</option>
                                    <option value="7">Internal Revisions 1</option>
                                    <option value="28">Internal Revisions 2</option>
                                    <option value="29">Internal Revisions 3</option>
                                    <option value="37">Redesign 1</option>
                                    <option value="43">Redesign 2</option>
                                    <option value="44">Redesign 3</option>
                                </select>
                            <?php elseif ($status_id == 55): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="55" selected="selected">REJECTED BY PROD - Unclear WCF</option>
                                    <option value="2">For Proofreading</option>
                                    <option value="1">Lined Up for Production</option>
                                </select>
                            <?php elseif ($status_id == 56): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="56" selected="selected">REJECTED BY PROD - Incomplete Mats</option>
                                    <option value="2">For Proofreading</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="8">Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                </select>
                            <?php elseif ($status_id == 57): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="57" selected="selected">REJECTED BY PROD - Incomplete WCF</option>
                                    <option value="2">For Proofreading</option>
                                    <option value="1">Lined Up for Production</option>
                                </select>
                            <?php elseif ($status_id == 58): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="58" selected="selected">REJECTED BY PROD - Instructions Not Followed</option>
                                    <option value="1">Lined Up for Production</option>
                                    <option value="4">For Editor QA</option>
                                    <option value="2">For Proofreading</option>
                                    <option value="3">Under Construction</option> 
                                    <option value="38">CSR QA</option>
                                </select>
                            <?php elseif ($status_id == 59): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="59" selected="selected">REJECTED BY FINANCE - Unclear WCF</option>
                                    <option value="16">Paid (Full)</option>
                                    <option value="17">Paid (Partial)</option>
                                </select>
                            <?php elseif ($status_id == 60): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="60" selected="selected">REJECTED BY FINANCE - Incomplete Mats</option>
                                    <option value="16">Paid (Full)</option>
                                    <option value="17">Paid (Partial)</option>
                                </select>
                            <?php elseif ($status_id == 61): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>
                                    <option value="61" selected="selected">REJECTED BY FINANCE - Incomplete WCF</option>
                                    <option value="16">Paid (Full)</option>
                                    <option value="17">Paid (Partial)</option>
                                </select>
                            <?php elseif ($status_id == 62): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="62" selected="selected">REJECTED BY PROD - Unclear Instruction</option>
                                    <option value="8" >Client Revisions 1</option>
                                    <option value="30">Client Revisions 2</option>
                                    <option value="31">Client Revisions 3</option>
                                    <option value="41">Client Revisions 4</option>
                                    <option value="42">Client Revisions 5</option>
                                </select>
                            <?php elseif ($status_id == 39): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="39" selected="selected">REJECTED BY FINANCE - No Contract</option>
                                    <option value="13" >Signed (for Payment)</option>

                                </select>
                            <?php elseif ($status_id == 63): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="63" selected="selected">REJECTED by Editor - Instructions Not Followed 1</option>
                                    <option value="3" >Under Construction</option>

                                </select>
                            <?php elseif ($status_id == 64): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="64" selected="selected">REJECTED by Editor - Instructions Not Followed 2</option>
                                    <option value="3" >Under Construction</option>

                                </select>
                            <?php elseif ($status_id == 65): ?>
                                <select name='status_id' id='status_id' class="required">
                                    <option value="">Select Status</option>

                                    <option value="65" selected="selected">REJECTED by Editor - Instructions Not Followed 3</option>
                                    <option value="3" >Under Construction</option>

                                </select>

                            <?php endif; ?>
                            <input type="hidden" id="oldstatusid" name="oldstatusid" value="<?php echo $status_id; ?>" />
                            <input type="hidden" id="status_id_orig" name="status_id_orig" value="<?php echo $status_id; ?>" />
                            <input type="hidden" id="newstatusid" name="newstatusid" value="" />
                            <input type="hidden" id="account_paid" name="account_paid" value="<?php echo $account_paid; ?>" />

                        </td>
                    </tr>
                    <tr id="decline_container">
                        <td class="grid-head">*Reason:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='decline_reason' id='decline_reason' >
                                <option value="">Select Reason</option>
                                <option <?php if ($decline_id == 1)
                                echo "selected='selected'"; ?>value="1">Already Has a Website</option>
                                <option <?php if ($decline_id == 2)
                                        echo "selected='selected'"; ?>value="2">Website Not a Priority</option>
                                <option <?php if ($decline_id == 3)
                                        echo "selected='selected'"; ?>value="3">No Budget / Limited Resources</option>
                                <option <?php if ($decline_id == 4)
                                        echo "selected='selected'"; ?>value="4">Not Keen on Online Marketing</option>
                                <option <?php if ($decline_id == 5)
                                        echo "selected='selected'"; ?>value="5">Package is Expensive</option>
                                <option <?php if ($decline_id == 6)
                                        echo "selected='selected'"; ?>value="6">Limited Features</option>
                                <option <?php if ($decline_id == 7)
                                        echo "selected='selected'"; ?>value="7">Other</option>

                            </select> 
                            <input type="hidden" id="decline_reason_orig" name="decline_reason_orig" value="<?php echo $decline_id; ?>" />
                        </td>
                    </tr>
                    <tr id="reason_container">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <input type="text" id="other_reason" name="other_reason" value="<?php echo $decline_reason; ?>" />
                            <input type="hidden" id="other_reason_orig" name="other_reason_orig" value="<?php echo $decline_reason; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Package:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='package_id' id='package_id' class="required">
                                <option value=''>Select Package</option>
                                <?php echo $packageoptions; ?>
                            </select> 
                            <input type="hidden" id="package_id_orig" name="package_id_orig" value="<?php echo $package_id; ?>" />

                            <div style="float:right;margin-right:60px;">
                                <?php if (!empty($account_contractnumber) || $dep_id != 2) { ?>
                                    <a class="link-button" style="background-color: red; border: 1px solid red; color: #fff;margin-bottom:0px;" href="/88dbphcrm/addons/add.php?cid=<?php echo $account_contractnumber; ?>">Add Add-ons</a>
                                    <?php
                                }
                                ?>
                            </div>

                        </td>
                    </tr>
                    <?php if (($role_id == 1 && $dep_id == 4) || ($role_id == 4 && $dep_id == 2)) { ?>
                        <tr>
                            <td class="grid-head">*Account Created On:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="text" name="date_created" maxlength="10" id="date_created" class="required" value="<?php echo $account_createdon; ?>"/>
                                <input type="hidden" name="date_created_orig" maxlength="10" id="date_created_orig"  value="<?php echo $account_createdon; ?>"/>

                                <input type="hidden" name="time_created" id="time_created" />
                            </td>
                        </tr>
                        <tr>
                            <td class="grid-head">Seo:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="checkbox" name="account_seo" id="account_seo" value="1" <?php
                    if ($account_seo == 1) {
                        echo 'checked="checked"';
                    }
                        ?>/>
                                <input type="hidden" name="account_seo_hidden" id="account_seo_hidden" value="<?php echo $account_seo ?>"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="grid-head">With Ads:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="checkbox" name="account_ads" id="account_ads" value="1" <?php
                                   if ($account_ads == 1) {
                                       echo 'checked="checked"';
                                   }
                        ?>/>
                                <input type="hidden" name="account_ads_hidden" id="account_ads_hidden" value="<?php echo $account_ads ?>"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="grid-head">With Backup:</td>
                            <td>&nbsp;</td>
                            <td>
                                <span>Cms V.1</span>
                                <input type="checkbox" name="account_backupv1" id="account_backupv1" value="1" <?php
                                   if ($account_backupv1 == 1) {
                                       echo 'checked="checked"';
                                   }
                        ?>/>
                                <input type="hidden" name="account_backupv1_hidden" id="account_backupv1_hidden" value="<?php echo $account_backupv1 ?>"  />
                                <span>Cms V.2</span>
                                <input type="checkbox" name="account_backupv2" id="account_backupv2" value="1" <?php
                                   if ($account_backupv2 == 1) {
                                       echo 'checked="checked"';
                                   }
                        ?>/>
                                <input type="hidden" name="account_backupv2_hidden" id="account_backupv2_hidden" value="<?php echo $account_backupv2 ?>"  />
                            </td>
                        </tr>

                        <tr>
                            <td class="grid-head">*Account classification :</td>
                            <td>&nbsp;</td>
                            <td>
                                <select name='acc_classification' id='acc_classification' class="required">
                                    <option value="">Select Classification</option>
                                    <option value="1" <?php
                                   if ($account_classification == 1) {
                                       echo 'selected';
                                   }
                        ?>>VIP</option>
                                    <option value="2" <?php
                                        if ($account_classification == 2) {
                                            echo 'selected';
                                        }
                        ?>>Irate</option>
                                    <option value="3" <?php
                                        if ($account_classification == 3) {
                                            echo 'selected';
                                        }
                        ?>>Cooperative</option>
                                    <option value="4" <?php
                                        if ($account_classification == 4) {
                                            echo 'selected';
                                        }
                        ?>>Aging</option>
                                    <option value="5" <?php
                                        if ($account_classification == 5) {
                                            echo 'selected';
                                        }
                        ?>>Normal (default)</option>
                                    <option value="6" <?php
                                        if ($account_classification == 6) {
                                            echo 'selected';
                                        }
                        ?> >Non-cooperative</option>

                                </select>

                            </td>
                        </tr>
                    <?php } elseif (($role_id == 1 && $dep_id == 1) || ($role_id == 1 && $dep_id == 4)) { ?>
                        <tr>
                            <td class="grid-head">Seo:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="checkbox" name="account_seo" id="account_seo" value="1" <?php
                    if ($account_seo == 1) {
                        echo 'checked="checked"';
                    }
                        ?>/>
                                <input type="hidden" name="account_seo_hidden" id="account_seo_hidden" value="<?php echo $account_seo ?>"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="grid-head">With Ads:</td>
                            <td>&nbsp;</td>
                            <td>
                                <input type="checkbox" name="account_ads" id="account_ads" value="1" <?php
                                   if ($account_ads == 1) {
                                       echo 'checked="checked"';
                                   }
                        ?>/>
                                <input type="hidden" name="account_ads_hidden" id="account_ads_hidden" value="<?php echo $account_ads ?>"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="grid-head">*Account classification :</td>
                            <td>&nbsp;</td>
                            <td>
                                <select name='acc_classification' id='acc_classification' class="required">
                                    <option value="">Select Classification</option>
                                    <option value="1" <?php
                                   if ($account_classification == 1) {
                                       echo 'selected';
                                   }
                        ?>>VIP</option>
                                    <option value="2" <?php
                                        if ($account_classification == 2) {
                                            echo 'selected';
                                        }
                        ?>>Irate</option>
                                    <option value="3" <?php
                                        if ($account_classification == 3) {
                                            echo 'selected';
                                        }
                        ?>>Cooperative</option>
                                    <option value="4" <?php
                                        if ($account_classification == 4) {
                                            echo 'selected';
                                        }
                        ?>>Aging</option>
                                    <option value="5" <?php
                                        if ($account_classification == 5) {
                                            echo 'selected';
                                        }
                        ?>>Normal (default)</option>
                                    <option value="6" <?php
                                        if ($account_classification == 6) {
                                            echo 'selected';
                                        }
                        ?> >Non-cooperative</option>

                                </select>

                            </td>
                        </tr>
                    <?php } ?>
                    <input type="hidden" name="acc_classification_data" id="acc_classification_data" value="<?php echo $account_classification; ?>"/>
                    <tr>
                        <td class="grid-head">Member ID:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_memberid" id="account_memberid" value="<?php echo $account_memberid; ?>"/>
                            <input type="hidden" name="account_memberid_orig" id="account_memberid_orig" value="<?php echo $account_memberid; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">B.R. No.:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_brno" id="account_brno" value="<?php echo $account_brno; ?>"/>
                            <input type="hidden" name="account_brno_orig" id="account_brno_orig" value="<?php echo $account_brno; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Ad ID:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="account_adid" id="account_adid" value="<?php echo $account_adid; ?>"/>
                            <input type="hidden" name="account_adid_orig" id="account_adid_orig" value="<?php echo $account_adid; ?>"/>

                        </td>
                    </tr>   
                    <tr>
                        <td class="grid-head">Contract Number:</td>
                        <td style="background-color: green;">&nbsp;</td>
                        <td><input type="text" <?php
                    if ($dep_id == 2) {
                        echo "readonly='readonly'";
                    }
                    ?> name="account_contractnumber" id="account_contractnumber" value="<?php echo $account_contractnumber; ?>"/>
                            <input type="hidden" name="account_contractnumber_orig" id="account_contractnumber_orig" value="<?php echo $account_contractnumber; ?>"/>

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Contract Amount:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" <?php
                                   if ($dep_id == 2) {
                                       echo "readonly='readonly'";
                                   }
                    ?> name="account_contractamount" id="account_contractamount" value="<?php echo $account_contractamount; ?>"/>
                            <input type="hidden" name="account_contractamount_orig" id="account_contractamount_orig" value="<?php echo $account_contractamount; ?>"/>

                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Paid Amount:</td>
                        <td>&nbsp;</td>
                        <td>
                            <div style="width:362px;">
                                <div style="float:left;">
                                    <strong><?php echo number_format($paid_amount, 2, '.', ','); ?></strong>
                                    <input type="hidden" readonly="readonly" name="paid_amount" id="paid_amount" value="

                                           <?php
                                           if ($paid_amount == "") {

                                               echo "0";
                                           } else {
                                               echo $paid_amount;
                                           }
                                           ?>" />
                                </div>
                                <div style="float:right;">
                                    <?php if (!empty($account_contractnumber) || $dep_id != 2) { ?>

                                        <a id="add-payment" class="link-button" style="background-color: red; border: 1px solid red; color: #fff;margin-bottom:0px;" href="/88dbphcrm/payments/add.php?cid=<?php echo $account_contractnumber; ?>">Add Payment</a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td class="grid-head">Balance:</td>
                        <td>&nbsp;</td>
                        <td><strong><?php echo number_format($account_contractamount - $paid_amount, 2, '.', ','); ?></strong></td>
                    </tr>
                    <tr>
                        <td class="grid-head">Shop Type:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_type' id='shop_type' class='required'>
                                <option value=''>Select Shop Type</option>
                                <?php echo $shoptypesoptions; ?>
                            </select>
                            <input type="hidden" name="shop_type_orig" id="shop_type_orig" value="<?php echo $shop_type; ?>"/>
                        </td>
                    </tr>              
                    <tr>
                        <td class="grid-head">Shop URL:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="shop_url" id="shop_url" value="<?php echo $shop_url; ?>"/>
                            <input type="hidden" name="shop_url_orig" id="shop_url_orig" value="<?php echo $shop_url; ?>"/>
                        </td>

                    </tr>
                    <?php if ($shop_url) { ?>
                        <tr>
                            <td class="grid-head">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td> 

                                <a class="link-button" target="_blank" style="background-color: red; border: 1px solid red; color: #fff;margin-bottom:0px;" href="
                                <?php
                                if (substr($shop_url, 0, 4) != "http") {
                                    echo "http://" . $shop_url;
                                } else {
                                    echo $shop_url;
                                }
                                ?>" id="viewurl1">View URL</a>
                            </td>

                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td class="grid-head">Shop Domain:</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="shop_domain" id="shop_domain" value="<?php echo $shop_domain; ?>"/>
                            <input type="hidden" name="shop_domain_orig" id="shop_domain_orig" value="<?php echo $shop_domain; ?>"/>
                        </td>
                    </tr>   
                    <tr>
                        <td class="grid-head">Shop Domain Type:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="shop_domaintype" id="shop_domaintype">
                                <option value="0">Select Domain Type</option>
                                <option value="1" <?php
                    if ($shop_domaintype == 1) {
                        echo "selected='selected'";
                    }
                    ?>>Partial (New)</option>
                                <option value="2" <?php
                                        if ($shop_domaintype == 2) {
                                            echo "selected='selected'";
                                        }
                    ?>>Full (New)</option>
                                <option value="3" <?php
                                        if ($shop_domaintype == 3) {
                                            echo "selected='selected'";
                                        }
                    ?>>Redirect (New)</option>
                                <option value="4" <?php
                                        if ($shop_domaintype == 4) {
                                            echo "selected='selected'";
                                        }
                    ?>>Partial (Existing)</option>
                                <option value="5" <?php
                                        if ($shop_domaintype == 5) {
                                            echo "selected='selected'";
                                        }
                    ?>>Full (Existing)</option>
                                <option value="6" <?php
                                        if ($shop_domaintype == 6) {
                                            echo "selected='selected'";
                                        }
                    ?>>Redirect (Existing)</option>
                            </select>
                            <input type="hidden" name="shop_domaintype_orig" id="shop_domaintype_orig" value="<?php echo $shop_domaintype; ?>"/>
                            <span style="font-size: 9px;color: red;">UPDATE LIMITED TO EDITORS ONLY</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Shop Domain Status:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="shop_domainstatus" id="shop_domainstatus">
                                <option value="0">Select Domain Status</option>
                                <option value="1" <?php
                                        if ($shop_domainstatus == 1) {
                                            echo "selected='selected'";
                                        }
                    ?>>For Request</option>
                                <option value="2" <?php
                                        if ($shop_domainstatus == 2) {
                                            echo "selected='selected'";
                                        }
                    ?>>Pending</option>
                                <option value="3" <?php
                                        if ($shop_domainstatus == 3) {
                                            echo "selected='selected'";
                                        }
                    ?>>Activated</option>
                                <option value="4" <?php
                                        if ($shop_domainstatus == 4) {
                                            echo "selected='selected'";
                                        }
                    ?>>Domain unavailable</option>
                            </select>
                            <input type="hidden" name="shop_domainstatus_orig" id="shop_domainstatus_orig" value="<?php echo $shop_domainstatus; ?>"/>
                            <span style="font-size: 9px;color: red;">UPDATE LIMITED TO EDITORS ONLY</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">Shop Public CMS:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="shop_cmsstatus" id="shop_cmsstatus">
                                <option value="0">Select Domain Status</option>
                                <option value="1" <?php
                                        if ($shop_cmsstatus == 1) {
                                            echo "selected='selected'";
                                        }
                    ?>>For Backup</option>
                                <!--                                <option value="2" <?php
                                        if ($shop_cmsstatus == 2) {
                                            echo "selected='selected'";
                                        }
                    ?>>Pending</option>-->
                                <option value="3" <?php
                                if ($shop_cmsstatus == 3) {
                                    echo "selected='selected'";
                                }
                    ?>>Activated</option>
                                <option value="4" <?php
                                        if ($shop_cmsstatus == 4) {
                                            echo "selected='selected'";
                                        }
                    ?>>Deactivated</option>
                            </select>
                            <input type="hidden" name="shop_cmsstatus_orig" id="shop_cmsstatus_orig" value="<?php echo $shop_cmsstatus; ?>"/>
                            <span style="font-size: 9px;color: red;">UPDATE LIMITED TO EDITORS ONLY</span>
                        </td>
                    </tr>   
                    <tr>
                        <td class="grid-head">Assigned CSR:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_csr' id='shop_csr'>
                                <option value='0'>Select CSR</option>
                                <?php echo $csr; ?>
                            </select>
                            <input type="hidden" name="shop_csr_orig" id="shop_csr_orig" value="<?php echo $shop_csr; ?>"/>


                        </td>
                    </tr> 
                    <tr>
                        <td class="grid-head">Assigned Designer:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_designer' id='shop_designer'>
                                <option value='0'>Select Designer</option>
                                <?php echo $designers; ?>
                            </select>
                            <input type="hidden" id="oldshopdesigner" name="oldshopdesigner" value="<?php echo $shop_designer; ?>" />
                            <input type="hidden" id="newshopdesigner" name="newshopdesigner" value="" />
                            <input type="hidden" id="shop_designer_orig" name="shop_designer_orig" value="<?php echo $shop_designer; ?>" />
                            <span style="font-size: 9px;color: red;">RE-ASSIGNMENT LIMITED TO PRODUCTION ONLY</span>
                        </td>
                    </tr> 


                    <tr>
                        <td class="grid-head">Assigned Editor:</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name='shop_editor' id='shop_editor'>
                                <option value='0'>Select Editor</option>
                                <?php echo $editors; ?>
                            </select>
                            <input type="hidden" id="shop_editor_orig" name="shop_editor_orig" value="<?php echo $shop_editor; ?>" />
                            <span style="font-size: 9px;color: red;">RE-ASSIGNMENT LIMITED TO PRODUCTION ONLY</span>
                        </td>
                    </tr>      


                    <tr class="edit-account-footer">
                        <td colspan="4">
                            <?php echo "Created by: " . $createdby . " On: " . $createdon . " | Modified by: " . $modifiedby; ?>
                        </td>
                    </tr>
                </table>



            </div><!-- gen-section --> 
        </form>

    </div><!-- main-section --> 


    <div id="colobox_form" style="display:none;">

        <form action="functions/add_remarks.php" method="post" name="remarks_frm" id="remarks_frm" style="width:500px;margin:0 auto;">
            <div class="commands">
                <div class="head-label">
                    <h2>Add New Remark</h2>
                </div><!-- end of add new account -->

                <ul>
                    <!--<li><a class="link-button gray" href=''>Cancel</a></li>-->
                    <li><input type="submit" value="Submit" /></li>
                    <!--<li><a  id="remark_submit" onclick="CheckRemark()" name="remark_submit" >Submit</a></li>-->
                </ul>
            </div><!-- end of grid-commands -->



            <table cellpadding="5" cellspacing="0">
                <tr>
                    <td class="grid-head">Remark:</td>
                    <td>&nbsp;</td>
                    <td>
                        <textarea cols="25" rows="3" name="accountremark"  id="accountremark"></textarea>
                    </td>
                </tr>

            </table>



            <input type="hidden" name="remark_type" id="remark_type" value="1" />
            <input type="hidden" name="remark_uid" id="remark_uid" value="<?php echo $_REQUEST['account_id']; ?>" />

        </form>

    </div>

    <script type="text/javascript">
        jQuery.validator.addMethod(
        "multiemails",
        function(value, element) {
            if (this.optional(element)) // return true on optional element
                return true;
            var account_email = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in account_email) {
                value = account_email[i];
                valid = valid &&
                    jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

        jQuery.validator.messages.multiemails
    );
    	
    	
    	
    	
        $(document).ready(function() {
                
                
            $("#viewurl").click(function()
            {
                var url = '<?php echo $shop_url; ?>';
                alert(url);
                //                window.location = url;
                window.open(url);
            });
            
            
            $("#account_name").blur(function(){
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: 'functions/ajax.check.accountname.php',
                    timeout: 20000,
                    data: { value: $("#account_name").val() },
                    success: function(html){

                        $("#verify_accountname").val(html);
                    },
    					
                });
            });
    			
            $("#account_name").keyup(function(){
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: 'functions/ajax.check.accountname.php',
                    timeout: 20000,
                    data: { value: $("#account_name").val() },
                    success: function(html){
                        $("#verify_accountname").val(html);
                    },
    					
                });
            });



            $('.iframe').click(function () {
    	
                $(".iframe").colorbox({iframe:false,html:$("#colobox_form").html(), width:"600px", height:"300px"});
            });

            $("#source_id").change(function()
            {
                if ($("#source_id").val()==="5")
                {
                    $("#events_source").show();
                    $("#event_box").addClass("required");
                }
                else
                {	$("#events_source").hide();
                    $("#event_box").removeClass("required");
                }
            });
            $("#status_id").change(function()
            {
                if ($("#status_id").val()==="14")
                {
    			 
                    $("#decline_container").show();
                    $("#decline_reason").addClass("required");
                }
                else
                {
                    $("#decline_container").hide();
                    $("#decline_reason").removeClass("required");
                    $("#reason_container").hide();
                    $("#other_reason").removeClass("required");
                    $("#other_reason").val("");
                    $("#decline_reason").val("");
                }
    		 
            });
    		 
            $("#decline_reason").change(function()
            {
                if ($("#decline_reason").val()==="5" || $("#decline_reason").val()==="6" || $("#decline_reason").val()==="7")
                {
    			 
                    $("#reason_container").show();
                    $("#other_reason").addClass("required");
                }
                else
                {
                    $("#reason_container").hide();
                    $("#other_reason").removeClass("required");
                }
    		 
            });
    	
        

    		
            $("#form1").validate({
                rules: {
                    account_email: { required: true, multiemails: true }
                },
                messages: {
                    account_email: {
    								
                        required: "This field is required",
                        multiemails: "Please enter a valid email address. Multiple emails are separated by comma or semicolon."
                    }
                }

            });
            $("#account_telephone").numeric();
            //            $("#account_contractnumber").numeric();
            $("#account_contractamount").numeric();
            $("#account_contactperson_age").numeric();
            $("#account_mobile").numeric();
            $("#account_fax").numeric();     
    		
            $('#account_contractamount').keyup(function() {
                if($("#account_contractnumber").val() === "" )
                {
                    $("#account_contractamount").val("");
                    alert("Account must have Contract #");
                    $("#account_contractnumber").focus();
    				
    				
                }
    			
            });
    		
            $("#form1").submit(function() 
            {
                var contract = parseInt($("#account_contractamount").val());
                var paid = parseInt($("#paid_amount").val());
                var account_paid = parseInt($("#account_paid").val())
                var status = parseInt($("#status_id").val())
    			 
    					
                if(contract >= paid ) 
                {
    											
                    if(status === 16 || status === 17)
                    {
                        if(account_paid != 1)
    													
                        //if(($("#status_id").val() == 16 || account_paid != 1 ) && ( $("#status_id").val() ==17 || account_paid != 1))
                        {
                            alert("Account not yet paid");
                        }
                        else {
    													
                            return true;
                        }
                    }
                    else
                    {
    												
                        return true;
                    }
    													
    											
                }
                else
                {
                    if(($("label.error").length <= 0) || ($("label.error").is(":hidden")))
                    {
                        alert($("#account_name").val()+" Contract Amount is: P " + $("#account_contractamount").val() + "\n\n" + 
                            "Verify Contract Amount."
                    );
                    }
                }
    					
    				
    						
    						
                return false;
            });
    		
        });
        function statusCheck() {
            var stat_id = document.getElementById("status_id").value;
            var account_memberid = document.getElementById("account_memberid").value;
            if(stat_id == '2'){
                if(account_memberid == ''){
                    alert('Member ID REQUIRED!');
                    return false;
                }                  
                else{
                    document.getElementById("newstatusid").value = document.getElementById("status_id").value;
                    document.getElementById("newshopdesigner").value = document.getElementById("shop_designer").value;
                    return true;
                }
                    
            }
            else{
                document.getElementById("newstatusid").value = document.getElementById("status_id").value;
                document.getElementById("newshopdesigner").value = document.getElementById("shop_designer").value;
                return true;
            }
        }

        $(function() {
    		
            if ($("#source_id").val()==="5")
            {
    	 
                $("#events_source").show();
                $("#event_box").addClass("required");
            }
            else
            {
                $("#events_source").hide();
                $("#event_box").removeClass("required");
            }
    	 
            if ($("#status_id").val()==="14")
            {
    	 
                $("#decline_container").show();
                $("#decline_reason").addClass("required");
            }
            else
            {
                $("#decline_container").hide();
                $("#decline_reason").removeClass("required");
            }
    	 
            if ($("#decline_reason").val()==="5" || $("#decline_reason").val()==="6" || $("#decline_reason").val()==="7" || $("#status_id").val()==="14" )
            {
    			 
                $("#reason_container").show();
                $("#other_reason").addClass("required");
                if($("#other_reason").val() === 0)
                {
                    $("#other_reason").val("");
    				
                }
            }
            else
            {
                $("#reason_container").hide();
                $("#other_reason").removeClass("required");
            }	
    		 
    	
        });
    </script>
    <?php if ($role_id == 4 || $dep_id == 4) { ?>
        <script type="text/javascript">
            function GetCurrentTime()
            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        		
                var currentTime = new Date()
                var hours = currentTime.getHours()
                var minutes = currentTime.getMinutes()
                var seconds = currentTime.getSeconds()
                if (minutes < 10){
                    minutes = "0" + minutes
                }
                if (seconds < 10){
                    seconds = "0" + seconds
                }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        			
                $("#time_created").val(hours + ":" + minutes + ":" + seconds);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        		
            }
            $(function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        		
                $('#date_created').datepicker({dateFormat: 'yy-mm-dd'});
                setInterval('GetCurrentTime()', 100 );
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        			
            });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	
        </script>
    <?php } ?>
</body>
</html>