<?php
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Error</title>
    
    <!-- STYLESHEETS -->
    <link rel="stylesheet" type="text/css" href="/88dbphcrm/css/style.css" />

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="/88dbphcrm/css/ie.css" />
    <![endif]-->

</head>

<body>


    <div class="log-main-container">

        <div class="log-section">

            <div class="inner-box">
                <?php
                $err = $_REQUEST['err'];

                switch ($err) {
                    case 1:
                        echo 'Invalid username/password. <a href="/88dbphcrm/login/">Try again</a>';
                        break;
                    case 2:
                        echo 'Not logged in. <a href="/88dbphcrm/login/">Login</a>';
                        break;
                    case 3:
                        echo 'Email has been signed up already. <a href="/88dbphcrm/login/signup.php">Try again</a>';
                        break;
					case 4:
						echo 'Username is already taken <a href="/88dbphcrm/login/signup.php">Try again</a>';
						break;
					case 5:
						echo 'Your account is currently unavailable. <br/> Contact the administrator. <a href="/88dbphcrm/login/">Back</a>';
                        break;
					case 6:
						echo "Your account is subject for Administrator approval. <br/> Check your email address for confirmation. <a href='/88dbphcrm/login/'>Back</a>";
                        break;
						
                    default:
                        echo 'Error!';
                }
                ?>
            </div>

        </div><!-- end of login-section -->

    </div><!-- end of main-login-container -->


</body>
</html>
