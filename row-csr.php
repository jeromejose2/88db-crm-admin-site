<?php
session_start();

connect();

$date_condition = "DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday last week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "')";

//echo('test ni lawrence');
?>
<style type="text/css">
    /*---------------------------------------------------------------------------------*/
    /*	CSS Reset Style
    /*---------------------------------------------------------------------------------*/

    html, body, div, dl, dt, dd, ul, li, h1, h2, h3, h4, h5, h6, form, label, fieldset, input, p, blockquote, th, td { margin:0; padding:0 }
    table { border-collapse:collapse; border-spacing:0 }
    fieldset, img { border:0 }
    address, caption, cite, code, dfn, em, strong, th, var { font-style:normal; font-weight:normal }
    ul { list-style:none }
    ol { padding-left: 17px;	}
    caption, th { text-align:left }
    h1, h2, h3, h4, h5, h6 { font-size:100%; font-weight:normal }
    q:before, q:after { content:''}
    header, nav, article, footer, address {display: block;}
    strong { font-weight:bold }
    em { font-style:italic }
    a img { border:none } /* Gets rid of IE's blue borders */
    pre { padding: 10px; white-space: pre; white-space: pre-wrap; white-space: pre-line; word-wrap: break-word;}


    /*---------------------------------------------------------------------------------*/
    /*	CSS Reset Style
    /*---------------------------------------------------------------------------------*/

    .dash-row-container{
        overflow: hidden;
        /*        float: left;*/
        width: 870px;
        margin: 0 auto;
        margin-top: 5px;
        position: relative;
    }
    .dashboard-box{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 170px;
        height: 100px;
        margin-left: 2px;
        margin-bottom: 5px;
        float: left;
    }
    .dashboard-box-5{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 170px;
        height: 130px;
        margin-left: 2px;
        margin-bottom: 5px;
        float: left;
    }
    .dashboard-box-title{
        width: 170px;
        overflow: hidden;
        margin: 0 auto;
        text-align: center;
        padding-top: 5px;
    }
    .dashboard-box-total{
        width: 170px;
        overflow: hidden;
        margin: 0 auto;
        text-align: center;
        /*        padding-top: 20px;*/

    }
    .total-count{
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 24px;
        font-weight: bold;
        color: #666;
    }
    .dashboard-box-title ul li{
        color: #666;
        font-size: 13px;
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    .stat-desc-count{
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 11px;

        color: #666;
    }
    .dashboard-link{
        text-decoration: none;
        color: #F68520;
    }
    .dashboard-link:hover{
        text-decoration: underline;

    }
    .dept-title{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 866px;
        margin: 0 auto;
        margin-top: 5px;
        margin-bottom: 5px;
        margin-left: 2px;
        background: #F90;
    }
    .dept-title span{
        text-transform: uppercase;
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        color: white;
        font-weight: bold;
        font-size: 12px;
        margin-left: 5px;
    }
    .dept-title-span{
        /*        border: 1px solid #D9D9D9;*/
        overflow: hidden;
        width: 866px;
        margin: 0 auto;
        margin-top: 5px;
        margin-bottom: 5px;
        margin-left: 2px;
        /*        background: #F90;*/

    }
    .dept-title-span span{
        text-transform: uppercase;
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        color: #666;
        font-weight: bold;
        font-size: 12px;
        margin-left: 5px;
        position: relative;
    }
    .dept-title-span i {
        width: 20px;
        height: 15px;
        width: 20px;
        height: 15px;
        position: relative;
        float: left;

    }
    i.csr{
        background: #FFFBD0;
    }
    i.prod{
        background: #CEE6CE;
    }
    .backcsr{
        background: #FFFBD0;
    }
    .backprod{
        background: #CEE6CE;
    }
    .backfinance{
        background: #CDB99C;
    }
    .prod-table-admin {
        border-spacing: 2px;
        border-color: gray;
        border-collapse: collapse;
        border-top: 1px solid #DDD;
        border-left: 1px solid #DDD;
        margin-bottom: 20px;
    }
    .thead {
        text-decoration: none;
        color: #222;
        font-weight: bold;
        background-color: #EFEFEF;
    }
    .theadtwo {
        text-decoration: none;
        background-color: white;
    }
    .thead td {
        border-right: 1px solid #DDD;
        border-bottom: 1px solid #DDD;
        background-color: #EFEFEF;
        color: #222;
        font-weight: bold;
        padding: 7px;
    }
    .theadtwo td {
        border-right: 1px solid #DDD;
        border-bottom: 1px solid #DDD;
        padding: 7px;
    }
</style>
<!---dash-row-container start---> 
<div class="dash-row-container">
    <?php if ($department_id == 2 || $department_id == 4) { ?>   
        <!--SALES START-->
        <div class="dept-title">
            <span>SALES AND FINANCE</span>
        </div>

        <!---First Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE DATE(a.account_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "')" . $condition;
//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Accounts Today
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo $row[0];
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " WHERE r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition;

            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Total Accounts to Date
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    $total_accounts_to_date = number_format($row[0]);
                                    echo $total_accounts_to_date;
                                    echo "<input type='hidden' id='h2' value='" . $total_accounts_to_date . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h2' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";


                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                $condition_au = "AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " WHERE r.user_id = " . $userid . " ";
                                $condition_au = "AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                $condition_au = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        $condition_au = "AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }

                $query = "SELECT (
						SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition . ") - (SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition_au . " GROUP BY a.account_id
						) x) - (SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 )" . $condition_au . ")";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Leads:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "
						SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x
					 ";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "
						 SELECT count(a.account_id) as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 ) " . $condition . "

					 ";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Declined: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";

            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') " . $condition . " GROUP BY au.account_id
						) x";
//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops This Week
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h5' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
                $result = mysql_query($query);
                ?>   
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "  ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";
//            echo $query;
                $result = mysql_query($query);
                ?>     
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') " . $condition . " GROUP BY au.account_id
						) x";




            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops This Month
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h3' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')  and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
                $result = mysql_query($query);
                ?>     

                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . "  AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";



                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x";



            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops to Date
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h2' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }

                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a 
						INNER JOIN users r ON a.account_createdby = r.user_id 
						 
						INNER JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON a.account_id = pay.account_id
						WHERE  a.account_contractamount <> pay.paid_amount GROUP BY a.account_id   " . $condition . " 

						) x";

//                echo $query;

                $result = mysql_query($query);
                ?>  
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial:
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='17' class='dashboard-link'>" . $row[0] . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a 
						INNER JOIN users r ON a.account_createdby = r.user_id 
						INNER JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON a.account_id = pay.account_id
						WHERE a.account_contractamount = pay.paid_amount GROUP BY a.account_id   " . $condition . "  

						) x";

//             echo $query;

                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) {
                    echo "<a href='16' class='dashboard-link'>" . $row[0] . "</a>";
                } else {
                    echo $row[0];
                }
                echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h1' value='0' />";
            }
                ?>


                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---First Row End--->


        <!---2nd Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a  LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 22 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Prospecting
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='22' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 10 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        Set Appointment
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='10' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 11 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        Sent Proposal
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='11' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 12 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>

                <ul>
                    <li>
                        For Callback
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='12' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 15 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        For Follow Up
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='15' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 

        <!---2nd Row End---> 



        <!---3rd Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 19 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Interested
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='19' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 14 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Declined
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='14' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 20 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>

            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Signing
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='20' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 13 " . $condition . " GROUP by a.account_id
						) x";


//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed (for Payment) 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='13' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
                                                 
                                                SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 60 " . $condition . " GROUP by a.account_id
						
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 39 " . $condition . " GROUP by a.account_id
						
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 49 " . $condition . " GROUP by a.account_id
						) x";
//
//        echo $query;
            //        echo $query2;
//        echo $query3;
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);


//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by Finance 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">

                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="financetotal">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Reject - No Payment:  
                                    <?php
                                    if ($result5) {
                                        $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                        if ($row5[0] != 0) {
                                            echo "<a href='49' class='dashboard-link'>" . $row5[0] . "</a>";
                                        } else {
                                            echo $row5[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Unclear WCF: 
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='59' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete Mats: 
                                    <?php
                                    if ($result2) {
                                        $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                        if ($row2[0] != 0) {
                                            echo "<a href='60' class='dashboard-link'>" . $row2[0] . "</a>";
                                        } else {
                                            echo $row2[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete WCF: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='61' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    No Contract:  <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='39' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                    ?>
                                </span>
                                <input type="hidden" id="total-finance" name="total-finance" value="<?php
                                echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0];
                                    ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>

        </div>
        <!---dashboard-box End---> 

        <!---3rd Row End---> 

        <!--SALES END-->
    <?php } ?>
    <?php if ($department_id == 1 || $department_id == 4) { ?>
        <!--PROD START-->
        <div class="dept-title">
            <span>PRODUCTION AND CSR</span>
        </div>
        <div class="dept-title-span">
            <div style="overflow: hidden;float: left;width: 100px;">
                <i class="csr"></i><span>CSR</span>  
            </div>
            <div style="overflow: hidden;float: left;width: 120px;">
                <i class="prod"></i><span>PRODUCTION</span>
            </div>
        </div>
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 16 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 17 " . $condition . " GROUP by a.account_id
						) x";





            $result = mysql_query($query);
            $result2 = mysql_query($query2);


//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>

                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="partialfull">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total" style="padding-top: 30px;">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count" style="font-size: 15px;">
                                    Paid Full:  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='16' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count" style="font-size: 15px;">
                                    Paid Partial: <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='17' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>



        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">

            <?php
            $condition = "";

            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 66 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 50 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 51 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 52 " . $condition . " GROUP by a.account_id
						) x";
            //echo $query;
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by CSR to Sales
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="csr-reject-to-sale">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);
                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Inc. Contact Details:  <?php
            if ($row[0] != 0) {
                echo "<a href='66' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);
                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                             Unclear WCF:  <?php
            if ($row2[0] != 0) {
                echo "<a href='50' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);
                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                             Incomplete Mats:  <?php
            if ($row3[0] != 0) {
                echo "<a href='51' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result4) : ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);
                if ($row4[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                             Incomplete WCF:  <?php
            if ($row4[0] != 0) {
                echo "<a href='52' class='dashboard-link'>" . $row4[0] . "</a>";
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <input type="hidden" name="csr-reject-to-sale-total" id="csr-reject-to-sale-total" value="<?php echo $total = $row[0] + $row2[0] + $row3[0] + $row4[0]; ?>" />

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">

            <?php
            $condition = "";

            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 1 " . $condition . " GROUP by a.account_id
						) x";
            //echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Lined up for Production
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="lptotal">
                                    <?php
                                    if ($result) {
                                        $rowlp = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($rowlp[0] != 0) {
                                            echo "<a href='1' class='dashboard-link'>" . $rowlp[0] . "</a>";
                                        } else {
                                            echo $rowlp[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($rowlp[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 55 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 56 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 57 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 58 " . $condition . " GROUP by a.account_id
						) x";

            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 62 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by Prod
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="prodtotal">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Unclear WCF:  <?php
            if ($row[0] != 0) {
                echo "<a href='55' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Incomplete Mats: <?php
            if ($row2[0] != 0) {
                echo "<a href='56' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3): ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Incomplete WCF:<?php
            if ($row3[0] != 0) {
                echo "<a href='57' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result5): ?>
                <?php
                $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                if ($row5[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Unclear Instruction:<?php
            if ($row5[0] != 0) {
                echo "<a href='62' class='dashboard-link'>" . $row5[0] . "</a>";
            } else {
                echo $row5[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result4) : ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                if ($row4[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Ins. Not Followed: <?php
            if ($row4[0] != 0) {
                echo "<a href='58' class='dashboard-link'>" . $row4[0] . "</a>";
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="prod-total" name="prod-total" value="<?php
        echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0];
            ?>"/>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 2 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);

            //echo($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Proofreading
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="pftotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='2' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <!--        <div class="dashboard-box-5 backprod">
        <?php
//            $condition = "";
//            if ($_SESSION['dashboard_shoptype'] == 0) {
//                if ($_SESSION['dashboard_user'] == 0) {
//                    if ($_SESSION['dashboard_shopcsr'] == 0) {
//                        switch ($role_id) {
//                            case 2:
//                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                                break;
//                            case 3:
//                                $condition = " AND r.user_id = " . $userid . " ";
//                                break;
//                        }
//                    } else {
//                        switch ($role_id) {
//
//                            case 2:
//                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                                break;
//                            case 3:
//                                $condition = " AND r.user_id = " . $userid . " ";
//                                break;
//                        }
//                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
//                    }
//                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
//                    switch ($role_id) {
//                        case 1:
//                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
//                            break;
//                        case 2:
//                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                            break;
//                        case 3:
//                            $condition = " AND r.user_id = " . $userid . " ";
//                            break;
//                    }
//                } else {
//                    switch ($role_id) {
//                        case 1:
//                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
//                            break;
//                        case 2:
//                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                            break;
//                        case 3:
//                            $condition = " AND r.user_id = " . $userid . " ";
//                            break;
//                    }
//                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
//                }
//            } elseif ($_SESSION['dashboard_user'] == 0) {
//                if ($_SESSION['dashboard_shopcsr'] == 0) {
//                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
//                } else {
//                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
//                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
//                }
//            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
//                switch ($role_id) {
//                    case 1:
//                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
//                        break;
//                    case 2:
//                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                        break;
//                    case 3:
//                        $condition = " AND r.user_id = " . $userid . " ";
//                        break;
//                }
//                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
//            } else {
//                switch ($role_id) {
//                    case 1:
//                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
//                        break;
//                    case 2:
//                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
//                        break;
//                    case 3:
//                        $condition = " AND r.user_id = " . $userid . " ";
//                        break;
//                }
//                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
//                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
//            }
//
//
//            $query = "select count(x.total) from
//						(
//						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 18 " . $condition . " GROUP by a.account_id
//						) x";
//            $result = mysql_query($query);
        ?>
                    <div class="dashboard-box-title">
                        <ul>
                            <li>
                                For Design Instructions
                            </li>
                        </ul>
                    </div>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="total-count" id="fditotal">
        <?php
//                                    if ($result) {
//                                        $row = mysql_fetch_array($result, MYSQL_NUM);
//
//                                        if ($row[0] != 0) {
//                                            echo "<a href='18' class='dashboard-link'>" . $row[0] . "</a>";
//                                        } else {
//                                            echo $row[0];
//                                        }
//                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
//                                    } else {
//                                        echo "0";
//                                        echo "<input type='hidden' id='h1' value='0' />";
//                                    }
        ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
        
                    </div>
                </div>-->
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }




            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 21 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Development
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="fdevetotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='21' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 3 " . $condition . " GROUP by a.account_id
						) x";

//        echo($query);

            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Under Construction
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="undercontotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='3' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 4 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Editor QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="edqatotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='4' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?> 
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 7 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 28 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 29 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Internal Revisions-Editor
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="Int-rev-ed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) :
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 1: <?php
            if ($row[0] != 0) {
                echo "<a href='7' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 2:<?php
            if ($row2[0] != 0) {
                echo "<a href='28' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
            echo "<input type='hidden' id='h1' value='0' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3) : ?>

                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 3: <?php
            if ($row3[0] != 0) {
                echo "<a href='29' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" name="Int-rev-ed-total" id="Int-rev-ed-total" value=" <?php echo $row[0] + $row2[0] + $row3[0]; ?>"/>

        </div>
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 63 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 64 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 65 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED BY EDITOR
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="reject-editor-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Ins. not followed 1: <?php
            if ($row[0] != 0) {
                echo "<a href='63' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>
            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Ins. not followed 2:<?php
            if ($row2[0] != 0) {
                echo "<a href='64' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3): ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Ins. not followed 3: <?php
            if ($row3[0] != 0) {
                echo "<a href='65' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" name="reject-editor" id="reject-editor" value=" <?php echo $row[0] + $row2[0] + $row3[0]; ?>">
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 5 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Design QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="forsrdqatotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='5' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 32 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 33 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 34 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Internal Revisions-QA 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="int-rev-qa">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 1: <?php
            if ($row[0] != 0) {
                echo "<a href='32' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2): ?>

                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 2: <?php
            if ($row2[0] != 0) {
                echo "<a href='33' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3): ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 3: <?php
            if ($row3[0] != 0) {
                echo "<a href='34' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" name="int-rev-qa-total" id="int-rev-qa-total" value=" <?php
        echo $row[0] + $row2[0] + $row3[0];
            ?>"/>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 38 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        CSR QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="finalqatotal">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='38' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 6 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 26 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 25 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Client Approval
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="client-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Approval :  <?php
            if ($row[0] != 0) {
                echo "<a href='6' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2): ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Feedback: <?php
            if ($row2[0] != 0) {
                echo "<a href='26' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Consolidation: <?php
            if ($row3[0] != 0) {
                echo "<a href='25' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="client-total-total" value="<?php
        echo $row[0] + $row2[0] + $row3[0];
            ?>"/>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 8 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 30 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 31 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 41 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 42 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);

//        echo ($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Client Revisions
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="clientrev">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 1 : <?php
            if ($row[0] != 0) {
                echo "<a href='8' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2): ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 2 : <?php
            if ($row2[0] != 0) {
                echo "<a href='30' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3): ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 3 : <?php
            if ($row3[0] != 0) {
                echo "<a href='31' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result4): ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);
                if ($row4[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 4: <?php
            if ($row4[0] != 0) {
                echo "<a href='41' class='dashboard-link'>" . $row4[0] . "</a>";
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result5): ?>
                <?php
                $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                if ($row5[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Revisions 5:   <?php
            if ($row5[0] != 0) {
                echo "<a href='42' class='dashboard-link'>" . $row5[0] . "</a>";
            } else {
                echo $row5[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="clientrevtotal" value="<?php echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0]; ?>">

        </div>
        <!---dashboard-box End---> 

        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }




            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 37 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 43 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 44 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Redesign
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="redesign-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <?php if ($result): ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Redesign 1 :  <?php
            if ($row[0] != 0) {
                echo "<a href='37' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result2): ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Redesign 2:  <?php
            if ($row2[0] != 0) {
                echo "<a href='43' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Redesign 3: <?php
            if ($row3[0] != 0) {
                echo "<a href='44' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="redesign-total-total" value="<?php
        echo $row[0] + $row2[0] + $row3[0];
            ?>"/>

        </div>
        <!---dashboard-box End---> 

        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 53 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 54 " . $condition . " GROUP by a.account_id
						) x";

//            $query3 = "select count(x.total) from
//						(
//						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 50 " . $condition . " GROUP by a.account_id
//						) x";
//            $query4 = "select count(x.total) from
//						(
//						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 51 " . $condition . " GROUP by a.account_id
//						) x";
//            $query5 = "select count(x.total) from
//						(
//						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 52 " . $condition . " GROUP by a.account_id
//						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
//            $result3 = mysql_query($query3);
//
//            $result4 = mysql_query($query4);
//            $result5 = mysql_query($query5);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by CSR to Prod
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="reject-csr">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>



            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Ins. Not Followed: <?php
            if ($row[0] != 0) {
                echo "<a href='53' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Design not Followed: <?php
            if ($row2[0] != 0) {
                echo "<a href='54' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <input type="hidden" id="reject-csr-total" value=" <?php
        echo $row[0] + $row2[0];
            ?>"/>

        </div>
        <!---dashboard-box End---> 
		<!-- dashboard-box for client updates -->
		<div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 27 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 35 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 36 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 45 " . $condition . " GROUP by a.account_id
						) x";
            $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 46 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            $result6 = mysql_query($query6);
            ?>
            <div class="dashboard-box-total">
                <div class="dashboard-box-title">
                    <ul>
                        <li>
                           Client Updates
                        </li>
                    </ul>
                </div>
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" style="font-size: 12px;">
										<?php
											$row2 = mysql_fetch_array($result2, MYSQL_NUM);
											if ($row2[0] != 0) {
												
												echo "<span class='total-count'><a href='27' class='dashboard-link'>" . $row2[0] . "</a></span>";
											} else {
												echo $row2[0];
											}
											echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
										?>
                                </span>
                            </td>
                        </tr>
                    </tbody>
				</table>
            </div>

            <?php if ($result2) : ?>
                <?php
                

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
  
									
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
						</table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            

        </div>
		<!-- end dashboard-box client updates-->
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 27 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 35 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 36 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 45 " . $condition . " GROUP by a.account_id
						) x";
            $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 46 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            $result6 = mysql_query($query6);
            ?>
            <div class="dashboard-box-total">
                <div class="dashboard-box-title">
                    <ul>
                        <li>
                            LIVE
                        </li>
                    </ul>
                </div>
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="live-total" style="font-size: 12px;">

                                </span>
                            </td>
                        </tr>
                    </tbody>
				</table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Live: <?php
            if ($row[0] != 0) {
                echo "<a href='9' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Live (Client Updates) :  
											<?php
												if ($row2[0] != 0) {
													echo  $row2[0] ;
												} else {
													echo $row2[0];
												}
												echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
											?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Live (Temporary) : <?php
            if ($row3[0] != 0) {
                echo "<a href='35' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result4) : ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                if ($row4[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Temp(Bal. Collection) : <?php
            if ($row4[0] != 0) {
                echo "<a href='36' class='dashboard-link'>" . $row4[0] . "</a>";
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result5) : ?>
                <?php
                $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                if ($row5[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Temp(Awaiting Mats) :<?php
            if ($row5[0] != 0) {
                echo "<a href='45' class='dashboard-link'>" . $row5[0] . "</a>";
            } else {
                echo $row5[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result6) : ?>
                <?php
                $row6 = mysql_fetch_array($result6, MYSQL_NUM);

                if ($row6[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Temp(Awaiting Final) :<?php
            if ($row6[0] != 0) {
                echo "<a href='46' class='dashboard-link'>" . $row6[0] . "</a>";
            } else {
                echo $row6[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row6[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="live-total-total" value="<?php
        echo number_format($row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0] + $row6[0]);
            ?>"/>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 

        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 24 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 47 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id  WHERE a.account_paid=1 and a.status_id = 48 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Temporary Closed
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="temp-closed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Closed: <?php
            if ($row[0] != 0) {
                echo "<a href='24' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($result2): ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Delinquent:  <?php
            if ($row2[0] != 0) {
                echo "<a href='47' class='dashboard-link'>" . $row2[0] . "</a>";
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Request from Client:   <?php
            if ($row3[0] != 0) {
                echo "<a href='48' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <input type="hidden" id="temp-closed-total" value=" <?php
        echo $row[0] + $row2[0] + $row3[0];
            ?>"/>

        </div>
        <!---dashboard-box End---> 


        <!---dashboard-box start---> 

        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 9 and a.account_seo = 1 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 9 and a.account_ads = 1 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        SEO
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="temp-closed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Done:  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='/88dbphcrm/accounts/accountseo.php?accountseo=true'   target='_blank'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Not done:  
                                    <a href='/88dbphcrm/accounts/accountseo.php?accountseo=false'   target='_blank'>
                                        <?php
                                        $row3 = mysql_fetch_array($result3, MYSQL_NUM);
                                        echo $row3[0] - $row[0];
                                        ?>         
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-title">
                <ul>
                    <li>
                        With ADS
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Done:   <?php
                                    if ($result2) {
                                        $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                        if ($row2[0] != 0) {
                                            echo "<a href='/88dbphcrm/accounts/accountseo.php?accountads=true'   target='_blank'>" . $row2[0] . "</a>";
                                        } else {
                                            echo $row2[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                        ?>         </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Not done:   
                                    <a href='/88dbphcrm/accounts/accountseo.php?accountads=false'   target='_blank'>
                                        <?php
                                        echo $row3[0] - $row2[0];
                                        ?>   
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

        </div>
        <div class="dashboard-box-5 backprod">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_cmstype = 1  and a.account_paid=1 and a.status_id = 9 and a.account_backupv1 = 1 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_cmstype = 2 and a.account_paid=1 and a.status_id = 9 and a.account_backupv2 = 1 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_cmstype = 1 and a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_cmstype = 2 and a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        BACKUP V1
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="temp-closed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Done:  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='/88dbphcrm/accounts/accountbackup.php?accountbackupv1=true'   target='_blank'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Not done:  
                                    <a href='/88dbphcrm/accounts/accountbackup.php?accountbackupv1=false'   target='_blank'>
                                        <?php
                                        $row3 = mysql_fetch_array($result3, MYSQL_NUM);
                                        echo $row3[0] - $row[0];
                                        ?>         
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-title">
                <ul>
                    <li>
                        BACKUP V2
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Done:   <?php
                                    if ($result2) {
                                        $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                        if ($row2[0] != 0) {
                                            echo "<a href='/88dbphcrm/accounts/accountbackup.php?accountbackupv2=true'   target='_blank'>" . $row2[0] . "</a>";
                                        } else {
                                            echo $row2[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                        ?>         </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Not done:   
                                    <a href='/88dbphcrm/accounts/accountbackup.php?accountbackupv2=false'   target='_blank'>
                                        <?php
                                        $row4 = mysql_fetch_array($result4, MYSQL_NUM);
                                        echo $row4[0] - $row2[0];
                                        ?>   
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 1 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 2 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 3 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 4 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 5 " . $condition . " GROUP by a.account_id
						) x";
            $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.package_id = 6 " . $condition . " GROUP by a.account_id
						) x";

            //echo $query3;

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            $result6 = mysql_query($query6);
            ?>
            <div class="dashboard-box-total">
                <div class="dashboard-box-title">
                    <ul>
                        <li>
                            PACKAGES
                        </li>
                    </ul>
                </div>
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="live-totalaa" style="font-size: 12px;">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Shop - Starter: <?php
            if ($row[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=1&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=1&shoptype=0'   target='_blank'>" . $row[0] . "</a>";
                }
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>
            <?php if ($result2): ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Shop - Basic: <?php
            if ($row2[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=2&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row2[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=2&shoptype=0'   target='_blank'>" . $row2[0] . "</a>";
                }
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Shop - Business: <?php
            if ($row3[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=3&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row3[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=3&shoptype=0'   target='_blank'>" . $row3[0] . "</a>";
                }
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result4) : ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                if ($row4[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Shop - Premium: <?php
            if ($row4[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=4&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row4[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=4&shoptype=0'   target='_blank'>" . $row4[0] . "</a>";
                }
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result5) : ?>
                <?php
                $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                if ($row5[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Priority Listing: <?php
            if ($row5[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=5&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row5[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=5&shoptype=0'   target='_blank'>" . $row5[0] . "</a>";
                }
            } else {
                echo $row5[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result6) : ?>
                <?php
                $row6 = mysql_fetch_array($result6, MYSQL_NUM);

                if ($row6[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Paid Listing: <?php
            if ($row6[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=6&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row6[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=6&shoptype=0'   target='_blank'>" . $row6[0] . "</a>";
                }
            } else {
                echo $row6[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row6[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>


        </div>
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    if ($_SESSION['dashboard_shopcsr'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                    } else {
                        switch ($role_id) {

                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    }
                } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    switch ($role_id) {
                        case 1:
                            $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                            break;
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                    $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                }
            } elseif ($_SESSION['dashboard_user'] == 0) {
                if ($_SESSION['dashboard_shopcsr'] == 0) {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                } else {
                    $condition = " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                    $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
            } elseif ($_SESSION['dashboard_shopcsr'] == 0) {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            } else {
                switch ($role_id) {
                    case 1:
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        break;
                    case 2:
                        $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                        break;
                    case 3:
                        $condition = " AND r.user_id = " . $userid . " ";
                        break;
                }
                $condition .= " AND a.shop_csr = " . $_SESSION['dashboard_shopcsr'] . " ";
                $condition .= " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 1 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 2 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 3 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 4 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 5 " . $condition . " GROUP by a.account_id
						) x";
            $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id or  a.account_createdby = r.user_id WHERE a.account_paid=1 and a.account_classification = 6 " . $condition . " GROUP by a.account_id
						) x";

//echo $query3;

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            $result6 = mysql_query($query6);
            ?>
            <div class="dashboard-box-total">
                <div class="dashboard-box-title">
                    <ul>
                        <li>
                            CLASSIFICATION
                        </li>
                    </ul>
                </div>
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="live-totalaa" style="font-size: 12px;">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <?php if ($result) : ?>
                <?php
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            VIP: <?php
            if ($row[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=1&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=1&shoptype=0'   target='_blank'>" . $row[0] . "</a>";
                }
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result2) : ?>
                <?php
                $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                if ($row2[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Irate: <?php
            if ($row2[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=2&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row2[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=2&shoptype=0'   target='_blank'>" . $row2[0] . "</a>";
                }
            } else {
                echo $row2[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result3) : ?>
                <?php
                $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                if ($row3[0] != 0) :
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Cooperative: <?php
            if ($row3[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=3&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row3[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=3&shoptype=0'   target='_blank'>" . $row3[0] . "</a>";
                }
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                    ?>    
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result4): ?>
                <?php
                $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                if ($row4[0] != 0):
                    ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Aging: <?php
            if ($row4[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=4&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row4[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=4&shoptype=0'   target='_blank'>" . $row4[0] . "</a>";
                }
            } else {
                echo $row4[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($result5) : ?>
                <?php $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                if ($row5[0] != 0) : ?>

                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Normal: <?php
            if ($row5[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=5&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row5[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountclass.php?class=5&shoptype=0'   target='_blank'>" . $row5[0] . "</a>";
                }
            } else {
                echo $row5[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($result6) : ?>
                <?php
                $row6 = mysql_fetch_array($result6, MYSQL_NUM);

                if ($row6[0] != 0):
                    ?>
                    <div class="dashboard-box-total">
                        <table width="100%">
                            <tbody><tr>
                                    <td align="center" valign="middle"> 
                                        <span class="stat-desc-count">
                                            Non-cooperative: <?php
            if ($row6[0] != 0) {
                if ($_SESSION['dashboard_shoptype']) {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=6&shoptype=" . $_SESSION['dashboard_shoptype'] . "'   target='_blank'>" . $row6[0] . "</a>";
                } else {
                    echo "<a href='/88dbphcrm/accounts/accountpackage.php?package=6&shoptype=0'   target='_blank'>" . $row6[0] . "</a>";
                }
            } else {
                echo $row6[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row6[0], 2, '.', ',') . "' />";
                    ?>  
                                        </span>

                                    </td>
                                </tr>
                            </tbody></table>
                    </div>

                <?php endif; ?>
            <?php endif; ?>
        </div>
        <!---dashboard-box End---> 
        <div class="dashboard-box-5">

            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Production/CSR TOTAL
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="prodcsrtotal">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    <a href="/88dbphcrm/cronjob/sp_prodcount.php" target="_blank">PROD Backlog Count View                              
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    <a href="/88dbphcrm/cronjob/sp_prodallstatus.php" target="_blank">PROD Account Backlog List View                              
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>




        </div>
        <?php if (($department_id == 1 && $role_id == 1 && $_SESSION['job_desc'] == 6) || ($department_id == 4 && $role_id == 1)) : ?>

            <div style="clear: both;height: 10px;"></div>
            <div style="width: 870px;">
                <table width="100%" style="" class="prod-table-admin">
                    <tr class="thead">
                        <td colspan="7" align="left">88DB</td>

                    </tr>
                    <tr class="thead">
                        <td>CSR</td>
                        <td>Lined up for production</td>
                        <td>Rejected by production</td>
                        <td>CSR QA</td>
                        <td>For client approval</td>

                    </tr>
                    <?php
                    $query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as CSR FROM users WHERE department_id in (1,4) AND enabled = 1 AND job_desc = 6 AND user_type = 1 ORDER BY CSR ASC ";
                    $result = mysql_query($query);
                    $CSR = "";
                    while ($row = mysql_fetch_array($result)) {
                        $CSRid = $row["user_id"];
                        $CSRname = $row["CSR"];
                        echo '<tr class="theadtwo">';
                        echo '<td>' . $CSRname . '</td>';
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_csr = r.user_id WHERE a.account_paid=1 and a.status_id = 1  AND r.user_id = " . $CSRid . " GROUP by a.account_id
						) x";
                        $resultlined = mysql_query($query);
                        if ($resultlined) {
                            $row = mysql_fetch_array($resultlined, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowlined = $row[0];
                                $color = '#FFE1E1';
                            } else {
                                $rowlined = $row[0];
                                $color = '#fff';
                            }
                        } else {
                            $rowlined = "0";
                            $color = '#fff';
                        }
                        $newurl = '/88dbphcrm/accounts/accountdashboard.php?dash=7&user_id=' . $CSRid;
                        if ($rowlined == 0) {
                            echo '<td style="background:' . $color . '"> ' . $rowlined . ' </td>';
                        } else {
                            echo '<td style="background:' . $color . '"><a href="' . $newurl . '" target="_blankb">' . $rowlined . '</a></td>';
                        }

                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_csr = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $CSRid . " and  a.status_id in (62,57,56,55)  GROUP by a.account_id
						) x";
                        $resultrejectbyprod = mysql_query($query);
                        if ($resultrejectbyprod) {
                            $row = mysql_fetch_array($resultrejectbyprod, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowrejectbyprod = $row[0];
                                $color2 = '#FFE1E1';
                            } else {
                                $rowrejectbyprod = $row[0];
                                $color2 = '#fff';
                            }
                        } else {
                            $rowrejectbyprod = "0";
                            $color2 = '#fff';
                        }
                        $newurl3 = '/88dbphcrm/accounts/accountdashboard.php?dash=8&user_id=' . $CSRid;
                        if ($rowrejectbyprod == 0) {
                            echo '<td style="background:' . $color2 . '">' . $rowrejectbyprod . '</td>';
                        } else {
                            echo '<td style="background:' . $color2 . '"><a href="' . $newurl3 . '" target="_blankb">' . $rowrejectbyprod . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_csr = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $CSRid . " and a.status_id = 38   GROUP by a.account_id
						) x";
                        $resultcsrqa = mysql_query($query);
                        if ($resultcsrqa) {
                            $row = mysql_fetch_array($resultcsrqa, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowcsrqa = $row[0];
                                $color3 = '#FFE1E1';
                            } else {
                                $rowcsrqa = $row[0];
                                $color3 = '#fff';
                            }
                        } else {
                            $rowcsrqa = "0";
                            $color3 = '#fff';
                        }
                        $newurl4 = '/88dbphcrm/accounts/accountdashboard.php?dash=9&user_id=' . $CSRid;
                        if ($rowcsrqa == 0) {
                            echo '<td style="background:' . $color3 . '">' . $rowcsrqa . '</td>';
                        } else {
                            echo '<td style="background:' . $color3 . '"><a href="' . $newurl4 . '" target="_blankb">' . $rowcsrqa . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_csr = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $CSRid . " and a.status_id in (25,26,6)   GROUP by a.account_id
						) x";
                        $resultclientapp = mysql_query($query);
                        if ($resultclientapp) {
                            $row = mysql_fetch_array($resultclientapp, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowclientapp = $row[0];
                                $color4 = '#FFE1E1';
                            } else {
                                $rowclientapp = $row[0];
                                $color4 = '#fff';
                            }
                        } else {
                            $rowclientapp = "0";
                            $color4 = '#fff';
                        }
                        $newurl5 = '/88dbphcrm/accounts/accountdashboard.php?dash=10&user_id=' . $CSRid;
                        if ($rowclientapp == 0) {
                            echo '<td style="background:' . $color4 . '">' . $rowclientapp . '</td>';
                        } else {
                            echo '<td style="background:' . $color4 . '"><a href="' . $newurl5 . '" target="_blankb">' . $rowclientapp . '</a></td>';
                        }

                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        <?php endif; ?>
        <?php if (($department_id == 1 && $role_id == 1 && $_SESSION['job_desc'] == 3) || ($department_id == 4 && $role_id == 1) || ($department_id == 1 && $role_id == 1 && $_SESSION['job_desc'] == 5)) : ?>
            <div style="clear: both;height: 10px;"></div>
            <!--        <div class="dept-title">
                            <span>Running shops for designers</span>
                        </div>   -->
            <div style="width: 870px;">
                <table width="100%" style="" class="prod-table-admin">
                    <tr class="thead">
                        <td colspan="7" align="left">88DB</td>

                    </tr>
                    <tr class="thead">
                        <td>Designers</td>
                        <td>For Development</td>
                        <td>Under Construction</td>
                        <td>Client Revisions</td>
                        <td>Internal Revisions</td>
                        <td>Redesign</td>
                        <td>Client Updates</td>
                    </tr>

                    <?php
                    $query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 2 AND user_type = 1 ORDER BY designer ASC ";
                    $result = mysql_query($query);
                    $designers = "";
                    while ($row = mysql_fetch_array($result)) {
                        $designerid = $row["user_id"];
                        $designername = $row["designer"];
                        echo '<tr class="theadtwo">';
                        echo '<td>' . $designername . '</td>';
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 21  AND r.user_id = " . $designerid . " GROUP by a.account_id
						) x";
                        $resultfordev = mysql_query($query);
                        if ($resultfordev) {
                            $row = mysql_fetch_array($resultfordev, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowfordev = $row[0];
                                $color = '#FFE1E1';
                            } else {
                                $rowfordev = $row[0];
                                $color = '#fff';
                            }
                        } else {
                            $rowfordev = "0";
                            $color = '#fff';
                        }
                        $newurl = '/88dbphcrm/accounts/accountdashboard.php?dash=1&user_id=' . $designerid;
                        if ($rowfordev == 0) {
                            echo '<td style="background:' . $color . '"> ' . $rowfordev . ' </td>';
                        } else {
                            echo '<td style="background:' . $color . '"><a href="' . $newurl . '" target="_blankb">' . $rowfordev . '</a></td>';
                        }
                        $query = "select count(x.account_id) , x.before_status_id from
						(
                                                
                                                SELECT 
                                                a.account_id,

                                                au.before_status_id,
                                                au.after_status_id,
                                                au.audit_createdon 
                                                FROM accounts a
                                                INNER JOIN users r ON a.shop_designer = r.user_id
                                                INNER JOIN audit  au ON (a.account_id = au.account_id)
                                                LEFT OUTER JOIN audit au2 ON 
                                                (a.account_id = au2.account_id 
                                                AND 
                                                (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
                                                AND au.account_id < au.account_id))
                                                WHERE au2.account_id IS NULL
                                                AND a.account_paid= 1 and a.status_id = 3 
                                                AND a.status_id =au.after_status_id
                                                AND r.user_id = " . $designerid . " 
 GROUP by a.account_id 
                                                
                                                ) x";
                        $resultunder = mysql_query($query);
                        if ($resultunder) {
                            $row = mysql_fetch_array($resultunder, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowunder = $row[0];
                                $color1 = '#FFE1E1';
                            } else {
                                $rowunder = $row[0];
                                $color1 = '#fff';
                            }
                            if ($row[1] != 21) {
                                $rowunderprevstat = '- &#174;';
                            } else {
                                $rowunder = $row[0];
                                $rowunderprevstat = '';
                            }
                        } else {
                            $rowunder = "0";
                            $color1 = '#fff';
                        }
                        $newurl2 = '/88dbphcrm/accounts/accountdashboard.php?dash=2&user_id=' . $designerid;
                        if ($rowunder == 0) {
                            echo '<td style="background:' . $color1 . '">' . $rowunder . '</td>';
                        } else {
                            echo '<td style="background:' . $color1 . '"><a href="' . $newurl2 . '" target="_blankb">' . $rowunder . '' . $rowunderprevstat . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 8 or a.status_id = 30 or a.status_id = 31 or a.status_id = 41 or a.status_id = 42) GROUP by a.account_id
						) x";
                        $resultclient = mysql_query($query);
                        if ($resultclient) {
                            $row = mysql_fetch_array($resultclient, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowclient = $row[0];
                                $color2 = '#FFE1E1';
                            } else {
                                $rowclient = $row[0];
                                $color2 = '#fff';
                            }
                        } else {
                            $rowclient = "0";
                            $color2 = '#fff';
                        }
                        $newurl3 = '/88dbphcrm/accounts/accountdashboard.php?dash=3&user_id=' . $designerid;
                        if ($rowclient == 0) {
                            echo '<td style="background:' . $color2 . '">' . $rowclient . '</td>';
                        } else {
                            echo '<td style="background:' . $color2 . '"><a href="' . $newurl3 . '" target="_blankb">' . $rowclient . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 7 or a.status_id = 28 or a.status_id = 29 or a.status_id = 32 or a.status_id = 33 or a.status_id = 34) GROUP by a.account_id
						) x";
                        $resultintrev = mysql_query($query);
                        if ($resultintrev) {
                            $row = mysql_fetch_array($resultintrev, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowintrev = $row[0];
                                $color3 = '#FFE1E1';
                            } else {
                                $rowintrev = $row[0];
                                $color3 = '#fff';
                            }
                        } else {
                            $rowintrev = "0";
                            $color3 = '#fff';
                        }
                        $newurl4 = '/88dbphcrm/accounts/accountdashboard.php?dash=4&user_id=' . $designerid;
                        if ($rowintrev == 0) {
                            echo '<td style="background:' . $color3 . '">' . $rowintrev . '</td>';
                        } else {
                            echo '<td style="background:' . $color3 . '"><a href="' . $newurl4 . '" target="_blankb">' . $rowintrev . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 37 or a.status_id = 43 or a.status_id = 44) GROUP by a.account_id
						) x";
                        $resultredesign = mysql_query($query);
                        if ($resultredesign) {
                            $row = mysql_fetch_array($resultredesign, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowredesign = $row[0];
                                $color4 = '#FFE1E1';
                            } else {
                                $rowredesign = $row[0];
                                $color4 = '#fff';
                            }
                        } else {
                            $rowredesign = "0";
                            $color4 = '#fff';
                        }
                        $newurl5 = '/88dbphcrm/accounts/accountdashboard.php?dash=5&user_id=' . $designerid;
                        if ($rowredesign == 0) {
                            echo '<td style="background:' . $color4 . '">' . $rowredesign . '</td>';
                        } else {
                            echo '<td style="background:' . $color4 . '"><a href="' . $newurl5 . '" target="_blankb">' . $rowredesign . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and a.status_id = 27 GROUP by a.account_id
						) x";
                        $resultclientupdates = mysql_query($query);
                        if ($resultclientupdates) {
                            $row = mysql_fetch_array($resultclientupdates, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowclientup = $row[0];
                                $color5 = '#FFE1E1';
                            } else {
                                $rowclientup = $row[0];
                                $color5 = '#fff';
                            }
                        } else {
                            $rowclientup = "0";
                            $color5 = '#fff';
                        }
                        $newurl6 = '/88dbphcrm/accounts/accountdashboard.php?dash=6&user_id=' . $designerid;
                        if ($rowclientup == 0) {
                            echo '<td style="background:' . $color5 . '">' . $rowclientup . '</td>';
                        } else {
                            echo '<td style="background:' . $color5 . '"><a href="' . $newurl6 . '" target="_blankb">' . $rowclientup . '</a></td>';
                        }
                        echo '</tr>';
                    }
                    ?>



                </table>
            </div>
            <div style="clear: both;height: 10px;"></div>
            <div style="width: 870px;">
                <table width="100%" style="" class="prod-table-admin">
                    <tr class="thead">
                        <td colspan="7" align="left">OpenRice</td>

                    </tr>
                    <tr class="thead">
                        <td>Designers</td>
                        <td>For Development</td>
                        <td>Under Construction</td>
                        <td>Client Revisions</td>
                        <td>Internal Revisions</td>
                        <td>Redesign</td>
                        <td>Client Updates</td>
                    </tr>

                    <?php
                    $query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 2 AND user_type = 2 ORDER BY designer ASC ";
                    $result = mysql_query($query);
                    $designers = "";
                    while ($row = mysql_fetch_array($result)) {
                        $designerid = $row["user_id"];
                        $designername = $row["designer"];
                        echo '<tr class="theadtwo">';
                        echo '<td>' . $designername . '</td>';
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 21  AND r.user_id = " . $designerid . " GROUP by a.account_id
						) x";
                        $resultfordev = mysql_query($query);
                        if ($resultfordev) {
                            $row = mysql_fetch_array($resultfordev, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowfordev = $row[0];
                                $color = '#FFE1E1';
                            } else {
                                $rowfordev = $row[0];
                                $color = '#fff';
                            }
                        } else {
                            $rowfordev = "0";
                            $color = '#fff';
                        }
                        $newurl = '/88dbphcrm/accounts/accountdashboard.php?dash=1&user_id=' . $designerid;
                        if ($rowfordev == 0) {
                            echo '<td style="background:' . $color . '"> ' . $rowfordev . ' </td>';
                        } else {
                            echo '<td style="background:' . $color . '"><a href="' . $newurl . '" target="_blankb">' . $rowfordev . '</a></td>';
                        }
                        $query = "select count(x.account_id) , x.before_status_id from
						(
                                                
                                                SELECT 
                                                a.account_id,

                                                au.before_status_id,
                                                au.after_status_id,
                                                au.audit_createdon 
                                                FROM accounts a
                                                INNER JOIN users r ON a.shop_designer = r.user_id
                                                INNER JOIN audit  au ON (a.account_id = au.account_id)
                                                LEFT OUTER JOIN audit au2 ON 
                                                (a.account_id = au2.account_id 
                                                AND 
                                                (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
                                                AND au.account_id < au.account_id))
                                                WHERE au2.account_id IS NULL
                                                AND a.account_paid= 1 and a.status_id = 3 
                                                AND a.status_id =au.after_status_id
                                                AND r.user_id = " . $designerid . " 
 GROUP by a.account_id 
                                                
                                                ) x";
                        $resultunder = mysql_query($query);
                        if ($resultunder) {
                            $row = mysql_fetch_array($resultunder, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowunder = $row[0];
                                $color1 = '#FFE1E1';
                            } else {
                                $rowunder = $row[0];
                                $color1 = '#fff';
                            }
                            if ($row[1] != 21) {
                                $rowunderprevstat = '- &#174;';
                            } else {
                                $rowunder = $row[0];
                                $rowunderprevstat = '';
                            }
                        } else {
                            $rowunder = "0";
                            $color1 = '#fff';
                        }
                        $newurl2 = '/88dbphcrm/accounts/accountdashboard.php?dash=2&user_id=' . $designerid;
                        if ($rowunder == 0) {
                            echo '<td style="background:' . $color1 . '">' . $rowunder . '</td>';
                        } else {
                            echo '<td style="background:' . $color1 . '"><a href="' . $newurl2 . '" target="_blankb">' . $rowunder . '' . $rowunderprevstat . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 8 or a.status_id = 30 or a.status_id = 31 or a.status_id = 41 or a.status_id = 42) GROUP by a.account_id
						) x";
                        $resultclient = mysql_query($query);
                        if ($resultclient) {
                            $row = mysql_fetch_array($resultclient, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowclient = $row[0];
                                $color2 = '#FFE1E1';
                            } else {
                                $rowclient = $row[0];
                                $color2 = '#fff';
                            }
                        } else {
                            $rowclient = "0";
                            $color2 = '#fff';
                        }
                        $newurl3 = '/88dbphcrm/accounts/accountdashboard.php?dash=3&user_id=' . $designerid;
                        if ($rowclient == 0) {
                            echo '<td style="background:' . $color2 . '">' . $rowclient . '</td>';
                        } else {
                            echo '<td style="background:' . $color2 . '"><a href="' . $newurl3 . '" target="_blankb">' . $rowclient . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 7 or a.status_id = 28 or a.status_id = 29 or a.status_id = 32 or a.status_id = 33 or a.status_id = 34) GROUP by a.account_id
						) x";
                        $resultintrev = mysql_query($query);
                        if ($resultintrev) {
                            $row = mysql_fetch_array($resultintrev, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowintrev = $row[0];
                                $color3 = '#FFE1E1';
                            } else {
                                $rowintrev = $row[0];
                                $color3 = '#fff';
                            }
                        } else {
                            $rowintrev = "0";
                            $color3 = '#fff';
                        }
                        $newurl4 = '/88dbphcrm/accounts/accountdashboard.php?dash=4&user_id=' . $designerid;
                        if ($rowintrev == 0) {
                            echo '<td style="background:' . $color3 . '">' . $rowintrev . '</td>';
                        } else {
                            echo '<td style="background:' . $color3 . '"><a href="' . $newurl4 . '" target="_blankb">' . $rowintrev . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and (a.status_id = 37 or a.status_id = 43 or a.status_id = 44) GROUP by a.account_id
						) x";
                        $resultredesign = mysql_query($query);
                        if ($resultredesign) {
                            $row = mysql_fetch_array($resultredesign, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowredesign = $row[0];
                                $color4 = '#FFE1E1';
                            } else {
                                $rowredesign = $row[0];
                                $color4 = '#fff';
                            }
                        } else {
                            $rowredesign = "0";
                            $color4 = '#fff';
                        }
                        $newurl5 = '/88dbphcrm/accounts/accountdashboard.php?dash=5&user_id=' . $designerid;
                        if ($rowredesign == 0) {
                            echo '<td style="background:' . $color4 . '">' . $rowredesign . '</td>';
                        } else {
                            echo '<td style="background:' . $color4 . '"><a href="' . $newurl5 . '" target="_blankb">' . $rowredesign . '</a></td>';
                        }
                        $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a INNER JOIN users r ON a.shop_designer = r.user_id WHERE a.account_paid=1  AND r.user_id = " . $designerid . " and a.status_id = 27 GROUP by a.account_id
						) x";
                        $resultclientupdates = mysql_query($query);
                        if ($resultclientupdates) {
                            $row = mysql_fetch_array($resultclientupdates, MYSQL_NUM);

                            if ($row[0] != 0) {
                                $rowclientup = $row[0];
                                $color5 = '#FFE1E1';
                            } else {
                                $rowclientup = $row[0];
                                $color5 = '#fff';
                            }
                        } else {
                            $rowclientup = "0";
                            $color5 = '#fff';
                        }
                        $newurl6 = '/88dbphcrm/accounts/accountdashboard.php?dash=6&user_id=' . $designerid;
                        if ($rowclientup == 0) {
                            echo '<td style="background:' . $color5 . '">' . $rowclientup . '</td>';
                        } else {
                            echo '<td style="background:' . $color5 . '"><a href="' . $newurl6 . '" target="_blankb">' . $rowclientup . '</a></td>';
                        }
                        echo '</tr>';
                    }
                    ?>



                </table>
            </div>

        <?php endif; ?>
        <!--PROD END-->

    <?php } ?>

</div>
<!---dash-row-container End---> 
<script>
    $(function() {
        var totalfinance = $("#total-finance").val();
        var totalprod = $("#prod-total").val();
        var Intreved  = $("#Int-rev-ed-total").val();
        var intrevqa  = $("#int-rev-qa-total").val();
                        
        var clienttotal  = $("#client-total-total").val();
        var clientrev  = $("#clientrevtotal").val();
        var redesigntotal  = $("#redesign-total-total").val();
        var rejectcsr  = $("#reject-csr-total").val();
        var livetotal  = $("#live-total-total").val();
        var tempclosed  = $("#temp-closed-total").val();
        var totalrejecteditor  = $("#reject-editor").val();
        var totalcsrtosale = $("#csr-reject-to-sale-total").val();
            
            
        $("#financetotal").text(totalfinance);
        $("#prodtotal").text(totalprod);
        $("#Int-rev-ed").text(Intreved);
        $("#int-rev-qa").text(intrevqa);
        $("#client-total").text(clienttotal);
            
        $("#clientrev").text(clientrev);
        
        
        $("#redesign-total").text(redesigntotal);
        $("#reject-csr").text(rejectcsr);
        $("#live-total").text(livetotal);
        $("#temp-closed").text(tempclosed);
        $("#reject-editor-total").text(totalrejecteditor);
        $("#csr-reject-to-sale").text(totalcsrtosale);        
        
        var a =  parseInt($("#lptotal").text()) ;
        var b =  parseInt($("#prodtotal").text());
        var c =  parseInt($("#pftotal").text());
        //        var d =  parseInt($("#fditotal").text());
        var e =  parseInt($("#fdevetotal").text());
        var f =  parseInt($("#undercontotal").text());
        var g =  parseInt($("#edqatotal").text());
        var h =  parseInt($("#Int-rev-ed").text());
        var i =  parseInt($("#forsrdqatotal").text());
        var j =  parseInt($("#int-rev-qa").text());
        var k =  parseInt($("#finalqatotal").text());
        var l =  parseInt($("#client-total").text());
        var m =  parseInt($("#clientrev").text());
        var n =  parseInt($("#redesign-total").text() );
        var o =  parseInt($("#reject-csr").text());
        var p =  parseInt($("#live-total").text());
        var q =  parseInt($("#temp-closed").text());
        var r =  parseInt($("#csr-reject-to-sale").text());
        
        
        var alltotal =  a + b + c  + e + f + g + h + i + j + k + l + m + n + o + p + q + r;  
        //         var alltotal =  a + b + c + d + e + f + g + h + i + j + k + l + m + n + o + p + q + r;             
        $("#prodcsrtotal").text(alltotal);            
    });
                                                                                                                                                 
</script>


<script>
    function slotmachine(id, changeto) {
        var thisid = '#' + id;
        var $obj = $(thisid);
        $obj.css('opacity', '.3');
        var original = $obj.text();

        var spin = function() {
            return Math.floor(Math.random() * 10);
        };

        var spinning = setInterval(function() {
            $obj.text(function() {
                var result = '';
                for (var i = 0; i < original.length; i++) {
                    result += spin().toString();
                }
                return result;
            });
        }, 50);

        var done = setTimeout(function() {
            clearInterval(spinning);
            $obj.text(changeto).css('opacity', '1');
        }, 1000);
    }           
           
</script>