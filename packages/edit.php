<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$package_id = $_REQUEST['package_id'];
mysql_connect('localhost','root','');
@mysql_select_db('88dbphcrm') or die( "Unable to select database");

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {

	$package_name = $_POST['package_name'];
	$package_promo = $_POST['package_promo'];
	$package_amount = $_POST['package_amount'];
	$package_remarks = $_POST['package_remarks'];
	$package_id = $_REQUEST['package_id'];
   


    $query = "UPDATE packages SET package_name = '".mysql_real_escape_string($package_name)."', package_promo = '".mysql_real_escape_string($package_promo)."', package_amount = '".mysql_real_escape_string($package_amount)."', package_remarks = '".mysql_real_escape_string($package_remarks)."', package_modifiedby = $userid,  package_modifiedon = now() WHERE package_id = $package_id";
  
   $result = mysql_query($query,connect());
   
}
else 
{
    $package_id = $_REQUEST['package_id'];
    
}

$query = "SELECT p.*, concat(u1.user_firstname,' ',u1.user_lastname) AS createdby, concat(u2.user_firstname,' ',u2.user_lastname) AS modifiedby FROM packages p LEFT JOIN users u1 ON p.package_createdby = u1.user_id LEFT JOIN users u2 ON p.package_modifiedby = u2.user_id WHERE p.package_id = $package_id";


$result = mysql_query($query,connect());
$num = mysql_numrows($result);
if ($num > 0) {
		
		$package_name = mysql_result($result, 0, 'package_name');
	$package_promo = mysql_result($result, 0, 'package_promo');
	$package_amount = mysql_result($result, 0, 'package_amount');
	$package_remarks = mysql_result($result, 0, 'package_remarks');
	

    $createdby = mysql_result($result, 0, 'createdby');
    $createdon = mysql_result($result, 0, 'package_createdon');
    $modifiedby = mysql_result($result, 0, 'modifiedby');

}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Edit Account</title>

    <?php include '../header.php'; ?>

    <div class="main-section">
        
        <form name="form1" id="form1" action="edit.php?package_id=<?php echo $_GET['package_id'];?>" method="post" >

            <div class="commands">
                
                <div class="head-label">
                    <h2>Edit Payment</h2>
                </div><!-- end of add new account -->
                
                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/packages'>Cancel</a></li>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

               
                 <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Package Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="package_name" id="package_name" value="<?php echo $package_name; ?>" class="required"  />
						
						</td>
                    </tr>
					 <tr>
                        <td class="grid-head">*Amount:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="package_amount" id="package_amount" value="<?php echo $package_amount; ?>" class="required" autocomplete="off"  />
						
						</td>
                    </tr>
					 <tr>
                        <td class="grid-head">*Promo:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="package_promo" id="package_promo" value="<?php echo $package_promo; ?>"  />
						
						</td>
                    </tr>
					
					<tr>
                        <td class="grid-head">*Remarks:</td>
                        <td>&nbsp;</td>
                        <td><textarea cols="25" rows="3" name="package_remarks" id="package_remarks" ><?php echo $package_remarks; ?></textarea></td>
                    </tr>
              
                    <tr class="edit-account-footer">
                        <td colspan="4">
                            <?php echo "Created by: ".$createdby." On: " . $createdon . " | Modified by: ".$modifiedby; ?>
                        </td>
                    </tr>
                </table>

          

            </div><!-- gen-section --> 
        </form>
        
    </div><!-- main-section --> 

	
	
	
    <script type="text/javascript">
	
	
	
	
	
    $(document).ready(function() {
	


	
    

		$("#form1").validate();

        $("#payment_amount").numeric();        
    });

    </script>
   
</body>
</html>