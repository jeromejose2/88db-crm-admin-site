<?php 
include_once ('functions/functions.php');
include_once ('functions/connection.php');



session_start();
$_SESSION['title_page']  = "Packages";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];


$query = "SELECT status_id, status_name, department_id FROM statuses";
$result = mysql_query($query,connect());
$statusoptions = "";
while ($row = mysql_fetch_array($result)) {
        $statusid = $row["status_id"];
        $statusname = $row["status_name"];
        if ($_REQUEST['sid']) {
            if ($statusid == $_REQUEST['sid']) {
                $statusoptions .= '<option value="'.$statusid.'" selected="selected">' . $statusname . "</option>";
            } else {
                $statusoptions .= '<option value="'.$statusid.'">' . $statusname . "</option>";
            }
        } else {
            $statusoptions .= '<option value="'.$statusid.'">' . $statusname . "</option>";
        }
    }
?>
 <?php
               
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	
	$fquery = "SELECT Count(*) as num, a.*, s.status_name, c.category_name, sr.source_name FROM accounts a LEFT JOIN statuses s ON a.status_id = s.status_id LEFT JOIN categories c ON a.category_id = c.category_id LEFT JOIN sources sr ON a.source_id = sr.source_id LEFT JOIN users r ON a.account_createdby = r.user_id ".$condition." ORDER BY a.account_createdon DESC";
	$total_pages = mysql_fetch_array(mysql_query($fquery));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	
	
	
	
	$condition = RoleId($role_id,$_REQUEST['sid'],$_REQUEST['cat'],$_REQUEST['source'],$_REQUEST['search'],$_SESSION['department_id']);
	
	
	
	$total_pages = GetSumData($condition);
	
	$req_limit = "25";
	
	$page = $_REQUEST['page'];
	if($limit) 
		$limit = $req_limit; 			//how many items to show per page
	else
		$limit = 25;		
		
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;		
		
	
			
                $table = GetData($condition,$total_pages,$role_id);
             
				
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
						//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Packages</title>

<?php include '../header.php'; ?>

    <div class="main-section">

        <div class="commands">
            
            <div class="head-label">
                <h2>Packages</h2>
            </div><!-- end of add new account -->

            <ul>
                <li><a class="link-button" href='/88dbphcrm/packages/add.php'>Add New</a></li>
            </ul>
        </div><!-- end of grid-commands -->

        <div style="height: 100%; width: 100%; border: 1px solid #DDD; margin-bottom: 15px; padding-top: 15px;">
	
			
			
			<div style="margin-left:15px;padding-top:5px;padding-bottom:15px;">
			<form method="get" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" id="searchfrm" name="searchfrm" >
			<input type="text" value="<?php if($_REQUEST['search']) echo $_REQUEST['search'];?>" style="color:#000;width:150px;padding-top:2px;padding-bottom:2px;margin-right: 10px;" placeholder="Enter Package Name" id="search" name="search" />
			
			<a class="link-button" href="javascript:void(0)" onclick="document.searchfrm.submit();" >Search</a>
			</form>
			
			</div>
        </div>
        
        
        <div class="gen-section">

            <table class="main-grid">
                <tr class="grid-head">
                    <td>ID</td>
                    <td>Package Name</td>
                    <td>Amount</td>
                    <td>Promo</td>
                    <td>Created by</td>
                    <td>Date Created</td>
                    <?php if ($role_id == 1) echo "<td></td>"; ?>
                </tr>

               <?php
	
	$table = GetData($condition,$start,$limit);
	//print_r($table);
	
	echo $table;
                ?>
            </table>
<div style="margin:20px auto;width:180px;">
			
			<?php
	
	if($page==1)
	{
	$status_id = $_REQUEST['sid'];
	if ($status_id) $sid = "&sid=".$status_id;	
	
	echo "<a href='#'> <-Previous</a>";
	}
	else
	{
	$previous = $page - 1;
	
		
		//if((strpos($_SERVER["REQUEST_URI"], 'search=') ===false) || (strpos($_SERVER["REQUEST_URI"], 'sid=') ===false) )
		if((strpos($_SERVER["REQUEST_URI"], 'search=') ===false) )
					{ 
						$prev_new_url = "<a href='".$current_url."?page=$previous'> Previous-></a>";
					}
					
					else
					{ 
						
						//$prev_new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&sid=". $_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=". $previous ."'> Previous-></a>";
						
						$prev_new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&page=". $previous ."'> Previous-></a>";
					}
	
	echo $prev_new_url;
	}
			
			
			
			?>
			<select id="selectfield" name="selectfield" style="margin: 0 15px;" onchange="document.location.href=this.options[this.selectedIndex].value">
                <?php
				//$status_id = $_REQUEST['sid'];
				//if ($status_id) $sid = "&sid=".$status_id;	
				$ct = 1;
				
			
					
				while($ct<=$lastpage)
				{
				
					if((strpos($_SERVER["REQUEST_URI"], 'search=') === false) )
					{ 
						$url_filter ="?page=";
					}
					else
					{
				
					//$url_filter ="?search=".$_REQUEST['search']."&sid=".$_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=";
					$url_filter ="?search=".$_REQUEST['search']."&page=";
					}
					
					
				if($page == $ct)
				{
				echo "<option value='".basename($_SERVER["SCRIPT_NAME"]).$url_filter.$ct."' selected>".$ct."</option>";
				}
				else
				{
				echo "<option value='".basename($_SERVER["SCRIPT_NAME"]).$url_filter.$ct."'>".$ct."</option>";
				}
				
				$ct++;
				}
				?>
            </select>
	<?php
	
	if($page)
	{
	$next = $page + 1;
	$status_id = $_REQUEST['sid'];
	
	
		if($next >= $lastpage)
		{
		echo "<a href='#'> Next-></a>";
		
		}
		else
		{
				
				//if((strpos($_SERVER["REQUEST_URI"], 'search=') ===false) || (strpos($_SERVER["REQUEST_URI"], 'sid=') ===false) )
				if((strpos($_SERVER["REQUEST_URI"], 'search=') ===false)  )
					{ 
						$new_url = "<a href='".$current_url."?page=$next'> Next-></a>";
					}
					
					else
					{ 
						
						//$new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&sid=". $_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=". $next ."'> Next-></a>";
						
						$new_url = "<a href='".basename($_SERVER["SCRIPT_NAME"])."?search=". $_REQUEST['search'] ."&page=". $next ."'> Next-></a>";
					}

				
				
				
				//echo "<a href='{$_SERVER["REQUEST_URI"]}&page=$next'> Next-></a>";
				echo $new_url;
		}
	
	
	
	}
	
			
			
			
			?>
			</div>
        </div><!-- end of gen-section -->
		
    </div><!-- end of main-section -->

</div><!-- end of main-container -->
<script type="text/javascript">
    function filter() 
	{
        var e = document.getElementById("status_id");
        var g = e.options[e.selectedIndex].value;
		
	if(g=="")
	{
        window.location.href='/88dbphcrm/accounts/';
	}
	else
	{
	   window.location.href='/88dbphcrm/accounts/?sid='+g;
	}
    }
</script>
</body>
</html>