<?php
 
// if the 'term' variable is not sent with the request, exit
if ( !isset($_REQUEST['term']) )
	exit;
 
// connect to the database server and select the appropriate database for use
$dblink = mysql_connect('localhost', 'root', '') or die( mysql_error() );
mysql_select_db('88dbphcrm');
 
// query the database table for zip codes that match 'term'
$rs = mysql_query('SELECT account_name FROM accounts WHERE account_name LIKE "'. mysql_real_escape_string($_REQUEST['term']) .'%" ORDER BY account_name ASC LIMIT 0,10', $dblink);
 
// loop through each zipcode returned and format the response for jQuery
$data = array();
if ( $rs && mysql_num_rows($rs) )
{
	while( $row = mysql_fetch_array($rs, MYSQL_ASSOC) )
	{
		$data[] = array(
			'label' => $row['account_name'],
			'value' => $row['account_name']
		);
	}
}
 
// jQuery wants JSON data
echo json_encode($data);
flush();