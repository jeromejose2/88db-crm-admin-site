<?php
include_once("functions/connection.php");
include_once("functions/functions.php");
session_start();
$_SESSION['title_page'] = "Home";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$department_id = $_SESSION['department_id'];
$from = strtotime('monday last week');
$to = strtotime('friday this week');
if (isset($_POST['user'])) {
    $_SESSION['dashboard_user'] = $_POST['user'];
}
if (isset($_POST['shop_type'])) {
    $_SESSION['dashboard_shoptype'] = $_POST['shop_type'];
}
if (isset($_POST['shop_csr'])) {
    $_SESSION['dashboard_shopcsr'] = $_POST['shop_csr'];
}
//echo $_SERVER['REMOTE_ADDR'];
//echo 'dashboar :' .$_SESSION['dashboard_user'] ;
//echo 'shoptype :' .$_SESSION['dashboard_shoptype'] ;
//echo 'csr :' .$_SESSION['dashboard_shopcsr'] ;


$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE  enabled = 1 AND job_desc = 6  ORDER BY designer ASC ";
$result = mysql_query($query, connect());
$csr = "";
while ($row = mysql_fetch_array($result)) {
    $csrid = $row["user_id"];
    $csrname = $row["designer"];
    if ($csrid == $_SESSION['dashboard_shopcsr']) {
        $csr .= '<option value="' . $csrid . '" selected="selected">' . $csrname . "</option>";
    } else {
        $csr .= '<option value="' . $csrid . '">' . $csrname . "</option>";
    }
}
?>
<!DOCTYPE HTML >
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>88DB PH CRM</title>

        <?php include 'header.php'; ?>

    <div class="main-section">

        <div class="commands">
            <div class="head-label">
                <h2>Dashboard</h2>
            </div><!-- end of add new account -->

        </div><!-- end of grid-commands -->
        <?php if ($role_id != 3) { ?>
            <div style="height: 100%; width: 100%; border: 1px solid #DDD; margin-bottom: 15px; padding-top: 15px;">
                <div style="margin-left:15px;padding-top:5px;padding-bottom:15px;">
                    <form method="post" action="/88dbphcrm/index.php" id="searchfrm" name="searchfrm">			
                        Filter Data by User: 
                        <select id="user" name="user" style="margin-left: 15px;" onchange="document.searchfrm.submit();">
                            <option value="0">All Users</option>
                            <?php echo UserBox($department_id, $role_id, $userid, $_SESSION['dashboard_user']); ?>
                        </select>
                        Filter by Shop Type:
                        <select name="shop_type" id="shop_type"  onchange="document.searchfrm.submit();">
                            <option value="0" <?php if ($_SESSION['dashboard_shoptype'] == 0): ?> selected="selected" <?php endif; ?>>Select Shop Type</option>
                            <option value="1" <?php if ($_SESSION['dashboard_shoptype'] == 1): ?> selected="selected" <?php endif; ?>>88DB</option>
                            <option value="2" <?php if ($_SESSION['dashboard_shoptype'] == 2): ?> selected="selected" <?php endif; ?>>OpenRice</option>                            
                        </select>
                        Filter by CSR :
                        <select name='shop_csr' id='shop_csr' onchange='document.searchfrm.submit();'>
                            <option value='0'>Select CSR</option>
                            <?php echo $csr; ?>
                        </select>
                        <a class="link-button" href='/88dbphcrm/reset.php' style="background-color: red; border: 1px solid red; color: #fff;margin-bottom:0px;">Reset</a>
                    </form>
                </div>
            </div>
        <?php } ?>
        <?php include 'row-csr.php'; ?>
        <div class="clear"></div>
        <?php if ($department_id == 2 || $department_id == 4) { ?>


        <?php } ?>			
        <div class="clear"></div>

        <script>
            $(function(){
			
                $('.dashboard-link').click(function(e){
                    e.preventDefault();
			   
			   
                    $.ajax({
                        type: "POST",
                        dataType: "html",
                        url: 'accounts/index.php',
                        timeout: 20000,
                        data: { sid:$(this).attr('href')<?php
        if (isset($_SESSION['dashboard_user'])) {
            echo ",user:" . $_SESSION['dashboard_user'];
        }
        ?>},
                    success: function(){

                        window.location="/88dbphcrm/accounts/index.php";
					


                    }
                });
			  
            });
        });
			
        </script>
        <?php if ($department_id == 2 || $department_id == 4) { ?>
            <script>
                    jQuery('#tb1Leads').gvChart({
                        chartType: 'AreaChart',
                                                                			
                        gvSettings: {
                            //vAxis: {title: 'No of Sales Leads'},
                            hAxis: {title: '<?php echo "From " . date('M d,Y', $from) . " to " . date('M d,Y', $to); ?>',
                                textPosition:'none'},
                                                                					
                                                                				
                            width: 900,
                            pointSize: 3,
                            height:200,
                            legend: {position: 'bottom'},
                            chartArea: {width: 800}
                        }
                    });
                                                                			
                                                                			
                    jQuery('#tb2Signed').gvChart({
                        chartType: 'AreaChart',
                        gvSettings: {
                            vAxis: {title: 'No of Signed Shops'},
                            hAxis: {title: '<?php echo "From " . date('M d,Y', $from) . " to " . date('M d,Y', $to); ?>',textPosition:'none'},
                                                                					
                            legend:{position: 'bottom'},
                            width: 900,
                            pointSize: 3,
                                                                					
                            height: 200, chartArea: {width: 800}
                        }
                    });
                    jQuery('#tb3SignedCat').gvChart({
                        chartType: 'PieChart',
                        gvSettings: {
                            title:'<?php echo "From " . date('M d,Y', $from) . " to " . date('M d,Y', $to); ?>',
                            chartArea:{left:10,width:250},
                            legend: {position: 'none'},
                            width: 300,
                            height: 350,
                                                                				
                        }
                    });
                    jQuery('#tb4SignedSource').gvChart({
                        chartType: 'PieChart',
                        gvSettings: {
                            title:'<?php echo "From " . date('M d,Y', $from) . " to " . date('M d,Y', $to); ?>',
                            chartArea:{left:10,width:250},
                            legend: {position: 'none'},
                            width: 300,
                            height: 350
                        }
                    });
                                                                			


                                                                			

                                                                            
            </script>
        <?php } ?>
    </div><!-- end of main-section -->

</div><!-- end of main-container -->

</body>
</html>

