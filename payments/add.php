<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');


session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$contract_number = $_REQUEST['cid'];

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {

    $contract_number = $_POST['contract_number'];
    $account_id = $_POST['account_id'];
    $payment_amount = $_POST['payment_amount'];
    $payment_date = $_POST['payment_date'] . " " . $_POST['payment_time'];
    mysql_connect('localhost', 'root', '');
    mysql_select_db("88dbphcrm");


    $query = "INSERT INTO payments (account_id, payment_amount,  payment_createdby,  payment_createdon) 
			VALUES ('" . mysql_real_escape_string($account_id) . "', '" . mysql_real_escape_string($payment_amount) . "', '$userid', '$payment_date')";
    $result = mysql_query($query, connect());
    //$account_id = mysql_insert_id();


    $remark = "Amount paid: " . number_format($payment_amount);
    $insert_query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$remark','1', $account_id, $userid)";
    $result = mysql_query($insert_query, connect());




    $payment_query = "SELECT SUM(p.payment_amount) as paid_amount, 		
					a.account_contractamount,
					a.account_id,
					a.account_name,
					a.status_id,
					st.status_name,
					st.department_id FROM payments p 
					LEFT JOIN accounts a ON a.account_id = p.account_id 
					LEFT JOIN statuses st ON a.status_id = st.status_id 
					WHERE p.account_id  = $account_id";



    $payment_result = mysql_query($payment_query);

    $paid_amount = mysql_result($payment_result, 0, 'paid_amount');
    $account_status_dept = mysql_result($payment_result, 0, 'department_id');

    $account_contractamount = mysql_result($payment_result, 0, 'account_contractamount');

    if ($account_contractamount == $paid_amount) {

        $status_id = "16";
        $query = "INSERT INTO audit (account_id, before_status_id, after_status_id, audit_createdby, audit_createdon) VALUES ('$account_id', 13, 16, $userid,now())";
        $result = mysql_query($query, connect());
    } else {

        $status_id = "17";
        $query = "INSERT INTO audit (account_id, before_status_id, after_status_id, audit_createdby, audit_createdon) VALUES ('$account_id', 13, 17, $userid,now())";
        $result = mysql_query($query, connect());
    }
    if ($account_status_dept != 1) {
        if ($paid_amount) {
            $update_status_query = "UPDATE accounts SET  account_paid = 1 WHERE account_id = $account_id";
            $result = mysql_query($update_status_query, connect());
        } else {
            $update_status_query = "UPDATE accounts SET status_id = $status_id, account_paid = 1 WHERE account_id = $account_id";
            $result = mysql_query($update_status_query, connect());
        }
    } else {
        $update_status_query = "UPDATE accounts SET account_paid = 1 WHERE account_id = $account_id";
        $result = mysql_query($update_status_query, connect());
    }
    if ($_POST['payment_from'] == 1) {

        header('Location: /88dbphcrm/accounts/edit.php?account_id=' . $account_id);
        //exit;
    } else {
        header('Location: /88dbphcrm/payments/');
        //exit;	
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
            <title>Add Account</title>

            <?php include '../header.php'; ?>

            <div class="main-section">
                <form name="form1" id="form1" action="add.php" method="post">
                    <div class="commands">
                        <div class="head-label">
                            <h2>Add New Payment</h2>
                        </div><!-- end of add new account -->

                        <ul>
                            <?php if (!empty($contract_number)) { ?>
                                <li><a id="cancel_btn" class="link-button gray" >Cancel</a></li>
                            <?php } else { ?>
                                <li><a class="link-button gray" href='/88dbphcrm/payments/'>Cancel</a></li>
                            <?php } ?>
                            <li><input type="submit" value="Submit"/></li>
                        </ul>
                    </div><!-- end of grid-commands -->

                    <div class="gen-section">

                        <table cellpadding="5" cellspacing="0">
                            <tr>
                                <td class="grid-head">*Contract Number:</td>
                                <td>&nbsp;</td>
                                <td>
                                    <?php if (!empty($contract_number)) { ?>

                                        <input type="text" name="contract_number" id="contract_number" class="required"  value="<?php echo $contract_number; ?>" readonly="readonly" />

                                        <?php
                                    } else {
                                        ?>
                                        <input type="text" name="contract_number" id="contract_number" class="required"  />

                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="grid-head">*Account Name:</td>
                                <td>&nbsp;</td>
                                <td><input type="text" name="account_name" id="account_name" class="required" placeholder="No selected Account" autocomplete="off" readonly="readonly" />
                                    <input type="hidden" name="account_id" id="account_id" class="required" autocomplete="off" readonly="readonly" />
                                    <input type="hidden" name="contract_amount" id="contract_amount" readonly="readonly" />
                                    <input type="hidden" name="paid_amount" id="paid_amount" readonly="readonly" />

                                    <input type="hidden" name="payment_from" id="payment_from" 
                                           value="<?php if (!empty($_REQUEST['cid']))
                                        echo '1'; ?> " 

                                           readonly="readonly" />


                                </td>
                            </tr>

                            <tr>
                                <td class="grid-head">*Payment Amount:</td>
                                <td>&nbsp;</td>
                                <td><input type="text" name="payment_amount" maxlength="9" id="payment_amount" class="required"/></td>
                            </tr>
                            <tr>
                                <td class="grid-head">*Date of Payment:</td>
                                <td>&nbsp;</td>
                                <td><input type="text" name="payment_date" maxlength="10" id="payment_date" class="required"/>
                                    <input type="hidden" name="payment_time" id="payment_time" />
                                </td>
                            </tr>



                            <tr class="edit-account-footer">
                                <td colspan="4">

                                </td>
                            </tr>
                        </table>


                    </div><!-- end of gen-section -->
                </form>
            </div><!-- end of main-section -->

            <script type="text/javascript">
                $(document).ready(function(){

                    $("#payment_amount").numeric();
		
                    $("#contract_number").numeric();

                    $("#form1").validate();
		
                    $('#payment_date').datepicker({dateFormat: 'yy-mm-dd'})
                    $("#form1form1").validate();
		
		
                    $("#form1").submit(function() 
                    {

                        var sum = parseInt($("#paid_amount").val()) + parseInt($("#payment_amount").val());
						 
                        if($("#contract_amount").val() != "0")
                        {
						
                            if($("#contract_amount").val() >= sum  ) 
                            {
									 
                                return true;
                            }
                            else
                            {
                                if(($("label.error").length <= 0) || ($("label.error").is(":hidden")))
                                {
                                    alert($("#account_name").val()+" Contract Amount is: P " + $("#contract_amount").val() + "\n\n" + 
                                        "Payment Exceeded."
											
											
                                );
                                }
                                return false;
									 
                            }
									 
									 
                        }
                        else
                        {
                            if(($("label.error").length <= 0) || ($("label.error").is(":hidden")))
                            {
                                alert($("#account_name").val()+" Contract Amount is: P " + $("#contract_amount").val() + "\n\n" + "Kindly verify Account's Contract amount.");
                            }
                            return false;
                        }




                    });
		 
	
		 
		 
                    $('#contract_number').change(function() {
		 
                        $('#contract_number').trigger("keyup");
				
                    });
  
  





                    $("#contract_number").keyup(function() 
                    { 
 
					 

                        if($.trim($("#contract_number").val()) == '')
                        {
					 
                            $("#account_name").val("");
							
                        }
                        else
                        {
					 
                            $.ajax({
                                type: "POST",
                                dataType: "html",
                                url: 'functions/ajax.search.account.php',
                                timeout: 20000,
                                data:{data: $('#contract_number').val()},
                                success: function(html)
                                {
                                    var account = html;
                                    var account_info = account.split("*");
						
							
							
                                    $("#account_name").val(account_info[0]);	
                                    $("#account_id").val(account_info[1]);	
                                    $("#contract_amount").val(account_info[2]);	
                                    $("#paid_amount").val(account_info[3]);
                                },
							
                            });
					  
                        }
                    });
  
  
                });
                function GetCurrentTime()
                {
	
                    var currentTime = new Date()
                    var hours = currentTime.getHours()
                    var minutes = currentTime.getMinutes()
                    var seconds = currentTime.getSeconds()
                    if (minutes < 10){
                        minutes = "0" + minutes
                    }
                    if (seconds < 10){
                        seconds = "0" + seconds
                    }
		
                    $("#payment_time").val(hours + ":" + minutes + ":" + seconds);
	
                }
                $(function() {
	


                    setInterval('GetCurrentTime()', 100 );
                    if($.trim($("#contract_number").val()) == '')
                    {
					 
                        $("#account_name").val("");
							
                    }
                    else
                    {
					 
                        $.ajax({
                            type: "POST",
                            dataType: "html",
                            url: 'functions/ajax.search.account.php',
                            timeout: 20000,
                            data:{data: $('#contract_number').val()},
                            success: function(html)
                            {
                                var account = html;
                                var account_info = account.split("*");
							
							
                                if($('#cancel_btn').length) {
                                    $("#cancel_btn").attr('href', '/88dbphcrm/accounts/edit.php?account_id='+ account_info[1]);
                                }
							
						
							
                                $("#account_name").val(account_info[0]);	
                                $("#account_id").val(account_info[1]);	
                                $("#contract_amount").val(account_info[2]);	
                                $("#paid_amount").val(account_info[3]);
                            },
							
                        });
					  
                    }
	
	
                });
            </script>
            </body>
            </html>