<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');
session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$payment_id = $_REQUEST['payment_id'];
mysql_connect('localhost','root','');
@mysql_select_db('88dbphcrm') or die( "Unable to select database");

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {

	
    $account_name =  $_POST['account_name'];
	$account_id =  $_POST['account_id'];
	$payment_amount =  $_POST['payment_amount'];
    $old_amount = $_POST['old_amount'];
	$payment_date = $_POST['payment_date'];
	$first_payment_date = $_POST['first_payment_date'];
	$payment_time = $_POST['payment_time'];
	
   if($old_amount != $payment_amount)
   {
   $remark = "Edited payment amount from ".number_format($old_amount, 2, '.', ',')." to ".number_format($payment_amount, 2, '.', ',')."";
	$insert_query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$remark','1', $account_id, $userid)";    
    $remark_result = mysql_query($insert_query,connect());
   }
   
	if($payment_date != $first_payment_date)
	{
		$newdate = $payment_date." ".$payment_time;
		 $query = "UPDATE payments SET payment_createdon ='".$newdate."', payment_amount = '".mysql_real_escape_string($payment_amount)."',  payment_modifiedby = $userid,  payment_modifiedon = now() WHERE payment_id = $payment_id";
	
	}
	else
	{
		 $query = "UPDATE payments SET payment_amount = '".mysql_real_escape_string($payment_amount)."',  payment_modifiedby = $userid,  payment_modifiedon = now() WHERE payment_id = $payment_id";
		
	}
   
  
   $result = mysql_query($query,connect());
   
   
   
   
	
} 
else 
{
    $payment_id = $_REQUEST['payment_id'];
    
}

$query = "SELECT p.*, concat(u1.user_firstname,' ',u1.user_lastname) AS createdby, concat(u2.user_firstname,' ',u2.user_lastname) AS modifiedby, a.account_id,a.account_name,a.account_contractnumber,a.account_contractamount , SUM(p2.payment_amount) as paid_amount FROM payments p LEFT JOIN users u1 ON p.payment_createdby = u1.user_id LEFT JOIN users u2 ON p.payment_modifiedby = u2.user_id LEFT JOIN accounts a ON p.account_id = a.account_id LEFT JOIN payments p2 ON p.account_id = p2.account_id WHERE p.payment_id = $payment_id";


$result = mysql_query($query,connect());
$num = mysql_numrows($result);
if ($num > 0) {
	$account_contractnumber = mysql_result($result, 0, 'account_contractnumber');
    $account_name = mysql_result($result, 0, 'account_name');
	$account_id = mysql_result($result,0 , 'account_id');
	$payment_amount = mysql_result($result,0 , 'payment_amount');
    $createdby = mysql_result($result, 0, 'createdby');
    $createdon = mysql_result($result, 0, 'payment_createdon');
    $modifiedby = mysql_result($result, 0, 'modifiedby');
	$paid_amount = (mysql_result($result, 0, 'paid_amount')) - $payment_amount;
	$contract_amount = mysql_result($result, 0, 'account_contractamount');
	$payment_date = mysql_result($result, 0, 'payment_createdon');
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>Edit Payment</title>

    <?php include '../header.php'; ?>

    <div class="main-section">
        
        <form name="form1" id="form1" action="edit.php?payment_id=<?php echo $_GET['payment_id'];?>" method="post" >

            <div class="commands">
                
                <div class="head-label">
                    <h2>Edit Payment</h2>
                </div><!-- end of add new account -->
                
                <ul>
                    <li><a class="link-button gray" href='/88dbphcrm/payments/'>Cancel</a></li>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

               
                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Contract Number:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="contract_number" id="contract_number" value="<?php echo $account_contractnumber; ?>"class="required" readonly="readonly" />
						
						</td>
                    </tr>
					 <tr>
                        <td class="grid-head">*Account Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_name" id="account_name" class="required" value="<?php echo $account_name; ?>" readonly="readonly" />
						<input type="hidden" name="account_id" id="account_id" class="required" value="<?php echo $account_id; ?>" readonly="readonly" />
						<input type="hidden" name="contract_amount" id="contract_amount" value="<?php echo $contract_amount; ?>" readonly="readonly" />
						<input type="hidden" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" readonly="readonly" />
						<input type="hidden" name="old_amount" id="old_amount" value="<?php echo $payment_amount; ?>" readonly="readonly" />
						</td>
                    </tr>

					<tr>
                        <td class="grid-head">*Payment Amount:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="payment_amount" maxlength="9" id="payment_amount" value="<?php echo $payment_amount; ?>" class="required"/></td>
                    </tr>
					<tr>
                        <td class="grid-head">*Date of Payment:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="payment_date" maxlength="10" id="payment_date" class="required" value="<?php echo DATE('Y-m-d', strtotime($payment_date)); ?>" />
						<input type="hidden" name="payment_time" id="payment_time" value="<?php echo DATE('H:i:s', strtotime($payment_date)); ?>"/>
						
						<input type="hidden" name="first_payment_date" maxlength="10" id="first_payment_date" class="required" value="<?php echo DATE('Y-m-d', strtotime($payment_date)); ?>" />
						</td>
                    </tr>
                                   
                               
                                   
                    <tr class="edit-account-footer">
                        <td colspan="4">
                            
                        </td>
                    </tr>
            
              
                    <tr class="edit-account-footer">
                        <td colspan="4">
                            <?php echo "Created by: ".$createdby." On: " . $createdon . " | Modified by: ".$modifiedby; ?>
                        </td>
                    </tr>
                </table>

          

            </div><!-- gen-section --> 
        </form>
        
    </div><!-- main-section --> 

	
	<div id="colobox_form" style="display:none;">
		
	
	
	</div>
	
    <script type="text/javascript">
	
	
	
	
	
    $(document).ready(function() {
	

	
		

	
   
		$('#payment_date').datepicker({dateFormat: 'yy-mm-dd'})

		$("#form1").validate();

        $("#payment_amount").numeric();       



$("#form1").submit(function() 
						{

						 var sum = parseInt($("#paid_amount").val()) + parseInt($("#payment_amount").val());
						 
						if($("#contract_amount").val() != "0")
						{
									 if($("#contract_amount").val() >= sum  ) 
									 {
									 
									return true;
									 }
									 else
									 {
									 if(($("label.error").length <= 0) || ($("label.error").is(":hidden")))
										{
										alert($("#account_name").val()+" Contract Amount is: P " + $("#contract_amount").val() + "\n\n" + 
											"Payment Exceeded.");
												
										}
											
									
											return false;
									 
									 }
									 
									 
						}
						else
						{
						if(($("label.error").length <= 0) || ($("label.error").is(":hidden")))
						{
						alert($("#account_name").val()+" Contract Amount is: P " + $("#contract_amount").val() + "\n\n" + "Kindly verify Account's Contract amount.");
						}
											return false;
						}
						 	
						});



						
    });

    </script>
   
</body>
</html>