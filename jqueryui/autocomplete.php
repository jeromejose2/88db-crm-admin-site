<?php
session_start();

require_once("../models/tbuser.php");
$tbuser = new TB_USER();

if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$users = $tbuser->Selectalluser();
$results = array();
foreach ($users as $val) {
    $results[] = $val;
}
echo json_encode($results);
?>


<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>jQuery UI Autocomplete - Default functionality</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
        <style>
            body {
                font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
                font-size: 62.5%;
            }
        </style>

        <script>
            $(function() {
                var availableTags = [
                    "ActionScript1",
                    "ActionScript2",
                    "ActionScript",
                    "AppleScript",
                    "Asp",
                    "BASIC",
                    "C",
                    "C++",
                    "Clojure",
                    "COBOL",
                    "ColdFusion",
                    "Erlang",
                    "Fortran",
                    "Groovy",
                    "Haskell",
                    "Java",
                    "JavaScript",
                    "Lisp",
                    "Perl",
                    "PHP",
                    "Python",
                    "Ruby",
                    "Scala",
                    "Scheme"
                ];
                var availableTagss = <?php echo json_encode($results); ?> ;
                $( "#tags" ).autocomplete({ 
                    minLength: 0,
                    source: availableTagss
                });
            });
        </script>
    </head>
    <body>

        <div class="ui-widget">
            <label for="tags">Name : </label>
            <input id="tags" />
        </div>


    </body>
</html>
