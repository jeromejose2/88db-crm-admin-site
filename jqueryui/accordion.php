<?php
session_start();

require_once("../models/tbuser.php");
$tbuser = new TB_USER();

$dept = $tbuser->Selectdepartment();
//require_once ("../ctrl/ctrl_user.php");
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<style>
    body {
        font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
        font-size: 62.5%;
    }
</style>
<script>
    $(function() {
        $( "#accordion" ).accordion();
    });
</script>

<body>

    <div id="accordion">
        <?php if ($dept)
            foreach ($dept as $deptval): ?>
                <h3><?php echo $deptval[department_name] ?></h3>
                <div>
                    <p>
                    <table class="main-grid">


                        <?php
                        $users = $tbuser->Selectdepartmentusers($deptval[department_id]);
                        if ($users):
                            ?>
                            <tr class="grid-head">
                                <td>ID</td>
                                <td>Name</td>
                                <td>Email</td>
                            </tr>
                            <?php foreach ($users as $usersval): ?>
                                <tr class='grid-content'>

                                    <td><?php echo $usersval[user_id] ?></td>
                                    <td>
                                        <a href="/88dbphcrm/users/edit.php?user_id=<?php echo $usersval[user_id] ?>" >
                                            <?php echo "$usersval[user_firstname]" . " " . "$usersval[user_lastname]" ?>
                                        </a>

                                    </td>
                                    <td><?php echo $usersval[user_email] ?></td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                    </p>
                </div>
            <?php endforeach; ?>
    </div>


</body>












