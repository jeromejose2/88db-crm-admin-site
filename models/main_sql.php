<?php
    include $extra.'../sql/db_connect.php';
	include $extra.'../sql/func_Database.php';	

	
class TB_SQL 
{
  var $db;
  function TB_SQL ()
  {  	
  	global $gdb; 
  	if(!$gdb)   
  	  $this->db=connect_DB();
  	$this->db=$gdb;  
  }
  function Insert($data,$table)
  {  	
  	$check = Add2Database($table, $data); 
  	if($check)
  	 $ret=db_insert_id();
  	else 
  	 $ret=0;

  	return $ret; 
  } 
  function Update($data,$table,$params='')
  {
  	$ret= Edit2Database ($table, $data, $params);
  	
  	return $ret;
  } 
  function RunQuery($sql)
  {  	
  	return database_Query($sql);
  }
  function GetNewSeq($table)
  {  	  	
    $myRecs = database_Query("SELECT max(seq) nseq FROM ".$table);
    
	$Row = record_Fetch($myRecs); 
	
	if($Row['nseq'])
	  $ret=$Row['nseq']+1;
	else 
	  $ret=1;
	    
  	return $ret; 
  }
    
  function GetRows($query)
  { 	    	
  	
    $myRecs = database_Query($query);    
	
	while ($Row = record_Fetch($myRecs))
	{
		$arr[]=$Row;
	}
	//echo '<pre>';
    //echo print_r($arr);
	//echo '</pre>';
  	return $arr; 
  }
    
  function GetRow($query)
  {  	  	
    $myRecs = database_Query($query);        
  	return record_Fetch($myRecs); 
  }
    
  function Delete($table,$params)
  {  	  	
    $ret = Delete2Database ($table, $params);  
    return $ret;      
  }
    
  function ReconstructSeq($table,$params,$pk)
  {  	  	
  	$where=trim($params)?' where '.$params:'';
  	$rows=$this->GetRows('select * from '.$table.$where.' order by seq');
  	$c=0;
  	foreach ($rows as $val)
  	{ 		
  	  $c++;
  	  $data=array('seq'=>$c);  	
      $this->Update($data,$table,$pk.'='.$val[$pk]);          		
  	}
  	return $c;
  }
  
  function CloseDB ()
  {  	    
  	close_DB($this->db);
  }
}




?>
