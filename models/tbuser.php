<?php

require_once($extra . 'main_sql.php');

class TB_USER extends TB_SQL {

    function EmailNewPassword($data) {
        
    }

    function TB_USER() {
        $this->TB_SQL();
    }

    function Insertusers($data) {
        $id = $this->Insert($data, 'users');
        return $id;
    }

    function Updateusers($data, $params) {
        $ret = $this->Update($data, 'users', $params);
        return $ret;
    }

    function Deleteusers($id) {
        $ret = $this->Delete('users', 'user_id=' . $id);

        return $ret;
    }

    function Selectusermodified($id) {
        $sql = " Select concat(b.user_firstname, ' ', b.user_lastname) as user_completename ,c.role_name as role, d.department_name as dept_name";
        $sql .= " FROM users a, users b , roles c , departments d";
        $sql .= " Where b.user_id = a.user_modifiedby";
        $sql .= " and b.role_id = c.role_id";
        $sql .= " and b.department_id = d.department_id";
        $sql .= " and a.user_id = $id";
        //$sql .= " and password = '$pass'";
//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectuser($id) {
        $sql = " Select *";
        $sql .= " FROM users ";
        $sql .= " Where user_id = $id";
        //$sql .= " and password = '$pass'";
        return $this->GetRow($sql);
    }

    function Selectalluser() {
        $sql = " Select concat(user_firstname, ' ', user_lastname) as user_completename";
        $sql .= " FROM users ";
        $sql .= " Where enabled = 1";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function Selectusers() {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ";
        $sql .= " FROM users u  LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " WHERE u.enabled = 1";
        $sql .= " ORDER BY u.log  DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function Selectusersnot() {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ";
        $sql .= " FROM users u  LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " WHERE u.enabled = 0";
        $sql .= " ORDER BY user_completename , u.user_createdon  DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

//    function Selectusers() {
//        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ,t.team_name";
//        $sql .= " FROM users u ,roles r ,teams t";
//        $sql .= " WHERE  u.role_id = r.role_id";
//        $sql .= " AND  u.team_id = t.team_id";
//        $sql .= " ORDER BY u.user_createdon DESC";
//        //$sql .= " and password = '$pass'";
//        return $this->GetRows($sql);
//    }

    function Selectusers2($parse, $userid) {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ,t.team_name";
//        $sql .= " FROM users u LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " FROM users u ,roles r ,teams t";
        $sql .= " WHERE  u.role_id = r.role_id";
        $sql .= " AND  u.team_id = t.team_id";
        $sql .= " AND u.department_id = $parse ";
        $sql .= " AND u.manager_id = $userid ";
        $sql .= " AND u.enabled = 1";
        $sql .= " ORDER BY u.user_createdon DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function SelectusersTL($parse, $teamid) {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ,t.team_name";
//        $sql .= " FROM users u LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " FROM users u ,roles r ,teams t";
        $sql .= " WHERE  u.role_id = r.role_id";
        $sql .= " AND  u.team_id = t.team_id";
        $sql .= " AND u.department_id = $parse ";
        $sql .= " AND u.team_id = $teamid ";
        $sql .= " AND u.enabled = 1";
        $sql .= " ORDER BY u.user_createdon DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function Selectusers2not($parse, $userid) {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ,t.team_name";
//        $sql .= " FROM users u LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " FROM users u ,roles r ,teams t";
        $sql .= " WHERE  u.role_id = r.role_id";
        $sql .= " AND  u.team_id = t.team_id";
        $sql .= " AND u.department_id = $parse ";
        $sql .= " AND u.manager_id = $userid ";
        $sql .= " AND u.enabled = 0";
        $sql .= " ORDER BY u.user_createdon DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function Selectusers3($parse) {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ";
        $sql .= " FROM users u LEFT JOIN roles r ON u.role_id = r.role_id";
        $sql .= " WHERE a.account_createdby = $parse ";
        $sql .= " ORDER BY u.user_createdon DESC";
        //$sql .= " and password = '$pass'";
        return $this->GetRows($sql);
    }

    function Selectuserlog($id) {
        $sql = ' select * ';
        $sql .= ' from audit_log';
        $sql .= " where user_id = $id";
        $sql .= " order by audit_log_time desc";

        return $this->GetRows($sql);
    }

    function Selectdepartment() {
        $sql = " Select *";
        $sql .= " FROM departments ";
//        $sql .= " Where departments";

        return $this->GetRows($sql);
    }

    function Selectdepartmentusers($id) {
        $sql = " SELECT u.*, concat(user_firstname, ' ', user_lastname) as user_completename ";
        $sql .= " FROM users u LEFT JOIN roles r ON u.role_id = r.role_id";

        $sql .= " WHERE u.department_id  = $id ";
        $sql .= " AND u.enabled = 1";
        $sql .= " ORDER BY u.user_createdon DESC";
//        echo $sql;
        return $this->GetRows($sql);
    }

}

?>