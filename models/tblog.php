<?php

require_once($extra . 'main_sql.php');

class TB_LOG extends TB_SQL {

    function TB_LOG() {
        $this->TB_SQL();
    }

    function Insertaudit_log($data) {
        $id = $this->Insert($data, 'audit_log');
        return $id;
    }

    function Insertaudit_account_log($data) {
        $id = $this->Insert($data, 'audit_account_log');
        return $id;
    }

    function Updateaudit_log($data, $params) {
        $ret = $this->Update($data, 'audit_log', $params);
        return $ret;
    }

    function Deleteaudit_log($id) {
        $ret = $this->Delete('audit_log', 'audit_log_id=' . $id);

        return $ret;
    }

}

?>