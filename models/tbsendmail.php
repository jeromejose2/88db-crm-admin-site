<?php

require_once($extra . 'main_sql.php');

class TB_SENDMAIL extends TB_SQL {

    function Insertsalesreport($data) {
        $id = $this->Insert($data, 'salesreport');
        return $id;
    }

    function Insertcsrreport($data) {
        $id = $this->Insert($data, 'csrreport');
        return $id;
    }

    function Insertstatusreport($data) {
        $id = $this->Insert($data, 'statushistorycount');
        return $id;
    }

    function Insertfinancereport($data) {
        $id = $this->Insert($data, 'financereport');
        return $id;
    }

    function Insertdesignerreport($data) {
        $id = $this->Insert($data, 'designerreports');
        return $id;
    }

    function Insertdesignerreport2($data) {
        $id = $this->Insert($data, 'designerreportsv2');
        return $id;
    }

    function Inserteditorreport($data) {
        $id = $this->Insert($data, 'editorsreports');
        return $id;
    }

    function Selectsignedforpayment() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 13";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
//        $sql .= " AND a.account_createdby = c.user_id";
//        $sql .= " AND a.shop_csr = d.user_id";
//        $sql .= " AND a.account_id = b.account_id";
//        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 24.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";
// echo $sql;
        return $this->GetRows($sql);
    }

    function Selectclientapp() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,d.user_email as csr_email,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
//        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date";
//        $sql .= " FROM accounts a, audit b";
        $sql .= " WHERE a.status_id = 6";
//        $sql .= " AND a.account_paid = 1";
//        $sql .= " AND a.account_id = b.account_id";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 48.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectclientco() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 25";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 48.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectclientfeed() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 26";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 48.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectclientupdates() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 27";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectforproof() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 2";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 24.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectlined() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 1";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 48.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selecttemporary() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 35";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";


        return $this->GetRows($sql);
    }

    function Selectunder() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 3";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 24.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";
        return $this->GetRows($sql);
    }

    function Selectdesignqa() {
        $sql .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date , CONCAT( c.user_firstname,' ',c.user_lastname ) AS AE , CONCAT( d.user_firstname,' ',d.user_lastname ) AS CSR,c.user_id as ae_id ,d.user_id as csr_id,stat.status_name";
        $sql .= " FROM accounts a ";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users d ON  a.shop_csr = d.user_id";
        $sql .= " LEFT JOIN audit b ON a.account_id = b.account_id";
        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
        $sql .= " WHERE a.status_id = 5";
//        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.status_id = b.after_status_id";
        $sql .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 24.000";
        $sql .= "  GROUP BY a.account_name";
        $sql .= "  ORDER BY a.account_name ASC";
        return $this->GetRows($sql);
    }

    function Selectdailysalesreport() {
        $sql = " SELECT r.user_id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS AE,  a.calls, au.Meetings, aud.Closed";
        $sql .= " FROM  users r";
        $sql .=" LEFT JOIN(Select count(account_id) as calls,account_createdby  from accounts WHERE DATE(account_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by account_createdby ) a ON a.account_createdby = r.user_id";
//        $sql .= " LEFT JOIN users r ON a.account_createdby = r.user_id";
        $sql .= " LEFT JOIN(select count(audit_id) as Meetings ,audit_createdby from audit where after_status_id = 10 and DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby) au ON r.user_id = au.audit_createdby ";
        $sql .= " LEFT JOIN(select count(audit_id) as Closed,audit_createdby from audit where after_status_id = 13 and DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby) aud ON r.user_id = aud.audit_createdby ";
        $sql .= " WHERE r.department_id = 2 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 8";
//        $sql .= " LEFT JOIN statuses stat ON a.status_id = stat.status_id";
//        $sql .= " WHERE DATE(a.account_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "')";
        $sql .= " GROUP BY r.user_id ";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function Selectweeklysalesreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS AE,  a.calls, au.Meetings, aud.Closed";
        $sql .= " FROM  users r";
        $sql .=" LEFT JOIN(Select count(account_id) as calls,account_createdby  from accounts WHERE DATE(account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by account_createdby ) a ON a.account_createdby = r.user_id";
//        $sql .= " LEFT JOIN users r ON a.account_createdby = r.user_id";
        $sql .= " LEFT JOIN(select count(audit_id) as Meetings ,audit_createdby from audit where after_status_id = 10 and DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "')  group by audit_createdby) au ON r.user_id= au.audit_createdby ";
        $sql .= " LEFT JOIN(select count(audit_id) as Closed,audit_createdby from audit where after_status_id = 13 and DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby) aud ON r.user_id = aud.audit_createdby ";
        $sql .= " WHERE r.department_id = 2 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 8";
        $sql .= " GROUP BY r.user_id ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectmonthlysalesreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS AE,  a.calls, au.Meetings, aud.Closed";
        $sql .= " FROM  users r";
        $sql .=" LEFT JOIN(Select count(account_id) as calls,account_createdby  from accounts WHERE DATE(account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by account_createdby ) a ON a.account_createdby = r.user_id";
//
//        $sql .= " LEFT JOIN users r ON a.account_createdby = r.user_id";
        $sql .= " LEFT JOIN(select count(audit_id) as Meetings ,audit_createdby from audit where after_status_id = 10 and DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby) au ON r.user_id = au.audit_createdby ";
        $sql .= " LEFT JOIN(select count(audit_id) as Closed,audit_createdby from audit where after_status_id = 13 and DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby) aud ON r.user_id = aud.audit_createdby ";
        $sql .= " WHERE r.department_id = 2 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 8";
        $sql .= " GROUP BY r.user_id ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectdailycsrreport() {
        $sql = " SELECT r.user_id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS CSR, au.stagedtoprod, aua.approve";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 2 AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon ) =  DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE (r.department_id = 1 OR r.department_id = 4) AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 6";
        $sql .= " GROUP BY r.user_id ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectweeklycsrreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS CSR, au.stagedtoprod, aua.approve,act.notstage ,actnot.notapprove";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT(audit_id) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 2 AND DATE(audit_createdon)BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT(audit_id) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon )  BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " LEFT JOIN(SELECT count(a.account_id) as notstage ,a.shop_csr as csr from accounts a,audit b where a.account_id = b.account_id and a.status_id = 1 and ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000 GROUP BY a.account_id) act ON r.user_id = act.csr";
        $sql .= " LEFT JOIN(SELECT count(a.account_id) as notapprove ,a.shop_csr as csr from accounts a,audit b where a.account_id = b.account_id and (a.status_id = 6  OR a.status_id =26 OR a.status_id =38) and ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000 GROUP BY a.account_id) actnot ON r.user_id = actnot.csr";
        $sql .= " WHERE (r.department_id = 1 OR r.department_id = 4) AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 6";
        $sql .= " GROUP BY r.user_id ";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function Selectmonthlycsrreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS CSR, au.stagedtoprod, aua.approve,act.notstage ,actnot.notapprove";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT(audit_id) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 2 AND DATE(audit_createdon)BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT(audit_id) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon )  BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " LEFT JOIN(SELECT count(a.account_id) as notstage ,a.shop_csr as csr from accounts a,audit b where a.account_id = b.account_id and a.status_id = 1 and ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000 GROUP BY a.account_id) act ON r.user_id = act.csr";
        $sql .= " LEFT JOIN(SELECT count(a.account_id) as notapprove ,a.shop_csr as csr from accounts a,audit b where a.account_id = b.account_id and (a.status_id = 6  OR a.status_id =26 OR a.status_id =38) and ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 168.000 GROUP BY a.account_id) actnot ON r.user_id = actnot.csr";
        $sql .= " WHERE (r.department_id = 1 OR r.department_id = 4) AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 6";
        $sql .= " GROUP BY r.user_id ";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function Selectstatusallcount() {
        $sql = " SELECT a.status_id as id,a.status_name AS status_name,act.count";
        $sql .= " FROM statuses a";
        $sql .= " LEFT JOIN (SELECT COUNT( account_id ) AS count, status_id FROM accounts GROUP BY status_id)act ON act.status_id = a.status_id";
        $sql .= " ORDER BY  act.count DESC ";
        return $this->GetRows($sql);
    }

    function Selectdailydesignerreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 4 AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
//        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon ) =  DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 2";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function SelectDailyDesignerReportv2New() {
        $sql = " 
            SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer , if(aa.NewShop IS NULL ,'0' ,aa.NewShop) as NewShop , if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop ) as OldShop ,(if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop) + if(aa.NewShop IS NULL ,'0',aa.NewShop) ) as Total
                ";
        $sql .= " FROM users r
            LEFT JOIN
            (
            SELECT  count(c.account_id) as NewShop , c.audit_createdby  FROM (

            SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

            SELECT account_id 
            FROM audit 
            WHERE DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') 
            AND before_status_id = 21 
            AND after_status_id = 3 
            GROUP BY audit_createdby  

            )
            ";
        $sql .= "
                a LEFT JOIN (

                SELECT account_id ,audit_createdby 
                FROM audit 
                WHERE before_status_id = 3 
                AND after_status_id = 4 
                GROUP BY account_id

                ) b ON a.account_id = b.account_id

                ) c WHERE c.new_shop IS NOT NULL
                GROUP by c.account_id
                )aa ON r.user_id = aa.audit_createdby  


                LEFT JOIN (

                SELECT count(c.account_id) as OldShop , c.audit_createdby  FROM (

                SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

                SELECT account_id 
                FROM audit 
                WHERE DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') 
                AND before_status_id IN (8,27,30,31,37,41,42,43,44)
                AND after_status_id = 3 
                GROUP BY account_id

                )             
";
        $sql .= " 
                        a LEFT JOIN (

            SELECT account_id ,audit_createdby 
            FROM audit 
            WHERE before_status_id = 3 
            AND after_status_id = 4 
            GROUP BY account_id

            ) b ON a.account_id = b.account_id

            ) c WHERE c.new_shop IS NOT NULL
            GROUP by c.audit_createdby  
            ) aaa ON r.user_id = aaa.audit_createdby 

            WHERE r.user_type <> 2 AND r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
";
        $sql .= " ORDER BY Total  desc ";

        //echo $sql;
        return $this->GetRows($sql);
    }

    function SelectDailyDesignerReportv2() {
        $sql = " SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod as OldShop ,(aua.stagedtoprod - au.stagedtoprod) as NewShop , aua.stagedtoprod as TotalShop FROM users r LEFT JOIN (";
        $sql .= " SELECT COUNT( a.audit_id) AS stagedtoprod, b.audit_createdby FROM (";
        $sql .= " SELECT a.audit_id,b.account_id, b.account_name, a.audit_createdon FROM audit a";
        $sql .= " INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.before_status_id";
        $sql .= " IN ( 7, 8, 27,28, 29, 30, 31, 32, 33, 34,37, 41, 42 ,43,44 )  ";
        $sql .= " GROUP BY a.account_id )a INNER JOIN (";
        $sql .= " SELECT account_id, audit_createdby FROM audit WHERE before_status_id =3 AND after_status_id =4 AND DATE( audit_createdon ) = DATE('" . date('Y-m-d', strtotime('today')) . "') GROUP BY account_id )b ON a.account_id = b.account_id";
        $sql .= " GROUP BY b.audit_createdby )au ON r.user_id = au.audit_createdby 
            LEFT JOIN(SELECT COUNT( distinct (account_id) ) AS stagedtoprod, audit_createdby  from audit WHERE before_status_id = 3 AND after_status_id = 4 AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby
            WHERE r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
            ";
        $sql .= " ORDER BY au.stagedtoprod DESC ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectweeklydesignerreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 4 AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby)au ON r.user_id = au.audit_createdby";
//        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon ) =  DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 2";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function SelectWeeklyDesignerReportv2() {
        $sql = " SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod as OldShop ,(aua.stagedtoprod - au.stagedtoprod) as NewShop , aua.stagedtoprod as TotalShop FROM users r LEFT JOIN (";
        $sql .= " SELECT COUNT( a.audit_id) AS stagedtoprod, b.audit_createdby FROM (";
        $sql .= " SELECT a.audit_id,b.account_id, b.account_name, a.audit_createdon FROM audit a";
        $sql .= " INNER JOIN accounts b ON a.account_id = b.account_id WHERE before_status_id";
        $sql .= " IN ( 7, 8, 27,28, 29, 30, 31, 32, 33, 34,37, 41, 42 ,43,44)  ";
        $sql .= " GROUP BY a.account_id )a INNER JOIN (";
        $sql .= " SELECT account_id, audit_createdby FROM audit WHERE before_status_id =3 AND after_status_id =4 AND DATE( audit_createdon ) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this  week')) . "') GROUP BY account_id )b ON a.account_id = b.account_id";
        $sql .= " GROUP BY b.audit_createdby )au ON r.user_id = au.audit_createdby 
            LEFT JOIN(SELECT COUNT( distinct (account_id) ) AS stagedtoprod, audit_createdby  from audit WHERE before_status_id = 3 AND after_status_id = 4 AND DATE( audit_createdon ) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby
            WHERE r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
            ";
        $sql .= " ORDER BY au.stagedtoprod DESC ";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function SelectWeeklyDesignerReportv2New() {
        $sql = " 
            SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer , if(aa.NewShop IS NULL ,'0' ,aa.NewShop) as NewShop , if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop ) as OldShop ,(if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop) + if(aa.NewShop IS NULL ,'0',aa.NewShop) ) as Total
                ";
        $sql .= " FROM users r
            LEFT JOIN
            (
            SELECT  count(c.account_id) as NewShop , c.audit_createdby  FROM (

            SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

            SELECT account_id 
            FROM audit 
            WHERE DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this  week')) . "')
            AND before_status_id = 21 
            AND after_status_id = 3 
            GROUP BY audit_createdby  
            )
            ";
        $sql .= "
                a LEFT JOIN (

                SELECT account_id ,audit_createdby 
                FROM audit 
                WHERE before_status_id = 3 
                AND after_status_id = 4 
                GROUP BY account_id

                ) b ON a.account_id = b.account_id

                ) c WHERE c.new_shop IS NOT NULL
                GROUP by c.account_id
                )aa ON r.user_id = aa.audit_createdby  


                LEFT JOIN (

                SELECT count(c.account_id) as OldShop , c.audit_createdby  FROM (

                SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

                SELECT account_id 
                FROM audit 
                WHERE DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this  week')) . "')
                AND before_status_id <> 21 
                AND after_status_id = 3 
                GROUP BY account_id

                )             
";
        $sql .= " 
                        a LEFT JOIN (

            SELECT account_id ,audit_createdby 
            FROM audit 
            WHERE before_status_id = 3 
            AND after_status_id = 4 
            GROUP BY account_id

            ) b ON a.account_id = b.account_id

            ) c WHERE c.new_shop IS NOT NULL
            GROUP by c.audit_createdby  
            ) aaa ON r.user_id = aaa.audit_createdby 

            WHERE r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
";
        $sql .= " ORDER BY Total  desc ";



        //echo $sql;
        return $this->GetRows($sql);
    }

    function Prod_DesignerWeeklyReport() {
        $sql = "
                SELECT 
                designer , 
                sum(Newshop) as NewShop , 
                sum(Oldshop) as Oldshop , 
                sum(TotalShop) as TotalShop
                FROM `designerreports`
                WHERE 
                DATE(data_createdon) 
                BETWEEN
                DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this  week')) . "')
                GROUP BY designer
                ORDER BY TotalShop Desc
               ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Prod_DesignerMonthlyReport() {
        $sql = "
                SELECT 
                designer , 
                sum(Newshop) as NewShop , 
                sum(Oldshop) as Oldshop , 
                sum(TotalShop) as TotalShop
                FROM `designerreports`
                WHERE
                DATE(data_createdon) 
                BETWEEN
                DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')
                GROUP BY designer
                ORDER BY TotalShop Desc
               ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectmonthlydesignerreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 4 AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby)au ON r.user_id = au.audit_createdby";
//        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS approve,  audit_createdby  from audit WHERE after_status_id = 9  AND (before_status_id =6 OR before_status_id =26 OR before_status_id =38 )AND DATE( audit_createdon ) =  DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 2";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        // echo $sql;
        return $this->GetRows($sql);
    }

    function SelectMonthlyDesignerReportv2() {
        $sql = " SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer, au.stagedtoprod as OldShop ,(aua.stagedtoprod - au.stagedtoprod) as NewShop , aua.stagedtoprod as TotalShop FROM users r LEFT JOIN (";
        $sql .= " SELECT COUNT( a.audit_id) AS stagedtoprod, b.audit_createdby FROM (";
        $sql .= " SELECT a.audit_id,b.account_id, b.account_name, a.audit_createdon FROM audit a";
        $sql .= " INNER JOIN accounts b ON a.account_id = b.account_id WHERE before_status_id";
        $sql .= " IN (7, 8, 27,28, 29, 30, 31, 32, 33, 34,37, 41, 42 ,43,44 )  ";
        $sql .= " GROUP BY a.account_id )a INNER JOIN (";
        $sql .= " SELECT account_id, audit_createdby FROM audit WHERE before_status_id =3 AND after_status_id =4 AND DATE( audit_createdon ) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') GROUP BY account_id )b ON a.account_id = b.account_id";
        $sql .= " GROUP BY b.audit_createdby )au ON r.user_id = au.audit_createdby 
            LEFT JOIN(SELECT COUNT( distinct (account_id) ) AS stagedtoprod, audit_createdby  from audit WHERE before_status_id = 3 AND after_status_id = 4 AND DATE( audit_createdon ) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby
            WHERE r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
            ";
        $sql .= " ORDER BY au.stagedtoprod DESC ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function SelectMonthlyDesignerReportv2New() {
        $sql = " 
            SELECT  r.user_id as id , CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Designer , if(aa.NewShop IS NULL ,'0' ,aa.NewShop) as NewShop , if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop ) as OldShop ,(if(aaa.OldShop IS NULL ,'0' ,aaa.OldShop) + if(aa.NewShop IS NULL ,'0',aa.NewShop) ) as Total
                ";
        $sql .= " FROM users r
            LEFT JOIN
            (
            SELECT  count(c.account_id) as NewShop , c.audit_createdby  FROM (

            SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

            SELECT account_id 
            FROM audit 
            WHERE DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')
            AND before_status_id = 21 
            AND after_status_id = 3 
            GROUP BY audit_createdby  

            )
            ";
        $sql .= "
                a LEFT JOIN (

                SELECT account_id ,audit_createdby 
                FROM audit 
                WHERE before_status_id = 3 
                AND after_status_id = 4 
                GROUP BY account_id

                ) b ON a.account_id = b.account_id

                ) c WHERE c.new_shop IS NOT NULL
                GROUP by c.account_id
                )aa ON r.user_id = aa.audit_createdby  


                LEFT JOIN (

                SELECT count(c.account_id) as OldShop , c.audit_createdby  FROM (

                SELECT a.account_id, b.account_id AS new_shop ,b.audit_createdby FROM (

                SELECT account_id 
                FROM audit 
                WHERE DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')
                AND before_status_id <> 21 
                AND after_status_id = 3 
                GROUP BY account_id

                )             
";
        $sql .= " 
                        a LEFT JOIN (

            SELECT account_id ,audit_createdby 
            FROM audit 
            WHERE before_status_id = 3 
            AND after_status_id = 4 
            GROUP BY account_id

            ) b ON a.account_id = b.account_id

            ) c WHERE c.new_shop IS NOT NULL
            GROUP by c.audit_createdby  
            ) aaa ON r.user_id = aaa.audit_createdby 

            WHERE r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 GROUP BY r.user_id
";
        $sql .= " ORDER BY Total  desc ";

        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectdailyeditorreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Editor, au.stagedtoprod , aua.stagedtoprod2";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 21 AND before_status_id = 2 AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod2,  audit_createdby  from audit WHERE before_status_id = 4  AND (after_status_id = 5 OR after_status_id = 7 OR after_status_id = 28 OR after_status_id = 29  )AND DATE( audit_createdon ) =  DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 3";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectweeklyeditorreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Editor, au.stagedtoprod , aua.stagedtoprod2";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 21 AND before_status_id = 2 AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod2,  audit_createdby  from audit WHERE before_status_id = 4  AND (after_status_id = 5 OR after_status_id = 7 OR after_status_id = 28 OR after_status_id = 29  )AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 3";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectmonthlyeditorreport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS Editor, au.stagedtoprod , aua.stagedtoprod2";
        $sql .= " FROM users r";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod, audit_createdby  from audit WHERE after_status_id = 21 AND before_status_id = 2 AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') group by audit_createdby ) au ON r.user_id = au.audit_createdby";
        $sql .= " LEFT JOIN(SELECT COUNT( audit_id ) AS stagedtoprod2,  audit_createdby  from audit WHERE before_status_id = 4  AND (after_status_id = 5 OR after_status_id = 7 OR after_status_id = 28 OR after_status_id = 29  )AND DATE(audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')  group by audit_createdby ) aua ON r.user_id = aua.audit_createdby";
        $sql .= " WHERE r.department_id = 1 AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 3";
        $sql .= " GROUP BY r.user_id ";
        $sql .= " ORDER BY au.stagedtoprod desc";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectdailyfinancereport() {
        $sql = " SELECT CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS finance,   au.partial, aud.full";
        $sql .= " FROM  users r";

        $sql .= " LEFT JOIN(select count(audit_id) as partial ,audit_createdby from audit where after_status_id = 17 and DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby) au ON r.user_id = au.audit_createdby ";
        $sql .= " LEFT JOIN(select count(audit_id) as full ,audit_createdby from audit where after_status_id = 16 and DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') group by audit_createdby) aud ON r.user_id = aud.audit_createdby ";
        $sql .= " WHERE (r.department_id = 4 OR r.department_id = 3)AND r.role_id = 1 and r.enabled = 1 and r.job_desc = 10";

        $sql .= " GROUP BY r.user_id ";
        // echo $sql;
        return $this->GetRows($sql);
    }

//    update data
    function SelectDailyDesignerReportv2aa() {
        $sql = " SELECT r.user_id ,CONCAT( r.user_firstname,  ' ', r.user_lastname ) AS AE,  a.calls, au.Meetings, aud.Closed";
        $sql .= " FROM  users r";
        $sql .=" LEFT JOIN(Select count(account_id) as calls,account_createdby  from accounts WHERE DATE(account_createdon) = DATE('2012-12-01') group by account_createdby ) a ON a.account_createdby = r.user_id";
        $sql .= " LEFT JOIN(select count(audit_id) as Meetings ,audit_createdby from audit where after_status_id = 10 and DATE(audit_createdon) = DATE('2012-12-01') group by audit_createdby) au ON r.user_id = au.audit_createdby ";
        $sql .= " LEFT JOIN(select count(audit_id) as Closed,audit_createdby from audit where after_status_id = 13 and DATE(audit_createdon) = DATE('2012-12-01') group by audit_createdby) aud ON r.user_id = aud.audit_createdby ";
        $sql .= " WHERE r.department_id = 2 AND r.role_id = 3 and r.enabled = 1 and r.job_desc = 8";
        $sql .= " GROUP BY r.user_id ";
        echo $sql;
        return $this->GetRows($sql);
    }

    //    update data
}

?>