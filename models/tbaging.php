<?php

require_once($extra . 'main_sql.php');

class TB_AGING extends TB_SQL {

    function TB_AGING() {
        $this->TB_SQL();
    }

    function Selectaccountsleads($id) {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.source_id = 5";
        $sql .= " AND a.event_id = $id";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
//        echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsseo() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_seo = 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsbackup1() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_backupv1 = 1";
        $sql .= " AND a.account_cmstype = 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsbackup1not() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_backupv1 != 1";
        $sql .= " AND a.account_cmstype = 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsbackup2() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_backupv2 = 1";
        $sql .= " AND a.account_cmstype = 2";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsbackup2not() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_backupv2 != 1";
        $sql .= " AND a.account_cmstype = 2";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsseonot() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_seo != 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsads() {
        $sql = " SELECT a.* , b.status_name  , CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS AE";
        $sql .= " FROM accounts a , statuses b , users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_ads = 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsadsnot() {
        $sql = " SELECT a.*, b.status_name, CONCAT( c.user_firstname, ' ', c.user_lastname ) AS AE";

        $sql .= " FROM accounts a, statuses b, users c";
        $sql .= " WHERE a.status_id = 9";
        $sql .= " AND a.account_paid = 1";
        $sql .= " AND a.account_ads != 1";
        $sql .= " AND a.status_id = b.status_id";
        $sql .= " AND a.account_createdby = c.user_id";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountspackage($id, $id1) {

        $sql = " SELECT a.* , b.status_name  , concat(ed.user_firstname,' ',ed.user_lastname) as editor, concat(c.user_firstname,' ',c.user_lastname) as AE,";
        $sql .= " concat(de.user_firstname,' ',de.user_lastname) as DE,";
        $sql .= " concat(csr.user_firstname, ' ', csr.user_lastname) as CSR ,p.package_name";
        $sql .= " FROM accounts a";
        $sql .= " LEFT JOIN statuses b ON a.status_id = b.status_id";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id";
        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id";
        $sql .= " LEFT JOIN packages p ON a.package_id = p.package_id";
        if ($id == 10) {
            $sql .= " WHERE (a.package_id is null or a.package_id = 0)";
        } else {
            $sql .= " WHERE a.package_id = $id";
        }
        if ($id1 == 0) {
            
        } else {
            $sql .= " AND a.shop_type = $id1";
        }
        $sql .= " AND a.account_paid = 1";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsclass($id, $id1) {

        $sql = " SELECT a.* , b.status_name  , concat(ed.user_firstname,' ',ed.user_lastname) as editor, concat(c.user_firstname,' ',c.user_lastname) as AE,";
        $sql .= " concat(de.user_firstname,' ',de.user_lastname) as DE,";
        $sql .= " concat(csr.user_firstname, ' ', csr.user_lastname) as CSR ";
        $sql .= " FROM accounts a";
        $sql .= " LEFT JOIN statuses b ON a.status_id = b.status_id";
        $sql .= " LEFT JOIN users c ON a.account_createdby = c.user_id";
        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id";
        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id";

        $sql .= " WHERE a.account_classification = $id";
        if ($id1 == 0) {
            
        } else {
            $sql .= " AND a.shop_type = $id1";
        }
        $sql .= " AND a.account_paid = 1";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectpackages() {
        $sql = " SELECT * ";
        $sql .= " FROM packages";
//        $sql .= " WHERE event_id= $id";
//        echo $sql;
        return $this->GetRows($sql);
    }

    function Selectpackagesbyid($id) {
        $sql = " SELECT * ";
        $sql .= " FROM packages";
        $sql .= " WHERE package_id= $id";
//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectevent($id) {
        $sql = " SELECT * ";
        $sql .= " FROM events";
        $sql .= " WHERE event_id= $id";

//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectaccounthistory(
    $parse) {
        $sql = " SELECT *";
        $sql .= " FROM audit_log ";
        $sql .= " WHERE audit_act LIKE '%$parse%'";
        $sql .= " ORDER BY audit_log_time desc";
//        $sql .= " AND a.shop_editor = b.user_id";
//        $sql .= " AND account_paid = 1";
//        echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccount(
    $id) {
        $sql = " SELECT *";
        $sql .= " FROM accounts ";
        $sql .= " WHERE account_id= $id";
//        $sql .= " AND a.shop_editor = b.user_id";
//        $sql .= " AND account_paid = 1";
//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectaccounteditor(
    $id) {
        $sql = " SELECT a.account_name, CONCAT( b.user_firstname, ' ', b.user_lastname ) AS editor ";
        $sql .= " FROM accounts a, users b";
        $sql .= " WHERE a.account_id= $id";
        $sql .= " AND a.shop_editor = b.user_id";
//        $sql .= " AND a.account_paid = 1";
//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectaccountdesigner(
    $id) {
        $sql = " SELECT a.account_name, CONCAT( c.user_firstname, ' ', c.user_lastname ) AS designer";
        $sql .= " FROM accounts a, users c";
        $sql .= " WHERE a.account_id= $id";

        $sql .= " AND a.shop_designer = c.user_id";
//        $sql .= " AND a.account_paid = 1";
//        echo $sql;
        return $this->GetRow($sql);
    }

    function Selectaccountaging($id) {
        $lquery .= "SELECT d.status_name, c.audit_createdon AS log, CONCAT( e.user_firstname, ' ', e.user_lastname ) AS changelog";
        $lquery .= " FROM users b";
        $lquery .= " INNER JOIN accounts a ON b.user_id = a.account_createdby";
        $lquery .= " INNER JOIN audit c ON a.account_id = c.account_id";
        $lquery .= " INNER JOIN statuses d ON c.after_status_id = d.status_id";
        $lquery .= " INNER JOIN users e ON c.audit_createdby = e.user_id";
//        $lquery .= " WHERE a.account_paid =1";
//        $lquery .= " AND a.account_id =$id";

        $lquery .= " WHERE a.account_id =$id";
        $lquery .= " ORDER BY c.audit_createdon desc";
//        echo $lquery;
        return $this->GetRows($lquery);
    }

    function Selectaccountsfordev($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";
        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and a.status_id = 21 
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";
//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id";
//        $sql .= " WHERE a.account_paid=1 and a.status_id = 21 AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
//        $sql = " GROUP by a.account_id ORDER BY a.account_classification,enter_status ASC";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsforundercons($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";
        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and a.status_id = 3 
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";

//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//
//
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
//                 LEFT OUTER JOIN audit au2 ON 
//                (a.account_id = au2.account_id 
//                AND 
//                (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
//                AND au.account_id < au.account_id))
//                ";
//        $sql .= " WHERE a.account_paid=1 and a.status_id = 3 AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
//        $sql = " GROUP by a.account_id ORDER BY a.account_classification,enter_status ASC";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsforclientrev($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";
        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and (a.status_id = 8 or a.status_id = 30 or a.status_id = 31 or a.status_id = 41 or a.status_id = 42)
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";
//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id";
//        $sql .= " WHERE a.account_paid=1 and (a.status_id = 8 or a.status_id = 30 or a.status_id = 31 or a.status_id = 41 or a.status_id = 42) AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
//        $sql = " GROUP by a.account_id ORDER BY a.account_classification,enter_status ASC";
//        echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsforinternalrev($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";
        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and (a.status_id = 7 or a.status_id = 28 or a.status_id = 29 or a.status_id = 32 or a.status_id = 33 or a.status_id = 34)
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";
//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id";
//        $sql .= " WHERE a.account_paid=1 and (a.status_id = 7 or a.status_id = 28 or a.status_id = 29 or a.status_id = 32 or a.status_id = 33 or a.status_id = 34) AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
//        $sql = " GROUP by a.account_id ORDER BY a.account_classification,enter_status ASC";
//        echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsforredesign($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";
        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and (a.status_id = 37 or a.status_id = 43 or a.status_id = 44)
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";


//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id";
//        $sql .= " WHERE a.account_paid=1 and (a.status_id = 37 or a.status_id = 43 or a.status_id = 44) AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountsforclientupdates($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";

        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            INNER JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            INNER JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
            AND a.account_paid= 1 and a.status_id = 27
            AND a.status_id =au.after_status_id
            AND de.user_id = " . $id . "
            GROUP by a.account_id 
                ";

//        
//        $sql .= " From accounts a ";
//        $sql .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $sql .= " LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $sql .= " LEFT JOIN users ae ON a.account_createdby = ae.user_id ";
//        $sql .= " LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id ";
//        $sql .= " LEFT JOIN users de ON a.shop_designer = de.user_id";
//        $sql .= " LEFT JOIN users csr ON a.shop_csr = csr.user_id ";
//        $sql .= " LEFT JOIN audit au ON a.account_id = au.account_id";
//        $sql .= " LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id";
//        $sql .= " WHERE a.account_paid=1 and a.status_id = 27 AND de.user_id = $id";
//        $sql .= " AND a.status_id = au.after_status_id";
//        $sql .= " GROUP by a.account_id ";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function Selectaccountslinedup($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";

        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            LEFT JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            LEFT JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
              and a.status_id = 1
             
            AND csr.user_id = " . $id . "
            GROUP by a.account_id 
                ";
        
       // echo $sql;
        return $this->GetRows($sql);
    }
    function Selectaccountsrejected($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";

        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            LEFT JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            LEFT JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
             and a.status_id in (62,57,56,55)
            
            AND csr.user_id = " . $id . "
            GROUP by a.account_id 
                ";
        return $this->GetRows($sql);
    }
    
    function Selectaccountscsrqa($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";

        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            LEFT JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            LEFT JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
              and a.status_id = 38
             
            AND csr.user_id = " . $id . "
            GROUP by a.account_id 
                ";
        return $this->GetRows($sql);
    }
    
    function Selectaccountsclientapp($id) {
        $sql = " SELECT a.account_classification,a.account_id, a.account_name, a.shop_url, a.account_cmstype, a.account_seo, a.account_ads, a.account_createdby,a.account_createdon,";
        $sql .= " a.account_modifiedon, s.status_id, s.status_name AS current_status, prev.status_name AS prev_status, CONCAT( ed.user_firstname,  ' ', ed.user_lastname ) AS editor, ";
        $sql .= " CONCAT( ae.user_firstname,  ' ', ae.user_lastname ) AS AE, CONCAT( de.user_firstname,  ' ', de.user_lastname ) AS DE, CONCAT( csr.user_firstname,  ' ', csr.user_lastname ) AS CSR,au.audit_createdon AS enter_status , prod.prodentry";

        $sql .=" 
            FROM accounts a
            LEFT JOIN statuses s ON a.status_id = s.status_id 
            LEFT JOIN users ed ON a.shop_editor = ed.user_id 
            LEFT JOIN users ae ON a.account_createdby = ae.user_id 
            LEFT JOIN users de ON a.shop_designer = de.user_id
            LEFT JOIN users csr ON a.shop_csr = csr.user_id
            LEFT JOIN audit  au ON (a.account_id = au.account_id)
            LEFT JOIN statuses prev ON prev.status_id  = au.before_status_id
            LEFT OUTER JOIN audit au2 ON 
            (a.account_id = au2.account_id 
            AND 
            (au.audit_createdon < au2.audit_createdon OR au.audit_createdon = au.audit_createdon 
            AND au.account_id < au.account_id))
            LEFT JOIN (select MAX(audit_createdon) as prodentry,account_id from audit where after_status_id = 2 group by account_id) prod ON prod.account_id = a.account_id 
            WHERE au2.account_id IS NULL
             and a.status_id in (25,26,6)
             
            AND csr.user_id = " . $id . "
            GROUP by a.account_id 
                ";
        return $this->GetRows($sql);
    }

}

?>