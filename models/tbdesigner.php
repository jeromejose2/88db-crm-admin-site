<?php

require_once($extra . 'main_sql.php');

class TB_DESIGNER extends TB_SQL {

    function Insertdesignertotalshops($data) {
        $id = $this->Insert($data, 'designershopsdone');
        return $id;
    }

    function Insertcurrentworkingnewshops($data) {
        $id = $this->Insert($data, 'currentworkingnewshops');
        return $id;
    }

    function Insertcurrentworkingrevshops($data) {
        $id = $this->Insert($data, 'currentworkingrevshops');
        return $id;
    }

    function Insertdesignershopsdonedetails($data) {
        $id = $this->Insert($data, 'designershopsdonedetails');
        return $id;
    }

    function SelectDesignerLoadNewShops() {




        $sql = "
                SELECT u.user_id,
	CONCAT(u.user_firstname ,' ' , u.user_lastname) as designer ,  a.account_id , a.account_name , p.package_name as package , stat2.status_name as PrevStatus, stat.status_name as CurrentStatus, 
	b.audit_createdon    , NOW() as CurrentDate,DATEDIFF(CURDATE() , DATE(b.audit_createdon )) as  days 
FROM accounts a
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		audit_createdon
		FROM audit 
		WHERE after_status_id = 3
		AND before_status_id = 21
		GROUP BY account_id
) b ON a.account_id = b.account_id
LEFT JOIN statuses stat ON a.status_id = stat.status_id
LEFT JOIN statuses stat2 ON stat2.status_id = b.before_status_id
LEFT JOIN users u ON u.user_id = a.shop_designer
LEFT JOIN (
	SELECT 
		account_id  
		FROM audit 
		WHERE after_status_id = 3
		AND before_status_id <> 21
		GROUP BY account_id
) rev on a.account_id = rev.account_id
LEFT JOIN packages p ON a.package_id = p.package_id
WHERE a.status_id = 3
AND rev.account_id is null
AND a.shop_type <> 2
AND b.audit_createdon is NOT NULL
ORDER BY  designer 
                ";
// echo $sql;
        return $this->GetRows($sql);
    }

    function SelectDesignerLoadOldShops() {

        $sql = "
             SELECT u.user_id,
	CONCAT(u.user_firstname ,' ' , u.user_lastname) as designer ,  a.account_id , a.account_name , p.package_name as package , stat2.status_name as PrevStatus, stat.status_name as CurrentStatus, 
	b.audit_createdon    , NOW() as CurrentDate,DATEDIFF(CURDATE() , DATE(b.audit_createdon )) as  days 
FROM accounts a
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		max(audit_createdon) as audit_createdon
		FROM audit 
		WHERE after_status_id = 3
		AND before_status_id  IN (8,27,30,31,37,41,42,43,44)
		GROUP BY account_id
) b ON a.account_id = b.account_id
LEFT JOIN statuses stat ON a.status_id = stat.status_id
LEFT JOIN statuses stat2 ON stat2.status_id = b.before_status_id
LEFT JOIN users u ON u.user_id = a.shop_designer
 
LEFT JOIN packages p ON a.package_id = p.package_id
WHERE a.status_id = 3
 
AND a.shop_type <> 2
AND b.audit_createdon is NOT NULL
ORDER BY  designer 
                ";

        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDone() {

        $sql = "
              SELECT u.user_id, 
	CONCAT(u.user_firstname ,' ' , u.user_lastname) as designer , 
    a.account_id , a.account_name , p.package_name as package , stat.status_name as CurrentStatus,
	under.audit_createdon as FromDate,
	b.audit_createdon as DateDone ,
	DATEDIFF(DATE(b.audit_createdon) , DATE(under.audit_createdon)) as  DaysDone 
	 
FROM accounts a
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		audit_createdon
		FROM audit 
		WHERE before_status_id = 3
		AND  after_status_id = 4
		AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') 
		GROUP BY account_id
) b ON a.account_id = b.account_id
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		max(audit_createdon) as audit_createdon
		FROM audit 
		WHERE  after_status_id = 3
		GROUP BY account_id
) under ON a.account_id = under.account_id
LEFT JOIN statuses stat ON a.status_id = stat.status_id
LEFT JOIN statuses stat2 ON stat2.status_id = b.before_status_id
LEFT JOIN users u ON u.user_id = a.shop_designer
LEFT JOIN packages p ON a.package_id = p.package_id
WHERE a.shop_type <> 2
AND b.account_id is not null
ORDER BY  designer 
            ";
        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDoneTotalDaily() {
        $sql = "
            SELECT r.user_id,CONCAT(r.user_firstname ,' ' , r.user_lastname) as designer  ,a.TotalShops
FROM users r
LEFT JOIN(
SELECT
	u.user_id	,
	CONCAT(u.user_firstname ,' ' , u.user_lastname) as designer , 
   count(  a.account_id  ) as TotalShops
FROM accounts a
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		audit_createdon
		FROM audit 
		WHERE before_status_id = 3
		AND  after_status_id = 4
		AND DATE(audit_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "') 
		GROUP BY account_id
) b ON a.account_id = b.account_id
LEFT JOIN ( 
		SELECT 
		account_id , 
		audit_createdby ,
		before_status_id,
		after_status_id,
		max(audit_createdon) as audit_createdon
		FROM audit 
		WHERE  after_status_id = 3
		GROUP BY account_id
) under ON a.account_id = under.account_id
LEFT JOIN statuses stat ON a.status_id = stat.status_id
LEFT JOIN statuses stat2 ON stat2.status_id = b.before_status_id
LEFT JOIN users u ON  a.shop_designer = u.user_id 
LEFT JOIN packages p ON a.package_id = p.package_id
WHERE a.shop_type <> 2
AND b.account_id is not null

GROUP BY  designer ) a ON r.user_id = a.user_id
WHERE  r.user_type <> 2 AND r.department_id =1 AND r.role_id =3 AND r.enabled =1 AND r.job_desc = 2 
ORDER BY a.TotalShops desc
                ";

        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDoneDetailWeekly() {
        $sql = "Select * from designershopsdonedetails  WHERE DATE(data_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') order by designer";

        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDoneDetailMonthly() {
        $sql = "Select * from designershopsdonedetails  WHERE DATE(data_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') order by designer";
        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDoneWeekly() {
        $sql = "SELECT designer, sum(total_shops) as total from designershopsdone WHERE DATE(data_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') GROUP BY designer_id ORDER BY total DESC";
        //echo $sql;
        return $this->GetRows($sql);
    }

    function SelectDesignerLoadShopsDoneMonthly() {
        $sql = "SELECT  designer, sum(total_shops) as total from designershopsdone WHERE DATE(data_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') GROUP BY designer_id ORDER BY total DESC";
        return $this->GetRows($sql);
    }

}

?>