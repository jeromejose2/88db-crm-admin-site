<?php
include_once("../functions/connection.php");
session_start(); 
$_SESSION['title_page']  = "Home";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$department_id = $_SESSION['department_id'];
$from =  strtotime('monday last week');
$to = strtotime('friday this week');
?>
<!DOCTYPE HTML >
<html>
<head>
    <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
    <title>88DB PH CRM</title>

    <?php include 'header.php'; ?>
	
    <div class="main-section">
    
        <div class="commands">
            <div class="head-label">
                <h2>Dashboard</h2>
            </div><!-- end of add new account -->
        </div><!-- end of grid-commands -->
		 <?php include 'row2.php'; ?>
        <div class="clear"></div>
        
        <div class="dashboard3-chart">

            <div class="chart-label"><h2>Sales Leads</h2></div>

            <table id='tb1Leads' >
                <!-- <caption>Sales Leads</caption> -->

                <thead>
                    <tr>
                        <th></th>
                        <th><?php echo date('m-d-Y', strtotime('monday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('tuesday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('wednesday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('thursday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('friday last week')); ?></th>
						<th><?php echo date('m-d-Y', strtotime('monday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('tuesday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('wednesday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('thursday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('friday this week')); ?></th>
                        
						
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                       
                        $condition = "";
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $query = "SELECT 
									CONCAT(u.user_firstname,' ', u.user_lastname) AS user_name, 
									SUM(ct1) AS ct1, 
									SUM(ct2) AS ct2, 
									SUM(ct3) AS ct3, 
									SUM(ct4) AS ct4, 
									SUM(ct5) AS ct5, 
									SUM(ct6) AS ct6,
									SUM(ct7) AS ct7,
									SUM(ct8) AS ct8,
									SUM(ct9) AS ct9,
									SUM(ct10) AS ct10
									FROM 
									(
									SELECT a.account_createdby, 
									
									if(DATE(a.account_createdon)= DATE('".DATE('Y-m-d', strtotime('monday last week'))."'),count(a.account_createdby),0) AS ct1,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('tuesday last week'))."'),count(a.account_createdby),0) AS ct2,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('wednesday last week'))."'),count(a.account_createdby),0) AS ct3,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('thursday last week'))."'),count(a.account_createdby),0) AS ct4,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('friday last week'))."'),count(a.account_createdby),0) AS ct5,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('monday this week'))."'),count(a.account_createdby),0) AS ct6, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('tuesday this week'))."'),count(a.account_createdby),0) AS ct7, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('wednesday this week'))."'),count(a.account_createdby),0) AS ct8, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('thursday this week'))."'),count(a.account_createdby),0) AS ct9, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('friday this week'))."'),count(a.account_createdby),0) AS ct10 
									
									FROM accounts a LEFT JOIN users r on a.account_createdby = r.user_id WHERE DATE(a.account_createdon) BETWEEN DATE('".DATE('Y-m-d', $FROM)."') and DATE('".DATE('Y-m-d', $to)."') ".$condition." GROUP BY a.account_createdby, a.account_createdon
									) 
									b LEFT JOIN users u on b.account_createdby = u.user_id GROUP BY u.user_name";
                        //echo $query;
                        $result = mysql_query($query,connect());
                        if (mysql_num_rows($result) > 0) {
                            while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
                                echo "<tr>";
                                echo "<th>".$row[0]."</th>";
                                echo "<td>".$row[1]."</td>";
                                echo "<td>".$row[2]."</td>";
                                echo "<td>".$row[3]."</td>";
                                echo "<td>".$row[4]."</td>";
                                echo "<td>".$row[5]."</td>";
								echo "<td>".$row[6]."</td>";
								echo "<td>".$row[7]."</td>";
								echo "<td>".$row[8]."</td>";
								echo "<td>".$row[9]."</td>";
								echo "<td>".$row[10]."</td>";
								
                                echo "</tr>";
                            }
                        } else {
                             echo "<tr>";
                                echo "<th></th>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								
                                echo "</tr>";
                        }
                    ?>
                </tbody>
                               
            </table><!-- end of tb1 -->
        </div><!-- end of dashboard-chart -->


        
		 <div class="dashboard3-chart">

            <div class="chart-label"><h2>Signed Shops</h2></div>

            <table id='tb2Signed' >
                <!-- <caption>Sales Leads</caption> -->

                <thead>
                    <tr>
                        <th></th>
                        <th><?php echo date('m-d-Y', strtotime('monday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('tuesday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('wednesday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('thursday last week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('friday last week')); ?></th>
						 <th><?php echo date('m-d-Y', strtotime('monday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('tuesday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('wednesday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('thursday this week')); ?></th>
                        <th><?php echo date('m-d-Y', strtotime('friday this week')); ?></th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                      
                        $condition = "";
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }
                        $query = "SELECT 
									CONCAT(u.user_firstname,' ', u.user_lastname) AS user_name,
									SUM(ct1) AS ct1, 
									SUM(ct2) AS ct2, 
									SUM(ct3) AS ct3, 
									SUM(ct4) AS ct4, 
									SUM(ct5) AS ct5, 
									SUM(ct6) AS ct6,
									SUM(ct7) AS ct7,
									SUM(ct8) AS ct8,
									SUM(ct9) AS ct9,
									SUM(ct10) AS ct10
									FROM 
									(
									SELECT 
									a.account_createdby, 
									
									if(DATE(a.account_createdon)= DATE('".DATE('Y-m-d', strtotime('monday last week'))."'),count(a.account_createdby),0) AS ct1,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('tuesday last week'))."'),count(a.account_createdby),0) AS ct2,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('wednesday last week'))."'),count(a.account_createdby),0) AS ct3,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('thursday last week'))."'),count(a.account_createdby),0) AS ct4,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('friday last week'))."'),count(a.account_createdby),0) AS ct5,
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('monday this week'))."'),count(a.account_createdby),0) AS ct6, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('tuesday this week'))."'),count(a.account_createdby),0) AS ct7, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('wednesday this week'))."'),count(a.account_createdby),0) AS ct8, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('thursday this week'))."'),count(a.account_createdby),0) AS ct9, 
									if(DATE(a.account_createdon)=DATE('".DATE('Y-m-d', strtotime('friday this week'))."'),count(a.account_createdby),0) AS ct10 
								
									FROM accounts a LEFT JOIN users r on a.account_createdby = r.user_id WHERE a.status_id =13 AND DATE(a.account_createdon) between DATE('".DATE('Y-m-d', $from)."') and DATE('".DATE('Y-m-d', $to)."') ".$condition." GROUP BY a.account_createdby, a.account_createdon
									) b LEFT JOIN users u on b.account_createdby = u.user_id GROUP BY u.user_name";
                        //echo $query;
                        $result = mysql_query($query,connect());
                        if (mysql_num_rows($result) > 0) {
                            while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
                                echo "<tr>";
                                echo "<th>".$row[0]."</th>";
                                echo "<td>".$row[1]."</td>";
                                echo "<td>".$row[2]."</td>";
                                echo "<td>".$row[3]."</td>";
                                echo "<td>".$row[4]."</td>";
                                echo "<td>".$row[5]."</td>";
								echo "<td>".$row[6]."</td>";
								echo "<td>".$row[7]."</td>";
								echo "<td>".$row[8]."</td>";
								echo "<td>".$row[9]."</td>";
								echo "<td>".$row[10]."</td>";
                                echo "</tr>";
                            }
                        } else {
                              echo "<tr>";
                                echo "<th>0</th>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
                                echo "</tr>";
                        }
                    ?>
                </tbody>
                               
            </table><!-- end of tb1 -->
        </div><!-- end of dashboard-chart -->
		
		<div class="dashboard3-chart">

            <div class="chart-label"><h2>Signed Shop Categories</h2></div>

            <table id='tb3SignedCat'>

                <!-- <caption>Top Categories</caption> -->

                <thead>
                    <tr>
                        <th></th>
                        <th>Arts &amp; Crafts</th>
                        <th>Automotive</th>
                        <th>Beauty</th>
                        <th>Business Services</th>
                        <th>Buy and Sell</th>
                        <th>Clubs and Associations</th>
                        <th>Computers and Digital Media</th>
                        <th>Flowers &amp; Gifts</th>
                        <th>Food Hub</th>
                        <th>Health and Fitness</th>
                        <th>Job Opportunities</th>
                        <th>Pets</th>
                        <th>Properties</th>
                        <th>Services</th>
                        <th>Travel Needs</th>
                        <th>Weddings and Events</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                       
                        $condition = "";
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                        }                                
                        $query = "select 
									sum(if(x.category_id=2,ct1,0)) as 'Arts & Crafts', 
									sum(if(x.category_id=3,ct1,0)) as 'Automotive',
									sum(if(x.category_id=4,ct1,0)) as 'Beauty',
									sum(if(x.category_id=5,ct1,0)) as 'Business Services',
									sum(if(x.category_id=6,ct1,0)) as 'Buy and Sell',
									sum(if(x.category_id=7,ct1,0)) as 'Clubs and Associations',
									sum(if(x.category_id=8,ct1,0)) as 'Computers and Digital Media',
									sum(if(x.category_id=9,ct1,0)) as 'Flowers & Gifts',
									sum(if(x.category_id=10,ct1,0)) as 'Food Hub',
									sum(if(x.category_id=11,ct1,0)) as 'Health and Fitness',
									sum(if(x.category_id=12,ct1,0)) as 'Job Opportunities',
									sum(if(x.category_id=13,ct1,0)) as 'Pets',
									sum(if(x.category_id=14,ct1,0)) as 'Properties',
									sum(if(x.category_id=15,ct1,0)) as 'Services',
									sum(if(x.category_id=16,ct1,0)) as 'Travel Needs',
									sum(if(x.category_id=17,ct1,0)) as 'Weddings and Events' 
									
									from 
									(
									select c.category_id, c.category_name, count(a.category_id) as ct1 from categories c right join accounts a on a.category_id = c.category_id left join users r on a.account_createdby = r.user_id WHERE a.status_id = 13 AND date(a.account_createdon) between date('".date('Y-m-d', $from)."') and date('".date('Y-m-d', $to)."') ".$condition." group by c.category_id
									) x";
                        
                        $result = mysql_query($query,connect());
                        if (mysql_num_rows($result) > 0) {
                            while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
							{
                                echo "<tr>";
                                echo "<th>Categories</th>";
                                echo "<td>".$row[0]."</td>";
                                echo "<td>".$row[1]."</td>";
                                echo "<td>".$row[2]."</td>";
                                echo "<td>".$row[3]."</td>";
                                echo "<td>".$row[4]."</td>";
                                echo "<td>".$row[5]."</td>";
                                echo "<td>".$row[6]."</td>";
                                echo "<td>".$row[7]."</td>";
                                echo "<td>".$row[8]."</td>";
                                echo "<td>".$row[9]."</td>";
                                echo "<td>".$row[10]."</td>";
                                echo "<td>".$row[11]."</td>";
                                echo "<td>".$row[12]."</td>";
                                echo "<td>".$row[13]."</td>";
                                echo "<td>".$row[14]."</td>";
                                echo "<td>".$row[15]."</td>";
                                echo "</tr>";
                            }
                        }
						else {
                              echo "<tr>";
                                echo "<th>Categories</th>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "<td>0</td>";
                                echo "</tr>";                           
                           
                        }
                    ?>
                </tbody>           
            </table>
        </div><!-- end of dashboard-chart -->
		<div class="clear"></div>

	<div class="dashboard3-chart">

				<div class="chart-label"><h2>Signed Shop Source</h2></div>

				<table id='tb4SignedSource'>

					

					<thead>
						<tr>
							<th></th>
							<th>88DB - Free Listing</th>
							<th>EYP</th>
							<th>Uknown</th>
							<th>Referrals</th>
							<th>Events</th>
							<th>Saturation</th>
							<th>Multiply</th>
							<th>Sulit</th>
							<th>Ayos Dito</th>
							<th>88DB - Database</th>
							
						</tr>
					</thead>

					<tbody>
						<?php
						   
							$condition = "";
							switch ($role_id) {
								case 2:
									$condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
									break;
								case 3:
									$condition = " AND r.user_id = " . $userid . " ";
									break;
							}                                
							$query = "select 
										sum(if(x.source_id=1,ct1,0)) as '88DB - Free Listing', 
										sum(if(x.source_id=2,ct1,0)) as 'EYP',
										sum(if(x.source_id=3,ct1,0)) as 'Unknown',
										sum(if(x.source_id=4,ct1,0)) as 'Referrals',
										sum(if(x.source_id=5,ct1,0)) as 'Events',
										sum(if(x.source_id=6,ct1,0)) as 'Saturation',
										sum(if(x.source_id=7,ct1,0)) as 'Multiply',
										sum(if(x.source_id=8,ct1,0)) as 'Sulit',
										sum(if(x.source_id=9,ct1,0)) as 'Ayos Dito',
										sum(if(x.source_id=10,ct1,0)) as '88DB - Database'
										from 
										(
										select 
										s.source_id, 
										s.source_name, 
										count(a.source_id) as ct1 
										from sources s right join accounts a on a.source_id = s.source_id left join users r on a.account_createdby = r.user_id WHERE a.status_id = 13 AND date(a.account_createdon) between date('".date('Y-m-d', $from)."') and date('".date('Y-m-d', $to)."') ".$condition." group by s.source_id
										) x ";
							
							$result = mysql_query($query,connect());
							if (mysql_num_rows($result) > 0) {
								while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
									echo "<tr>";
									echo "<th>Categories</th>";
									echo "<td>".$row[0]."</td>";
									echo "<td>".$row[1]."</td>";
									echo "<td>".$row[2]."</td>";
									echo "<td>".$row[3]."</td>";
									echo "<td>".$row[4]."</td>";
									echo "<td>".$row[5]."</td>";
									echo "<td>".$row[6]."</td>";
									echo "<td>".$row[7]."</td>";
									echo "<td>".$row[8]."</td>";
									echo "<td>".$row[9]."</td>";

									
									echo "</tr>";
								}
							} else {
								echo "<tr>";
								echo "<th>Categories</th>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								echo "<td>0</td>";
								                                 
								echo "</tr>";
							}
						?>
					</tbody>     
					
				</table>
			</div><!-- end of dashboard-chart -->
			
	<div class="dashboard4-chart">

				<div class="chart-label"><h2>Declined Reasons</h2></div>

				<table id='main-grid'>

					<!-- <caption>Top Categories</caption> -->

					
						<?php
						   $value = Array("Already Has a Website","Website Not a Priority","No Budget / Limited Resources","Not Keen on Online Marketing Option","Package is Expensive","Limited Features","Other","No Reason");
							$condition = "";
							switch ($role_id) {
								case 2:
									$condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
									break;
								case 3:
									$condition = " AND r.user_id = " . $userid . " ";
									break;
							}                                
							$query = "select 
										sum(if(x.declined_status_id=1,ct1,0)) as 'Already Has a Website', 
										sum(if(x.declined_status_id=2,ct1,0)) as 'Website Not a Priority',
										sum(if(x.declined_status_id=3,ct1,0)) as 'No Budget / Limited Resources',
										sum(if(x.declined_status_id=4,ct1,0)) as 'Not Keen on Online Marketing Option',
										sum(if(x.declined_status_id=5,ct1,0)) as 'Package is Expensive',
										sum(if(x.declined_status_id=6,ct1,0)) as 'Limited Features',
										sum(if(x.declined_status_id=7,ct1,0)) as 'Other',
										sum(if(x.declined_status_id<=>NULL,ct1,0)) as 'No Reason'
										from 
										(
										select 
										a.declined_status_id,
										count(IF(a.declined_status_id<=>NULL,1,a.declined_status_id)) as ct1 
										from accounts a 
										
										left join users r on a.account_createdby = r.user_id 
										WHERE a.status_id = 14 AND date(a.account_createdon) between date('".date('Y-m-d', $from)."') and date('".date('Y-m-d', $to)."') ".$condition." group by a.declined_status_id
										) x";
							
							$result = mysql_query($query,connect());
							if (mysql_num_rows($result) > 0) {
								while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
									echo "<tr class='grid-content'>";
									echo "<td>".$value[0]."</td>";
									echo "<td>".$row[0]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[1]."</td>";
									echo "<td>".$row[1]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[2]."</td>";
									echo "<td>".$row[2]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[3]."</td>";
									echo "<td>".$row[3]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[4]."</td>";
									echo "<td>".$row[4]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[5]."</td>";
									echo "<td>".$row[5]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[6]."</td>";
									echo "<td>".$row[6]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$value[7]."</td>";
									echo "<td>".$row[7]."</td>";
									echo "</tr>";
									
									
								}
							} 
						?>
				         
				</table>
			</div><!-- end of dashboard-chart -->
			<?php if($department_id == 2) { ?>
			<div class="dashboard4-chart">

				<div class="chart-label"><h2>Status</h2></div>

				<table id='main-grid'>

					<!-- <caption>Top Categories</caption> -->

					
						<?php
																   
									
							$condition = "";
							switch ($role_id) {
								case 1:
									$condition ="";
									break;
								case 2:
									$condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
									break;
								case 3:
									$condition = " AND r.user_id = " . $userid . " ";
									break;
							}   
						
							$status_name = Array("Set Appointment","Sent Proposal","For Call Back","Signed (for Payment)","Declined","For Follow up","Interested","For Signing","For Prospecting");
							
							$query = "select 
										sum(if(x.status_id=10,ct1,0)) as 'Set Appointment', 
										sum(if(x.status_id=11,ct1,0)) as 'Sent Proposal',
										sum(if(x.status_id=12,ct1,0)) as 'For Call Back',
										sum(if(x.status_id=13,ct1,0)) as 'Signed (for Payment)',
										sum(if(x.status_id=14,ct1,0)) as 'Declined',
										sum(if(x.status_id=15,ct1,0)) as 'For Follow up',
										sum(if(x.status_id=19,ct1,0)) as 'Interested',
										sum(if(x.status_id=20,ct1,0)) as 'For Signing',
										sum(if(x.status_id=22,ct1,0)) as 'For Prospecting'
										from 
										(
										select 
										count(a.status_id) as ct1,
										a.status_id
										from accounts a 
										left join users r on a.account_createdby = r.user_id 
										WHERE date(a.account_createdon) between date('".date('Y-m-d', $from)."') and date('".date('Y-m-d', $to)."') ".$condition." group by a.status_id
										) x";
							
							$result = mysql_query($query,connect());
							if (mysql_num_rows($result) > 0) {
							
								while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[0]."</td>";
									echo "<td>".$row[0]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[1]."</td>";
									echo "<td>".$row[1]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[2]."</td>";
									echo "<td>".$row[2]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[3]."</td>";
									echo "<td>".$row[3]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[4]."</td>";
									echo "<td>".$row[4]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[5]."</td>";
									echo "<td>".$row[5]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[6]."</td>";
									echo "<td>".$row[6]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[7]."</td>";
									echo "<td>".$row[7]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[8]."</td>";
									echo "<td>".$row[8]."</td>";
									echo "</tr>";
							
									
								
								}
							}
						
						
						
						?>
				       
				</table>
			</div><!-- end of dashboard-chart -->
			<?php } ?>
			<?php if($department_id == 4) { ?>
			<div class="dashboard4-chart">

				<div class="chart-label"><h2>Status</h2></div>

				<table id='main-grid'>

					<!-- <caption>Top Categories</caption> -->

					
						<?php
																   
									
							$condition = "";
							switch ($role_id) {
								case 1:
									$condition ="";
									break;
								case 2:
									$condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
									break;
								case 3:
									$condition = " AND r.user_id = " . $userid . " ";
									break;
							}   
						
														

							
							$status_name = Array
										(	'Lined Up for Production', 
											'For Proofreading',
											'Under Construction',
											'For Editor QA',
											'For Creative Director QA',
											'For Client Approval',
											'For Revisions - 1st Pass',
											'For Revisions - 2nd Pass',
											'Live',
											'Set Appointment', 
											'Sent Proposal',
											'For Call Back',
											'Signed (for Payment)',
											'Declined',
											'For Follow up',
											'Paid (Full)',
											'Paid (Partial)',
											'For Design Instructions',
											'Interested',
											'For Signing',
											'For Development',
											'For Prospecting'
										);
							
							$query = "select 
										sum(if(x.status_id=1,ct1,0)) as 'Lined Up for Production', 
										sum(if(x.status_id=2,ct1,0)) as 'For Proofreading',
										sum(if(x.status_id=3,ct1,0)) as 'Under Construction',
										sum(if(x.status_id=4,ct1,0)) as 'For Editor QA',
										sum(if(x.status_id=5,ct1,0)) as 'For Creative Director QA',
										sum(if(x.status_id=6,ct1,0)) as 'For Client Approval',
										sum(if(x.status_id=7,ct1,0)) as 'For Revisions - 1st Pass',
										sum(if(x.status_id=8,ct1,0)) as 'For Revisions - 2nd Pass',
										sum(if(x.status_id=9,ct1,0)) as 'Live',
										sum(if(x.status_id=10,ct1,0)) as 'Set Appointment', 
										sum(if(x.status_id=11,ct1,0)) as 'Sent Proposal',
										sum(if(x.status_id=12,ct1,0)) as 'For Call Back',
										sum(if(x.status_id=13,ct1,0)) as 'Signed (for Payment)',
										sum(if(x.status_id=14,ct1,0)) as 'Declined',
										sum(if(x.status_id=15,ct1,0)) as 'For Follow up',
										sum(if(x.status_id=16,ct1,0)) as 'Paid (Full)',
										sum(if(x.status_id=17,ct1,0)) as 'Paid (Partial)',
										sum(if(x.status_id=18,ct1,0)) as 'For Design Instructions',
										sum(if(x.status_id=19,ct1,0)) as 'Interested',
										sum(if(x.status_id=20,ct1,0)) as 'For Signing',
										sum(if(x.status_id=21,ct1,0)) as 'For Development',
										sum(if(x.status_id=22,ct1,0)) as 'For Prospecting'
										from 
										(
										select 
										count(a.status_id) as ct1,
										a.status_id
										from accounts a 
										left join users r on a.account_createdby = r.user_id 
										WHERE date(a.account_createdon) between date('".date('Y-m-d', $from)."') and date('".date('Y-m-d', $to)."') ".$condition." group by a.status_id
										) x";
							
							$result = mysql_query($query,connect());
							if (mysql_num_rows($result) > 0) {
							
								while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[0]."</td>";
									echo "<td>".$row[0]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[1]."</td>";
									echo "<td>".$row[1]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[2]."</td>";
									echo "<td>".$row[2]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[3]."</td>";
									echo "<td>".$row[3]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[4]."</td>";
									echo "<td>".$row[4]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[5]."</td>";
									echo "<td>".$row[5]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[6]."</td>";
									echo "<td>".$row[6]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[7]."</td>";
									echo "<td>".$row[7]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[8]."</td>";
									echo "<td>".$row[8]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[9]."</td>";
									echo "<td>".$row[9]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[10]."</td>";
									echo "<td>".$row[10]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[11]."</td>";
									echo "<td>".$row[11]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[12]."</td>";
									echo "<td>".$row[12]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[13]."</td>";
									echo "<td>".$row[13]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[14]."</td>";
									echo "<td>".$row[14]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[15]."</td>";
									echo "<td>".$row[15]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[16]."</td>";
									echo "<td>".$row[16]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[17]."</td>";
									echo "<td>".$row[17]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[18]."</td>";
									echo "<td>".$row[18]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[19]."</td>";
									echo "<td>".$row[19]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[20]."</td>";
									echo "<td>".$row[20]."</td>";
									echo "</tr>";
									echo "<tr class='grid-content'>";
									echo "<td>".$status_name[21]."</td>";
									echo "<td>".$row[21]."</td>";
									echo "</tr>";
							
									
								
								}
							}
						
						
						
						?>
				       
				</table>
			</div><!-- end of dashboard-chart -->
			
			<?php } ?>
			<div class="clear"></div>
			
        <script>
            jQuery('#tb1Leads').gvChart({
            chartType: 'AreaChart',
			
            gvSettings: {
                    vAxis: {title: 'No of Sales Leads'},
                    hAxis: {title: '<?php echo "From ".date('M d,Y',$from)." to ".date('M d,Y',$to); ?>',textPosition:'none'},
                    width: 300,
					pointSize: 3,
					legend: {position: 'bottom'},
                    height: 350
                    }
            });
			
			jQuery('#tb2Signed').gvChart({
            chartType: 'AreaChart',
            gvSettings: {
                    vAxis: {title: 'No of Signed Shops'},
                    hAxis: {title: '<?php echo "From ".date('M d,Y',$from)." to ".date('M d,Y',$to); ?>',textPosition:'none'},
					legend:{position: 'bottom'},
                    width: 300,
					pointSize: 3,
					
                    height: 350
                    }
            });
            jQuery('#tb3SignedCat').gvChart({
            chartType: 'PieChart',
            gvSettings: {
					title:'<?php echo "From ".date('M d,Y',$from)." to ".date('M d,Y',$to); ?>',
					chartArea:{left:10,width:250},
					legend: {position: 'none'},
                    width: 300,
                    height: 350,
				
                    }
            });
			 jQuery('#tb4SignedSource').gvChart({
            chartType: 'PieChart',
            gvSettings: {
					title:'<?php echo "From ".date('M d,Y',$from)." to ".date('M d,Y',$to); ?>',
					chartArea:{left:10,width:250},
					legend: {position: 'none'},
                    width: 300,
                    height: 350
                    }
            });
			

            
        </script>

    </div><!-- end of main-section -->
    
</div><!-- end of main-container -->

</body>
</html>

