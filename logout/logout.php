<?php

session_start();

require_once("../models/tblog.php");
$tblog = new TB_LOG();

require_once("../models/tbuser.php");
$tbuser = new TB_USER();



$userid = $_SESSION['user_id'];
if ($userid) {
    $datauser = array(
        'log' => 0
    );

    $tbuser->Updateusers($datauser, "user_id=$userid");



    $datalog = array(
        'user_id' => $_SESSION['user_id'],
        'audit_act' => 'Log Out',
        'ip_add' => $_SESSION['ipaddniya']
//            'audit_log_time' => $datetime
    );

    $tblog->Insertaudit_log($datalog);

    session_unset();
    session_destroy();
}
header('Location: /88dbphcrm/logout/');
exit;
?>