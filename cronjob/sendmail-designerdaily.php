<?php
ob_start();

require_once("../models/tbsendmail.php");
$tbsendmail = new TB_SENDMAIL();
require_once("../models/tbdesigner.php");
$tbdesigner = new TB_DESIGNER();
$DailyDesigner = $tbsendmail->SelectDailyDesignerReportv2New();



$Designer = $tbdesigner->SelectDesignerLoadNewShops();
$Designerrev = $tbdesigner->SelectDesignerLoadOldShops();
$Designerdone = $tbdesigner->SelectDesignerLoadShopsDone();
$Designertotaldone = $tbdesigner->SelectDesignerLoadShopsDoneTotalDaily();
if ($Designertotaldone) {

    foreach ($Designertotaldone as $val) {
        $data = array(
            'designer_id' => $val['user_id'],
            'designer' => $val['designer'],
            'total_shops' => $val['TotalShops'] ? $val['TotalShops'] : '0'
        );
        $id = $tbdesigner->Insertdesignertotalshops($data);
    }
    //echo $c;
}

if ($Designerdone) {

    foreach ($Designerdone as $val) {
        $data = array(
            'designer_id' => $val['user_id'],
            'designer' => mysql_real_escape_string($val['designer']),
            'shop' => mysql_real_escape_string($val['account_name']),
            'package' => mysql_real_escape_string($val['package']),
            'days' => $val['DaysDone']
        );
        $id = $tbdesigner->Insertdesignershopsdonedetails($data);
    }
    //echo $c;
}

if ($Designerrev) {

    foreach ($Designerrev as $val) {
        $data = array(
            'designer_id' => $val['user_id'],
            'designer' => mysql_real_escape_string($val['designer']),
            'shop' => mysql_real_escape_string($val['account_name']),
            'package' => mysql_real_escape_string($val['package']),
            'days' => $val['days']
        );
        $id = $tbdesigner->Insertcurrentworkingrevshops($data);
    }
    //echo $c;
}

if ($Designer) {

    foreach ($Designer as $val) {
        $data = array(
            'designer_id' => $val['user_id'],
            'designer' => mysql_real_escape_string($val['designer']),
            'shop' => mysql_real_escape_string($val['account_name']),
            'package' => mysql_real_escape_string($val['package']),
            'days' => $val['days']
        );
        $id = $tbdesigner->Insertcurrentworkingnewshops($data);
    }
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Daily Designer Report</h2>

<h4>Finished Shops by Designer</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class=" ">Total Shops</td>


    </tr>
    <?php if ($Designertotaldone)
        foreach ($Designertotaldone as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line1"> <span <?php if ($val['TotalShops'] < 1): ?>style="color: red;"<?php endif; ?>><?php echo $val['TotalShops'] ? $val['TotalShops'] : '0'; ?></span></td>

            </tr>
            <?php
            $totalTotalShops += $val['TotalShops'];

        endforeach;
    ?>

    <tr>
        <td class="line" width="200"><b>Total</b></td>
        <td class="line1"><b><?php echo $totalTotalShops ?></b></td>

    </tr>

</table>
<h4>Details of finished shops by designer</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerdone)
        foreach ($Designerdone as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['DaysDone']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>



<h4>Currently working on NEW Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designer)
        foreach ($Designer as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>

<h4> Currently working on REVISED Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerrev)
        foreach ($Designerrev as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>



<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?php
//$fn = "Temporary_Live-report_" . date('Y-m-d-G-i') . ".xls";
//system("/usr/bin/php createexcel/create_excel_tl.php >> $fn"); //$fn is your new filename


if (!$DailyDesigner)
    exit();
require_once('PHPMailer/class.phpmailer.php');

try {
    $mail = new PHPMailer(true); //New instance, with exceptions enabled
//    $body = file_get_contents('contents.html');
    $body = preg_replace('/\\\\/', '', $body); //Strip backslashes

    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPAuth = false;                  // enable SMTP authentication
    $mail->Port = 25;                    // set the SMTP server port
    $mail->Host = "localhost"; // SMTP server
//    $mail->Username = "";     // SMTP server username
//    $mail->Password = "";            // SMTP server password

    $mail->IsSendmail();  // tell the class to use Sendmail

    $mail->AddReplyTo("lawrence.c@88db.com.ph", "Lawrence Cruz");

    $mail->From = "ADMINISTRATOR";
    $mail->FromName = "Admin 88dbcrm";
    $mail->AddAddress('lawrence.c@88db.com.ph', 'Second Person');
    
    
//    $mail->AddAddress('Emir.E@88db.com.ph', 'Fifth Person');
//    $mail->AddCC('charles.c@88db.com.ph', 'First Person');
//    $mail->AddCC('lawrence.c@88db.com.ph', 'Second Person');
//    $mail->AddCC('Marc.C@88db.com.ph', 'Third Person');


    $mail->Subject = "Daily Designer Report";


    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
    $mail->WordWrap = 80; // set word wrap

    $mail->MsgHTML($body);

    $mail->IsHTML(true); // send as HTML
//    $mail->AddAttachment($fn);
    $mail->Send();
    echo 'Message has been sent.';
} catch (phpmailerException $e) {
    echo $e->errorMessage();
}
?>