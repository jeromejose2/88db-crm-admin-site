<?php
ob_start();
require_once ("../ctrl/ctrl_sendemail.php");
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Monthly Sales Report</h2>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">AE</td>
        <td class="line2"># Calls</td>
        <td class="line2"># Meetings</td>
        <td ># Closed(Signed for payment)</td>
    </tr>
    <?php if ($monthlysales)
        foreach ($monthlysales as $val): ?>
            <tr>
                <td class="line"><?php echo $val['AE']; ?></td>
                <td class="line" ><span <?php if ($val['calls'] < 500): ?>style="color: red;"<?php endif; ?>><?php echo $val['calls'] ? $val['calls'] : '0'; ?></span></td>
                <td  class="line" ><span <?php if ($val['Meetings'] < 20): ?>style="color: red;"<?php endif; ?>><?php echo $val['Meetings'] ? $val['Meetings'] : '0'; ?></span></td>
                <td  class="line1" ><span <?php if ($val['Closed'] < 5): ?>style="color: red;"<?php endif; ?>><?php echo $val['Closed'] ? $val['Closed'] : '0'; ?></a></td>

            </tr>
            <?php
            $totalcalls += $val['calls'];
            $totalMeetings += $val['Meetings'];
            $totalClosed += $val['Closed'];
        endforeach;
    ?>
    <tr>
        <td class="line" width="200"><b>Total</b></td>
        <td class="line"><b><?php echo $totalcalls ?></b></td>
        <td class="line"><b><?php echo $totalMeetings ?></b></td>
        <td class="line1" ><b><?php echo $totalClosed ?></b></td>
    </tr>
</table>
<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?php
//$fn = "Temporary_Live-report_" . date('Y-m-d-G-i') . ".xls";
//system("/usr/bin/php createexcel/create_excel_tl.php >> $fn"); //$fn is your new filename

if (date('Y-m-d', strtotime('today')) == date('Y-m-d', strtotime('last day of this month'))) {
    if (!$monthlysales)
        exit();
    require_once('PHPMailer/class.phpmailer.php');

    try {
        $mail = new PHPMailer(true); //New instance, with exceptions enabled
//    $body = file_get_contents('contents.html');
        $body = preg_replace('/\\\\/', '', $body); //Strip backslashes

        $mail->IsSMTP();                           // tell the class to use SMTP
        $mail->SMTPAuth = false;                  // enable SMTP authentication
        $mail->Port = 25;                    // set the SMTP server port
        $mail->Host = "localhost"; // SMTP server
//    $mail->Username = "";     // SMTP server username
//    $mail->Password = "";            // SMTP server password

        $mail->IsSendmail();  // tell the class to use Sendmail

        $mail->AddReplyTo("lawrence.c@88db.com.ph", "Lawrence Cruz");

        $mail->From = "ADMINISTRATOR";
        $mail->FromName = "Admin 88dbcrm";




//        $mail->AddAddress('lawrence.c@88db.com.ph', 'First Person');

        $mail->AddAddress('Chris.B@88db.com.ph', 'Fifth Person');
        $mail->AddAddress('Angelo.D@88db.com.ph', 'Fifth Person');
        $mail->AddCC('charles.c@88db.com.ph', 'First Person');
        $mail->AddCC('lawrence.c@88db.com.ph', 'Second Person');
        $mail->AddCC('Marc.C@88db.com.ph', 'Third Person');

        $mail->Subject = "Monthly Sales Report";


        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->WordWrap = 80; // set word wrap

        $mail->MsgHTML($body);

        $mail->IsHTML(true); // send as HTML
//    $mail->AddAttachment($fn);
        $mail->Send();
        echo 'Message has been sent.';
    } catch (phpmailerException $e) {
        echo $e->errorMessage();
    }
} else {
    echo 'Not the last day of the month';
}
?>