<?php
require_once("../models/tbdesigner.php");
$tbdesigner = new TB_DESIGNER();
$DesignerMonthly = $tbdesigner->SelectDesignerLoadShopsDoneMonthly();
$Designerdone = $tbdesigner->SelectDesignerLoadShopsDoneDetailMonthly();
$Designer = $tbdesigner->SelectDesignerLoadNewShops();
$Designerrev = $tbdesigner->SelectDesignerLoadOldShops();
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Monthly Designer Report</h2>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class=" ">Total Shops</td>


    </tr>
    <?php if ($DesignerMonthly)
        foreach ($DesignerMonthly as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line1"> <span <?php if ($val['total'] < 1): ?>style="color: red;"<?php endif; ?>><?php echo $val['total'] ? $val['total'] : '0'; ?></span></td>

            </tr>
        <?php endforeach; ?>
</table>
<h4>Details of finished shops by designer</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>
        <td class="line2">Date finished</td>
        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerdone)
        foreach ($Designerdone as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['shop']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>
                <td class="line"> <?php echo substr($val['data_createdon'], 0, 10); ?></td>
                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>

<h4>Currently working on NEW Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designer)
        foreach ($Designer as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>

<h4> Currently working on REVISED Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerrev)
        foreach ($Designerrev as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>
