<?php

//include_once("classes/class.export_excel.php");
include_once("connection.php");
//include_once("functions/functions.php");
include_once("class.export_excel.php");



date_default_timezone_set('Asia/Manila');

//Signed(For payment)

//$query .= " SELECT a.account_id, a.account_name, b.audit_createdon AS date";
//$query .= " FROM accounts a, audit b";
//$query .= " WHERE status_id = 6";
//$query .= " AND a.account_id = b.account_id";
//$query .= " AND a.status_id = b.after_status_id";
//$query .= " AND DATE_ADD( b.audit_createdon, INTERVAL 2 DAY ) > NOW( ) ";

$query .= " SELECT a.account_id, a.account_name, max(b.audit_createdon) AS date";
$query .= " FROM accounts a, audit b";
$query .= " WHERE status_id = 6";
$query .= " AND a.account_id = b.account_id";
$query .= " AND a.status_id = b.after_status_id";
$query .= " AND ( UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( b.audit_createdon )) / ( 60 *60 ) >= 48.000";
$query .= "  GROUP BY a.account_name";
$query .= "  ORDER BY a.account_name ASC";

$result = mysql_query($query, connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}

$heads = array("ID", "Account Name", "Date");
$fn = "For_Client_Approval-report_" . date('Y-m-d-G-i') . ".xls";

//Signed(For payment)
//create the instance of the exportexcel format
$excel_obj = new ExportExcel("$fn");

//setting the values of the headers and data of the excel file 
//and these values comes from the other file which file shows the data
$excel_obj->setHeadersAndValues($heads, $account_table);

//now generate the excel file with the data and headers set
$excel_obj->GenerateExcelFile();
?>
