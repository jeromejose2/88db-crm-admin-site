<?php
require_once("../models/tbsendmail.php");
$tbsendmail = new TB_SENDMAIL();
$DailyDesigner = $tbsendmail->SelectDailyDesignerReportv2New();

if (date('Y-m-d', strtotime('today')) == date('Y-m-d', strtotime('last day of this month'))) {
    $test = 'now is the last day of the month';
} else {
    $test = 'not the last day of the month';
}
echo 'today: ' . date('Y-m-d', strtotime('today')) . ' last day of the month : ' . date('Y-m-d', strtotime('last day of this month')) . ' ' . $test . ' date :' . date('Y-m-d');
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Daily Designer Report</h2>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2"># New Shop</td>
        <td class="line2"># Old Shop (Revised)</td>
        <td class="line1"># Total Shops</td>


    </tr>
    <?php if ($DailyDesigner)
        foreach ($DailyDesigner as $val): ?>
            <tr>
                <td class="line"><?php echo $val['Designer']; ?></td>
                <td class="line"> <?php echo $val['NewShop'] ? $val['NewShop'] : '0'; ?></td>
                <td class="line"> <?php echo $val['OldShop'] ? $val['OldShop'] : '0'; ?></td>
                <td class="line1"><span <?php if ($val['Total'] < 1): ?>style="color: red;"<?php endif; ?>><?php echo $val['Total'] ? $val['Total'] : '0'; ?></span></td>
            </tr>
        <?php endforeach; ?>
</table>



