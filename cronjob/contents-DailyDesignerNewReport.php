<?php
require_once("../models/tbdesigner.php");
$tbdesigner = new TB_DESIGNER();
$Designer = $tbdesigner->SelectDesignerLoadNewShops();
$Designerrev = $tbdesigner->SelectDesignerLoadOldShops();
$Designerdone = $tbdesigner->SelectDesignerLoadShopsDone();
$Designertotaldone = $tbdesigner->SelectDesignerLoadShopsDoneTotalDaily();

//if ($Designertotaldone) {
//
//    foreach ($Designertotaldone as $val) {
//        $data = array(
//            'designer_id' => $val['user_id'],
//            'designer' => mysql_real_escape_string($val['designer']),
//            'total_shops' => $val['TotalShops'] ? $val['TotalShops'] : '0'
//        );
//        $id = $tbdesigner->Insertdesignertotalshops($data);
//    }
//    //echo $c;
//}

//if ($Designerdone) {
//
//    foreach ($Designerdone as $val) {
//        $data = array(
//            'designer_id' => $val['user_id'],
//            'designer' => mysql_real_escape_string($val['designer']),
//            'shop' => mysql_real_escape_string($val['account_name']),
//            'package' => mysql_real_escape_string($val['package']),
//            'days' => $val['DaysDone']
//        );
//        $id = $tbdesigner->Insertdesignershopsdonedetails($data);
//    }
//    //echo $c;
//}
//
//if ($Designerrev) {
//
//    foreach ($Designerrev as $val) {
//        $data = array(
//            'designer_id' => $val['user_id'],
//            'designer' => mysql_real_escape_string($val['designer']),
//            'shop' => mysql_real_escape_string($val['account_name']),
//            'package' => mysql_real_escape_string($val['package']),
//            'days' => $val['days']
//        );
//        $id = $tbdesigner->Insertcurrentworkingrevshops($data);
//    }
//    //echo $c;
//}
//
//if ($Designer) {
//
//    foreach ($Designer as $val) {
//        $data = array(
//            'designer_id' => $val['user_id'],
//            'designer' => mysql_real_escape_string($val['designer']),
//            'shop' => mysql_real_escape_string($val['account_name']),
//            'package' => mysql_real_escape_string($val['package']),
//            'days' => $val['days']
//        );
//        $id = $tbdesigner->Insertcurrentworkingnewshops($data);
//    }
//    //echo $c;
//}

$b = '09-2498';
$c = substr($b ,-4);

$d = $c + 1;

$a = substr(date('Y'),2,4);

$generate = $a .'-' . $d;

echo $generate;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Daily Designer Report</h2>

<h4>Finished Shops by Designer</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class=" ">Total Shops</td>


    </tr>
    <?php if ($Designertotaldone)
        foreach ($Designertotaldone as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line1"> <span <?php if ($val['TotalShops'] < 1): ?>style="color: red;"<?php endif; ?>><?php echo $val['TotalShops'] ? $val['TotalShops'] : '0'; ?></span></td>

            </tr>
        <?php endforeach; ?>
</table>
<h4>Details of finished shops by designer</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerdone)
        foreach ($Designerdone as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['DaysDone']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>



<h4>Currently working on NEW Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designer)
        foreach ($Designer as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>

<h4> Currently working on REVISED Shops</h4>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Designer</td>
        <td class="line2">Shop</td>
        <td class="line2">Package</td>

        <td class="">No. of days</td>


    </tr>
    <?php if ($Designerrev)
        foreach ($Designerrev as $val): ?>
            <tr>
                <td class="line"><?php echo $val['designer']; ?></td>
                <td class="line"> <?php echo $val['account_name']; ?></td>
                <td class="line"> <?php echo $val['package']; ?></td>

                <td class="line1" align="center"> <?php echo $val['days']; ?> </td>
            </tr>
        <?php endforeach; ?>
</table>





