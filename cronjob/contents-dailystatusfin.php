<?php
require_once("../models/tbsendmail.php");
$tbsendmail = new TB_SENDMAIL();
$dailyfinance = $tbsendmail->Selectdailyfinancereport();

if ($dailyfinance) {
    foreach ($dailyfinance as $fin) {

        $data = array(
            'finance' => $fin['finance'],
            'Stage_to_partial' => $fin['partial'] ? $val['partial'] : '0',
            'Stage_to_full' => $fin['full'] ? $val['full'] : '0'
        );
        $id = $tbsendmail->Insertfinancereport($data);
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=5; IE=8; IE=9">
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Daily Finance History Report</h2>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="200">Finance</td>
        <td class="line2">Partial</td>
        <td class="line2">Full</td>


    </tr>
    <?php if ($dailyfinance)
        foreach ($dailyfinance as $val): ?>
            <tr>
                <td class="line"><?php echo $val['finance']; ?></td>
                <td class="line"><?php echo $val['partial'] ? $val['partial'] : '0'; ?></td>
                <td class="line"><?php echo $val['full'] ? $val['full'] : '0'; ?></td>
            </tr>
        <?php endforeach; ?>
</table>



