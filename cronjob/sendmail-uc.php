<?php
ob_start();
require_once ("../ctrl/ctrl_sendemail.php");
?>
<style>
    table {border:1px solid #000;}
    td.line {border-right: 1px solid #000;padding-right: 2px;margin-right: 5px;border-top: 1px solid #000;}
    td.line1{border-top: 1px solid #000;}
    td.line2{border-right: 1px solid #000;}
</style>
<h2>Please update the following accounts:</h2>
<table cellspacing="0"  >

    <tr>
        <td class="line2" width="80">ID</td>
        <td class="line2">ACCOUNT NAME</td>
        <td   width="150">DATE</td>
    </tr>
    <?php if ($uc)
        foreach ($uc as $val): ?>
            <tr>
                <td class="line"><?php echo $val[account_id]; ?></td>
                <td class="line"><?php echo $val[account_name]; ?></td>
                <td  class="line1"><?php echo $val[date]; ?></td>
            </tr>
        <?php endforeach; ?>
</table>
<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?php
$fn = "Under_Construction-report_" . date('Y-m-d-G-i') . ".xls";
system("/usr/bin/php createexcel/create_excel_uc.php >> $fn"); //$fn is your new filename

if (!$uc)
    exit();
require_once('PHPMailer/class.phpmailer.php');

try {
    $mail = new PHPMailer(true); //New instance, with exceptions enabled
//    $body = file_get_contents('contents.html');
    $body = preg_replace('/\\\\/', '', $body); //Strip backslashes

    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPAuth = false;                  // enable SMTP authentication
    $mail->Port = 25;                    // set the SMTP server port
    $mail->Host = "localhost"; // SMTP server
//    $mail->Username = "";     // SMTP server username
//    $mail->Password = "";            // SMTP server password

    $mail->IsSendmail();  // tell the class to use Sendmail

    $mail->AddReplyTo("lawrence.c@88db.com.ph", "Lawrence Cruz");

    $mail->From = "ADMINISTRATOR";
    $mail->FromName = "Admin 88dbcrm";

    //    $to = "charles.c@88db.com.ph" . ', '; // note the comma
//    $to .= "lawrence.c@88db.com.ph";
//
////    $to = "lawrence.c@88db.com.ph";
//
//    $mail->AddAddress($to);
    $mail->AddAddress('gladys.s@88db.com.ph', 'First Person');
    $mail->AddAddress('cris.t@88db.com.ph', 'Second Person');
    $mail->AddAddress('Gaile.O@88dbh.com.ph', 'Third Person');
    $mail->AddCC('charles.c@88db.com.ph', 'Fourth Person');
    $mail->AddCC('lawrence.c@88db.com.ph', 'Fifth Person');
    $mail->AddCC('Marc.C@88db.com.ph', 'Sixth Person');
//    $mail->AddAddress('cris.t@88db.com.ph', 'Cris T');
//    $mail->AddAddress('Gladys.S@88db.com.ph', 'Gladys S');


    $mail->Subject = "ACCOUNT UPDATE: Under Construction (Delayed)";

    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
    $mail->WordWrap = 80; // set word wrap

    $mail->MsgHTML($body);

    $mail->IsHTML(true); // send as HTML
//    $mail->AddAttachment($fn);
    $mail->Send();
    echo 'Message has been sent.';
} catch (phpmailerException $e) {
    echo $e->errorMessage();
}
?>