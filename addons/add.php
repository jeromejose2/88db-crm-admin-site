<?php
include_once ('functions/functions.php');
include_once ('functions/connection.php');


session_start();
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];
$contract_number = $_REQUEST['cid'];

$isPostBack = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $isPostBack = true;
}

if ($isPostBack) {

    $contract_number = $_POST['contract_number'];
    $account_id = $_POST['account_id'];
    $payment_amount = $_POST['payment_amount'];
    $description = $_POST['addon_desc'];
    mysql_connect('localhost', 'root', '');
    mysql_select_db("88dbphcrm");

//        aken
    $query = "SELECT a.* FROM accounts WHERE a.account_id = $account_id";
    $result = mysql_query($query);
    $num = mysql_numrows($result);
    if ($num > 0) {
        $account_contractamount = mysql_result($result, 0, 'account_contractamount') == "" ? 0 : mysql_result($result, 0, 'account_contractamount');
    }
//        aken


    $query = "INSERT INTO addon (account_id, addon_amount,  addon_createdby,  addon_createdon, addon_description) 
			VALUES ('" . mysql_real_escape_string($account_id) . "', '" . mysql_real_escape_string($payment_amount) . "', '$userid', now(), '" . mysql_real_escape_string($description) . "')";
    $result = mysql_query($query, connect());



    $remark = "Added Addon Php " . number_format($payment_amount) . " amount";
    $insert_query = "INSERT INTO remarks (remark_remarks, remark_type, remark_uid, remark_createdby) VALUES ('$remark','1', $account_id, $userid)";
    $result = mysql_query($insert_query, connect());

    $newaccount_contractnumber = $account_contractnumber + $payment_amount;
    $queryact = "UPDATE accounts SET account_contractnumber = '$newaccount_contractnumber',  account_modifiedby = $userid , account_modifiedon = now() WHERE account_id = $account_id";
    $resultact = mysql_query($queryact, connect());

    if ($_POST['addon_from'] == 1) {

        header('Location: /88dbphcrm/accounts/edit.php?account_id=' . $account_id);
        //exit;
    } else {
        header('Location: /88dbphcrm/addons/');
        //exit;	
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
        <title>Add Account</title>

        <?php include '../header.php'; ?>

    <div class="main-section">
        <form name="form1" id="form1" action="add.php" method="post">
            <div class="commands">
                <div class="head-label">
                    <h2>Add New Addon</h2>
                </div><!-- end of add new account -->

                <ul>
                    <?php if (!empty($contract_number)) { ?>
                        <li><a id="cancel_btn" class="link-button gray" >Cancel</a></li>
                    <?php } else { ?>
                        <li><a class="link-button gray" href='/88dbphcrm/addons/'>Cancel</a></li>
                    <?php } ?>
                    <li><input type="submit" value="Submit"/></li>
                </ul>
            </div><!-- end of grid-commands -->

            <div class="gen-section">

                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="grid-head">*Contract Number:</td>
                        <td>&nbsp;</td>
                        <td>
                            <?php if (!empty($contract_number)) { ?>

                                <input type="text" name="contract_number" id="contract_number" class="required"  value="<?php echo $contract_number; ?>" readonly="readonly" />

                                <?php
                            } else {
                                ?>
                                <input type="text" name="contract_number" id="contract_number" class="required"  />

                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Account Name:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="account_name" id="account_name" class="required" placeholder="No selected Account" autocomplete="off" readonly="readonly" />
                            <input type="hidden" name="account_id" id="account_id" class="required" autocomplete="off" readonly="readonly" />


                            <input type="hidden" name="addon_from" id="addon_from" 
                                   value="<?php if (!empty($_REQUEST['cid']))
                                echo '1'; ?> " 

                                   readonly="readonly" />


                        </td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Addon Description:</td>
                        <td>&nbsp;</td>
                        <td><textarea cols="25" rows="3" name="addon_desc" id="addon_desc" ></textarea></td>
                    </tr>
                    <tr>
                        <td class="grid-head">*Addon Amount:</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="payment_amount" maxlength="9" id="payment_amount" class="required"/></td>
                    </tr>



                    <tr class="edit-account-footer">
                        <td colspan="4">

                        </td>
                    </tr>
                </table>


            </div><!-- end of gen-section -->
        </form>
    </div><!-- end of main-section -->

    <script type="text/javascript">
        $(document).ready(function(){

            $("#payment_amount").numeric();
		
            $("#contract_number").numeric();

            $("#form1").validate();
	
            $('#contract_number').change(function() {
		 
                $('#contract_number').trigger("keyup");
				
            });
  
            $("#contract_number").keyup(function() 
            { 
 
					 

                if($.trim($("#contract_number").val()) == '')
                {
					 
                    $("#account_name").val("");
							
                }
                else
                {
					 
                    $.ajax({
                        type: "POST",
                        dataType: "html",
                        url: 'functions/ajax.search.account.php',
                        timeout: 20000,
                        data:{data: $('#contract_number').val()},
                        success: function(html)
                        {
                            var account = html;
                            var account_info = account.split("*");
						
							
							
                            $("#account_name").val(account_info[0]);	
                            $("#account_id").val(account_info[1]);	
							
                        },
							
                    });
					  
                }
            });
  
  
        });
	
        $(function() {
	
            if($.trim($("#contract_number").val()) == '')
            {
					 
                $("#account_name").val("");
							
            }
            else
            {
					 
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: 'functions/ajax.search.account.php',
                    timeout: 20000,
                    data:{data: $('#contract_number').val()},
                    success: function(html)
                    {
                        var account = html;
                        var account_info = account.split("*");
							
							
                        if($('#cancel_btn').length) {
                            $("#cancel_btn").attr('href', '/88dbphcrm/accounts/edit.php?account_id='+ account_info[1]);
                        }
							
						
							
                        $("#account_name").val(account_info[0]);	
                        $("#account_id").val(account_info[1]);	
						
                    },
							
                });
					  
            }
	
	
        });
	
    </script>
</body>
</html>