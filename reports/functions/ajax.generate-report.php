<?php
include_once ('connection.php');
include_once ('functions.php');

	$user = $_POST['userid'];
	$status = $_POST['status'];
	$source = $_POST['source'];

	$date = $_POST['date'];
	$start = $_POST['start'];
	$end = $_POST['end'];
	$condition = GetCondition($user,$status,$source,$date,$start,$end);

	$report_type = $_POST['report'];

switch($report_type)
{
case Leads:
$query = "SELECT a.account_id,
				a.account_name, 		
				a.account_contactperson,
				a.account_telephone,
				a.account_mobile,
				a.account_fax,
				a.account_email,
				a.account_address,
				c.category_name,
				sr.source_name,
				s.status_name,
				concat(r.user_firstname,' ',r.user_lastname) AS createdby,
				a.account_createdon,
				concat(r2.user_firstname,' ',r2.user_lastname) as modifiedby,
				a.account_modifiedon
    FROM accounts a 
	LEFT JOIN statuses s ON a.status_id = s.status_id 
	LEFT JOIN categories c ON a.category_id = c.category_id 
	LEFT JOIN sources sr ON a.source_id = sr.source_id 
	LEFT JOIN users r ON a.account_createdby = r.user_id 
	LEFT JOIN users r2 ON a.account_modifiedby = r2.user_id ".$condition." ORDER BY a.account_createdon DESC";
$result = mysql_query($query,connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}


$heads=array("Account ID","Account Name","Contact Person","Telephone #","Mobile #","Fax #","Email Address","Adress","Category","Source","Status","Created By","Created on","Modified By","Modified on");
//code to download the data of report in the excel format
	
break;

Case Pipeline:
$query = "SELECT a.account_id,
				a.account_name, 		
				a.account_email,
				c.category_name,
				s.status_name,
				pa.package_name,
				a.account_memberid,
				a.account_brno,
				a.account_adid,
				
							case a.shop_type
									when '0' then ''
									when '1' then '88DB'
									when '2' then 'Open Rice'
							end,
							
				a.shop_url,
				a.shop_domain,
							case a.shop_domaintype
							
									when '0' then ''
									when '1' then 'For Request'
									when '2' then 'Pending'
									when '3' then 'Activated'
							end,
							case a.shop_domainstatus
									when '0' then ''
									when '1' then 'For Request'
									when '2' then 'Pending'
									when '3' then 'Activated'
							end,
							
							case a.shop_cmsstatus
									when '0' then ''
									when '1' then 'For Request'
									when '2' then 'Pending'
									when '3' then 'Activated'
							end,
							
				concat(de.user_firstname,' ',de.user_lastname) AS designer,
				concat(ed.user_firstname,' ',ed.user_lastname) AS editor,
				a.account_contractnumber,
				a.account_contractamount,
				concat(r.user_firstname,' ',r.user_lastname) AS createdby,
				a.account_createdon,
				concat(r2.user_firstname,' ',r2.user_lastname) as modifiedby,
				a.account_modifiedon
				
    FROM accounts a 
	LEFT JOIN users ed ON a.shop_editor = ed.user_id
	LEFT JOIN users de ON a.shop_designer = de.user_id
	LEFT JOIN packages pa ON a.package_id = pa.package_id
	LEFT JOIN statuses s ON a.status_id = s.status_id 
	LEFT JOIN categories c ON a.category_id = c.category_id 
	LEFT JOIN sources sr ON a.source_id = sr.source_id 
	LEFT JOIN users r ON a.account_createdby = r.user_id 
	LEFT JOIN users r2 ON a.account_modifiedby = r2.user_id ".$condition." ORDER BY a.account_createdon DESC";
$result = mysql_query($query,connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}



$heads=array("Account ID","Account Name","Email Address","Category","Status","Package Type","Member ID","B.R. #","Ad ID","Shop Type","Shop URL","Shop Domain","Domain Type","Domain Status","CMS Status","Assigned Designer","Assigned Editor","Contract #","Contract Amount","Created By","Created on","Modified By","Modified on");
//code to download the data of report in the excel format
	
break;

Case Funnel:
$query = "SELECT 
			concat(r.user_firstname,' ',r.user_lastname) AS createdby, 
			a.account_id,
			a.account_name,
			c.category_name,
			a.shop_url,  
			a.account_contractnumber,
			pd.latest_payment,
			a.account_contractamount,
			pl.paid_amount,
			(a.account_contractamount - pl.paid_amount) as remaining_payment,
			s.status_name
		FROM accounts a 
		LEFT JOIN 
		(
			select sum(payment_amount) as paid_amount, account_id from payments group by account_id
		) pl ON a.account_id = pl.account_id 
		LEFT JOIN 
		(
			 SELECT MAX(payment_createdon) as latest_payment,account_id  from payments group by account_id 
		) pd ON a.account_id = pd.account_id 
		LEFT JOIN categories c ON a.category_id = c.category_id 
		LEFT JOIN users r ON a.account_createdby = r.user_id 
		LEFT JOIN statuses s ON a.status_id = s.status_id 
".$condition."

	
	ORDER BY a.account_createdon DESC
 ";
$result = mysql_query($query,connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}



$heads=array("Created by","Account ID","Account Name","Category","Shop URL","Contract Number","Last Payment","Contract Amount","Total Paid Amount","Account Balance","Status");
//code to download the data of report in the excel format
	
break;

Case "Running Production":
if (!empty($condition)) $q = $condition. "and a.account_paid=1";
else $q = " Where a.account_paid = 1";
$query = "select 
				a.account_id,
				a.account_name,
				a.shop_url,
				concat(r.user_firstname,' ',r.user_lastname) AS designer, 
				u.audit_createdon as Start_Date,
				p.audit_createdon as End_Date, 

				((Datediff(date(p.audit_createdon),date(u.audit_createdon)) * 24) + (TIME_to_SEC(Timediff(TIME_FORMAT(time(p.audit_createdon),'%r'),TIME_FORMAT(time(u.audit_createdon),'%r'))) / 3600)) as Hours
				from accounts a


				LEFT JOIN
				(
				select audit_createdon, account_id from audit where before_status_id = 21 and after_status_id = 3 order by audit_id  DESC LIMIT 0,1
				) u on a.account_id = u.account_id
				LEFT JOIN
				(
				select audit_createdon, account_id from audit where before_status_id = 3 and after_status_id = 4 order by audit_id DESC LIMIT 0,1
				) p on a.account_id = p.account_id

				LEFT JOIN users r ON a.shop_designer = r.user_id ".$q."
 ";
$result = mysql_query($query,connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}



$heads=array("Account ID","Account Name","Shop URL","Assigned Designer","Start Date","End Date","Diff (Hours)");
//code to download the data of report in the excel format
	
break;


Case "Audit":
		switch($date)
		{
			case 1:			
				 $date_query = " WHERE DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP())";
	
			break;
			
			case 2:
				$date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('".date('Y-m-d',strtotime('monday this week'))."') AND DATE('".date('Y-m-d',strtotime('friday this week'))."') ";
			
			break;
			
			case 3:
				$date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('".date('Y-m-d',strtotime('first day of this month'))."') AND DATE('".date('Y-m-d',strtotime('last day of this month'))."') ";
			
			break;
			
			case 4:
				$date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('".$start."') AND DATE('".$end."') ";
			
			break;
		}




$query = "SELECT
				au.audit_createdon,
				ac.account_name,
				fstat.status_name,
				tstat.status_name,
				concat(u.user_firstname,' ',u.user_lastname)
				FROM audit au
				LEFT JOIN statuses fstat ON au.before_status_id = fstat.status_id
				LEFT JOIN statuses tstat ON au.after_status_id = tstat.status_id
				LEFT JOIN accounts ac ON au.account_id = ac.account_id
				LEFT JOIN users u ON au.audit_createdby = u.user_id
				".$date_query." ORDER BY au.audit_id
 ";
$result = mysql_query($query,connect());


while ($row = mysql_fetch_row($result)) {
    $account_table [] = $row;
}



$heads=array("Date","Account Name","From Status","To Status","User");

	
break;
}

		$table .="<table cellpadding='0' class='main-grid' cellspacing='0' >";
		$table .="<tr class='grid-head'>";
			for ($i=0;$i<count($heads);$i++)
			{
				$table .="<td>".$heads[$i]."</td>";
			}
				$table .="</tr>";
			
		for($i=0;$i<sizeof($account_table);$i++) 
 		{ 
 			$table .="<tr class='grid-content'>";
 			foreach($account_table[$i] as $value) 
			{ 
 				$table .="<td>".$value."</td>"; 
 			} 
 			$table .="</tr>";
 		}//end of the while 
		
		
		
		$table .="</table";




echo $table;

?>