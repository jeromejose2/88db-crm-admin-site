<?php
include_once("functions/functions.php");
include_once("functions/connection.php");

session_start();
$_SESSION['title_page'] = "Reports";
if (isset($_SESSION['isLoggedIn'])) {
    if ($_SESSION['isLoggedIn'] == 0) {
        header('Location: /88dbphcrm/error.php?err=2');
        exit;
    }
} else {
    header('Location: /88dbphcrm/error.php?err=2');
    exit;
}
$userid = $_SESSION['user_id'];
$role_id = $_SESSION['role_id'];
$dep_id = $_SESSION['department_id'];

mysql_connect('localhost', 'root', '');
mysql_select_db('88dbphcrm') or die("Unable to select database");

$user = $_GET['user_id'];
$dept = $_GET['department'];
$status = $_GET['status_id'];
$source = $_GET['source_id'];
$date = $_GET['date_report'];
$start_date = $_GET['fdate'];
$end_date = $_GET['edate'];
$report_type = $_GET['report_type'];


$condition = GetCondition($user, $status, $source, $date, $start_date, $end_date);

$total_pages = GetSumData($condition, $report_type);
$req_limit = "25";

$page = $_REQUEST['page'];
if ($limit)
    $limit = $req_limit;    //how many items to show per page
else
    $limit = 25;

if ($page)
    $start = ($page - 1) * $limit;    //first item to display on this page
else
    $start = 0;

/* Setup page vars for display. */
/*
if ($page == 0)
    $page = 1;     //if no page var is given, default to 1.
*/
    
//next page is page + 1
$lastpage = ceil($total_pages / $limit);  //lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;      //last page minus 1

$table = GetData($condition, $start, $limit, $report_type, $start_date, $end_date);
$department = FillDeptDropDownBox($dept);
$user_sales = FillUserDropDownBox($user);
$sourceoptions = FillSourceDropDownBox($source);
$statusoptions = FillStatusDropDownBox($_REQUEST['status_id']);


$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as designer FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 2 ORDER BY designer ASC ";
$result = mysql_query($query);
$shop_designer = $_GET['shop_designer'];
$designers = "";
while ($row = mysql_fetch_array($result)) {
    $designerid = $row["user_id"];
    $designername = $row["designer"];
    if ($designerid == $shop_designer) {
        $designers .= '<option value="' . $designerid . '" selected="selected">' . $designername . "</option>";
    } else {
        $designers .= '<option value="' . $designerid . '">' . $designername . "</option>";
    }
}

$query = "SELECT user_id, concat(user_firstname,' ',user_lastname) as editorname FROM users WHERE department_id = 1 AND enabled = 1 AND job_desc = 3 ORDER BY editorname ASC ";
$result = mysql_query($query);
$shop_editor = $_GET['shop_editor'];
$editors = "";
while ($row = mysql_fetch_array($result)) {
    $editorid = $row["user_id"];
    $editorname = $row["editorname"];
    if ($editorid == $shop_editor) {
        $editors .= '<option value="' . $editorid . '" selected="selected">' . $editorname . "</option>";
    } else {
        $editors .= '<option value="' . $editorid . '">' . $editorname . "</option>";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
            <title>Reports</title>

<?php include '../header.php'; ?>


            <script type="text/javascript">

	
                $(document).ready(function(){
                    $("#form1").validate();
		

	
		 
	
                    $.datepicker.formatDate('yyyy-mm-dd');
		 
                    $("#report_type").change(function()
                    {
                        if($("#report_type").val()==="Shopcreated" || $("#report_type").val()==="Shoplive")
                        { 
                            $("#source_id").attr("disabled",true);
                            $("#status_id").attr("disabled",true);
                            $("#user_id").attr("disabled",true);
                            $("#department").attr("disabled",true);
                            $("#department")[0].selectedIndex = 0;
                            $("#source_id")[0].selectedIndex = 0;
                            $("#status_id")[0].selectedIndex = 0;
                            $("#user_id")[0].selectedIndex = 0;
                            $("#shop_type").attr("disabled",false);
                            $("#shop_type")[0].selectedIndex = 0;
                            $("#shop_designer").attr("disabled",false);
                            $("#shop_editor").attr("disabled",false);
                                
                        }
                        else
                        {
                            $("#source_id").attr("disabled",false);
                            $("#status_id").attr("disabled",false);
                            $("#user_id").attr("disabled",false);
                            $("#shop_designer").attr("disabled",false);
                            $("#shop_editor").attr("disabled",false);
                        }
                        
                        if(($("#report_type").val()==="Running Production") || ($("#report_type").val()==="Audit") || ($("#report_type").val()==="Shopcreated") || ($("#report_type").val()==="Shoplive"))
                        {
                            $("#source_id").attr("disabled",true);
                            $("#status_id").attr("disabled",true);
                            $("#user_id").attr("disabled",true);
                            $("#department").attr("disabled",true);
                            $("#department")[0].selectedIndex = 0;
                            $("#source_id")[0].selectedIndex = 0;
                            $("#status_id")[0].selectedIndex = 0;
                            $("#user_id")[0].selectedIndex = 0;
                            $("#shop_type").attr("disabled",false);
                            $("#shop_type")[0].selectedIndex = 0;
                        }
                        else
                        {
                            $("#source_id").attr("disabled",false);
                            $("#status_id").attr("disabled",false);
                            $("#user_id").attr("disabled",false);
			
                        }
			
                        if($("#report_type").val()==="Aging" || $("#report_type").val()==="Transfer")
                        {
                            //                            $("#date_report").attr("disabled",true);
                            //                            $("#date_report")[0].selectedIndex = 0;
                            //                            
                            $("#source_id").attr("disabled",true);
                            $("#status_id").attr("disabled",true);
                            //                            $("#user_id").attr("disabled",true);
                            $("#source_id")[0].selectedIndex = 0;
                            $("#status_id")[0].selectedIndex = 0;
                            $("#department").attr("disabled",true);
                            $("#department")[0].selectedIndex = 0;
                            $("#shop_type").attr("disabled",false);
                            $("#shop_type")[0].selectedIndex = 0;
                            //                            $("#user_id")[0].selectedIndex = 0;
                        }
                        else
                        {
                            $("#date_report").attr("disabled",false);
			
                        }
                        
                        
                        if(($("#report_type").val()==="Log-in"))
                        {
                            $("#source_id").attr("disabled",true);
                            $("#status_id").attr("disabled",true);
                            $("#user_id").attr("disabled",true);
                            $("#shop_type").attr("disabled",true);
                            $("#date_report").attr("disabled",false);
                             
                            $("#shop_type")[0].selectedIndex = 0;
                            $("#source_id")[0].selectedIndex = 0;
                            $("#status_id")[0].selectedIndex = 0;
                            $("#user_id")[0].selectedIndex = 0;
                            $("#department").attr("disabled",false);
                        }
                    });
		 
		 
		 
		 
                    $("#date_report").change(function()
                    {
                        if ($("#date_report").val()==="4")
                        {
                            $("#fdate").val("");
                            $("#edate").val("");
                            $("#date_range").show();
                        }
                        else
                        {	$("#date_range").hide();
                            $("#fdate").val("");
                            $("#edate").val("");
			
                        }
		 
                    });
		 
	
		
                    $("#form1").submit(function() 
	  
                    {
						
					
                        if(($("label.error").is(":visible")))
                        {
							
                            return false;
                        }
                        else
                        {	
                            if($("#date_report").val()==="4")
                            {
                                if($("#fdate").datepicker("getDate") != null || $("#fdate").datepicker("getDate") != null)
                                {	
									
										
										
                                    window.open ("create_excel.php?user="+ $("#user_id").val() +"&status="+ $("#status_id").val() +"&source="+ $("#source_id").val()+"&report="+$("#report_type").val()+"&date="+$("#date_report").val()+"&start="+$("#fdate").val()+"&end="+$("#edate").val()+"&shop_type="+$("#shop_type").val()+"&dept="+$("#department").val()+"&shop_designer="+$("#shop_designer").val()+"&shop_editor="+$("#shop_editor").val());
                                    return true;
                                }
                                else
                                {
                                    alert("Check Date range");
                                    return false;
									
                                }
                            }
                            else
                            {		
									
									
											
                                window.open ("create_excel.php?user="+ $("#user_id").val() +"&status="+ $("#status_id").val() +"&source="+ $("#source_id").val()+"&report="+$("#report_type").val()+"&date="+$("#date_report").val()+"&shop_type="+$("#shop_type").val()+"&dept="+$("#department").val()+"&shop_designer="+$("#shop_designer").val()+"&shop_editor="+$("#shop_editor").val());
                                return true;
                            }
                        }
						
					

									
						
                    });
                });
	
	
                $(function(){
	
                    if(($("#report_type").val()==="Shopcreated") || ($("#report_type").val()==="Shoplive"))
                    { 
                        $("#source_id").attr("disabled",true);
                        $("#status_id").attr("disabled",true);
                        $("#user_id").attr("disabled",true);
                        $("#department").attr("disabled",true);
                        $("#department")[0].selectedIndex = 0;
                        $("#source_id")[0].selectedIndex = 0;
                        $("#status_id")[0].selectedIndex = 0;
                        $("#user_id")[0].selectedIndex = 0;
                        $("#shop_type").attr("disabled",false);
                        $("#shop_type")[0].selectedIndex = 0;
                        $("#shop_designer").attr("disabled",false);
                        $("#shop_editor").attr("disabled",false);
                                
                    }
                    else
                    {
                        $("#source_id").attr("disabled",false);
                        $("#status_id").attr("disabled",false);
                        $("#user_id").attr("disabled",false);
                        $("#shop_designer").attr("disabled",false);
                        $("#shop_editor").attr("disabled",false);
                    }
                        
                    if(($("#report_type").val()==="Running Production") || ($("#report_type").val()==="Audit") || ($("#report_type").val()==="Shopcreated") || ($("#report_type").val()==="Shoplive"))
                    {
                        $("#source_id").attr("disabled",true);
                        $("#status_id").attr("disabled",true);
                        $("#user_id").attr("disabled",true);
                        $("#source_id")[0].selectedIndex = 0;
                        $("#status_id")[0].selectedIndex = 0;
                        $("#user_id")[0].selectedIndex = 0;
                        $("#department").attr("disabled",true);
                        $("#department")[0].selectedIndex = 0;
                        $("#shop_type").attr("disabled",false);
                        $("#shop_type")[0].selectedIndex = 0;
                    }
                    else
                    {
                        $("#source_id").attr("disabled",false);
                        $("#status_id").attr("disabled",false);
                        $("#user_id").attr("disabled",false);
			
                    }
                    if($("#report_type").val()==="Aging" || $("#report_type").val()==="Transfer")
                    {
                        $("#source_id").attr("disabled",true);
                        $("#status_id").attr("disabled",true);
                        $("#shop_type").attr("disabled",false);
                        $("#shop_type")[0].selectedIndex = 0;
                        //                            $("#user_id").attr("disabled",true);
                        $("#source_id")[0].selectedIndex = 0;
                        $("#status_id")[0].selectedIndex = 0;
                        $("#department").attr("disabled",true);
                        $("#department")[0].selectedIndex = 0;
                        //                            $("#user_id")[0].selectedIndex = 0;
			
                    }
                    else
                    {
                        $("#date_report").attr("disabled",false);
			
                    }
                    if(($("#report_type").val()==="Log-in"))
                    {
                        $("#source_id").attr("disabled",true);
                        $("#status_id").attr("disabled",true);
                        $("#user_id").attr("disabled",true);
                        $("#shop_type").attr("disabled",true);
                        $("#date_report").attr("disabled",false);
                      
                        $("#shop_type")[0].selectedIndex = 0;
                        $("#source_id")[0].selectedIndex = 0;
                        $("#status_id")[0].selectedIndex = 0;
                        $("#user_id")[0].selectedIndex = 0;
                        $("#department").attr("disabled",false);
                          
                    }
                });

                $(function() {
	
	
	
                    var dates = $( "#fdate, #edate" ).datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        dateFormat:"yy-mm-dd",
                        numberOfMonths: 1,
                        onSelect: function( selectedDate ) {
                            var option = this.id == "fdate" ? "minDate" : "maxDate",
                            instance = $( this ).data( "datepicker" ),
                            date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                                $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings );
                            dates.not( this ).datepicker( "option", option, date );
                        }
                    });
                    if ($("#date_report").val()==="4")
                    {
                        $("#fdate").val("<?php echo $_GET['fdate']; ?>");
                        $("#edate").val("<?php echo $_GET['edate']; ?>");
                        $("#edate").addClass("required error");
                        $("#fdate").addClass("required error");
                        $("#date_range").show();
                    }
                    else
                    {	$("#date_range").hide();
                        $("#fdate").val("");
                        $("#edate").val("");
			
                    }
                });


            </script>
<?php if ($_GET['report_type'] == 'Aging'): ?>
                <style>
                    .gen-section table.main-grid tr.grid-content td:nth-child(5) {
                        width: 380px;
                    }
                    .gen-section table.main-grid tr.grid-content td:nth-child(4) {
                        width: 160px;
                    }
                    .gen-section table.main-grid tr.grid-content td:nth-child(2) {
                        width: 120px;
                    }

                </style>
<?php endif; ?>
            <div class="main-section">
                <form name="form1" id="form1" action="index.php" method="get">
                    <div class="commands">
                        <div class="head-label-rep">
                            <h2>Reports</h2>
                        </div><!-- end of add new account -->
                        <!--
                        <ul>
                            <li><a class="link-button gray" href='/88dbphcrm/accounts/'>Cancel</a></li>
                            <li></li>
                        </ul>
                    </div>
                        <!-- end of grid-commands -->
                        <div class="gen-section">

                            <table cellpadding="5" cellspacing="0">
                                <tr>
                                    <td class="grid-head">Report Type:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='report_type' id='report_type' class="required" >
                                            <option value="" >Select Report Type</option>
                                            <option value="Aging" <?php if ($_GET['report_type'] == "Aging")
    echo "selected='selected'"; ?> >Aging</option>
                                            <option value="Audit" <?php if ($_GET['report_type'] == "Audit")
    echo "selected='selected'"; ?> >Audit</option>
                                            <option value="Log-in" <?php if ($_GET['report_type'] == "Log-in")
    echo "selected='selected'"; ?> >Log-in</option>
                                            <option value="Funnel" <?php if ($_GET['report_type'] == "Funnel")
    echo "selected='selected'"; ?> >Funnel</option>
                                            <option value="Leads" <?php if ($_GET['report_type'] == "Leads")
    echo "selected='selected'"; ?> >Leads</option>
                                            <option value="Pipeline" <?php if ($_GET['report_type'] == "Pipeline")
    echo "selected='selected'"; ?> >Pipeline</option>
                                            <option value="Shopcreated" <?php if ($_GET['report_type'] == "Shopcreated")
    echo "selected='selected'"; ?> >Shop Created</option>
                                            <option value="Shoplive" <?php if ($_GET['report_type'] == "Shoplive")
    echo "selected='selected'"; ?> >Shop LIVE</option>
                                            <option value="Running Production" <?php if ($_GET['report_type'] == "Running Production")
    echo "selected='selected'"; ?> >Running Production</option>
                                            <option value="Transfer" <?php if ($_GET['report_type'] == "Transfer")
    echo "selected='selected'"; ?> >Transfer Accounts</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">Department:</td>
                                    <td>&nbsp;</td>
                                    <td>


                                        <select name="department" id="department"  >
                                            <option <?php if ($_GET['user_id'] == "")
    echo "selected='selected'"; ?> value="0">Select Department</option>

<?php echo $department; ?>                      
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">Shop Type:</td>
                                    <td>&nbsp;</td>
                                    <td>


                                        <select name="shop_type" id="shop_type"  >
                                            <option value="0" <?php if ($_GET["shop_type"] === 0): ?> selected="selected" <?php endif; ?> >Select Shop Type</option>
                                            <option value="1" <?php if ($_GET["shop_type"] === 1): ?> selected="selected" <?php endif; ?> >88DB</option>
                                            <option value="2" <?php if ($_GET["shop_type"] === 2): ?> selected="selected" <?php endif; ?> >OpenRice</option>                            
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">User:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='user_id' id='user_id' >
                                            <option <?php if ($_GET['user_id'] == "")
    echo "selected='selected'"; ?> value="0">Select User</option>

<?php echo $user_sales; ?>
                                        </select> 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">Select Designer:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='shop_designer' id='shop_designer'>
                                            <option value='0'>Select Designer</option>
<?php echo $designers; ?>
                                        </select>

                                    </td>
                                </tr> 
                                <tr>
                                    <td class="grid-head">Select Editor:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='shop_editor' id='shop_editor'>
                                            <option value='0'>Select Editor</option>
<?php echo $editors; ?>
                                        </select>

                                    </td>
                                </tr>   

                                <tr>
                                    <td class="grid-head">Status:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='status_id' id='status_id'  >
                                            <option <?php if ($_GET['status_id'] == "")
    echo "selected='selected'"; ?> value="0">Select Status</option>

<?php echo $statusoptions; ?>
                                        </select> 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">Source:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='source_id' id='source_id' >
                                            <option <?php if ($_GET['source_id'] == "")
    echo "selected='selected'"; ?> value="0">Select Source</option>

<?php echo $sourceoptions; ?>
                                        </select>                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="grid-head">Date:</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <select name='date_report' id='date_report' >
                                            <option <?php if ($_GET['date_report'] == "")
    echo "selected='selected'"; ?> value="0">ALL</option>
                                            <option <?php if ($_GET['date_report'] == "1")
    echo "selected='selected'"; ?> value="1" >Today</option>
                                            <option <?php if ($_GET['date_report'] == "2")
    echo "selected='selected'"; ?> value="2" >This Week</option>
                                            <option <?php if ($_GET['date_report'] == "3")
    echo "selected='selected'"; ?> value="3" >This Month</option>
                                            <option <?php if ($_GET['date_report'] == "4")
    echo "selected='selected'"; ?> value="4" >Date Range</option>
                                        </select>      

                                        <table border="0" cellpadding="0" cellspacing="0" style="width:200px;" id="date_range" name="date_range" >
                                            <tr>
                                                <td>
                                                    From : 
                                                </td>
                                                <td>
                                                    <input type="text" id="fdate" name="fdate"  style="width:90px" readonly="readonly" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    To : 
                                                </td>
                                                <td>
                                                    <input type="text" id="edate" name="edate" style="width:90px" size="15" readonly="readonly" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <input type="submit" value="View" id="view_report"/>
                                    </td>
                                </tr>
                            </table>

                        </div><!-- end of gen-section -->
                </form>
            </div><!-- end of main-section -->

            <div class="gen-section">
                <div id="table_report" style="overflow:auto;">
<?php echo $table; ?>

                </div>
<?php if (!empty($_REQUEST['report_type'])) { ?>
                    <div style="margin:20px auto;width:180px;">
                        <div class="head-label">
                            <h3>Page</h3>
                        </div>
                        <select id="selectfield" name="selectfield" style="margin: 0 15px;" onchange="document.location.href=this.options[this.selectedIndex].value">
                            <?php
                            //$status_id = $_REQUEST['sid'];
                            //if ($status_id) $sid = "&sid=".$status_id;	
                            $ct = 1;



                            while ($ct <= $lastpage) {

                                if ((strpos($_SERVER["REQUEST_URI"], 'report_type=') === false)) {
                                    $url_filter = "?page=";
                                } else {

                                    //$url_filter ="?search=".$_REQUEST['search']."&sid=".$_REQUEST['sid']."&cat=".$_REQUEST['cat']."&source=".$_REQUEST['source']."&page=";
                                    $url_filter = "?report_type=" . $_REQUEST['report_type'] . "&user_id=" . $_REQUEST['user_id'] . "&date_report=" . $_REQUEST['date_report'] . "&fdate=" . $_REQUEST['fdate'] . "&edate=" . $_REQUEST['edate'] . "&page=";
                                }


                                if ($page == $ct) {
                                    echo "<option value='" . basename($_SERVER["SCRIPT_NAME"]) . $url_filter . $ct . "' selected>" . $ct . "</option>";
                                } else {
                                    echo "<option value='" . basename($_SERVER["SCRIPT_NAME"]) . $url_filter . $ct . "'>" . $ct . "</option>";
                                }

                                $ct++;
                            }
                            ?>
                        </select>
                    </div>
<?php } ?>

            </div>				


            </body>
            </html>