<?php

session_start();
//include_once("classes/class.export_excel.php");
include_once("functions/connection.php");
include_once("functions/functions.php");
include_once("classes/class.export_excel.php");



date_default_timezone_set('Asia/Manila');

$user = $_GET['user'];
$status = $_GET['status'];
$source = $_GET['source'];

$date = $_GET['date'];
$start = $_GET['start'];
$end = $_GET['end'];
$condition = GetCondition($user, $status, $source, $date, $start, $end);

$report_type = $_GET['report'];
$repot_createdby = GetReportCreator($_SESSION['user_id']);
$shoptype = $_GET['shop_type'];
switch ($report_type) {

    case Leads:
        $shoptype = $_GET['shop_type'];
        $query = "SELECT a.account_id,
				a.account_name, 		
				a.account_contactperson,
				a.account_telephone,
				a.account_mobile,
				a.account_fax,
				a.account_email,
				a.account_address,
				c.category_name,
				sr.source_name,
				s.status_name,
				concat(r.user_firstname,' ',r.user_lastname) AS createdby,
				a.account_createdon,
				concat(r2.user_firstname,' ',r2.user_lastname) as modifiedby,
				a.account_modifiedon
    FROM accounts a 
	LEFT JOIN statuses s ON a.status_id = s.status_id 
	LEFT JOIN categories c ON a.category_id = c.category_id 
	LEFT JOIN sources sr ON a.source_id = sr.source_id 
	LEFT JOIN users r ON a.account_createdby = r.user_id 
	LEFT JOIN users r2 ON a.account_modifiedby = r2.user_id " . $condition . " ORDER BY a.account_createdon DESC";
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }


        $heads = array("Account ID", "Account Name", "Contact Person", "Telephone #", "Mobile #", "Fax #", "Email Address", "Adress", "Category", "Source", "Status", "Account Executive", "Created on", "Modified By", "Modified on");
//code to download the data of report in the excel format
        $fn = "Leads_" . date('Y-m-d-G-i') . ".xls";
        break;

    Case Pipeline:

        $shoptype = $_GET['shop_type'];
        $date = $_GET['date'];
        $status = $_GET['status'];
        $user = $_GET['user'];
        $source = $_GET['source'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
        $date_query = "";
        If ($source) {
            $source_query = " AND a.source_id = " . $source . " ";
        } else {
            $source_query = "";
        }
        If ($user) {
            $user_query = " AND a.account_createdby = " . $user . " ";
        } else {
            $user_query = "";
        }
        If ($status) {
            $status_query = " AND a.status_id = " . $status . " ";
        } else {
            $status_query = "";
        }
        switch ($date) {
            case 1:
                If ($status) {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP())";
                    }
                } else {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP())";
                    }
                }

                break;

            case 2:
                If ($status) {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                    }
                } else {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                    }
                }

                break;

            case 3:
                If ($status) {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                    }
                } else {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                    }
                }

                break;

            case 4:
                If ($status) {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                    }
                } else {
                    if ($shoptype == 1) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type = 1";
                    } elseif ($shoptype == 2) {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                    } else {
                        $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                    }
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = " AND a.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }


        $query .= "SELECT a.account_id, a.account_name,a.account_email,c.category_name,s.status_name,pa.package_name,a.account_memberid,a.account_brno,
						a.account_adid,
						
									case a.shop_type
											when '0' then ''
											when '1' then '88DB'
											when '2' then 'Open Rice'
									end,
									
						a.shop_url,
						a.shop_domain,
									case a.shop_domaintype
									
											when '0' then ''
											when '1' then 'For Request'
											when '2' then 'Pending'
											when '3' then 'Activated'
									end,
									case a.shop_domainstatus
											when '0' then ''
											when '1' then 'For Request'
											when '2' then 'Pending'
											when '3' then 'Activated'
									end,
									
									case a.shop_cmsstatus
											when '0' then ''
											when '1' then 'For Request'
											when '2' then 'Pending'
											when '3' then 'Activated'
									end,
									
						concat(de.user_firstname,' ',de.user_lastname) AS designer,
						concat(ed.user_firstname,' ',ed.user_lastname) AS editor,
						a.account_contractnumber,
						a.account_contractamount,
						concat(r.user_firstname,' ',r.user_lastname) AS createdby, 
						a.account_createdon,
						concat(r2.user_firstname,' ',r2.user_lastname) as modifiedby,
                                                concat(csr.user_firstname,' ',csr.user_lastname)  as csr,
                                                
                                                
";
        if ($status) {
            $query .= " max(au.audit_createdon) as log_created";
        } else {
            $query .= " a.account_modifiedon  as account_modifiedon";
        }

        $query .= " FROM accounts a ,users ed ,users de , packages pa , statuses s , categories c , sources sr , users r ,users r2 , audit au , users csr";
//        $query .= " FROM accounts a ";
//        $query .= "LEFT JOIN users ed ON a.shop_editor = ed.user_id ";
//        $query .= "LEFT JOIN users de ON a.shop_designer = de.user_id ";
//        $query .= "LEFT JOIN packages pa ON a.package_id = pa.package_id ";
//        $query .= "LEFT JOIN statuses s ON a.status_id = s.status_id  ";
//        $query .= "LEFT JOIN categories c ON  a.category_id = c.category_id  ";
//        $query .= "LEFT JOIN sources sr ON a.source_id = sr.source_id  ";
//        $query .= "LEFT JOIN  users r ON  a.account_createdby = r.user_id  ";
//        $query .= "LEFT JOIN users r2 ON a.account_modifiedby = r2.user_id  ";
//        $query .= "LEFT JOIN audit au ON a.account_id = au.account_id  ";
//        $query .= "LEFT JOIN users csr ON a.shop_csr = csr.user_id ";


        $query .= " WHERE a.shop_editor = ed.user_id ";
        $query .= "AND a.shop_designer = de.user_id ";
        $query .= "AND a.package_id = pa.package_id ";
        $query .= "AND a.status_id = s.status_id  ";
        $query .= "AND a.category_id = c.category_id  ";
        $query .= "AND a.source_id = sr.source_id  ";
        $query .= "AND a.account_createdby = r.user_id  ";
        $query .= "AND a.account_modifiedby = r2.user_id  ";
        $query .= " AND a.account_id = au.account_id  ";
        $query .= " AND a.shop_csr = csr.user_id  ";
        $query .= " " . $user_query . "  ";
        $query .= " " . $date_query . " ";
        $query .= " " . $status_query . " ";
        $query .= " " . $source_query . " ";
        $query .= " GROUP BY a.account_id ORDER BY a.account_createdon DESC ";
        //echo $query;
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Account ID", "Account Name", "Email Address", "Category", "Status", "Package Type", "Member ID", "B.R. #", "Ad ID", "Shop Type", "Shop URL", "Shop Domain", "Domain Type", "Domain Status", "CMS Status", "Assigned Designer", "Assigned Editor", "Contract #", "Contract Amount", "Created By", "Created on", "Modified By", "CSR", "Date Log/Modified on");

        //code to download the data of report in the excel format
        $fn = "Pipeline_" . date('Y-m-d-G-i') . ".xls";
        break;

    Case Funnel:
        $shoptype = $_GET['shop_type'];

//        
//        $query .= "SELECT concat(r.user_firstname,' ',r.user_lastname) AS createdby,a.account_id,a.account_name,date_format(Date(au.audit_createdon),'%M %e, %Y'),";
//        $query .= " a.account_address,a.account_telephone,a.account_contactperson,c.category_name,a.shop_url,a.account_contractnumber,date_format(Date(pd.latest_payment),'%M %e, %Y'),";
//        $query .= " a.account_contractamount,pl.paid_amount,(a.account_contractamount - pl.paid_amount) as remaining_payment,s.status_name,Replace(rm2.remark_remarks,'<br />',' '),date_format(Date(rm2.remark_createdon),'%M %e, %Y')";
//        $query .= " FROM accounts a LEFT JOIN(select sum(payment_amount) as paid_amount, account_id from payments group by account_id) pl O";
//        $query .= " LEFT JOIN (SELECT MAX(payment_createdon) as latest_payment,account_id  from payments group by account_id ) pd ON a.account_id = pd.account_id ";
//        $query .= " LEFT JOIN(SELECT audit_createdon,account_id from audit where after_status_id = 13) au on a.account_id = au.account_id";
//        $query .= " LEFT JOIN(SELECT Max(remark_createdon) as max_id,remark_uid from remarks group by remark_uid) rm ON a.account_id = rm.remark_uid";
//        $query .= " LEFT JOIN remarks rm2 on rm.max_id = rm2.remark_createdon";
//        $query .= " LEFT JOIN(SELECT account_id, date_format(Date(audit_createdon),'%M %e, %Y') as date_audit from audit where after_status_id = 13) aud ON a.account_id = aud.account_id";
//        $query .= " LEFT JOIN categories c ON a.category_id = c.category_id  ";
//        $query .= " LEFT JOIN users r ON a.account_createdby = r.user_id ";
//        $query .= " LEFT JOIN statuses s ON a.status_id = s.status_id ";
//        $query .= " " . $condition . " ";
//        $query .= " GROUP BY a.account_id ORDER BY a.account_createdon DESC ";


        $query = "SELECT  concat(r.user_firstname,' ',r.user_lastname) AS createdby,
					a.account_id,
					a.account_name,
					
					date_format(Date(au.audit_createdon),'%M %e, %Y'),
					a.account_address,
					a.account_telephone,
					a.account_contactperson,
					c.category_name,
					a.shop_url,  
					a.account_contractnumber,
					date_format(Date(pd.latest_payment),'%M %e, %Y'),
					a.account_contractamount,
					pl.paid_amount,
					(a.account_contractamount - pl.paid_amount) as remaining_payment,
					s.status_name,
					Replace(rm2.remark_remarks,'<br />',' '),
                                        
					date_format(Date(rm2.remark_createdon),'%M %e, %Y'),
concat(csr.user_firstname,' ',csr.user_lastname)  as csr,                                        
a.account_memberid
					
					
				FROM accounts a 
				LEFT JOIN 
				(
					select sum(payment_amount) as paid_amount, account_id from payments group by account_id
				) pl ON a.account_id = pl.account_id 
				LEFT JOIN 
				(
					 SELECT MAX(payment_createdon) as latest_payment,account_id  from payments group by account_id 
				) pd ON a.account_id = pd.account_id 
				
				
				LEFT JOIN
				(
				
				SELECT audit_createdon,account_id from audit where after_status_id = 13
				) au on a.account_id = au.account_id
				
				LEFT JOIN 
				( 
				SELECT Max(remark_createdon) as max_id,remark_uid from remarks group by remark_uid
					
				) rm ON a.account_id = rm.remark_uid
				
				LEFT JOIN remarks rm2 on rm.max_id = rm2.remark_createdon
				
				
				LEFT JOIN
				(
				SELECT account_id, date_format(Date(audit_createdon),'%M %e, %Y') as date_audit from audit where after_status_id = 13
				) aud ON a.account_id = aud.account_id
				
				LEFT JOIN categories c ON a.category_id = c.category_id 
				LEFT JOIN users r ON a.account_createdby = r.user_id 
				LEFT JOIN statuses s ON a.status_id = s.status_id
                                LEFT JOIN users csr ON a.shop_csr = csr.user_id
" . $condition . "

	
	GROUP BY a.account_id ORDER BY a.account_createdon DESC
 ";
//        echo $query;
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }


        $heads = array("Account Executive", "Account ID", "Account Name", "Date Signed", "Address", "Telephone", "Contact Person", "Category", "Shop URL", "Contract Number", "Last Payment", "Contract Amount", "Total Paid Amount", "Account Balance", "Status", "Remark", "Remark Date", "CSR", "Member ID");
        //code to download the data of report in the excel format
        $fn = "Funnel_" . date('Y-m-d-G-i') . ".xls";
        break;

    Case "Running Production":
        $shoptype = $_GET['shop_type'];
        if (!empty($condition))
            $q = $condition . " and a.account_paid=1";
        else
            $q = " Where a.account_paid = 1";
        $query = "select 
				a.account_id,
				a.account_name,
				a.shop_url,
				concat(r.user_firstname,' ',r.user_lastname) AS designer, 
				u.audit_createdon as Start_Date,
				p.audit_createdon as End_Date, 

				((Datediff(date(p.audit_createdon),date(u.audit_createdon)) * 24) + (TIME_to_SEC(Timediff(TIME_FORMAT(time(p.audit_createdon),'%r'),TIME_FORMAT(time(u.audit_createdon),'%r'))) / 3600)) as Hours
				from accounts a


				LEFT JOIN
				(
				select audit_createdon, account_id from audit where before_status_id = 21 and after_status_id = 3 order by audit_id  DESC LIMIT 0,1
				) u on a.account_id = u.account_id
				LEFT JOIN
				(
				select audit_createdon, account_id from audit where before_status_id = 3 and after_status_id = 4 order by audit_id DESC LIMIT 0,1
				) p on a.account_id = p.account_id

				LEFT JOIN users r ON a.shop_designer = r.user_id " . $q . "
 ";
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Account ID", "Account Name", "Shop URL", "Assigned Designer", "Start Date", "End Date", "Diff (Hours)");
//code to download the data of report in the excel format
        $fn = "Running Production_" . date('Y-m-d-G-i') . ".xls";
        break;


    Case "Audit":
        $shoptype = $_GET['shop_type'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
//        $shoptype = $_GET['shop_type'];
        $date_query = "";
        switch ($date) {
            case 1:
                if ($shoptype == 1) {
                    $date_query = "WHERE DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND ac.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "WHERE DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND ac.shop_type = 2";
                } else {
                    $date_query = "WHERE DATE(au.audit_createdon) = DATE(CURRENT_TIMESTAMP())";
                }

                break;

            case 2:
                if ($shoptype == 1) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND ac.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND ac.shop_type = 2";
                } else {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                }


                break;

            case 3:
                if ($shoptype == 1) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND ac.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND ac.shop_type = 2";
                } else {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                }


                break;

            case 4:
                if ($shoptype == 1) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND ac.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND ac.shop_type = 2";
                } else {
                    $date_query = "WHERE DATE(au.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " WHERE ac.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = " WHERE ac.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }




        $query = "SELECT
				au.audit_createdon,
				ac.account_name,
				fstat.status_name,
				tstat.status_name,
				concat(u.user_firstname,' ',u.user_lastname)  as user , 
                                concat(csr.user_firstname,' ',csr.user_lastname)  as csr
				FROM audit au
				LEFT JOIN statuses fstat ON au.before_status_id = fstat.status_id
				LEFT JOIN statuses tstat ON au.after_status_id = tstat.status_id
				LEFT JOIN accounts ac ON au.account_id = ac.account_id
				LEFT JOIN users u ON au.audit_createdby = u.user_id
                                LEFT JOIN users csr ON ac.shop_csr = csr.user_id
				" . $date_query . " ORDER BY ac.account_name , au.audit_id  ASC
 ";

//        echo $query;
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Date", "Account Name", "From Status", "To Status", "User", "Csr");
//code to download the data of report in the excel format
        $fn = "Audit_" . date('Y-m-d-G-i') . ".xls";
        break;


    Case "Shopcreated":
        $shoptype = $_GET['shop_type'];
//        $date = $_GET['date_report'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
        $date_query = "";
        $designer = $_GET['shop_designer'];
        $editor = $_GET['shop_editor'];
        $designer_query = "";
        if ($designer) {
            $designer_query = "AND a.shop_designer = " . $designer . "";
        } else {
            $designer_query = "";
        }
        $editor_query = "";
        if ($editor) {
            $editor_query = "AND a.shop_editor = " . $editor . "";
        } else {
            $editor_query = "";
        }
        switch ($date) {
            case 1:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP())";
                }

                break;

            case 2:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                }


                break;

            case 3:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                }


                break;

            case 4:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = " AND a.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }





        $query = "SELECT  a.account_id,  a.account_name, a.shop_url, CONCAT( b.user_firstname,  ' ', b.user_lastname ) AS editor, CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS designer , max(d.audit_createdon) , e.status_name as status
		 				FROM accounts a, users b, users c, audit d,statuses e
						WHERE d.after_status_id =3
						AND a.shop_designer = c.user_id
                                                AND a.shop_editor = b.user_id
                                                AND a.account_id = d.account_id
                                                AND a.status_id = e.status_id
						" . $designer_query . " " . $editor_query . " " . $date_query . "  GROUP BY a.account_name ORDER BY a.account_name , d.audit_createdon 
		 ";
//         echo($query);
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("ID", "Account Name", "Shop URL", "Editor", "Designer", "Date", "Current Status");

        $fn = "Shop_Created_" . date('Y-m-d-G-i') . ".xls";
        break;
    Case "Shoplive":
        $shoptype = $_GET['shop_type'];
//        $date = $_GET['date_report'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
        $date_query = "";
        $designer = $_GET['shop_designer'];
        $editor = $_GET['shop_editor'];
        $designer_query = "";
        if ($designer) {
            $designer_query = "AND a.shop_designer = " . $designer . "";
        } else {
            $designer_query = "";
        }
        $editor_query = "";
        if ($editor) {
            $editor_query = "AND a.shop_editor = " . $editor . "";
        } else {
            $editor_query = "";
        }
        switch ($date) {
            case 1:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) = DATE(CURRENT_TIMESTAMP())";
                }

                break;

            case 2:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                }


                break;

            case 3:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                }


                break;

            case 4:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(d.audit_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = " AND a.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }



        $query = "SELECT    a.account_name, a.shop_url, CONCAT( b.user_firstname,  ' ', b.user_lastname ) AS editor, CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS designer , max(d.audit_createdon) , CONCAT( e.user_firstname,  ' ', e.user_lastname ) AS modifier , a.account_memberid

         
        FROM accounts a
        LEFT JOIN users b ON a.shop_editor = b.user_id
        LEFT JOIN users c ON a.shop_designer = c.user_id
        LEFT JOIN audit d ON a.account_id = d.account_id
        LEFT JOIN users e ON d.audit_createdby = e.user_id
        
        WHERE d.after_status_id = 9
        
        " . $designer_query . " " . $editor_query . " " . $date_query . " GROUP BY a.account_name ORDER BY a.account_name, d.audit_createdon
        ";
//         echo($query);
//        AND a.shop_designer = c.user_id
//        AND a.shop_editor = b.user_id
//        AND d.audit_createdby = e.user_id
//        AND a.account_id = d.account_id , users b, users c, audit d, users e
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Account Name", "Shop URL", "Editor", "Designer", "Date", "Modified by", "Member ID");

        $fn = "Shop_LIVE_" . date('Y-m-d-G-i') . ".xls";
        break;



    Case "Log-in":
        $department = $_GET['dept'];
        $date = $_GET['date'];
        $date_query = "";
        switch ($date) {
            case 1:
                $date_query = " AND DATE(au.audit_log_time) = DATE(CURRENT_TIMESTAMP())";
                break;
            case 2:
                $date_query = " AND DATE(au.audit_log_time) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                break;
            case 3:
                $date_query = " AND DATE(au.audit_log_time) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                break;
            case 4:
                $date_query = " AND DATE(au.audit_log_time) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                break;
            default :
                $date_query = " ";
                break;
        }

//        $query = "SELECT
//							u.user_id, concat(u.user_firstname,' ',u.user_lastname) as name , count(au.audit_act) as log
//							FROM users u , audit_log au
//							WHERE u.user_id = au.user_id
//                                                        AND au.audit_act = 'Log In'
//                                                        AND u.department_id= " . $department . "
//                                                        GROUP BY name
//							ORDER BY name desc  
//			 ";

        $query .= " SELECT  concat(u.user_firstname,' ',u.user_lastname) as name , count(au.audit_act) as log";
        $query .= " FROM users u , audit_log au";
        $query .= " WHERE u.user_id = au.user_id";
        $query .= " AND au.audit_act = 'Log In'";
        $query .= " " . $date_query . "";
        if ($department != 0) {
            $query .= " AND u.department_id= " . $department . "";
        }
        $query .= " GROUP BY name ORDER BY log desc  ";


        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Name", "Login Frequency");
        $fn = "Log-in_" . date('Y-m-d-G-i') . ".xls";
        break;
    Case "Transfer":
        $shoptype = $_GET['shop_type'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
        $date = $_GET['date'];
        $user = $_GET['user'];
        $date_query = "";
        $user_query = "";
        If ($user) {
            $user_query = "AND a.account_transferedto = " . $user . "";
        } else {
            $user_query = "";
        }
        switch ($date) {
            case 1:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_transferedon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_transferedon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_transferedon) = DATE(CURRENT_TIMESTAMP())";
                }

                break;

            case 2:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                }


                break;

            case 3:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                }


                break;

            case 4:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_transferedon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " AND a.shop_type = 1";
                } elseif ($shoptype == 2) {
                    $date_query = " AND a.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }




        $query = "SELECT     a.account_name, CONCAT( b.user_firstname,  ' ', b.user_lastname ) AS oldowner, CONCAT( e.user_firstname,  ' ', e.user_lastname ) AS newowner, CONCAT( c.user_firstname,  ' ', c.user_lastname ) AS transferby, a.account_transferedon , a.account_memberid
		 				FROM accounts_transfer_logs a, users b, users c,  users e
						WHERE a.account_transferedby = c.user_id
						AND a.account_createdby = b.user_id
                                                AND a.account_transferedto = e.user_id
                                               " . $date_query . "  " . $user_query . " Group by a.account_name ORDER BY a.account_name  
		 ";
//            echo($query);
        $result = mysql_query($query, connect());


        while ($row = mysql_fetch_row($result)) {
            $account_table [] = $row;
        }



        $heads = array("Account Name", "Old Owner", "New Owner", "Transfer by", "Transfer date", "Member ID");
        $fn = "TransferAccounts_" . date('Y-m-d-G-i') . ".xls";

        break;
    Case "Aging":
        $shoptype = $_GET['shop_type'];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];
//        $query = "select status_id,status_name from statuses";
//        $result = mysql_query($query, connect());
//        $left = mysql_query($query, connect());
//        $head = mysql_query($query, connect());
//
//
//
//        $lquery .="SELECT concat(u.user_firstname,' ',u.user_lastname),a.account_name";
//        while ($row = mysql_fetch_array($result)) {
//            $lquery .= ", s" . $row['status_id'] . ".audit_createdon as " . trim(preg_replace('/\s*\([^)]*\)/', '', str_replace(' ', '', $row['status_name'])));
//        }
//
//        $lquery .= " FROM `accounts` a LEFT JOIN users u ON a.account_createdby = u.user_id";
//        while ($row = mysql_fetch_array($left)) {
//            $lquery .= " LEFT JOIN ( SELECT * from audit where after_status_id=" . $row['status_id'] . " ) s" . $row['status_id'] . " ON a.account_id = s" . $row['status_id'] . ".account_id ";
//        }
//
//
//
//
//        $result = mysql_query($lquery, connect());
//
//
//        while ($row = mysql_fetch_row($result)) {
//            $account_table [] = $row;
//        }
//
//        $heads[].="Account Executive";
//        $heads[].="Account Name";
//        while ($row = mysql_fetch_array($head)) {
//
//            $heads[] .= $row['status_name'];
//        }
//        lawrence
//        $shoptype = $_GET['shop_type'];
        $date = $_GET['date'];
        $user = $_GET['user'];
        $date_query = "";
        $user_query = "";
        If ($user) {
            $user_query = "AND a.account_createdby = " . $user . "";
        } else {
            $user_query = "";
        }


        $designer = $_GET['shop_designer'];
        $editor = $_GET['shop_editor'];
        $designer_query = "";
        if ($designer) {
            $designer_query = "AND a.shop_designer = " . $designer . "";
        } else {
            $designer_query = "";
        }
        $editor_query = "";
        if ($editor) {
            $editor_query = "AND a.shop_editor = " . $editor . "";
        } else {
            $editor_query = "";
        }


        switch ($date) {
            case 1:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type != 2";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP()) AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_createdon) = DATE(CURRENT_TIMESTAMP())";
                }

                break;

            case 2:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type != 2";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') ";
                }


                break;

            case 3:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type != 2";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') ";
                }


                break;

            case 4:
                if ($shoptype == 1) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "')  AND a.shop_type != 2";
                } elseif ($shoptype == 2) {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') AND a.shop_type = 2";
                } else {
                    $date_query = "AND DATE(a.account_createdon) BETWEEN DATE('" . $start_date . "') AND DATE('" . $end_date . "') ";
                }

                break;
            default :
                if ($shoptype == 1) {
                    $date_query = " AND a.shop_type != 2";
                } elseif ($shoptype == 2) {
                    $date_query = " AND a.shop_type = 2";
                } else {
                    $date_query = " ";
                }
                break;
        }
//        $lquery .= "SELECT a.account_id, CONCAT( b.user_firstname,  ' ', b.user_lastname ) AS name, a.account_name, CONCAT(e.user_firstname,  ' ', e.user_lastname ) AS editor,a.account_createdon as created, GROUP_CONCAT( CONCAT( d.status_name,':', c.audit_createdon )) AS log";
        $lquery .= "SELECT a.account_id, CONCAT( b.user_firstname,  ' ', b.user_lastname ) AS name, a.account_name,  a.account_createdon as created, e.status_name as current_status,a.account_contractamount,f.amount, if((a.account_contractamount-f.amount)=0,'Paid Full',if((a.account_contractamount-f.amount)>0,'Paid Partial','No Payment')), g.datediff,  GROUP_CONCAT( CONCAT( d.status_name,':', c.audit_createdon )) AS log";
        $lquery .= " FROM users b";
        $lquery .= " INNER JOIN accounts a ON b.user_id = a.account_createdby";
        $lquery .= " INNER JOIN audit c ON a.account_id = c.account_id";
        $lquery .= " INNER JOIN statuses d ON c.after_status_id = d.status_id";
        $lquery .= " INNER JOIN statuses e ON a.status_id = e.status_id";
        $lquery .= " INNER JOIN (select sum(payment_amount) as amount, account_id from payments group by account_id) f ON f.account_id = a.account_id";
        $lquery .= " INNER JOIN (select datediff(now(),max(audit_createdon)) as datediff, account_id from audit where after_status_id = 13 group by account_id) g ON g.account_id = a.account_id";

//        $lquery .= " INNER JOIN users e ON a.shop_editor = e.user_id";
//        $lquery .= " WHERE a.account_paid =1";
        $lquery .= " WHERE a.account_paid is not null";
        $lquery .= " " . $user_query . "";
        $lquery .= " " . $designer_query . "";
        $lquery .= " " . $editor_query . "";
        $lquery .= " " . $date_query . "";
        $lquery .= " GROUP BY a.account_name";
        $lquery .= " ORDER BY   c.after_status_id  ,c.audit_createdon   ";
//        $lquery .= " " . $codition . " LIMIT $start, $limit";
//            code ko
//        echo($lquery);
        $result = mysql_query($lquery, connect());
//        $heads = array("Account ID", "Account Executive", "Account name", "Editors", "Account created", "Signed (for Payment)", "Lined Up for Production", "For Proofreading", "For Design Instructions", "For Development", "Under Construction", "For Editor QA", "Internal Revisions-Editor 1", "Internal Revisions-Editor 2", "Internal Revisions-Editor 3", "For Creative Director QA", "Internal Revisions-FQA 1", "Internal Revisions-FQA 2", "Internal Revisions-FQA 3", "For Client Approval", "For Client Approval (Feedback)", "For Client Approval (Consolidation)", "Client Revisions 1", "Client Revisions 2", "Client Revisions 3", "Temporary Live", "Live", "Client Updates");
//        $heads = array("Account ID", "Account Executive", "Account name", "Account created", "Current Status", "Contract Amount", "Payment amount", "Payment Status", "No.of days", "Signed (for Payment)", "Lined Up for Production", "For Development", "For Proofreading", "For Design Instructions", "Under Construction", "For Editor QA", "For Creative Director QA", "Internal Revisions-Editor 1", "Internal Revisions-Editor 2", "Internal Revisions-Editor 3", "Internal Revisions-FQA 1", "Internal Revisions-FQA 2", "Internal Revisions-FQA 3", "For Client Approval", "For Client Approval (Feedback)", "For Client Approval (Consolidation)", "Temporary Live", "Client Revisions 1", "Client Revisions 2", "Client Revisions 3", "Live", "Client Updates");
        $heads = array("Account ID", "Account Executive", "Account name", "Account created", "Current Status", "Contract Amount", "Payment amount", "Payment Status", "No.of days", "For Prospecting", "Set Appointment", "Send Proposal", "For Callback", "For Follow Up", "Interested", "Declined", "For Signing", "For Deletion", "Signed (for Payment)", "Paid (Partial)", "Paid (Full)", "Lined Up for Production", "For Proofreading", "For Development", "Under Construction", "For Editor QA", "Internal Revisions 1", "Internal Revisions 2", "Internal Revisions 3", "For Sr. Designer QA", "Internal Revisions QA - 1", "Internal Revisions QA - 2", "Internal Revisions QA - 3", "Final QA", "For Client Approval", "For Client Approval - Consolidation", "For Client Approval - Feedback", "Client Updates", "Client Revisions 1", "Client Revisions 2", "Client Revisions 3", "Client Revisions 4", "Client Revisions 5", "Redesign 1", "Redesign 2", "Redesign 3", "Temporary Live", "Temporary Live - Awaiting Mats from Client", "Temporary Live - Awaiting Final Approval from Client", "Temporary Live - Awaiting Balance Collection", "Live", "Temporary Closed", "Temporary Closed - Delinquent", "Temporary Closed - Request from Client");

        $account_table = array();
        while ($row = mysql_fetch_row($result)) {
            $stats = explode(',', $row[9]);
            // echo '<pre>';
            // print_r($stats);
            // echo '</pre>';
            unset($row[9]);
            for ($i = 9; $i < count($heads); $i++) {
                $contains = false;
                foreach ($stats as $r) {

                    if (strpos($r, $heads[$i]) === 0) {
                        // echo $r.'pasok'.$heads[$i].'<br/>';
                        $r = explode(':', $r, 2);
                        $row[] = $r[1];
                        $contains = true;
                        break;
                    }
                }
                // echo 'hala';
                if (!$contains) {
                    $row[] = '';
                }
            }
            $account_table[] = $row;
        }




//        while ($row = mysql_fetch_row($result)) {
//            $row[4] = explode(',', $row[4]);
////            $row[4] = str_replace(',', '<br/>', $row[4]);
////                var_dump($row);
////                echo 'gana';
//            $account_table[] = $row;
//        }
//
//        $heads = array("Account ID", "Account Executive", "Account name", "Editors", "Signed (for Payment)", "Lined Up for Production" , "For Proofreading" , "For Design Instructions", "For Development", "Under Construction", "For Editor QA", "Internal Revisions-Editor 1", "Internal Revisions-Editor 2", "Internal Revisions-Editor 3", "For Creative Director QA", "Internal Revisions-FQA 1", "Internal Revisions-FQA 2", "Internal Revisions-FQA 3", "For Client Approval", "For Client Approval (Feedback)", "For Client Approval (Consolidation)", "Client Revisions 1", "Client Revisions 2", "Client Revisions 3", "Live", "Client Updates");
//        lawrence

        $fn = "Aging_" . date('Y-m-d-G-i') . ".xls";
        break;
}


//create the instance of the exportexcel format
$excel_obj = new ExportExcel("$fn");
//setting the values of the headers and data of the excel file 
//and these values comes from the other file which file shows the data
$excel_obj->setHeadersAndValues($heads, $account_table);
//now generate the excel file with the data and headers set
$excel_obj->GenerateExcelFile();
?>
