<?php
session_start();

connect();

$date_condition = "DATE(a.account_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday last week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "')";

//echo('test ni lawrence');
?>
<style type="text/css">
    /*---------------------------------------------------------------------------------*/
    /*	CSS Reset Style
    /*---------------------------------------------------------------------------------*/

    html, body, div, dl, dt, dd, ul, li, h1, h2, h3, h4, h5, h6, form, label, fieldset, input, p, blockquote, th, td { margin:0; padding:0 }
    table { border-collapse:collapse; border-spacing:0 }
    fieldset, img { border:0 }
    address, caption, cite, code, dfn, em, strong, th, var { font-style:normal; font-weight:normal }
    ul { list-style:none }
    ol { padding-left: 17px;	}
    caption, th { text-align:left }
    h1, h2, h3, h4, h5, h6 { font-size:100%; font-weight:normal }
    q:before, q:after { content:''}
    header, nav, article, footer, address {display: block;}
    strong { font-weight:bold }
    em { font-style:italic }
    a img { border:none } /* Gets rid of IE's blue borders */
    pre { padding: 10px; white-space: pre; white-space: pre-wrap; white-space: pre-line; word-wrap: break-word;}


    /*---------------------------------------------------------------------------------*/
    /*	CSS Reset Style
    /*---------------------------------------------------------------------------------*/

    .dash-row-container{
        overflow: hidden;
        /*        float: left;*/
        width: 870px;
        margin: 0 auto;
        margin-top: 5px;
        position: relative;
    }
    .dashboard-box{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 170px;
        height: 100px;
        margin-left: 2px;
        margin-bottom: 5px;
        float: left;
    }
    .dashboard-box-5{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 170px;
        height: 130px;
        margin-left: 2px;
        margin-bottom: 5px;
        float: left;
    }
    .dashboard-box-title{
        width: 170px;
        overflow: hidden;
        margin: 0 auto;
        text-align: center;
        padding-top: 5px;
    }
    .dashboard-box-total{
        width: 170px;
        overflow: hidden;
        margin: 0 auto;
        text-align: center;
        /*        padding-top: 20px;*/

    }
    .total-count{
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 24px;
        font-weight: bold;
        color: #666;
    }
    .dashboard-box-title ul li{
        color: #666;
        font-size: 13px;
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    .stat-desc-count{
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 11px;

        color: #666;
    }
    .dashboard-link{
        text-decoration: none;
        color: #F68520;
    }
    .dashboard-link:hover{
        text-decoration: underline;

    }
    .dept-title{
        border: 1px solid #D9D9D9;
        overflow: hidden;
        width: 866px;
        margin: 0 auto;
        margin-top: 5px;
        margin-bottom: 5px;
        margin-left: 2px;
        background: #F90;
        height: 2px;
    }
    .dept-title-span{
        /*        border: 1px solid #D9D9D9;*/
        overflow: hidden;
        width: 866px;
        margin: 0 auto;
        margin-top: 5px;
        margin-bottom: 5px;
        margin-left: 2px;
        /*        background: #F90;*/

    }
    .dept-title-span span{
        text-transform: uppercase;
        font-family: "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        color: #666;
        font-weight: bold;
        font-size: 12px;
        margin-left: 5px;
        position: relative;
    }
    .dept-title-span i {
        width: 20px;
        height: 15px;
        width: 20px;
        height: 15px;
        position: relative;
        float: left;

    }
    i.csr{
        background: #FC9;
    }
    i.prod{
        background: #FFFBD0;
    }
    .backcsr{
        background: #FFFBD0;
    }
    .backprod{
        background: #FC9;
    }
    .backfinance{
        background: #CDB99C;
    }
</style>
<!---dash-row-container start---> 
<div class="dash-row-container">
    <?php if ($department_id == 2 || $department_id == 4) { ?>   
        <!--SALES START-->
        <div class="dept-title">
            <span>SALES AND FINANCE</span>
        </div>

        <!---First Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE DATE(a.account_createdon) = DATE('" . date('Y-m-d', strtotime('today')) . "')" . $condition;
//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Accounts Today
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo $row[0];
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " WHERE r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition;

            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Total Accounts to Date
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    $total_accounts_to_date = number_format($row[0]);
                                    echo $total_accounts_to_date;
                                    echo "<input type='hidden' id='h2' value='" . $total_accounts_to_date . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h2' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";


                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                $condition_au = "AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " WHERE r.user_id = " . $userid . " ";
                                $condition_au = "AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " WHERE r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                $condition_au = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " WHERE r.user_id = " . $_SESSION['dashboard_user'] . " ";
                        $condition_au = "AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }

                $query = "SELECT (
						SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id " . $condition . ") - (SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition_au . " GROUP BY a.account_id
						) x) - (SELECT count(a.account_id) as ct1 FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 )" . $condition_au . ")";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Leads:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "
						SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x
					 ";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "
						 SELECT count(a.account_id) as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE (a.status_id = 14 or a.status_id = 23 ) " . $condition . "

					 ";

                $result = mysql_query($query);
                ?>	
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Declined: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";

            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') " . $condition . " GROUP BY au.account_id
						) x";
//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops This Week
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h5' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
                $result = mysql_query($query);
                ?>   
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "  ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('monday this week')) . "') AND DATE('" . date('Y-m-d', strtotime('friday this week')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";
//            echo $query;
                $result = mysql_query($query);
                ?>     
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h5' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h5' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total,
							au.audit_createdon
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE au.after_status_id = 13 AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') " . $condition . " GROUP BY au.account_id
						) x";




            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops This Month
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h3' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {

                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "')  and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount <> pay.paid_amount GROUP BY a.account_id 

						) x";
                $result = mysql_query($query);
                ?>     

                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . "  AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND act.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT distinct au.account_id as total FROM accounts a 
						LEFT JOIN users r ON a.account_createdby = r.user_id 
						LEFT JOIN audit au on a.account_id = au.account_id 
						LEFT JOIN accounts act ON au.account_id = act.account_id 
						LEFT JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON au.account_id = pay.account_id
						WHERE au.after_status_id = 13 " . $condition . " AND DATE(au.audit_createdon) BETWEEN DATE('" . date('Y-m-d', strtotime('first day of this month')) . "') AND DATE('" . date('Y-m-d', strtotime('last day of this month')) . "') and EXISTS (SELECT * FROM payments where payments.account_id = au.account_id)  AND act.account_contractamount = pay.paid_amount GROUP BY a.account_id 

						) x";



                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h3' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h3' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "SELECT count(x.total) as ct1
						from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " GROUP BY a.account_id
						) x";



            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed Shops to Date
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);
                                    echo number_format($row[0]);
                                    echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h2' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . " ";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " WHERE a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }

                $query = "select count(x.total) from
						(
							SELECT 
							distinct au.account_id as total
							FROM accounts a 
							LEFT JOIN users r ON a.account_createdby = r.user_id 
							LEFT JOIN audit au on a.account_id = au.account_id 
							WHERE  au.after_status_id = 13 " . $condition . " and NOT EXISTS (SELECT * FROM payments where payments.account_id = au.account_id) GROUP BY a.account_id
							
						) x";
                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Signed Only:  <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);
                echo number_format($row[0]);
                echo "<input type='hidden' id='h2' value='" . number_format($row[0]) . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h2' value='0' />";
            }
                ?>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND  a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT a.account_id as total 
                                                FROM accounts a 
						INNER JOIN users r ON a.account_createdby = r.user_id 
						 INNER JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON a.account_id = pay.account_id
						WHERE a.account_contractamount <> pay.paid_amount GROUP BY a.account_id " . $condition . " 

						) x";
              echo $query;

                $result = mysql_query($query);
                ?>  
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Partial:
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='17' class='dashboard-link'>" . $row[0] . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-box-total">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = "  AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";

                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = "AND  a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "SELECT count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a 
						INNER JOIN users r ON a.account_createdby = r.user_id 
						 						 
						INNER JOIN
						(
						select p.account_id, sum(p.payment_amount) as paid_amount from payments p group by p.account_id
						) pay ON a.account_id = pay.account_id
						WHERE a.account_contractamount = pay.paid_amount GROUP BY a.account_id " . $condition . " 

						) x";

              echo $query;

                $result = mysql_query($query);
                ?>
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="stat-desc-count">
                                Paid-Full: <?php
            if ($result) {
                $row = mysql_fetch_array($result, MYSQL_NUM);

                if ($row[0] != 0) {
                    echo "<a href='16' class='dashboard-link'>" . $row[0] . "</a>";
                } else {
                    echo $row[0];
                }
                echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
            } else {
                echo "0";
                echo "<input type='hidden' id='h1' value='0' />";
            }
                ?>


                            </span>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <!---dashboard-box End---> 
        <!---First Row End--->


        <!---2nd Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a  LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 22 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Prospecting
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='22' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 10 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        Set Appointment
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='10' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>

                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 11 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        Sent Proposal
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='11' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 12 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>

                <ul>
                    <li>
                        For Callback
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='12' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box">
            <div class="dashboard-box-title">
                <?php
                $condition = "";
                if ($_SESSION['dashboard_shoptype'] == 0) {
                    if ($_SESSION['dashboard_user'] == 0) {
                        switch ($role_id) {
                            case 2:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                                break;
                            case 3:
                                $condition = " AND r.user_id = " . $userid . " ";
                                break;
                            case 5:
                                $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                                break;
                        }
                    } else {
                        $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                    }
                } else {
                    $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
                }
                $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 15 " . $condition . " GROUP by a.account_id
						) x";
                $result = mysql_query($query);
                ?>
                <ul>
                    <li>
                        For Follow Up
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='15' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 

        <!---2nd Row End---> 



        <!---3rd Row start---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 19 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Interested
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='19' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 14 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Declined
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='14' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE a.status_id = 20 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>

            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Signing
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='20' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backfinance" >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 13 " . $condition . " GROUP by a.account_id
						) x";


//        echo $query;
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Signed (for Payment) 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tr>
                        <td align="center" valign="middle"> 
                            <span class="total-count">
                                <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='13' class='dashboard-link'>" . number_format($row[0]) . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                ?>
                            </span>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backfinance">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.manager_id = " . $userid . "";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                        case 5:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " AND r.team_id = " . $_SESSION['team_id'] . "";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
                                                 
                                                SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 60 " . $condition . " GROUP by a.account_id
						
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 61 " . $condition . " GROUP by a.account_id
						
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 39 " . $condition . " GROUP by a.account_id
						
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id WHERE  a.status_id = 49 " . $condition . " GROUP by a.account_id
						) x";
//
//        echo $query;
            //        echo $query2;
//        echo $query3;
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);


//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by Finance 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">

                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="financetotal">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Reject - No Payment:  
                                    <?php
                                    if ($result5) {
                                        $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                        if ($row5[0] != 0) {
                                            echo "<a href='49' class='dashboard-link'>" . $row5[0] . "</a>";
                                        } else {
                                            echo $row5[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Unclear WCF: 
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='59' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete Mats: 
                                    <?php
                                    if ($result2) {
                                        $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                        if ($row2[0] != 0) {
                                            echo "<a href='60' class='dashboard-link'>" . $row2[0] . "</a>";
                                        } else {
                                            echo $row2[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete WCF: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='61' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    No Contract:  <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='39' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
                                    ?>
                                </span>
                                <input type="hidden" id="total-finance" name="total-finance" value="<?php
                                echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0];
                                    ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>

        </div>
        <!---dashboard-box End---> 

        <!---3rd Row End---> 

        <!--SALES END-->
    <?php } ?>
    <?php if ($department_id == 1 || $department_id == 4) { ?>
        <!--PROD START-->
        <div class="dept-title">
        </div>
        <div class="dept-title-span">
            <i class="prod"></i><span>PRODUCTION </span>  
            <br>
            <i class="csr"></i><span> CSR</span>
        </div>
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr" >

            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 1 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Lined up for Production
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='1' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 55 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 56 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 57 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 58 " . $condition . " GROUP by a.account_id
						) x";

            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 62 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by Prod
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="prodtotal">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Unclear WCF:  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='55' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete Mats: <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='56' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete WCF:<?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='57' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Unclear Instruction:<?php
                                if ($result5) {
                                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                    if ($row5[0] != 0) {
                                        echo "<a href='62' class='dashboard-link'>" . $row5[0] . "</a>";
                                    } else {
                                        echo $row5[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Ins. Not Followed: <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='58' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="prod-total" name="prod-total" value="<?php
                                echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row4[5];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 2 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);

//        echo($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Proofreading
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='2' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 18 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Design Instructions
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='18' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 21 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Development
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='21' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 3 " . $condition . " GROUP by a.account_id
						) x";

//        echo($query);

            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Under Construction
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='3' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 4 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Editor QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='4' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?> 
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 7 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 28 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 29 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

//        echo($query2);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Internal Revisions-Editor
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="Int-rev-ed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 1: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='7' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 2:<?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='28' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 3: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='29' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" name="Int-rev-ed-total" id="Int-rev-ed-total" value=" <?php echo $row[0] + $row2[0] + $row3[0];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 5 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Sr. Designer QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='5' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 32 " . $condition . " GROUP by a.account_id
						) x";


            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 33 " . $condition . " GROUP by a.account_id
						) x";



            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 34 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

//        echo($query2);
            ?>
            <div class="dashboard-box-title" style="background: #FC9;" >
                <ul>
                    <li>
                        Internal Revisions-QA 
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="int-rev-qa">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 1: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='32' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 2: <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='33' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 3: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='34' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" name="int-rev-qa-total" id="int-rev-qa-total" value=" <?php
                                echo $row[0] + $row2[0] + $row3[0];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 38 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Final QA
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count">
                                    <?php
                                    if ($result) {
                                        $row = mysql_fetch_array($result, MYSQL_NUM);

                                        if ($row[0] != 0) {
                                            echo "<a href='38' class='dashboard-link'>" . $row[0] . "</a>";
                                        } else {
                                            echo $row[0];
                                        }
                                        echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                    } else {
                                        echo "0";
                                        echo "<input type='hidden' id='h1' value='0' />";
                                    }
                                    ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5"  style="background: #FFFBD0;">
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }
            /*
              $query = "SELECT
              count(a.account_id) as ct1
              FROM accounts a
              LEFT JOIN users r ON a.shop_editor = r.user_id

              WHERE a.account_paid=1 and a.status_id = 6 AND a.shop_editor <> 0 " . $condition;
             */
            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 6 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 26 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 25 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Client Approval
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="client-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Approval :  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='6' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Feedback: <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='26' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Consolidation: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='25' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="client-total-total" value="<?php
                                echo $row[0] + $row2[0] + $row3[0];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }

            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 8 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 30 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 31 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 41 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 42 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);

//        echo ($query);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        For Client Revisions
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="clientrev">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 1 : <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='8' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 2 : <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='30' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 3 : <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='31' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 4: <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='41' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Revisions 5:   <?php
                                if ($result5) {
                                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                    if ($row5[0] != 0) {
                                        echo "<a href='42' class='dashboard-link'>" . $row5[0] . "</a>";
                                    } else {
                                        echo $row5[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="clientrevtotal" value="<?php echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0]; ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 

        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backprod"  >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 37 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 43 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 44 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Redesign
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="redesign-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Redesign 1 :  <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='37' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Redesign 2:  <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='43' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Redesign 3: <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='44' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="redesign-total-total" value="<?php
                                echo $row[0] + $row2[0] + $row3[0];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 

        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr" >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 53 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 54 " . $condition . " GROUP by a.account_id
						) x";

            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE a.status_id = 50 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 51 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.account_createdby = r.user_id  WHERE  a.status_id = 52 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);

            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        REJECTED by CSR
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="reject-csr">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Unclear WCF:   <?php
        if ($result3) {
            $row3 = mysql_fetch_array($result3, MYSQL_NUM);

            if ($row3[0] != 0) {
                echo "<a href='50' class='dashboard-link'>" . $row3[0] . "</a>";
            } else {
                echo $row3[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete Mats: <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='51' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Incomplete WCF:<?php
                                if ($result5) {
                                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                    if ($row5[0] != 0) {
                                        echo "<a href='52' class='dashboard-link'>" . $row5[0] . "</a>";
                                    } else {
                                        echo $row5[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Ins. Not Followed: <?php
                                if ($result) {
                                    $row = mysql_fetch_array($result, MYSQL_NUM);

                                    if ($row[0] != 0) {
                                        echo "<a href='53' class='dashboard-link'>" . $row[0] . "</a>";
                                    } else {
                                        echo $row[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Design not Followed: <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='54' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="reject-csr-total" value=" <?php
                                echo $row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0];
            ?>"
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 
        <div class="dashboard-box-5 backcsr" >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {
                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }


            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 9 " . $condition . " GROUP by a.account_id
						) x";

            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 27 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 35 " . $condition . " GROUP by a.account_id
						) x";
            $query4 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 36 " . $condition . " GROUP by a.account_id
						) x";
            $query5 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 45 " . $condition . " GROUP by a.account_id
						) x";
            $query6 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 46 " . $condition . " GROUP by a.account_id
						) x";

            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            $result4 = mysql_query($query4);
            $result5 = mysql_query($query5);
            $result6 = mysql_query($query6);
            ?>
            <div class="dashboard-box-total">
                <div class="dashboard-box-title">
                    <ul>
                        <li>
                            LIVE
                        </li>
                    </ul>
                </div>
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="live-total">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Live: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='9' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Live (Client Updates) :  <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='27' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Live (Temporary) : <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='35' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Temp(Bal. Collection) : <?php
                                if ($result4) {
                                    $row4 = mysql_fetch_array($result4, MYSQL_NUM);

                                    if ($row4[0] != 0) {
                                        echo "<a href='36' class='dashboard-link'>" . $row4[0] . "</a>";
                                    } else {
                                        echo $row4[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row4[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Temp(Awaiting Mats) :<?php
                                if ($result5) {
                                    $row5 = mysql_fetch_array($result5, MYSQL_NUM);

                                    if ($row5[0] != 0) {
                                        echo "<a href='45' class='dashboard-link'>" . $row5[0] . "</a>";
                                    } else {
                                        echo $row5[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row5[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Temp(Awaiting Final) :<?php
                                if ($result6) {
                                    $row6 = mysql_fetch_array($result6, MYSQL_NUM);

                                    if ($row6[0] != 0) {
                                        echo "<a href='46' class='dashboard-link'>" . $row6[0] . "</a>";
                                    } else {
                                        echo $row6[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row6[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="live-total-total" value="<?php
                                echo number_format($row[0] + $row2[0] + $row3[0] + $row4[0] + $row5[0] + $row6[0]);
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 
        <!---dashboard-box start---> 

        <div class="dashboard-box-5 backcsr" >
            <?php
            $condition = "";
            if ($_SESSION['dashboard_shoptype'] == 0) {

                if ($_SESSION['dashboard_user'] == 0) {
                    switch ($role_id) {
                        case 2:
                            $condition = " AND r.department_id = " . $_SESSION['department_id'] . " ";
                            break;
                        case 3:
                            $condition = " AND r.user_id = " . $userid . " ";
                            break;
                    }
                } else {
                    $condition = " AND r.user_id = " . $_SESSION['dashboard_user'] . " ";
                }
            } else {
                $condition = " AND a.shop_type = " . $_SESSION['dashboard_shoptype'] . " ";
            }



            $query = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 24 " . $condition . " GROUP by a.account_id
						) x";
            $query2 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 47 " . $condition . " GROUP by a.account_id
						) x";
            $query3 = "select count(x.total) from
						(
						SELECT a.account_id as total FROM accounts a LEFT JOIN users r ON a.shop_editor = r.user_id or a.shop_designer = r.user_id WHERE a.account_paid=1 and a.status_id = 48 " . $condition . " GROUP by a.account_id
						) x";
            $result = mysql_query($query);
            $result2 = mysql_query($query2);
            $result3 = mysql_query($query3);
            ?>
            <div class="dashboard-box-title">
                <ul>
                    <li>
                        Temporary Closed
                    </li>
                </ul>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="total-count" id="temp-closed">

                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Closed: <?php
        if ($result) {
            $row = mysql_fetch_array($result, MYSQL_NUM);

            if ($row[0] != 0) {
                echo "<a href='24' class='dashboard-link'>" . $row[0] . "</a>";
            } else {
                echo $row[0];
            }
            echo "<input type='hidden' id='h1' value='" . number_format($row[0], 2, '.', ',') . "' />";
        } else {
            echo "0";
            echo "<input type='hidden' id='h1' value='0' />";
        }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Delinquent:  <?php
                                if ($result2) {
                                    $row2 = mysql_fetch_array($result2, MYSQL_NUM);

                                    if ($row2[0] != 0) {
                                        echo "<a href='47' class='dashboard-link'>" . $row2[0] . "</a>";
                                    } else {
                                        echo $row2[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row2[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="dashboard-box-total">
                <table width="100%">
                    <tbody><tr>
                            <td align="center" valign="middle"> 
                                <span class="stat-desc-count">
                                    Request from Client:   <?php
                                if ($result3) {
                                    $row3 = mysql_fetch_array($result3, MYSQL_NUM);

                                    if ($row3[0] != 0) {
                                        echo "<a href='48' class='dashboard-link'>" . $row3[0] . "</a>";
                                    } else {
                                        echo $row3[0];
                                    }
                                    echo "<input type='hidden' id='h1' value='" . number_format($row3[0], 2, '.', ',') . "' />";
                                } else {
                                    echo "0";
                                    echo "<input type='hidden' id='h1' value='0' />";
                                }
            ?>
                                </span>
                                <input type="hidden" id="temp-closed-total" value=" <?php
                                echo $row[0] + $row2[0] + $row3[0];
            ?>">
                            </td>
                        </tr>
                    </tbody></table>
            </div>


        </div>
        <!---dashboard-box End---> 

        <!--PROD END-->
    <?php } ?>

</div>
<!---dash-row-container End---> 
<script>
    $(function() {
        var totalfinance = $("#total-finance").val();
        var totalprod = $("#prod-total").val();
        var Intreved  = $("#Int-rev-ed-total").val();
        var intrevqa  = $("#int-rev-qa-total").val();
                        
        var clienttotal  = $("#client-total-total").val();
        var clientrev  = $("#clientrevtotal").val();
        var redesigntotal  = $("#redesign-total-total").val();
        var rejectcsr  = $("#reject-csr-total").val();
        var livetotal  = $("#live-total-total").val();
        var tempclosed  = $("#temp-closed-total").val();
                            
        $("#financetotal").text(totalfinance);
        $("#prodtotal").text(totalprod);
        $("#Int-rev-ed").text(Intreved);
        $("#int-rev-qa").text(intrevqa);
        $("#client-total").text(clienttotal);
        
        
        $("#clientrev").text(clientrev);
        
        
        $("#redesign-total").text(redesigntotal);
        $("#reject-csr").text(rejectcsr);
        $("#live-total").text(livetotal);
        $("#temp-closed").text(tempclosed);
                        
                        
                        
    });
                                                                                                                                                 
</script>


<script>
    function slotmachine(id, changeto) {
        var thisid = '#' + id;
        var $obj = $(thisid);
        $obj.css('opacity', '.3');
        var original = $obj.text();

        var spin = function() {
            return Math.floor(Math.random() * 10);
        };

        var spinning = setInterval(function() {
            $obj.text(function() {
                var result = '';
                for (var i = 0; i < original.length; i++) {
                    result += spin().toString();
                }
                return result;
            });
        }, 50);

        var done = setTimeout(function() {
            clearInterval(spinning);
            $obj.text(changeto).css('opacity', '1');
        }, 1000);
    }           
           
</script>